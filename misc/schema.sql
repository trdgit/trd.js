/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table approved
# ------------------------------------------------------------

CREATE TABLE `approved` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bookmark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chain` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pattern` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('WILDCARD','REGEX') COLLATE utf8_unicode_ci DEFAULT 'WILDCARD',
  `hits` int(10) DEFAULT 0,
  `maxlimit` int(10) DEFAULT 1,
  `created` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_bookmark_expires` (`bookmark`,`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


# Dump of table data_cache
# ------------------------------------------------------------

CREATE TABLE `data_cache` (
  `k` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_immutable` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `namespace` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `approved` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`k`),
  KEY `namespace` (`namespace`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table irc_message_queue
# ------------------------------------------------------------

CREATE TABLE `irc_message_queue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `processed` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dump of table pre
# ------------------------------------------------------------

CREATE TABLE `pre` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rlsname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dupe_k` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dupe_season_episode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dupe_resolution` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dupe_source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dupe_codec` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rlsname` (`rlsname`),
  KEY `dupe_k` (`dupe_k`,`dupe_season_episode`,`dupe_resolution`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Dump of table race
# ------------------------------------------------------------

CREATE TABLE `race` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bookmark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_sites` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chain_complete` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rlsname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `started` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `log` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rlsname` (`rlsname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table race_site
# ------------------------------------------------------------

CREATE TABLE `race_site` (
  `race_id` int(11) unsigned NOT NULL,
  `site` varchar(50) NOT NULL DEFAULT '',
  `created` datetime DEFAULT NULL,
  `ended` datetime DEFAULT NULL,
  PRIMARY KEY (`race_id`,`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
