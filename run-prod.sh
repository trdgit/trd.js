cmdApi="node -r dotenv/config .esbuild/api/src/start.js"
cmdDaemon="node -r dotenv/config .esbuild/daemon/src/start.js"
cmdGui="node -r dotenv/config .esbuild/gui/src/start.js"

(trap 'kill -9 0' SIGINT; $cmdApi & $cmdDaemon & $cmdGui)