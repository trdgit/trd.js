import type { Config } from '@jest/types'

const config: Config.InitialOptions = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    setupFiles: ['dotenv/config'],
    setupFilesAfterEnv: ['./jest.setup.ts'],
    testMatch: [
        '**/*.test.ts',
        '!**/*.live.test.ts',
        '!**/titles.test.ts',
        '!_src/**'
    ],
    testPathIgnorePatterns: [
        '_src',
        'node_modules',
        '.esbuild',
        '.build',
        'dist',
        'coverage'
    ],
    collectCoverageFrom: ['./src/**/*.ts', '!**/*.test.ts'],
    transform: {
        '\\.ts?$': [
            'ts-jest',
            {
                isolatedModules: false
            }
        ]
    }
}
export default config
