FROM node:20-alpine

# curl is needed for fetching deps
RUN apk add --no-cache curl pnpm

WORKDIR /trd
RUN chown -R node /trd
USER node

COPY --chown=node:node . /trd

ENV PATH=/trd/node_modules/.bin:$PATH
RUN pnpm install && \
    npx tsx build-prod.ts && \
    chmod +x run-prod.sh && \
    pnpm store prune
ENTRYPOINT [ "./run-prod.sh" ]
