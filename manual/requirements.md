# Minimum requirements

-   \*nix based OS (Not tested on Windows, but might work)
-   NodeJS 16+
-   MySQL 5.6+
-   A brain
-   Experience using command line
