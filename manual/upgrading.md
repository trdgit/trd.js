# Upgrading

-   Run `git pull` to pull in the latest source code.
-   Diff your configs to see if new settings have been added.
-   Restart the server

# Specific upgrade paths

## Switching to json datacache

1. Kill your daemon and backup your database or mirror it or however you want to do this
2. Set DATACACHE_JSON=1 to ENV
3. Run yarn task surgeon --simulate (You will see some errors probably, but wait for it to finish, it shouldn;'t actually do any mutation)
4. Run yarn task surgeon --cleanupBad and it'll migrate the serialized rows to json, and delete the bad ones with bad utf8 shit (in my case this was about 30 rows)
5. Restart daemon. And you should be running with the data_cache as json :) You can confirm by querying the data_cache table and looking at data and data_immutable field

## Switch to pnpm

> Requires JSON datacache migration above to be done before proceeding

"yarn" is replaced by "pnpm", as part of this change you need to make some changes.

* Manual install you'll have to replace "yarn" with "pnpm" for commands, such as the scheduled tasks
* Docker install you'll have to replace Dockerfile and make a new `docker compose build`
