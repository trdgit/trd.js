-   [Home](/)
-   [Requirements](requirements.md)
-   [Install](install.md)
    -   -   [Standard installation](install_manual.md)
    -   -   [Docker installation](install_docker.md)
-   [Migrating](migrating.md)
-   [Upgrading](upgrading.md)
-   Usage
    -   -   [Security](security.md)
    -   -   [IRC Relay](irc_relay.md)
    -   -   [Settings](settings.md)
    -   -   [Configuring prebots](prebots.md)
    -   -   [Skiplists](skiplists.md)
    -   -   [Configuring a site](sites.md)
    -   -   [Logging](logging.md)
    -   -   [Rule engine](rules.md)
    -   -   [Fixing bad lookups/data](data_channel.md)
    -   -   [Automated tasks](automated_tasks.md)
-   [Development](development.md)
    -   -   [TRD Daemon API Specification](api.md)
-   [Misc]()
    -   -   [2022-09-26 Upgrade instructions](misc_unique_index.md)
