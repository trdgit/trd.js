## API Specification 

The TRD daemon has a very simple TCP-based API. Messages are encoded with JSON and contain a `command` key indicating what command we are processing, and some other data as part of the payload depending on what you are doing.

## Logging on to TRD

Simply connect to the daemon using TCP on the port specified in your `.env` file.

The first command you receieve will look like so:
```json
{
   "command":"VERSION",
   "version":"1234"
}
```

## Receiving commands

`TRADE` - This command indicates a race has been evaluated and you could optionally trade it.

---

`APPROVED` - This command indicates that a race has been evaluated and it has either matched an approval rule, or an autorule and therefore can be blindly traded.

---

`IRCREPLY` - This command is special, and used for data channels. If you type a command that matches a certain format you will get an `IRCREPLY` command back, and you should parse it and write a message back to the channel using the data in the payload.

---

`RACECOMPLETESTATUS` - This command is received in real(ish)-time indicating which sites a race is complete on.

## Sending commands 

`IRC` - Used to send messages to the daemon from IRC so announces can be caught.

```
IRC #channel nick message
```

---

`APPROVED` - Send an approval to the daemon and it will be added to the list of approvals.

---


`PRETIP` - Send a releasename to the daemon before it's pred. This will:
* Use the tag and rlsname to evaluate a race
* Store the race in memory until it's observed
* Bypass pretime requirements
* If you have cbftp enabled it will also login and idle for up to 2 minutes on sites that would participate in the race

It's important to get the tag correct here, failing to do so will block attempts on other tags for the given releasename.

```
PRETIP tag rlsname
```

---

`RACED` or `TRADED` - Docs coming soon
