# Containerized installation (Docker)

**1. Install docker and docker-compose**

-   Install docker, use some tutorial like [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04](this one from DigitalOcean for Ubuntu)
-   Also install docker-compose: `apt-get install docker-compose` or equivalent for your distro

**2. Create folder for docker and data**

-   Create a `~/docker/trd` folder with `mkdir -p ~/docker/trd`
-   Change directory to your newly created directory: `cd ~/docker/trd`
-   Clone this repo to a folder called `source` using `git clone https://gitlab.com/trdgit/trd.js.git source`
    -   We recommend you to use a [proxy with git](https://gist.github.com/evantoli/f8c23a37eb3558ab8765)
-   Copy `source/misc/schema.sql` to `~/docker/trd`
-   Create a file called `docker-compose.yaml` with the following contents:

    ```yaml
    version: '3'
    services:
        trdjs:
            build: ./source
            container_name: trdjs
            restart: unless-stopped
            networks:
                trdjs:
                    ipv4_address: 172.16.160.2
            volumes:
                - $PWD/data/trd:/trd/.trd
            depends_on:
                - db
            env_file:
                - .env
        db:
            image: mariadb:10.7
            container_name: trdjs-db
            restart: unless-stopped
            networks:
                trdjs:
                    ipv4_address: 172.16.160.3
            volumes:
                - $PWD/data/db:/var/lib/mysql
                # Only when you want to create a new clean database
                - $PWD/schema.sql:/docker-entrypoint-initdb.d/schema.sql
            environment:
                MARIADB_ROOT_PASSWORD: SecretRootPasswordNobodyCanGuess
                MARIADB_DATABASE: trdjs
                MARIADB_USER: trdjs
                MARIADB_PASSWORD: SecretPasswordNobodyCanGuess

    networks:
        trdjs:
            ipam:
                driver: default
                config:
                    - subnet: '172.16.160.0/24'
    ```

-   Let's create folders for the trd.js data and MySQL database:

    -   `mkdir -p ~/docker/trd/data/mysql`
    -   `mkdir -p ~/docker/trd/data/trd`

-   Copy the example `~/docker/trd/source/.env.example` to `~/docker/trd/.env`
-   Edit `~/docker/trd/.env`
    -   Set all `HOST=` and `*_HOST=` to: `172.16.160.2` if you are accessing trd.js from the same computer as docker is running on
    -   Set all `HOST=` and `*_HOST=` to: `127.0.0.1` if you are accessing trd.js from a SSH tunnel
    -   Set all `HOST=` and `*_HOST=` to: `your.computer.ip.address` if you are accessing trd.js from a VPN or local network
    -   `DB_HOST=db`
    -   `DB_NAME` should match `MARIADB_DATABASE` in docker-compose.yaml
    -   `DB_USER` should match `MARIADB_USERNAME` in docker-compose.yaml
    -   `DB_PASSWORD` should match `MARIADB_PASSWORD` in docker-compose.yaml
    -   `DATA_PATH=/trd/.trd`

**3. New user setup - Database setup**

> If you are migrating from trd.php you can keep your database, but you have to clean the old race history: [See migration instructions](migrating.md)

**4. New user trd.js config**

-   Run the following command: `cp -R ~/docker/trd/source/.trd.example ~/docker/trd/data/trd`
-   Check that the config files copied correctly: `ls ~/docker/trd/data/trd` (You should see a few JSON files)

**5. Set up scheduled tasks**

Set up cron to perform nightly maintenance tasks:

```
# Cleanup old races and pre information
00 00 * * * docker exec trdjs pnpm task cleanup
# Refresh cached TV Shows if they were update on TVmaze in the last day
02 01 * * * docker exec trdjs pnpm task refreshUpdatedShows
# Weekly refresh of all cached TVmaze entries
00 01 * * 1 docker exec trdjs pnpm task refreshCache
# optional task to fetch credits from added sites and display them in the TRD Site List
00 10 * * * docker exec trdjs pnpm task credits
# optional task to update votes and rating for cached IMDB entries
03 00 * * * docker exec trdjs pnpm task imdbDataUpdate
```

**6. Start TRD**

-   Change directory back to: `cd ~/docker/trd`
-   Now we can start the containers by executing: `docker-compose up -d`
-   You can check for running containers with `docker ps` (requires sudo unless you added yourself to the docker group)
-   Read logs with `docker logs trdjs` (or tail the log with `docker logs -f trdjs`)

**7. Visit the web GUI**

For security reasons we bound trd to an internal IP so it's not acessible from outside.
You can verify that the API is up and responding by executing this from your shell: `curl http://172.16.160.2:1339/site/list`
Now that the API is responding you are going to need to visit the web GUI.
You will need to forward a port to visit this from whatever machine you are on. See [Security](security.md) for more information on this.
