# Automated tasks

TRD comes with some CLI tools out the box which you can configure to do a few things.

Run `pnpm task` in the trd folder to find out how to run them.

<!-- -   `preload_data` Uses third-party sources to try and pre-cache data provider information -->

-   `cleanup` Cleanup old races and pre data.
-   `credits` Gathers the credits in the web gui for each of your sites.
-   `refreshCache` Refresh the cache for tvmaze entries.
-   `refreshUpdatedShows` Refresh shows from [TVmaze API - Updates today](https://api.tvmaze.com/updates/shows?since=day)
-   `imdbDataUpdate` Update votes and rating for cached IMDB entries.
