# Migrating from trd.php to trd.js

-   Your files are compatible with trd.js, copy all files from inside your old `.trd/` folder into your new `.trd/` folder
-   Configure your new trd.js `.env` with the details to reach your database.

-   To make your old database compatible with trd.js we need to clean your `race` and `race_site` tables as they have a different data format. We recommend that you **make a copy** (or at least a **backup**) before you run:
    -   `DELETE FROM dbname.race;`
    -   `DELETE FROM dbname.race_site;`
