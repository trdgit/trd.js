# Standard installation

**1. NodeJS setup**

-   Install node.js via your favourite package manager: https://nodejs.org/en/download/package-manager/
-   Confirm it works by typing this on CLI: `node --version`
-   Install pnpm: `npm install -g pnpm`
-   Install the project dependencies by typing `pnpm install` in the project root.

**2. Database setup**

> If you are migrating from trd.php you can keep your database, but you have to clean the old race history: [See migration instructions](migrating.md)

-   Create a MySQL database and create the DB structure from `misc/db.sql`
-   Set up MySQL **non-root** user and [grant it privileges to the db](https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql)
-   Import MySQL timezone data: `mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -uroot -p mysql`

**3. TRD config setup**

-   Run the following commands (in order):

```
mkdir ~/.trd
cp -R /path/to/trd/.trd.example/* ~/.trd
```

-   Check that the config files copied correctly: `ls ~/.trd` (You should see a few JSON files)
-   Copy `.env.example` to `.env` and configure the settings (or find a way to set the environmental variables on your system/container yourself)

**4. Set up scheduled tasks**

Set up cron to perform nightly maintenance tasks:

```
# Cleanup old races and pre information
00 00 * * * cd /path/to/trd; pnpm task cleanup
# Refresh cached TV Shows if they were update on TVmaze in the last day
02 01 * * * cd /path/to/trd; pnpm task refreshUpdatedShows
# Weekly refresh of all cached TVmaze entries
00 01 * * 1 cd /path/to/trd; pnpm task refreshCache
# optional task to fetch credits from added sites and display them in the TRD Site List
00 10 * * * cd /path/to/trd; pnpm task credits
# optional task to update votes and rating for cached IMDB entries
03 00 * * * cd /path/to/trd; pnpm task imdbDataUpdate
```

**5. Learn tmux**

You will need to run the TRD server and web gui process in the background. We strongly recommend doing this using tmux or screen. So go learn how to use these.

**6. Start TRD**

Type `DEBUG=* pnpm dev` in your TRD directory and preferably run this in a tmux pane. When ran in dev-mode the web interface will listen to localhost:1400 only.

A more perfomant way to run would be `DEBUG=* pnpm prod`

**7. Visit the web GUI**

You are going to need to visit the web GUI. For security reasons we bound it to 127.0.0.1 so it's not acessible from outside. You will need to forward a port to visit this from whatever machine you are on. See [Security](security.md) for more information on this.
