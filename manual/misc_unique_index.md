Unfortunately an issue where duplicate races and data_cache entries
could occur has been present for a while. A DB table adjustment is required.

1. Stop trdjs
2. Open MySQL CLI:
3. Delete duplicate rows: `delete r from race as r inner join (select rlsname from race group by rlsname having count(*) > 1) as sq ON sq.rlsname = r.rlsname;`
4. Drop existing index: `alter table race drop index rlsname;`
5. Add unique index: `alter table race add constraint rlsname unique key(rlsname);`
