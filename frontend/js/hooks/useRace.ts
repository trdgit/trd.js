import axios, { AxiosError } from 'axios'
import { useState, useEffect, useCallback } from 'react'

import { getJSON } from '../helpers/api'

const useRace = (id: string) => {
    const [error, setError] = useState<AxiosError>()
    const [loadingRace, setLoadingRace] = useState(true)
    const [race, setRace] = useState<unknown>()

    const fetchRace = useCallback(async () => {
        setLoadingRace(true)
        try {
            const raceResponse = await getJSON(`/race/${id}/log`)
            setRace(raceResponse)
        } catch (e) {
            if (axios.isAxiosError(e)) {
                handleError(e)
            }
        }

        setLoadingRace(false)
    }, [id]) // if use

    const handleError = (err: AxiosError) => {
        console.log(err)
        setError(err)
    }

    useEffect(() => {
        fetchRace()
    }, [id])

    return {
        error,
        loadingRace,
        race,
    }
}

export default useRace
