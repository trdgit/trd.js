import { GetSitesResponse } from '@trd/shared/src/types'
import { useState, useEffect, useCallback } from 'react'

import { getJSON } from '../helpers/api'

const useSites = () => {
    const [error, setError] = useState<Error>()
    const [loadingSites, setLoadingSites] = useState<boolean>(true)
    const [sites, setSites] = useState<GetSitesResponse>()

    const fetchSites = useCallback(async () => {
        setLoadingSites(true)
        const response = await getJSON<GetSitesResponse>(`/site/list`)
        setSites(response)
        setLoadingSites(false)
    }, [])

    useEffect(() => {
        fetchSites()
    }, [])

    return {
        error,
        loadingSites,
        sites,
    }
}

export default useSites
