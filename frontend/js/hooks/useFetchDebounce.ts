import React, { useState, useEffect, useCallback } from 'react'
import { useDebounce } from 'use-debounce'

// export interface FetchFunction<Data> {
//     <Data>(): Promise<Data>
// }
export type FetchFunction<Data> = () => Promise<Data>
export type SaveFunction = (data: unknown) => void
export type AddNewFunction = () => void
export type OnChangeFunction = (e: React.FormEvent) => void

const useFetchDebounce = <T>(
    fetcher: FetchFunction<T>,
    saver: SaveFunction,
    onAddNew?: AddNewFunction,
    changed = false,
    delay = 500
) => {
    const [error, setError] = useState<unknown>()
    const [loading, setLoading] = useState(true)
    const [loaded, setLoaded] = useState(false)
    const [data, setData] = useState<T>()
    const [counter, setCounter] = useState(0)
    const [debouncedData] = useDebounce(data, delay)

    const fetchData = useCallback(async () => {
        setLoading(true)
        try {
            const response = await fetcher()
            setLoaded(true)
            setData(response)
        } catch (e: unknown) {
            handleError(e)
        }
        setLoading(false)
    }, [])

    const handleError = (err: unknown) => {
        console.log(err)
        setData(undefined)
        setError(err)
    }

    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        async function doSave() {
            await saver(data)
            if (onAddNew) {
                onAddNew()
            }
        }

        console.log(loaded, counter, debouncedData, changed)

        // allow us to continue only if we're loaded
        if (loaded) {
            setCounter(counter => counter + 1)
        }

        if (counter > 0 && debouncedData && changed) {
            doSave()
        }
    }, [loaded, debouncedData, changed])

    return {
        error,
        loading,
        data,
        setData,
    }
}

export default useFetchDebounce
