import {
    GetRacesResponse,
    Paging,
    RaceListResponse,
} from '@trd/shared/src/types'
import { useState, useEffect, useCallback } from 'react'

import { errorHandler, getJSON } from '../helpers/api'

const useRaces = (cursor: string, tag: string) => {
    const [loadingRaces, setLoadingRaces] = useState(true)
    const [races, setRaces] = useState<RaceListResponse[]>()
    const [dataSources, setDataSources] = useState({})
    const [paging, setPaging] = useState<Paging>()
    const [tags, setTags] = useState<string[]>([])
    const [error, setError] = useState<Error>()

    const fetchRaces = useCallback(async () => {
        setLoadingRaces(true)
        let url = `/race/list`

        const params = new URLSearchParams()
        if (cursor) {
            params.append('cursor', cursor)
        }
        if (tag) {
            params.append('tag', tag)
        }
        try {
            const response = await getJSON<GetRacesResponse>(
                `${url}?${params.toString()}`
            )
            setRaces(response.data.races)
            setDataSources(response.data.dataSources)
            setPaging(response.paging)
            setTags(response.data.tags)
            setLoadingRaces(false)
        } catch (e) {
            const error = errorHandler(e)
            setError(error)
        }
    }, [cursor, tag]) // if use

    useEffect(() => {
        fetchRaces()
    }, [cursor, tag])

    return {
        loadingRaces,
        races,
        dataSources,
        paging,
        tags,
        error,
    }
}

export default useRaces
