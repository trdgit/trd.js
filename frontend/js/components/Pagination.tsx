import React from 'react'
import { Pagination } from 'react-bootstrap'

interface PaginationProps {
    prev?: string
    next?: string
}

export default ({ prev, next }: PaginationProps) => (
    <Pagination className="justify-content-end" style={{ marginBottom: 0 }}>
        <Pagination.Item href={prev ?? ''} disabled={!prev}>
            Prev
        </Pagination.Item>
        <Pagination.Item href={next ?? ''} disabled={!next}>
            Next
        </Pagination.Item>
    </Pagination>
)
