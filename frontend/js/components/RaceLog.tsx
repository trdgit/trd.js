import { RaceResultLogs, RaceResultLog } from '@trd/shared/src/types'
import { DateTime, Interval } from 'luxon'
import React, { ReactElement } from 'react'

import { formatDistance } from '../helpers/date'

const is_string = (input: unknown): input is string =>
    typeof input === 'string' || input instanceof String

const joinReducer = (accu: React.JSX.Element[], elem: React.JSX.Element) => {
    return accu.length === 0 ? [elem] : [...accu, <span>{','}</span>, elem]
}

const joinReducerBr = (accu: React.JSX.Element[], elem: React.JSX.Element) => {
    return accu.length === 0 ? [elem] : [...accu, <br />, elem]
}

const timeAgo = (d1: string): string => {
    try {
        return formatDistance(DateTime.fromSQL(d1), DateTime.now())
    } catch (e) {
        return 'n/a'
    }
}

const dataValueColor = '#3e81b3'

interface CatastrophesProps {
    catastrophes: string[]
}

const RaceCatastrophes = ({ catastrophes }: CatastrophesProps) => (
    <>
        {catastrophes.length > 0 && (
            <>
                <h4>Catastrophes</h4>
                <div className="alert alert-danger">
                    {catastrophes.map(catastrophe => (
                        <span key={catastrophe}>- {catastrophe}</span>
                    ))}
                </div>
                <p style={{ fontSize: '.8em' }}>
                    Note: data cache lookups are not made as an optimisation
                    when catastrophes occur, to save time and effort.
                </p>
            </>
        )}
    </>
)

interface InvalidSite {
    site: string
    section: string
    invalidReasons: string[]
}
interface InvalidSitesProps {
    invalidSites: InvalidSite[]
}

const RaceLogInvalidSites = ({ invalidSites }: InvalidSitesProps) => (
    <>
        {invalidSites.length > 0 && (
            <div className="card">
                <div className="card-header">Invalid Sites</div>
                <div className="card-body">
                    <table className="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Site</th>
                                <th>Section</th>
                                <th>Reasons</th>
                            </tr>
                        </thead>
                        <tbody>
                            {invalidSites.map(row => (
                                <tr key={row.site}>
                                    <td width="10%">
                                        <strong>{row.site}</strong>
                                    </td>
                                    <td width="10%">{row.section}</td>
                                    <td>
                                        {row.invalidReasons
                                            .map(reason => (
                                                <span>{reason}</span>
                                            ))
                                            .reduce(joinReducerBr, [])}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )}
    </>
)

interface ValidSiteProps {
    site: string
    isAffil: boolean
}

const ValidSite = ({ site, isAffil }: ValidSiteProps) => {
    if (isAffil) {
        return <strong>{site}</strong>
    }
    return <>{site}</>
}

interface Log {
    validSites: string[]
    affilSites: string[]
    chain: string[]
}
interface LogProps {
    log: Log
}

const RaceLogValidSites = ({ log }: LogProps) => {
    const validSitesList = log.validSites
        .sort()
        .map(site => (
            <ValidSite
                key={site}
                site={site}
                isAffil={log.affilSites.includes(site)}
            />
        ))
        .reduce(joinReducer, [])

    const chainList = log.chain
        .sort()
        .map(site => (
            <ValidSite
                key={site}
                site={site}
                isAffil={log.affilSites.includes(site)}
            />
        ))
        .reduce(joinReducer, [])

    return (
        <>
            {log.validSites.length > 0 ? (
                <>
                    <div className="card">
                        <div className="card-header">
                            Valid Sites (affils in bold)
                        </div>
                        <div className="card-body">{validSitesList}</div>
                    </div>
                    <br />
                    <div className="card">
                        <div className="card-header">Chain</div>
                        <div className="card-body">{chainList}</div>
                    </div>
                    <br />
                </>
            ) : (
                <div className="alert alert-danger">
                    No valid sites found to race this
                </div>
            )}
        </>
    )
}

interface RaceLogDataKeyProps {
    k: string
    v: unknown
}

const RaceLogDataKey = ({ k, v }: RaceLogDataKeyProps) => {
    const tdStyles: Record<string, string> = {
        textAlign: 'right',
        paddingRight: '15px',
        paddingLeft: '20px'
    }
    if (v === null || (v !== false && v !== true && v.length === 0)) {
        tdStyles['opacity'] = '.5'
    }

    let label = <code>{k}</code>
    if (k.includes('.')) {
        label = (
            <code>
                <span style={{ opacity: 1, fontWeight: 'bold' }}>
                    {k.split('.')[0]}
                </span>
                {'.'}
                <span style={{ opacity: 1 }}>{k.split('.')[1]}</span>
            </code>
        )
    }

    return (
        <td width="10%" style={tdStyles}>
            {label}
        </td>
    )
}

const RaceLogDataValue = ({ data, rawData, k, v }) => {
    const equivalent = (v1: unknown, v2: unknown) => {
        if (Array.isArray(v1) && Array.isArray(v2)) {
            return JSON.stringify(v1) === JSON.stringify(v2)
        }
        console.log(k, typeof v1, typeof v2)
        if (typeof v1 !== typeof v2) {
            return true
        }
        return v1 === v2
    }

    const format = (value: unknown): string | React.JSX.Element => {
        if (Array.isArray(value)) {
            return value.join(',')
        } else if (value === true || value === false) {
            return value ? 'true' : 'false'
        } else if (typeof value === 'object') {
            return
        } else if (
            is_string(value) &&
            value.length &&
            (value.includes('http://') || value.includes('https://'))
        ) {
            return (
                <a
                    style={{ textDecoration: 'underline' }}
                    target="_blank"
                    rel="noreferrer external noopener"
                    href={value.trim()}
                >
                    {value.trim()}
                </a>
            )
        } else if (is_string(value)) {
            return value.trim()
        } else {
            return value as unknown as string // :(
        }
    }

    const tdStyles: Record<string, string> = { paddingLeft: '15px' }
    if (v === null || (v !== false && v !== true && v.length === 0)) {
        tdStyles['opacity'] = '.5'
    }

    let before = null
    if (rawData && k in rawData && !equivalent(rawData[k], v)) {
        const value = rawData[k]
        const label = format(value) || 'empty'
        before = <s style={{ color: '#e83e8c' }}>{label}</s>
    }

    const current = (
        <code>
            <span
                style={{
                    color: dataValueColor,
                    fontFamily: 'monospace',
                    fontSize: '1em'
                }}
                title={getType(v)}
            >
                {format(v)}
            </span>
        </code>
    )

    return (
        <td style={tdStyles}>
            {before} {current}
        </td>
    )
}

const getType = (v: any) => {
    if (Array.isArray(v)) {
        return 'array'
    }
    return typeof v
}

const RaceLogData = ({ data, rawData }) => (
    <>
        <div className="card">
            <div className="card-header">Data</div>
            <div className="card-body">
                <table className="table table-bordered table-striped table-condensed">
                    <tbody>
                        <tr>
                            <td
                                width="10%"
                                style={{
                                    textAlign: 'right',
                                    paddingRight: '15px',
                                    paddingLeft: '20px'
                                }}
                            >
                                <code>duration (ms)</code>
                            </td>
                            <td style={{ paddingLeft: '15px' }}>
                                <span
                                    style={{
                                        color: dataValueColor,
                                        fontFamily: 'monospace',
                                        fontSize: '1em'
                                    }}
                                >
                                    {data.duration}
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td
                                width="10%"
                                style={{
                                    textAlign: 'right',
                                    paddingRight: '15px',
                                    paddingLeft: '20px'
                                }}
                            >
                                <code>dataLookupDuration (ms)</code>
                            </td>
                            <td style={{ paddingLeft: '15px' }}>
                                <span
                                    style={{
                                        color: dataValueColor,
                                        fontFamily: 'monospace',
                                        fontSize: '1em'
                                    }}
                                >
                                    {data.dataLookupDuration}
                                </span>
                            </td>
                        </tr>
                        {Object.entries(data).map(([k, v]) => (
                            <tr key={k}>
                                <RaceLogDataKey key={`rlk-${k}`} k={k} v={v} />
                                <RaceLogDataValue
                                    key={`rlv-${k}`}
                                    k={k}
                                    v={v}
                                    rawData={rawData}
                                    data={data}
                                />
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    </>
)

const RaceLogDupeBlock = ({ sources, logs }) => (
    <div className="card mt-4">
        <div className="card-header">Dupe Engine</div>
        <div className="card-body">
            <h5>(Possible) Sources</h5>
            {sources.length ? (
                <table className="table table-bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th>Rlsname</th>
                            <th>When</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sources.map((row, idx) => (
                            <tr key={`dupeSource-${row.rlsname}-${idx}`}>
                                <td>{row.rlsname}</td>
                                <td>
                                    {row.created ? timeAgo(row.created) : 'n/a'}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            ) : (
                <p>No sources found</p>
            )}
            <h5>Logs</h5>
            {logs.length ? (
                <code style={{ color: 'inherit' }}>
                    {logs.map(log => (
                        <span>
                            {log}
                            <br />
                        </span>
                    ))}
                </code>
            ) : (
                <p>No logs</p>
            )}
        </div>
    </div>
)

const RaceDataProviderLogs = ({ logs }) => (
    <div className="card mt-4">
        <div className="card-header">Data provider logs</div>
        <div className="card-body">
            {logs.length ? (
                <table className="table table-bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th>Namespace</th>
                            <th>Logs</th>
                        </tr>
                    </thead>
                    <tbody>
                        {logs.map(row => (
                            <tr key={`log-${row.namespace}`}>
                                <td>
                                    <code>{row.namespace}</code>
                                </td>
                                <td>
                                    <pre>
                                        {row.logs.map(log => (
                                            <span key={log}>
                                                <span>- {log}</span>
                                                <br />
                                            </span>
                                        ))}
                                    </pre>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            ) : (
                <p>No sources found</p>
            )}
        </div>
    </div>
)

const RaceResultLogLine = ({ line }: { line: RaceResultLog }) => (
    <span>{`${line.type} - ${line.message}`}</span>
)

const RaceResultLogBlock = ({ logs }: { logs: RaceResultLogs }) => (
    <div className="card mt-4">
        <div className="card-header">Debug logs</div>
        <div className="card-body">
            {logs.length > 0 ? (
                logs.map((log, idx) => (
                    <li key={`log-${idx}`}>
                        <RaceResultLogLine line={log} />
                    </li>
                ))
            ) : (
                <p>No logs</p>
            )}
        </div>
    </div>
)

export default ({ race }) => {
    const paddedData = {
        ...race.data,
        duration: race.duration,
        dataLookupDuration: race.dataLookupDuration
    }

    return (
        <>
            <RaceCatastrophes catastrophes={race.catastrophes} />
            <RaceLogValidSites log={race} />
            <RaceLogInvalidSites invalidSites={race.invalidSites} />
            <br />
            <RaceLogData data={paddedData} rawData={race.rawData} />
            <RaceDataProviderLogs logs={race.logs} />
            <RaceResultLogBlock logs={race.resultLogs} />
            <RaceLogDupeBlock
                sources={race.dupeEngineSources}
                logs={race.dupeEngineLogs}
            />
        </>
    )
}
