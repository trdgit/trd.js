import { Lists } from '@trd/shared/src/types'
import axios from 'axios'
import _ from 'lodash'
import React, { useState } from 'react'
import { toast } from 'react-toastify'

import { getJSON, postJSON } from '../helpers/api'
import useFetchDebounce, {
    FetchFunction,
    SaveFunction
} from '../hooks/useFetchDebounce'
import Loading from './Loading'
import SkiplistItem, { SkiplistItemType } from './SkiplistItem'
import Title from './Title'

const load = async () => {
    return getJSON<Lists>(`/skiplist/list`)
}
const save: SaveFunction = async skiplists => {
    try {
        await postJSON(`/skiplist/save`, skiplists)
        toast.success('Saved!', {
            toastId: 'saved'
        })
    } catch (e) {
        if (axios.isAxiosError(e)) {
            // @ts-ignore
            toast.error(e.response?.data.error)
        }
    }
}
export default () => {
    const [changed, setChanged] = useState<boolean>(false)
    const {
        loading,
        data: skiplists,
        setData: setSkiplists
    } = useFetchDebounce<Lists>(load, save, undefined, changed, 2500)

    const scrollToSkiplist = (name: string) => {
        setTimeout(
            () => document.getElementById(`skiplist-${name}`)?.scrollIntoView(),
            1500
        )
    }

    const addNormal = () => {
        var name = prompt(
            'Enter a unique name for this item skiplist (a-z0-9_)'
        )
        if (name?.length) {
            if (_.has(skiplists, name)) {
                return alert('An item with this name already exists')
            }

            var newState = JSON.parse(JSON.stringify(skiplists))
            newState[name] = {
                items: [],
                shared: true
            }
            setChanged(true)
            setSkiplists(newState)
            scrollToSkiplist(name)
        } else {
            alert("You didn't enter anything")
        }
    }

    const addRegex = () => {
        var name = prompt(
            'Enter a unique name for this regex skiplist (a-z0-9_)'
        )
        if (name?.length) {
            if (_.has(skiplists, name)) {
                return alert('An item with this name already exists')
            }

            var newState = JSON.parse(JSON.stringify(skiplists))
            newState[name] = {
                regex: '',
                shared: true
            }
            setChanged(true)
            setSkiplists(newState)
            scrollToSkiplist(name)
        } else {
            alert("You didn't enter anything")
        }
    }

    const changeItem = (
        type: SkiplistItemType,
        name: string,
        value: unknown
    ) => {
        var newState = JSON.parse(JSON.stringify(skiplists))

        _.set(newState, name, {
            shared: true,
            ...(type === SkiplistItemType.REGEX && { regex: value }),
            ...(type === SkiplistItemType.ITEMS && { items: value })
        })
        setChanged(true)
        setSkiplists(newState)
    }

    const deleteItem = (name: string) => {
        if (
            confirm(
                'Are you sure you want to delete the skiplist "' + name + '"?'
            )
        ) {
            var newState = JSON.parse(JSON.stringify(skiplists))
            delete newState[name]
            setChanged(true)
            setSkiplists(newState)
        }
    }

    if (loading) {
        return <Loading />
    }

    let skiplistList
    if (skiplists) {
        skiplistList = Object.keys(skiplists).map(skiplistName => {
            let editor, skiplistType
            if (typeof skiplists[skiplistName].items !== 'undefined') {
                editor = (
                    <SkiplistItem
                        name={skiplistName}
                        items={skiplists[skiplistName].items}
                        onChange={changeItem}
                        type={SkiplistItemType.ITEMS}
                    />
                )
                skiplistType = (
                    <span className="badge rounded-pill text-bg-primary">
                        wildcard
                    </span>
                )
            } else {
                editor = (
                    <SkiplistItem
                        name={skiplistName}
                        regex={skiplists[skiplistName].regex}
                        onChange={changeItem}
                        type={SkiplistItemType.REGEX}
                    />
                )
                skiplistType = (
                    <span className="badge rounded-pill text-bg-primary">
                        regex
                    </span>
                )
            }

            return (
                <div className="row" key={skiplistName}>
                    <div className="col-md-3" id={`skiplist-${skiplistName}`}>
                        {skiplistType} <code>{skiplistName}</code>
                        <br />
                        <br />
                        <a
                            className="btn btn-outline-secondary"
                            onClick={() => deleteItem(skiplistName)}
                        >
                            Delete
                        </a>
                    </div>
                    <div className="col-md-9">
                        {editor}
                        <br />
                    </div>
                </div>
            )
        })
    }

    return (
        <>
            <Title
                title="Skiplists"
                rightChildren={
                    <>
                        <a
                            data-trd-help="skiplists"
                            className="btn btn-outline-secondary"
                        >
                            <i className="icon-question-circle"></i> Help
                        </a>{' '}
                        <a className="btn btn-primary" onClick={addRegex}>
                            + Add regex item
                        </a>{' '}
                        <a className="btn btn-primary" onClick={addNormal}>
                            + Add wildcard item
                        </a>
                    </>
                }
            />
            <br />
            <div>{skiplistList}</div>
        </>
    )
}
