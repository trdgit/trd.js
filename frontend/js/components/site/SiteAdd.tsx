import {
    AddSiteResponse,
    APIError,
    GetSitesResponse
} from '@trd/shared/src/types'
import axios from 'axios'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import * as yup from 'yup'

import { postForm } from '../../helpers/api'
import useSites from '../../hooks/useSites'
import Loading from '../Loading'
import Form from '../form/Form'
import Input from '../form/Input'

const schema = yup.object().shape({
    name: yup.string().required()
})

const SiteAddForm = () => {
    const [loading, setLoading] = useState<boolean>(false)
    const [error, setError] = useState<string>()
    const navigate = useNavigate()

    const onSubmit = async data => {
        const payload = new FormData()
        payload.append('name', data.name)
        try {
            const response = await postForm<AddSiteResponse>(
                `/site/add`,
                payload
            )
            if (response.success === 1) {
                navigate(`/site/${data.name}/edit`)
            }
            setLoading(false)
        } catch (e) {
            let msg = 'Something went wrong'
            if (axios.isAxiosError<APIError>(e)) {
                if (e.response?.data?.error.length) {
                    msg = e.response.data.error
                }
            }
            setError(msg)
            setLoading(false)
        }
    }

    return (
        <div>
            <h1>Add site</h1>
            {error && <div className="alert alert-danger">{error}</div>}
            <Form
                onSubmit={onSubmit}
                className="needs-validation"
                novalidate={true}
                schema={schema}
            >
                <Input label="Name" name="name" autocomplete={false} />
                <input type="submit" className="btn btn-primary" />
            </Form>
        </div>
    )
}

export default () => {
    return (
        <div className="container-fluid">
            <SiteAddForm />
        </div>
    )
}
