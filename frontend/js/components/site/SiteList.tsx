import { GetSitesResponse, Site } from '@trd/shared/src/types'
import classNames from 'classnames'
import React from 'react'
import { Link, useLocation } from 'react-router-dom'

import useSites from '../../hooks/useSites'
import Loading from '../Loading'
import Title from '../Title'
import { Alert } from 'react-bootstrap'

const hideCredits = window._settings.hide_credits_from_site_list ?? false

const creditsFormatter = (megabytes: number) => {
    if (megabytes > 1000000) {
        return `${(megabytes / 1024 / 1024).toFixed(2)} TB`
    } else if (megabytes > 1000) {
        return `${(megabytes / 1024).toFixed(2)} GB`
    } else {
        return `${megabytes.toFixed(2)} MB`
    }
}

const SitesTable = ({ sites }: { sites: GetSitesResponse | undefined }) => {
    return (
        <>
            <table className="table table-bordered table-hover table-striped table-sm">
                <thead>
                    <tr>
                        <th style={{ width: '1%' }}></th>
                        <th style={{ width: '20%' }}>Name</th>
                        {!hideCredits && (
                            <th style={{ width: '4%' }}>Credits</th>
                        )}
                        <th style={{ width: '2%' }}>Sections</th>
                        <th style={{ width: '2%' }}>Affils</th>
                        <th style={{ width: '3%' }}></th>
                    </tr>
                </thead>
                <tbody>
                    {sites &&
                        Object.entries(sites).map(site => {
                            return (
                                <SiteRow
                                    key={site[0]}
                                    name={site[0]}
                                    info={site[1]}
                                />
                            )
                        })}
                </tbody>
            </table>
        </>
    )
}

const SiteRow = ({ name, info }: { name: string; info: Site }) => {
    const rowClasses = classNames({
        //'table-danger': !info.enabled
    })

    let credits
    if (!hideCredits) {
        console.log(info)
        credits =
            Object.entries(info.credits).length > 0
                ? Object.entries(info.credits ?? {}).map(([bnc, credits]) => (
                      <p style={{ marginBottom: 0 }} key={bnc}>
                          {bnc}: {creditsFormatter(credits)}
                      </p>
                  ))
                : '-'
    }

    const siteName = name

    return (
        <tr className={rowClasses}>
            <td style={{ textAlign: 'center' }}>
                {info.enabled ? (
                    <i
                        className="icon-check text-success"
                        data-tooltip="Enabled"
                    ></i>
                ) : (
                    <i className="fa icon-times" style={{ color: 'red' }}></i>
                )}
            </td>
            <td style={{ position: 'relative' }}>
                <Link
                    to={`/site/${name}/edit`}
                    style={{
                        textDecoration: 'none',
                        color: 'inherit',
                        position: 'absolute',
                        top: 7,
                        bottom: 7,
                        left: 7,
                        right: 7
                    }}
                >
                    {siteName}
                </Link>
            </td>
            {!hideCredits && <td>{credits}</td>}
            <td>
                {info.sections && (
                    <span className="badge text-bg-secondary">
                        {info.sections.length}
                    </span>
                )}
            </td>
            <td>
                {info.affils && (
                    <span className="badge text-bg-secondary">
                        {info.affils.length}
                    </span>
                )}
            </td>
            <td>
                <div className="btn-group">
                    <Link
                        className="btn btn-outline-secondary btn-sm"
                        to={`/site/${name}/edit`}
                    >
                        Edit
                    </Link>
                    {/* <a
                        className="btn btn-outline-secondary btn-disabled hint--bottom"
                        aria-label="Not yet implemented"
                    >
                        <i className="icon icon-trash-o"></i>
                    </a> */}
                </div>
            </td>
        </tr>
    )
}

export default () => {
    const { sites, loadingSites } = useSites()

    const activeSites = sites
        ? Object.fromEntries(
              Object.entries(sites).filter(([_name, value]) => {
                  return value.enabled
              })
          )
        : {}

    const inactiveSites = sites
        ? Object.fromEntries(
              Object.entries(sites).filter(([_name, value]) => {
                  return !value.enabled
              })
          )
        : {}

    return (
        <div className="container-lg">
            {loadingSites ? (
                <Loading label="Loading sites" />
            ) : (
                <>
                    <Title
                        title="Site List"
                        rightChildren={
                            <Link to={`/site/add`} className="btn btn-primary">
                                + Add site
                            </Link>
                        }
                    />
                    <h4>Enabled</h4>
                    <SitesTable sites={activeSites} />
                    <h4>Disabled</h4>
                    <SitesTable sites={inactiveSites} />
                    <br />
                    <Alert variant="warning">
                        To rename a site, stop TRD and rename the JSON file in
                        your DATA_PATH
                    </Alert>
                </>
            )}
        </div>
    )
}
