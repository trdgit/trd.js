import {
    Site,
    SiteSection,
    TestExtractionResponse
} from '@trd/shared/src/types'
import classNames from 'classnames'
import _ from 'lodash'
import React from 'react'
import Toast from 'react-bootstrap/Toast'
import ToastContainer from 'react-bootstrap/ToastContainer'
import { Params, useParams } from 'react-router-dom'
import { toast } from 'react-toastify'

import { API_URL, errorHandler, getJSON, postJSON } from '../helpers/api'
import Loading from './Loading'
import { SiteEditSection } from './SiteEditSection'
import Title from './Title'

const testAnnounceString = async (name: string, key: string, info: Site) => {
    const str = _.get(info, key)

    if (!str?.length) {
        return
    }

    const section = _.get(info, key + '-section')
    const rlsname = _.get(info, key + '-rls')
    if (!section?.length || !rlsname?.length) {
        return
    }

    const testString = prompt('Enter a sample string...')
    if (!testString?.length) {
        return
    }

    const response = await postJSON<TestExtractionResponse>(
        `/site/${name}/testString`,
        {
            testString,
            key
        }
    )

    if (response.matched) {
        alert(
            'Matched section: ' +
                response.section +
                ' and rlsname: ' +
                response.rlsname
        )
    } else {
        alert('No match')
    }
}

const ucFirst = (str: string) => {
    return str ? str.charAt(0).toUpperCase() + str.slice(1) : str
}

const StringRow = ({
    siteName,
    k,
    placeholder,
    info,
    onChange
}: {
    siteName: string
    k: 'new' | 'end' | 'pre'
    placeholder: string
    info: Site
    onChange: (k: string, e: React.FormEvent<HTMLInputElement>) => void
}) => {
    let stringValue = info.irc.strings.newstring
    let sectionValue = info.irc.strings['newstring-section']
    let rlsValue = info.irc.strings['newstring-rls']
    let regexValue = info.irc.strings['newstring-isregex']

    if (k === 'end') {
        stringValue = info.irc.strings.endstring
        sectionValue = info.irc.strings['endstring-section']
        rlsValue = info.irc.strings['endstring-rls']
        regexValue = info.irc.strings['endstring-isregex']
    } else if (k === 'pre') {
        stringValue = info.irc.strings.prestring
        sectionValue = info.irc.strings['prestring-section']
        rlsValue = info.irc.strings['prestring-rls']
        regexValue = info.irc.strings['endstring-isregex']
    }

    return (
        <div className="row mb-3 align-items-center">
            <div className="col-sm-7">
                <div className="input-group">
                    <span
                        className="input-group-text"
                        style={{ minWidth: '60px' }}
                    >
                        {ucFirst(k)}
                    </span>
                    <input
                        type="text"
                        className="form-control"
                        id={`input${k}String`}
                        placeholder={placeholder}
                        value={stringValue}
                        onChange={e => onChange(`irc.strings.${k}string`, e)}
                    />
                </div>
            </div>
            <div className="col-md-1">
                <input
                    type="text"
                    className="form-control"
                    placeholder="Section"
                    value={sectionValue}
                    onChange={e =>
                        onChange(`irc.strings.${k}string-section`, e)
                    }
                    maxLength={2}
                    inputMode="numeric"
                    pattern="[0-9]*"
                />
            </div>
            <div className="col-md-1">
                <input
                    type="text"
                    className="form-control"
                    placeholder="Rls"
                    value={rlsValue}
                    onChange={e => onChange(`irc.strings.${k}string-rls`, e)}
                    maxLength={2}
                    inputMode="numeric"
                    pattern="[0-9]*"
                />
            </div>
            <div className="col-md-2">
                <div className="form-check">
                    <input
                        type="checkbox"
                        value=""
                        defaultChecked={regexValue}
                        onChange={e =>
                            onChange(`irc.strings.${k}string-isregex`, e)
                        }
                        className="form-check-input"
                    />
                    <label className="form-check-label">Regex?</label>
                </div>
            </div>
            <div className="col-md-1">
                <a
                    className="btn btn-outline-secondary btn-block"
                    onClick={e => {
                        testAnnounceString(
                            siteName,
                            `irc.strings.${k}string`,
                            info
                        )
                    }}
                >
                    Test
                </a>
            </div>
        </div>
    )
}

const load = async (name: string) => {
    return getJSON<Site>(`/site/${name}`)
}
const save = async (name: string, info: Site) => {
    try {
        await postJSON(`/site/${name}/save`, info)
        toast.success('Saved!', {
            toastId: 'saved'
        })
    } catch (e) {
        const error = errorHandler(e)
        toast.error(error.message)
    }
}

interface SiteEditProps {
    name: string
}

/*
const SiteEdit2 = ({ name }: SiteEditProps) => {
    const [info, setInfo] = useState<Site>()
    const [activeSection, setActiveSection] = useState<string>()
    const [showToast, setShowToast] = useState<boolean>()

    useEffect(() => {
        const fetchSiteInfo = async () => {
            const data = await getJSON<Site>(`/site/${name}`)
            setInfo(data)
            let section
            if (data.sections.length > 0) {
                section = _.sortBy(data.sections, 'name')[0].name
            }
            setActiveSection(section)
        }
        fetchSiteInfo()
    }, [])

    const save = async () => {
        const response = await postJSON(`${API_URL}/site/${name}/save`, info)
        if (!response.ok) {
            toast.error(body.error)
        } else {
            toast.success('Saved!', {
                toastId: 'saved',
            })
        }
    }
}
*/

interface SiteEditClassProps {
    params: Readonly<Params<string>>
    [x: string]: any
}

interface SiteEditClassState {
    info?: Site
    activeSection?: string
    showToast: boolean
}

class SiteEdit extends React.Component<SiteEditClassProps, SiteEditClassState> {
    save: _.DebouncedFunc<any>

    constructor(props: SiteEditClassProps) {
        super(props)

        this.state = {
            info: undefined,
            activeSection: undefined,
            showToast: false
        }

        const { name } = this.props.params

        this.save = _.debounce(async (newState: SiteEditClassState) => {
            if (name && newState.info) {
                await save(name, newState.info)
            }
        }, 2500)
    }

    async UNSAFE_componentWillMount() {
        var that = this

        if (!this.props.params.name) {
            return
        }

        const data = await load(this.props.params.name)
        let activeSection
        if (data.sections.length > 0) {
            activeSection = _.sortBy(data.sections, 'name')[0].name
        }

        that.setState({
            info: data,
            activeSection
        })
    }

    async UNSAFE_componentWillUpdate(
        _nextProps: SiteEditClassProps,
        nextState: SiteEditClassState
    ) {
        const current = JSON.stringify(this.state)
        const next = JSON.stringify(nextState)

        if (!nextState.info) {
            return
        }

        if (current === next) {
            return
        }

        // bail if toast is toggling
        if (this.state.showToast != nextState.showToast) {
            return
        }

        const sectionLengthChanged =
            this.state.info?.sections.length !== nextState.info.sections.length

        const sectionRenamed =
            JSON.stringify(
                this.state.info?.sections.map(s => s.name).sort()
            ) !==
            JSON.stringify(nextState.info?.sections.map(s => s.name).sort())

        // avoid if switching...
        if (
            this.state.activeSection !== nextState.activeSection &&
            !sectionLengthChanged &&
            !sectionRenamed
        ) {
            return
        }

        if (this.state.info) {
            await this.save(nextState)
        }
    }

    addSection() {
        if (!this.state.info) {
            return
        }

        const newSection = prompt('Enter section (directory) name:')
        if (newSection !== null) {
            const found = this.state.info.sections.some(section => {
                return newSection.toLowerCase() === section.name.toLowerCase()
            })

            if (found) {
                alert('Section already exists')
                return
            }

            const newState = JSON.parse(JSON.stringify(this.state.info)) as Site
            newState.sections.push({
                name: newSection,
                bnc: '',
                pretime: 5,
                tags: [],
                skiplists: [],
                rules: [],
                dupeRules: {
                    'source.firstWins': false,
                    'source.priority': ''
                },
                downloadOnly: false
            })
            this.setState({
                info: newState,
                activeSection: newSection
            })
        }
    }

    onDeleteSection(section: string, event: React.MouseEvent<HTMLElement>) {
        if (!this.state.info) {
            return
        }

        if (
            confirm(
                'Are you sure you want to permanently remove this section: ' +
                    section +
                    '?'
            )
        ) {
            var newState = JSON.parse(JSON.stringify(this.state.info)) as Site

            var sectionKey = _.findIndex(newState.sections, ['name', section])

            if (sectionKey >= 0) {
                newState.sections.splice(sectionKey, 1)

                let activeSection = this.state.activeSection
                if (section === this.state.activeSection) {
                    activeSection = _.sortBy(
                        this.state.info.sections,
                        'name'
                    )[0].name
                }

                this.setState({
                    info: newState,
                    activeSection
                })
            }
        }
        event.stopPropagation()
        event.preventDefault()
    }

    onChangeSection(newSection: string, e: React.MouseEvent<HTMLElement>) {
        this.setState({
            activeSection: newSection
        })
        e.preventDefault()
        return false
    }

    onChangeConfig(key: string, event: React.FormEvent<HTMLInputElement>) {
        var newState = JSON.parse(JSON.stringify(this.state.info)) as Site

        if (_.has(newState, key)) {
        }

        switch (key) {
            case 'enabled':
            case 'irc.strings.newstring-isregex':
            case 'irc.strings.endstring-isregex':
            case 'irc.strings.prestring-isregex':
                _.set(newState, key, event.currentTarget.checked)
                break

            case 'affils':
            case 'banned_groups':
                var val = event.currentTarget.value.toUpperCase()
                _.set(newState, key, val.split(' '))
                break

            default:
                _.set(newState, key, event.currentTarget.value)
        }

        this.setState({
            info: newState
        })
    }

    sortAffils() {
        if (!this.state.info) {
            return
        }

        var newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        _.set(newState, 'affils', _.uniq(this.state.info.affils.sort()))
        this.setState({
            info: newState
        })
    }

    sortBannedGroups() {
        if (!this.state.info) {
            return
        }

        var newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        _.set(
            newState,
            'banned_groups',
            _.uniq(this.state.info.banned_groups.sort())
        )
        this.setState({
            info: newState
        })
    }

    onChangeSectionConfig(
        section: string,
        key: keyof SiteSection,
        event: React.FormEvent<HTMLInputElement>
    ) {
        var newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        var sectionKey = _.findIndex(newState.sections, ['name', section])
        if (sectionKey >= 0) {
            if (key === 'rules') {
                newState.sections[sectionKey].rules =
                    event.currentTarget.value.split('\n')
            } else if (key == 'downloadOnly') {
                newState.sections[sectionKey].downloadOnly =
                    event.currentTarget.checked
            } else {
                // @ts-ignore
                newState.sections[sectionKey][key] = event.currentTarget.value
            }

            this.setState({
                info: newState
            })
        }
    }

    onChangeSectionConfigRules(value: string) {
        var newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        var sectionKey = _.findIndex(newState.sections, [
            'name',
            this.state.activeSection
        ])
        if (sectionKey >= 0) {
            newState.sections[sectionKey].rules = _.without(
                value.trim().split('\n'),
                ''
            )
            this.setState({
                info: newState
            })
        }
    }

    onChangeSectionDupeRules(
        k: string,
        event: React.FormEvent<HTMLInputElement>
    ) {
        const newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        var sectionKey = _.findIndex(newState.sections, [
            'name',
            this.state.activeSection
        ])
        if (sectionKey >= 0) {
            switch (k) {
                case 'source.firstWins':
                case 'range.firstWins':
                    newState.sections[sectionKey].dupeRules[k] =
                        event.currentTarget.checked
                    break
                case 'source.priority':
                case 'range.priority':
                    newState.sections[sectionKey].dupeRules[k] =
                        event.currentTarget.value
                    break
            }

            this.setState({
                info: newState
            })
        }
    }

    onAddTag(tag: string) {
        const trigger = prompt('Choose a regex trigger', '/.*/i')
        if (trigger?.length) {
            let newState = JSON.parse(JSON.stringify(this.state.info)) as Site
            const sectionKey = _.findIndex(newState.sections, [
                'name',
                this.state.activeSection
            ])
            if (sectionKey >= 0) {
                newState.sections[sectionKey].tags.push({
                    tag: tag,
                    trigger: trigger,
                    rules: []
                })
                this.setState({
                    info: newState
                })
            }
        }
    }

    onRemoveTag(tag: string) {
        if (confirm('Are you sure you want to remove this tag?')) {
            const newState = JSON.parse(JSON.stringify(this.state.info)) as Site
            const sectionKey = _.findIndex(newState.sections, [
                'name',
                this.state.activeSection
            ])
            const tagKey = _.findIndex(newState.sections[sectionKey].tags, [
                'tag',
                tag
            ])
            newState.sections[sectionKey].tags.splice(tagKey, 1)
            this.setState({
                info: newState
            })
        }
    }

    onChangeTag(tag: string, newValues: Record<string, unknown>) {
        var newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        const sectionKey = _.findIndex(newState.sections, [
            'name',
            this.state.activeSection
        ])
        const tagKey = _.findIndex(newState.sections[sectionKey].tags, [
            'tag',
            tag
        ])

        for (var k in newValues) {
            if (newValues.hasOwnProperty(k)) {
                var value = newValues[k]
                if (k === 'rules') {
                    value = _.without((value as string).trim().split('\n'), '')
                    // @ts-ignore
                    newState.sections[sectionKey].tags[tagKey].rules = value
                } else {
                    // @ts-ignore
                    newState.sections[sectionKey].tags[tagKey][k] = value
                }
            }
        }

        this.setState({
            info: newState
        })
    }

    onAddSkiplist(skiplist: string) {
        const newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        const sectionKey = _.findIndex(newState.sections, [
            'name',
            this.state.activeSection
        ])
        if (
            sectionKey >= 0 &&
            newState.sections[sectionKey].skiplists.indexOf(skiplist) === -1
        ) {
            newState.sections[sectionKey].skiplists.push(skiplist)
            this.setState({
                info: newState
            })
        }
    }

    onRemoveSkiplist(skiplist: string) {
        if (!this.state.info) {
            return
        }

        var newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        var sectionKey = _.findIndex(newState.sections, [
            'name',
            this.state.activeSection
        ])
        var skiplistKey =
            this.state.info.sections[sectionKey].skiplists.indexOf(skiplist)
        newState.sections[sectionKey].skiplists.splice(skiplistKey, 1)
        this.setState({
            info: newState
        })
    }

    onRenameSection(newSectionName: string) {
        console.log(this.state.activeSection, newSectionName)
        var newState = JSON.parse(JSON.stringify(this.state.info)) as Site
        var sectionKey = _.findIndex(newState.sections, [
            'name',
            this.state.activeSection
        ])
        if (sectionKey >= 0) {
            newState.sections[sectionKey].name = newSectionName
            this.setState({
                info: newState,
                activeSection: newSectionName
            })
        }
    }

    render() {
        var that = this

        if (!this.state.info) {
            return (
                <div id="site-edit">
                    <Loading />
                </div>
            )
        }

        const info = this.state.info as Site
        const { activeSection } = this.state

        const sectionNames = info.sections.map(s => s.name)

        var sectionList = _.sortBy(info.sections, 'name').map(
            function (section) {
                const hasNoTags =
                    section.tags.length == 0 ||
                    typeof section.rules == 'undefined' ||
                    (section.rules.length == 0 && section.tags.length == 0)

                const itemClasses = classNames({
                    'list-group-item': true,
                    'list-group-item-action': true,
                    'd-flex justify-content-between align-items-center': true,
                    active: activeSection == section.name,
                    notags: hasNoTags
                })

                const tagBadgeClasses = classNames({
                    'badge rounded-pill': true,
                    'text-bg-success': !hasNoTags,
                    'text-bg-danger': hasNoTags
                })

                const tagOrTags = section.tags.length === 1 ? 'tag' : 'tags'

                return (
                    <a
                        href="#"
                        key={section.name}
                        className={itemClasses}
                        onClick={that.onChangeSection.bind(that, section.name)}
                    >
                        <span
                            style={{
                                marginRight: 'auto'
                            }}
                        >
                            {section.name}
                        </span>
                        <span
                            className={tagBadgeClasses}
                            style={{
                                marginRight: '.5em'
                            }}
                        >
                            {`${section.tags.length} ${tagOrTags}`}
                        </span>
                        <span className="badge text-bg-secondary rounded-pill">
                            <i
                                onClick={that.onDeleteSection.bind(
                                    that,
                                    section.name
                                )}
                                className="icon-times"
                            />
                        </span>
                    </a>
                )
            }
        )

        let sectionDOM
        if (activeSection) {
            const section = info.sections.find(s => s.name === activeSection)
            if (section) {
                sectionDOM = (
                    <SiteEditSection
                        info={section}
                        onChange={this.onChangeSectionConfig.bind(this)}
                        onAddTag={this.onAddTag.bind(this)}
                        onRemoveTag={this.onRemoveTag.bind(this)}
                        onChangeTag={this.onChangeTag.bind(this)}
                        onAddSkiplist={this.onAddSkiplist.bind(this)}
                        onRemoveSkiplist={this.onRemoveSkiplist.bind(this)}
                        onChangeSectionRules={this.onChangeSectionConfigRules.bind(
                            this
                        )}
                        onChangeSectionDupeRules={this.onChangeSectionDupeRules.bind(
                            this
                        )}
                        sectionNames={sectionNames}
                        onRename={this.onRenameSection.bind(this)}
                    />
                )
            }
        }

        const {
            params: { name }
        } = this.props

        if (!name) {
            return <></>
        }

        const btnMargin = {
            marginLeft: '5px'
        }

        const rightChildren = (
            <>
                {(info.irc.channel.length === 0 ||
                    info.irc.bot.length === 0) && (
                    <span
                        className="btn btn-sm text-bg-danger"
                        style={btnMargin}
                    >
                        This site has no channel + bot configuration!
                    </span>
                )}
                {!info.sections.length ? (
                    <span
                        className="btn btn-sm text-bg-danger"
                        style={btnMargin}
                    >
                        This site has no sections!
                    </span>
                ) : (
                    <span
                        className="btn btn-sm text-bg-success"
                        style={btnMargin}
                    >
                        Sections{' '}
                        <span className="badge text-bg-light">
                            {info.sections.length}
                        </span>
                    </span>
                )}
                {!info.affils.length ? (
                    <span
                        className="btn btn-sm text-bg-secondary"
                        style={btnMargin}
                    >
                        This site has no affils
                    </span>
                ) : (
                    <span
                        className="btn btn-sm text-bg-success"
                        style={btnMargin}
                    >
                        Affils{' '}
                        <span className="badge text-bg-light">
                            {info.affils.length}
                        </span>
                    </span>
                )}
                {!info.enabled && (
                    <span
                        className="btn btn-sm text-bg-danger"
                        style={btnMargin}
                    >
                        This site is disabled!
                    </span>
                )}
            </>
        )

        return (
            <>
                <div id="site-edit" className="container-fluid">
                    <Title
                        title={
                            <>
                                Editing site:{' '}
                                <span className="text-primary">{name}</span>
                            </>
                        }
                        rightChildren={rightChildren}
                    />

                    <fieldset>
                        <h4 className="mb-3">Basic Configuration</h4>

                        <div className="row mb-3 align-items-center">
                            <label
                                htmlFor="inputAffils"
                                className="col-sm-2 control-label"
                            >
                                Enabled?
                            </label>
                            <div className="col-sm-10">
                                <div className="form-check form-switch">
                                    <input
                                        className="form-check-input"
                                        tabIndex={1}
                                        id="site-enabled"
                                        type="checkbox"
                                        value="1"
                                        defaultChecked={info.enabled}
                                        onChange={this.onChangeConfig.bind(
                                            this,
                                            'enabled'
                                        )}
                                        style={{ cursor: 'pointer' }}
                                    />
                                </div>
                            </div>
                        </div>

                        <div>
                            <div className="row mb-3 align-items-center">
                                <label
                                    htmlFor="inputAffils"
                                    className="col-sm-2 control-label"
                                >
                                    Affils
                                </label>
                                <div className="col-sm-10">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="inputAffils"
                                            placeholder="List of affils (seperated by spaces)"
                                            value={info.affils.join(' ')}
                                            onChange={this.onChangeConfig.bind(
                                                this,
                                                'affils'
                                            )}
                                            style={{
                                                textTransform: 'uppercase'
                                            }}
                                        />
                                        <button
                                            className="btn btn-outline-secondary"
                                            onClick={this.sortAffils.bind(this)}
                                        >
                                            Sort A-Z
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="row mb-3 align-items-center">
                                <label
                                    htmlFor="inputBannedGroups"
                                    className="col-sm-2 control-label"
                                >
                                    Banned groups
                                </label>
                                <div className="col-sm-10">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="inputBannedGroups"
                                            placeholder="List of banned groups (seperated by spaces)"
                                            value={info.banned_groups.join(' ')}
                                            onChange={this.onChangeConfig.bind(
                                                this,
                                                'banned_groups'
                                            )}
                                            style={{
                                                textTransform: 'uppercase'
                                            }}
                                        />
                                        <button
                                            className="btn btn-outline-secondary"
                                            onClick={this.sortBannedGroups.bind(
                                                this
                                            )}
                                        >
                                            Sort A-Z
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <hr className="my-4" />

                    <fieldset>
                        <h4 className="mb-3">IRC Configuration</h4>

                        <div className="row g-3">
                            <div className="col-12">
                                <div className="row mb-3">
                                    <label
                                        htmlFor="inputIrcChannel"
                                        className="col-sm-2 control-label"
                                    >
                                        Channel
                                    </label>
                                    <div className="col-sm-4">
                                        <input
                                            type="text"
                                            className="form-control form-control-code"
                                            id="inputIrcChannel"
                                            placeholder="Regex to match channel"
                                            value={info.irc.channel}
                                            onChange={this.onChangeConfig.bind(
                                                this,
                                                'irc.channel'
                                            )}
                                            required
                                        />
                                        <div className="form-text form-text-inline">
                                            (Regular expression e.g.{' '}
                                            <code>/#channel/i</code>)
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <label
                                        htmlFor="inputIrcBot"
                                        className="col-sm-2 control-label"
                                    >
                                        Bot
                                    </label>
                                    <div className="col-sm-4">
                                        <input
                                            type="text"
                                            className="form-control form-control-code"
                                            id="inputIrcBot"
                                            placeholder="Regex to match bot"
                                            value={info.irc.bot}
                                            onChange={this.onChangeConfig.bind(
                                                this,
                                                'irc.bot'
                                            )}
                                        />
                                        <div className="form-text form-text-inline">
                                            (Regular expression e.g.{` `}
                                            <code>/irc\.nick/i</code>)
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <hr className="my-4" />

                    <fieldset>
                        <h4 className="mb-3">Announce strings</h4>

                        <div>
                            <div className="row mb-3 align-items-center">
                                <div className="col-sm-7" />
                                <div className="col-sm-1">
                                    <strong>Section</strong>
                                </div>
                                <div className="col-sm-1">
                                    <strong>Rlsname</strong>
                                </div>
                                <div className="col-md-2" />
                            </div>
                            <StringRow
                                siteName={name}
                                k="new"
                                placeholder="n/a"
                                info={info}
                                onChange={this.onChangeConfig.bind(this)}
                            />
                            <StringRow
                                siteName={name}
                                k="end"
                                placeholder="n/a"
                                info={info}
                                onChange={this.onChangeConfig.bind(this)}
                            />
                            <StringRow
                                siteName={name}
                                k="pre"
                                placeholder="n/a"
                                info={info}
                                onChange={this.onChangeConfig.bind(this)}
                            />
                        </div>
                    </fieldset>

                    <hr className="my-4" />

                    <fieldset>
                        <legend>Sections ({info.sections.length})</legend>

                        <div className="row">
                            <div className="col-md-3">
                                <div
                                    className="list-group"
                                    style={{ marginBottom: '1em' }}
                                >
                                    {sectionList}
                                </div>
                                <a
                                    className="btn btn-block btn-outline-secondary"
                                    onClick={this.addSection.bind(this)}
                                >
                                    Add section
                                </a>
                            </div>
                            <div className="col-md-9">{sectionDOM}</div>
                        </div>
                    </fieldset>
                </div>
                <ToastContainer
                    position={`top-end`}
                    className="p-3 position-fixed"
                    style={{ zIndex: 9999 }}
                >
                    <Toast
                        onClose={() =>
                            this.setState({
                                showToast: false
                            })
                        }
                        show={this.state.showToast}
                        delay={3000}
                        autohide
                    >
                        <Toast.Header closeButton={false}>
                            <strong className="me-auto"></strong>
                        </Toast.Header>
                        <Toast.Body></Toast.Body>
                    </Toast>
                </ToastContainer>
            </>
        )
    }
}

export default ({ ...props }) => {
    const params = useParams()
    return <SiteEdit params={params} {...props} />
}
