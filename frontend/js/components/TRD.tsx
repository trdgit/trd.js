import 'bootstrap/js/dist/collapse'
import 'hint.css/hint.min.css'
import React from 'react'
import { createRoot } from 'react-dom/client'
import regeneratorRuntime from 'regenerator-runtime'

import '../../sass/trd.scss'
import App from './App'

declare global {
    interface Window {
        _env: any
        _settings: any
    }
    interface Array<T> {
        sortIgnoreCase(): Array<T>
    }
}

const container = document.getElementById('app')
const root = createRoot(container!) // createRoot(container!) if you use TypeScript
root.render(<App />)

if (!Array.prototype.sortIgnoreCase) {
    Array.prototype.sortIgnoreCase = function () {
        return this.sort(function (a, b) {
            return a.toLowerCase().localeCompare(b.toLowerCase())
        })
    }
}
