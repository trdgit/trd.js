import { Prebot } from '@trd/shared/src/types'
import React from 'react'

interface PrebotsEditFormProps {
    data: Prebot
    index: number
    onChange: ChangeEvent
    onDelete: DeleteEvent
}

type ChangeEvent = (index: number, field: string, value: string) => void

type DeleteEvent = (index: number) => void

export default ({ data, index, onChange, onDelete }: PrebotsEditFormProps) => {
    const onChangeLocal = (
        field: string,
        e: React.FormEvent<HTMLInputElement>
    ) => {
        onChange(index, field, e.currentTarget.value)
    }

    const onDeleteLocal = () => {
        const sure = confirm('You sure?')
        if (sure) {
            onDelete(index)
        }
    }

    return (
        <div>
            <div className="card">
                <div className="card-body">
                    <div className="mb-3">
                        <label className="form-label">Channel (regex)</label>
                        <input
                            className="form-control"
                            type="text"
                            value={data.channel}
                            onChange={e => {
                                onChangeLocal('channel', e)
                            }}
                        />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Bot (regex)</label>
                        <input
                            className="form-control"
                            type="text"
                            value={data.bot}
                            onChange={e => {
                                onChangeLocal('bot', e)
                            }}
                        />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">
                            String match (regex)
                        </label>
                        <input
                            className="form-control"
                            type="text"
                            value={data.string_match}
                            onChange={e => {
                                onChangeLocal('string_match', e)
                            }}
                        />
                        <div className="form-text">
                            Make sure to use parentheses for matching the
                            rlsname
                        </div>
                    </div>
                    <div className="mb-3">
                        <button
                            className="btn btn-outline-secondary"
                            onClick={onDeleteLocal}
                        >
                            Delete
                        </button>
                    </div>
                </div>
            </div>
            <br />
        </div>
    )
}
