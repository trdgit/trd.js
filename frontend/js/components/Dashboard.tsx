import React, { useEffect, useState } from 'react'
import { getJSON } from '../helpers/api'
import Loading from './Loading'

interface StatProps {
    title: string
    value: string | number
    unit?: string | number
}

const Stat = ({ title, value, unit }: StatProps) => (
    <div className="card">
        <div className="card-body">
            <h2 className="card-title">
                {value}
                {unit && <span style={{ fontSize: '0.5em' }}>{unit}</span>}
            </h2>
            <p className="card-text">{title}</p>
        </div>
    </div>
)

const DashboardHeading = ({ title }: { title: string }) => (
    <h5 style={{ marginTop: '2rem' }}>{title}</h5>
)

const Dashboard = () => {
    const [loaded, setLoaded] = useState<boolean>(false)
    const [data, setData] = useState()

    useEffect(() => {
        const fetchData = async () => {
            const response = await getJSON('/')
            console.log({ response })
            setData(response)
            setLoaded(true)
        }

        fetchData()
    }, [])

    if (!loaded) {
        return <Loading />
    }

    return (
        <div className="container-sm">
            <DashboardHeading title="Races" />
            <div className="row row-cols-md-2 row-cols-lg-4 g-4">
                <div className="col">
                    <Stat title="Total races" value={data.races.totalRaces} />
                </div>
                <div className="col">
                    <Stat
                        title="Most raced tag"
                        value={data.races.mostRacedTag}
                    />
                </div>
                <div className="col">
                    <Stat
                        title="Least raced tag"
                        value={data.races.leastRacedTag}
                    />
                </div>
                <div className="col">
                    <Stat
                        title="Oldest race"
                        unit="d"
                        value={data.races.oldestRace}
                    />
                </div>
            </div>

            <DashboardHeading title="Sites" />
            <div className="row row-cols-md-2 row-cols-lg-4 g-4">
                <div className="col">
                    <Stat
                        title="Total active sites"
                        value={data.sites.totalSites}
                    />
                </div>
                <div className="col">
                    <Stat title="Total affils" value={data.sites.totalAffils} />
                </div>
                <div className="col">
                    <Stat
                        title="Most popular affil"
                        value={data.sites.mostPopularAffil}
                    />
                </div>
            </div>
            <DashboardHeading title="Tags" />
            <div className="row row-cols-md-2 row-cols-lg-4 g-4">
                <div className="col">
                    <Stat title="Total tags" value={data.tags.totalTags} />
                </div>
            </div>
            <DashboardHeading title="Data" />
            <div className="row row-cols-md-2 row-cols-lg-4 g-4">
                <div className="col">
                    <Stat
                        title="Cached data entries"
                        value={data.data.totalEntries}
                    />
                </div>
                <div className="col">
                    <Stat
                        title="Fixed data entries"
                        value={data.data.fixedEntries}
                    />
                </div>
            </div>
            <DashboardHeading title="Pre database" />
            <div className="row row-cols-md-2 row-cols-lg-4 g-4">
                <div className="col">
                    <Stat
                        title="Total database entries"
                        value={data.pre.totalPres}
                    />
                </div>
                <div className="col">
                    <Stat
                        title="Database age"
                        unit="d"
                        value={data.pre.firstPre}
                    />
                </div>
            </div>
        </div>
    )
}

export default Dashboard
