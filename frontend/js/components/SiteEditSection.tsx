import {
    Lists,
    SettingsModelType,
    SiteSection,
    SiteSectionDupeRules
} from '@trd/shared/src/types'
import classNames from 'classnames'
import _ from 'lodash'
import React, { ReactNode, useEffect, useState } from 'react'

import { getJSON } from '../helpers/api'
import RulesEditor from './RulesEditor'

const tabs = ['general', 'tags', 'rules', 'dupes', 'skiplists', 'tools']

interface SiteEditSectionProps {
    info: SiteSection
    onChange: (
        section: string,
        key: keyof SiteSection,
        event: React.FormEvent<HTMLInputElement>
    ) => void
    onAddTag: (tag: string) => void
    onRemoveTag: (tag: string) => void
    onChangeTag: (tag: string, newValues: Record<string, unknown>) => void
    onAddSkiplist: (skiplistName: string) => void
    onRemoveSkiplist: (skiplistName: string) => void
    onChangeSectionRules: (rules: string) => void
    onChangeSectionDupeRules: (
        k: keyof SiteSectionDupeRules,
        event: React.FormEvent<HTMLInputElement>
    ) => void
    onRename: (newSectionName: string) => void
    sectionNames: string[]
}

const TagList = ({
    tags,
    info,
    onToggleTag,
    onChooseTagToEdit
}: {
    tags: string[]
    info: SiteSection
    onToggleTag: (tag: string, checked: boolean) => void
    onChooseTagToEdit: (tag: string, event: React.FormEvent) => void
}) => {
    if (!tags?.length) {
        return <></>
    }

    return (
        <>
            {tags.map(tag => {
                const ticked = info.tags.some(t => t.tag === tag)

                let extra
                if (ticked) {
                    extra = (
                        <span style={{ marginLeft: '.25em' }}>
                            <a
                                href="#/"
                                onClick={e => onChooseTagToEdit(tag, e)}
                                className="btn btn-outline-secondary btn-xs"
                            >
                                Edit
                            </a>
                        </span>
                    )
                }

                return (
                    <TickableItem
                        key={`tag-editor-${tag}`}
                        label={tag}
                        cols={4}
                        ticked={ticked}
                        onClick={() => onToggleTag(tag, !ticked)}
                        extra={extra}
                    />
                )
            })}
        </>
    )
}

const TickableItem = ({
    label,
    ticked,
    extra,
    cols = 6,
    onClick
}: {
    label: string
    ticked: boolean
    extra?: ReactNode
    cols?: number
    onClick: (label: string, ticked: boolean) => void
}) => {
    let iconClass = 'icon-times'
    let colorClass = 'text-secondary'
    if (ticked) {
        iconClass = 'icon-check'
        colorClass = 'text-success'
    }

    const icon = <i className={`${iconClass} ${colorClass}`}></i>

    return (
        <div className={`col-md-${cols}`} key={`tickable-item-${label}`}>
            <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    cursor: 'pointer'
                }}
            >
                <div onClick={() => onClick(label, !ticked)}>
                    {icon} <code style={{ marginLeft: '.25em' }}>{label}</code>
                </div>
                {extra && extra}
            </div>
        </div>
    )
}

const Skiplists = ({
    info,
    lists,
    onToggle
}: {
    info: SiteSection
    lists: Lists
    onToggle: (skiplistName: string, checked: boolean) => void
}) => {
    const l = Object.keys(lists)
    if (!l.length) {
        return <></>
    }

    return (
        <>
            {l.sort().map(skiplist => {
                const ticked = info.skiplists.some(s => s === skiplist)

                return (
                    <TickableItem
                        key={`skiplist-editor-${skiplist}`}
                        label={skiplist}
                        ticked={ticked}
                        onClick={e => onToggle(skiplist, !ticked)}
                    />
                )
            })}
        </>
    )
}

const TriggerEditor = ({
    tag,
    value,
    onChange
}: {
    tag: string
    value: string
    onChange: (e: React.FormEvent<HTMLInputElement>) => void
}) => (
    <div style={{ marginTop: '1em' }}>
        <label htmlFor="exampleInputName2">
            Trigger for tag <code>{tag}</code>:
        </label>
        <div className="form-group">
            <input
                type="text"
                className="form-control"
                id="exampleInputName2"
                placeholder="trigger.."
                size={6}
                value={value ? value : ''}
                onChange={onChange}
            />
        </div>
    </div>
)

const getTabClasses = (
    tabs: string[],
    activeTab?: string
): Record<string, string> => {
    const tabClasses: Record<string, string> = {}
    tabs.map(tab => {
        tabClasses[tab] = classNames({
            'tab-panel': true,
            'tab-panel-visible': activeTab === tab
        })
    })
    return tabClasses
}

const TabList = ({
    info,
    tabs,
    onChangeTab,
    activeTab
}: {
    info: SiteSection
    tabs: string[]
    onChangeTab: (tab: string, e: React.FormEvent) => void
    activeTab?: string
}) => {
    const tabList = tabs.map(tab => {
        const label = tab.charAt(0).toUpperCase() + tab.slice(1)
        let badge
        if (tab === 'rules' && info.rules) {
            badge = (
                <span className="badge rounded-pill text-bg-secondary">
                    {info.rules.length}
                </span>
            )
        } else if (tab === 'tags' && info.tags) {
            badge = (
                <span className="badge rounded-pill text-bg-secondary">
                    {info.tags.length}
                </span>
            )
        } else if (tab === 'skiplists' && info.skiplists) {
            badge = (
                <span className="badge rounded-pill text-bg-secondary">
                    {info.skiplists.length}
                </span>
            )
        }

        const classes = classNames({
            'nav-item': true
        })

        const anchorClasses = classNames({
            'nav-link': true,
            active: activeTab === tab
        })

        return (
            <li
                className={classes}
                key={tab}
                role="presentation"
                data-bs-toggle="general"
            >
                <a
                    className={anchorClasses}
                    href="#"
                    onClick={e => {
                        onChangeTab(tab, e)
                    }}
                >
                    {label} {badge}
                </a>
            </li>
        )
    })

    return <ul className="nav nav-tabs card-header-tabs">{tabList}</ul>
}

const SourceDupeHelpText = ({ info }: { info: SiteSection }) => {
    if (!!info.dupeRules['source.priority']) {
        return <></>
    }

    const dupeParts = (info.dupeRules['source.priority'] ?? '').split(',')
    if (dupeParts.length >= 2) {
        return (
            <>
                {`Firstly ${dupeParts[0]} is allowed, followed by ${dupeParts
                    .slice(1)
                    .join(', ')}`}
            </>
        )
    }

    return <></>
}

const RangeDupeHelpText = ({ info }: { info: SiteSection }) => {
    if (!!info.dupeRules['range.priority']) {
        return <></>
    }

    const dupeParts = (info.dupeRules['range.priority'] ?? '').split(',')
    if (dupeParts.length >= 2) {
        return (
            <>
                {`Firstly ${dupeParts[0]} is allowed, followed by ${dupeParts
                    .slice(1)
                    .join(', ')}`}
            </>
        )
    }

    return <></>
}

export const SiteEditSection = ({
    info,
    onChange,
    onAddTag,
    onRemoveTag,
    onChangeTag,
    onAddSkiplist,
    onRemoveSkiplist,
    onChangeSectionRules,
    onChangeSectionDupeRules,
    sectionNames,
    onRename
}: SiteEditSectionProps) => {
    const [tags, setTags] = useState<string[]>([])
    const [skiplists, setSkiplists] = useState<Lists>({})
    const [activeTab, setActiveTab] = useState<string>('general')
    const [editingTag, setEditingTag] = useState<string>()

    useEffect(() => {
        const fetchSkiplistsAndTags = async () => {
            const settingsResponse =
                await getJSON<SettingsModelType>('/settings/list')
            setTags(Object.keys(settingsResponse.tag_options).sort())

            const skiplistsResponse = await getJSON<Lists>('/skiplist/list')
            setSkiplists(skiplistsResponse)
        }
        fetchSkiplistsAndTags()
    }, [])

    const { name } = info
    useEffect(() => {
        setEditingTag(undefined)
    }, [name]) // reset editing tag if we get a new section

    const iOnChangeTab = (tab: string, event: React.FormEvent) => {
        setActiveTab(tab)
        event.preventDefault()
    }

    const iOnChooseTagToEdit = (tag: string, event: React.FormEvent) => {
        setEditingTag(tag)
    }

    const iOnToggleTag = (tag: string, checked: boolean) => {
        if (checked === false) {
            onRemoveTag(tag)
        } else {
            onAddTag(tag)
        }
        setEditingTag(undefined)
    }

    const iOnChangeTrigger = (event: React.FormEvent<HTMLInputElement>) => {
        if (editingTag) {
            onChangeTag(editingTag, {
                trigger: event.currentTarget.value
            })
        }
    }

    const iOnChangeTagRule = (rules: string) => {
        if (editingTag) {
            onChangeTag(editingTag, { rules: rules })
        }
    }

    const iOnToggleSkiplist = (skiplist: string, checked: boolean) => {
        if (checked === false) {
            onRemoveSkiplist(skiplist)
        } else {
            onAddSkiplist(skiplist)
        }
    }

    const startRenameSection = () => {
        const newSectionName = prompt('Choose the new section name')
        if (newSectionName && newSectionName.length > 0) {
            onRename(newSectionName)
        }
    }

    const tagList = (
        <TagList
            tags={tags}
            info={info}
            onToggleTag={iOnToggleTag}
            onChooseTagToEdit={iOnChooseTagToEdit}
        />
    )

    const skiplistList = (
        <Skiplists info={info} lists={skiplists} onToggle={iOnToggleSkiplist} />
    )

    let triggerEditor
    if (editingTag) {
        const tagIndex = _.findIndex(info.tags, ['tag', editingTag])
        if (tagIndex >= 0) {
            const trigger = info.tags[tagIndex].trigger
            triggerEditor = (
                <TriggerEditor
                    tag={editingTag}
                    value={trigger}
                    onChange={iOnChangeTrigger}
                />
            )
        }
    }

    let tagRuleEditor
    if (editingTag) {
        const tagIndex = _.findIndex(info.tags, ['tag', editingTag])
        if (tagIndex >= 0) {
            var rules = info.tags[tagIndex].rules
            let rulesValue = ''
            if (rules?.length) {
                rulesValue = rules.join('\n')
            }
            tagRuleEditor = (
                <div style={{ marginTop: '1em' }}>
                    <label>
                        Rules for tag <code>{editingTag}</code>:
                    </label>
                    <div className="form-group">
                        <RulesEditor
                            defaultValue={rulesValue}
                            onChange={iOnChangeTagRule}
                            keyRef={`${info.name}-${editingTag}-${activeTab}-tagEditor`}
                            autocomplete={true}
                        />
                    </div>
                </div>
            )
        }
    }

    const tabClasses = getTabClasses(tabs, activeTab)
    const tabList = (
        <TabList
            info={info}
            tabs={tabs}
            activeTab={activeTab}
            onChangeTab={iOnChangeTab}
        />
    )

    let rulesString = ''
    if (info.rules) {
        rulesString = info.rules.join('\n')
    }

    const sourceDupeHelpText = <SourceDupeHelpText info={info} />
    const rangeDupeHelpText = <RangeDupeHelpText info={info} />

    return (
        <div>
            <div className="card card-tag">
                <div className="card-header" id="section-tabs">
                    {tabList}
                </div>
                <div className="card-body">
                    <div id="general" className={tabClasses['general']}>
                        <div className="row g-3 align-items-center">
                            <div className="col-auto">
                                <label
                                    htmlFor="exampleInputName2"
                                    className="col-form-label"
                                >
                                    Pretime (min.)
                                </label>
                            </div>
                            <div className="col-auto">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="exampleInputName2"
                                    placeholder="300"
                                    size={6}
                                    onChange={e => {
                                        onChange(info.name, 'pretime', e)
                                    }}
                                    value={info.pretime ?? ''}
                                />
                            </div>
                            <div className="col-auto">
                                <label
                                    htmlFor="exampleInputEmail2"
                                    className="col-form-label"
                                >
                                    BNC
                                </label>
                            </div>
                            <div className="col-auto">
                                <input
                                    type="type"
                                    className="form-control"
                                    id="exampleInputEmail2"
                                    placeholder=""
                                    onChange={e => {
                                        onChange(info.name, 'bnc', e)
                                    }}
                                    value={info.bnc ?? ''}
                                />
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col-12">
                                <div className="form-check" key={info.name}>
                                    <input
                                        type="checkbox"
                                        defaultChecked={
                                            info['downloadOnly'] === true
                                        }
                                        onClick={e => {
                                            onChange(
                                                info.name,
                                                'downloadOnly',
                                                e
                                            )
                                        }}
                                        className="form-check-input"
                                    />
                                    <label className="form-check-label">
                                        Download only
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tags" className={tabClasses['tags']}>
                        <div className="">
                            <label htmlFor="exampleInputName2">
                                Choose tags:
                            </label>
                            <div className="row">{tagList}</div>
                            {triggerEditor && <hr />}
                            {triggerEditor}
                            {tagRuleEditor}
                        </div>
                    </div>
                    <div id="rules" className={tabClasses['rules']}>
                        <div className="">
                            <label
                                htmlFor="exampleInputName2"
                                style={{ display: 'block' }}
                            >
                                Rules:
                            </label>

                            <RulesEditor
                                key={`${info.name}-${editingTag}-${activeTab}-sectionEditor`}
                                keyRef={`${info.name}-${editingTag}-${activeTab}-sectionEditor`}
                                defaultValue={rulesString}
                                autocomplete={true}
                                onChange={onChangeSectionRules}
                            />
                        </div>
                    </div>
                    <div id="dupes" className={tabClasses['dupes']}>
                        <div className="">
                            <div className="alert alert-warning">
                                Only use on TV sections
                            </div>
                            <p>
                                <strong>Source</strong>
                            </p>
                            <div className="row mb-3">
                                <div className="col-12">
                                    <div className="form-check" key={info.name}>
                                        <input
                                            type="checkbox"
                                            defaultChecked={
                                                info.dupeRules[
                                                    'source.firstWins'
                                                ] === true
                                            }
                                            onClick={e => {
                                                onChangeSectionDupeRules(
                                                    'source.firstWins',
                                                    e
                                                )
                                            }}
                                            className="form-check-input"
                                        />
                                        <label className="form-check-label">
                                            First on site wins?
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-12">
                                    <label htmlFor="exampleInputEmail2">
                                        Priority:
                                    </label>
                                    <input
                                        type="type"
                                        className="form-control"
                                        id="exampleInputEmail2"
                                        placeholder="web,webrip,hdtv"
                                        onChange={e => {
                                            onChangeSectionDupeRules(
                                                'source.priority',
                                                e
                                            )
                                        }}
                                        value={
                                            info.dupeRules['source.priority'] ??
                                            ''
                                        }
                                    />
                                    {info.dupeRules['source.priority'] && (
                                        <span className="help-block">
                                            {sourceDupeHelpText}
                                        </span>
                                    )}
                                </div>
                            </div>
                            <p>
                                <strong>Range</strong>
                            </p>
                            <div className="row mb-3">
                                <div className="col-12">
                                    <div className="form-check" key={info.name}>
                                        <input
                                            type="checkbox"
                                            defaultChecked={
                                                info.dupeRules[
                                                    'range.firstWins'
                                                ] === true
                                            }
                                            onClick={e => {
                                                onChangeSectionDupeRules(
                                                    'range.firstWins',
                                                    e
                                                )
                                            }}
                                            className="form-check-input"
                                        />
                                        <label className="form-check-label">
                                            First on site wins?
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-12">
                                    <label htmlFor="exampleInputEmail2">
                                        Priority:
                                    </label>
                                    <input
                                        type="type"
                                        className="form-control"
                                        id="exampleInputEmail2"
                                        placeholder="dv,hdr"
                                        onChange={e => {
                                            onChangeSectionDupeRules(
                                                'range.priority',
                                                e
                                            )
                                        }}
                                        value={
                                            info.dupeRules['range.priority'] ??
                                            ''
                                        }
                                    />
                                    {info.dupeRules['range.priority'] && (
                                        <span className="help-block">
                                            {rangeDupeHelpText}
                                        </span>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="skiplists" className={tabClasses['skiplists']}>
                        <div className="">
                            <label htmlFor="">Choose skiplists:</label>
                            <div className="row">{skiplistList}</div>
                        </div>
                    </div>
                    <div id="tools" className={tabClasses['tools']}>
                        <div className="alert alert-warning">
                            None of this is yet implemented
                        </div>
                        <hr />
                        <div className="row g-3 align-items-center">
                            <div className="col-auto">
                                <label
                                    htmlFor="exampleInputName2"
                                    className="col-form-label"
                                >
                                    Copy config from:
                                </label>
                            </div>
                            <div className="col-auto">
                                <select className="form-control">
                                    <option>-- Choose section --</option>
                                    {sectionNames.sort().map(name => (
                                        <option key={`section-copy-${name}`}>
                                            {name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className="col-auto">
                                <button className="btn btn-primary">
                                    Copy
                                </button>
                            </div>
                        </div>
                        <hr />
                        <div>
                            <button
                                className="btn btn-warning"
                                onClick={() => startRenameSection()}
                            >
                                Rename section
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
