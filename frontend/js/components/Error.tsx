import React from 'react'
import { Alert } from 'react-bootstrap'

interface ErrorProps {
    msg: string
}

export default ({ msg }: ErrorProps) => <Alert variant="danger">{msg}</Alert>
