import React, { ReactNode } from 'react'

interface TitleProps {
    title: string | ReactNode
    description?: string
    rightChildren?: ReactNode
}

const Title = ({ title, description, rightChildren }: TitleProps) => (
    <div className="hbd">
        <h3>{title}</h3>
        {rightChildren && <div className="controls">{rightChildren}</div>}
        {description && <p className="description">{description}</p>}
    </div>
)

export default Title
