import React from 'react'

interface SettingsEditorFormRowProps {
    label: string
    children: React.ReactNode
}

export default ({ label, children }: SettingsEditorFormRowProps) => (
    <div className="row mb-3">
        <label className="col-sm-3 col-form-label pt-0">{label}</label>
        <div className="col-sm-9">{children}</div>
    </div>
)
