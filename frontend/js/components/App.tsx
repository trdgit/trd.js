import React from 'react'
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Navigate
} from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import AutoRules from './AutoRules'
import Nav from './Nav'
import PrebotsEdit from './PrebotsEdit'
import SettingsEditor from './SettingsEditor'
import Simulator from './Simulator'
import SiteEdit from './SiteEdit'
import SkiplistEdit from './SkiplistEdit'
import RaceList from './race/RaceList'
import RaceLogView from './race/RaceLogView'
import SiteAdd from './site/SiteAdd'
import SiteList from './site/SiteList'
import CBFTPRaw from './tools/CBFTPRaw'
import CBFTPTags from './tools/CBFTPTags'
import Cleanup from './tools/Cleanup'
import DataImmutable from './tools/DataImmutable'
import ToolsTagFinder from './tools/TagFinder'
import TagOverview from './tools/TagOverview'
import ToolsList from './tools/ToolsList'
import Dashboard from './Dashboard'

export default () => {
    return (
        <Router>
            <Nav />
            <main className="flex-shrink-0">
                <div className="container-fluid">
                    <Routes>
                        <Route path="/site/:name/edit" element={<SiteEdit />} />
                        <Route path="/prebots" element={<PrebotsEdit />} />
                        <Route
                            path="/tools/tag_finder"
                            element={<ToolsTagFinder />}
                        />
                        <Route path="/tools/cleanup" element={<Cleanup />} />
                        <Route path="/tools/simulate" element={<Simulator />} />
                        <Route path="/tools/cbftp_raw" element={<CBFTPRaw />} />
                        <Route
                            path="/tools/cbftp_tags"
                            element={<CBFTPTags />}
                        />
                        <Route
                            path="/tools/tag_overview"
                            element={<TagOverview />}
                        />
                        <Route
                            path="/tools/data_immutable"
                            element={<DataImmutable />}
                        />
                        <Route path="/tools" element={<ToolsList />} />
                        <Route path="/skiplists" element={<SkiplistEdit />} />
                        <Route path="/site/list" element={<SiteList />} />
                        <Route path="/site/add" element={<SiteAdd />} />
                        <Route path="/settings" element={<SettingsEditor />} />
                        <Route path="/race/:id/log" element={<RaceLogView />} />
                        <Route path="/race/list" element={<RaceList />} />
                        <Route path="/autorules" element={<AutoRules />} />
                        {/* <Route
                        path="/"
                        element={<Navigate to="/race/list" replace />}
                    /> */}
                        <Route path="/" element={<Dashboard />} />
                    </Routes>
                </div>
            </main>
            <ToastContainer style={{ width: 'auto' }} />
        </Router>
    )
}
