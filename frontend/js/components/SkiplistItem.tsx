import React from 'react'

interface SkiplistItemsProps {
    name: string
    onChange: Function
    type: SkiplistItemType
    items?: string[]
    regex?: string
}

export enum SkiplistItemType {
    ITEMS = 'items',
    REGEX = 'regex',
}

const valueMapper = (type: SkiplistItemType, value: string) =>
    type === SkiplistItemType.ITEMS ? value.split('\n') : value

const SkiplistItems = ({
    name,
    items,
    regex,
    type = SkiplistItemType.ITEMS,
    onChange,
}: SkiplistItemsProps) => {
    const rows = items ? items.join('\n') : undefined

    const styles = {
        width: '100%',
    }
    return (
        <textarea
            className="form-control"
            style={styles}
            rows={10}
            defaultValue={rows ?? regex}
            onChange={e => {
                onChange(type, name, valueMapper(type, e.target.value))
            }}
        ></textarea>
    )
}

export default SkiplistItems
