import React from 'react'
import { Link, useParams } from 'react-router-dom'

import useRace from '../../hooks/useRace'
import Error from '../Error'
import Loading from '../Loading'
import RaceLog from '../RaceLog'

export default () => {
    const params = useParams()

    if (!params.id) {
        return
    }

    const { race, loadingRace, error } = useRace(params.id)

    if (error) {
        return <Error msg="Race not found" />
    }

    if (loadingRace) {
        return <Loading />
    }

    return (
        <>
            <div className="hbd">
                <h3>Log for race #{params.id}</h3>
            </div>
            <p className="alert alert-warning">
                <Link
                    to={`/tools/simulate?tag=${race.tag}&rlsname=${race.rlsname}&use_cache=0`}
                >
                    Simulate with a cache refresh
                </Link>
            </p>
            <RaceLog race={race} />
        </>
    )
}
