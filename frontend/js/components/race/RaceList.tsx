import { Paging, RaceListResponse } from '@trd/shared/src/types'
import classNames from 'classnames'
import React from 'react'
import { Alert } from 'react-bootstrap'
import { Link, useNavigate, useLocation } from 'react-router-dom'

import { postJSON } from '../../helpers/api'
import { formatDistance } from '../../helpers/date'
import useRaces from '../../hooks/useRaces'
import Loading from '../Loading'
import Pagination from '../Pagination'
import Title from '../Title'

const timeAgo = formatDistance

const fullDates = window._settings.full_dates_on_race_list ?? false

const joinReducer = (accu: React.JSX.Element[], elem: React.JSX.Element) => {
    return accu.length === 0 ? [elem] : [...accu, <span>{', '}</span>, elem]
}

const roundDuration = (input: number) => Math.round(input * 100) / 100

const RaceTable = ({
    races,
    dataSources,
    paging,
    tags
}: {
    races: RaceListResponse[] | undefined
    tags: string[]
    paging: Paging | undefined
    dataSources: Record<string, number>
}) => {
    const navigate = useNavigate()
    const params = useLocation()
    const p = new URLSearchParams(params.search)

    let prev
    if (paging?.prev) {
        p.set('cursor', paging.prev)
        prev = `/race/list?${p.toString()}`
    }
    let next
    if (paging?.next) {
        p.set('cursor', paging.next)
        next = `/race/list?${p.toString()}`
    }

    const pagination = <Pagination prev={prev} next={next} />

    const rightChildren = (
        <div className="d-flex align-items-center">
            <select
                className="form-select"
                style={{ marginRight: '1rem' }}
                onChange={e => {
                    navigate(`/race/list?tag=${e.currentTarget.value}`)
                }}
                value={p.get('tag') ?? ''}
            >
                <option value="">Filter by tag:</option>
                {tags.map(tag => (
                    <option key={`tf-${tag}`} value={tag}>
                        {tag}
                    </option>
                ))}
            </select>
            <input
                type="text"
                className="form-control"
                style={{ marginRight: '1rem' }}
                placeholder="Jump to rlsname..."
                onKeyDown={async e => {
                    if (e.keyCode === 13) {
                        try {
                            const res = await postJSON<{ id: number }>(
                                '/race/lookup',
                                {
                                    rlsname: e.currentTarget.value
                                }
                            )

                            if (res?.id) {
                                navigate(`/race/${res.id}/log`)
                                return
                            }
                        } catch (e) {
                            alert('Race not found')
                        }
                    }
                }}
            />
            {pagination}
        </div>
    )

    return (
        <>
            <Title title="Race List" rightChildren={rightChildren} />
            {races ? (
                <>
                    <table
                        id="race-list"
                        className="table table-bordered table-striped"
                    >
                        <thead>
                            <tr>
                                <th width="4%">Tag</th>
                                <th width="1%"></th>
                                <th width="1%"></th>
                                <th width="4%">Options</th>
                                <th width="70%">Releasename / Chain</th>
                                <th width="15%%">When</th>
                                <th width="5%" style={{ textAlign: 'right' }}>
                                    Timing
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {races.map(race => (
                                <RaceRow
                                    key={`rr-${race.id}`}
                                    race={race}
                                    dataSources={dataSources}
                                />
                            ))}
                        </tbody>
                    </table>
                    {pagination}
                </>
            ) : (
                <p>No races</p>
            )}
        </>
    )
}

const RaceRowTag = ({ chain, tag }) => (
    <Link
        to={`/race/list?tag=${tag}`}
        className={`tag badge text-bg-secondary bg-${chain?.length > 0 ? 'success' : 'inactive'}`}
    >
        {tag}
    </Link>
)

const RaceRowCache = ({ tag, log, dataSources }) => {
    const hasCache =
        log.data.dataProviderData.tvmaze?.rawData?.url?.length ||
        log.data.dataProviderData.imdb?.rawData?.url?.length
    if (hasCache) {
        return (
            <span
                className="badge bg-success"
                style={{ display: 'block' }}
                aria-label="Used cache"
            >
                <i className="icon-check"></i>
            </span>
        )
    }

    if (dataSources[tag]) {
        return (
            <span className="badge bg-danger" style={{ display: 'block' }}>
                <i className="icon-times"></i>
            </span>
        )
    }

    return (
        <span className="badge bg-secondary" style={{ display: 'block' }}>
            <i className="icon-times"></i>
        </span>
    )
}

const RaceRowChain = ({ chain, chainComplete }) => {
    chain = chain ? chain.split(',') : []
    chainComplete = chainComplete ? chainComplete.split(',') : []

    if (!chain.length) {
        return <></>
    }

    const styles = {
        color: 'inherit',
        textDecoration: 'none'
    }

    const sortedChain = chain.sort()
    const formattedList = sortedChain
        .map(site => {
            if (chainComplete.includes(site)) {
                return (
                    <Link style={styles} to={`/site/${site}/edit`}>
                        <u>{site}</u>
                    </Link>
                )
            }
            return (
                <Link style={styles} to={`/site/${site}/edit`}>
                    {site}
                </Link>
            )
        })
        .reduce(joinReducer, [])

    // console.log(formattedList)

    return formattedList.length ? (
        <span style={{ fontSize: '.8em' }}>{formattedList}</span>
    ) : (
        <></>
    )
}

const RaceRowCreated = ({ created }) => {
    return fullDates ? created : timeAgo(new Date(created), new Date())
}

const RaceRow = ({
    race,
    dataSources
}: {
    race: RaceListResponse
    dataSources: Record<string, number>
}) => {
    if (!race.log) {
        return (
            <tr className="table-danger">
                <td colSpan={7}>
                    Something went wrong. Run this query:{' '}
                    <code>
                        DELETE FROM race WHERE rlsname = '{race.rlsname}'
                    </code>
                </td>
            </tr>
        )
    }

    const rowClasses = classNames({
        'race-empty': !race.chain || race.chain?.split(',').length < 2
    })

    const indicatorClasses = classNames({
        'race-indicator': true,
        'bg-success': race.started == 1,
        'bg-danger': race.log?.catastrophes.length > 0
    })

    return (
        <tr className={rowClasses} key={`racerow-${race.rlsname}`}>
            <td style={{ textAlign: 'center', position: 'relative' }}>
                <span className={indicatorClasses}></span>
                <RaceRowTag tag={race.bookmark} chain={race.chain} />
            </td>
            <td>
                <RaceRowCache
                    tag={race.bookmark}
                    log={race.log}
                    dataSources={dataSources}
                    aria-label="Data provider response"
                />
            </td>
            <td>
                <span
                    className="badge text-bg-secondary"
                    title="Dupe engine sources"
                >
                    {race.log.dupeEngineSources?.length >= 0 ? (
                        <>({race.log.dupeEngineSources.length})</>
                    ) : (
                        <>(0)</>
                    )}
                </span>
            </td>
            <td>
                <Link
                    className="btn btn-outline-secondary btn-xs hint--bottom"
                    to={`/tools/simulate?tag=${race.bookmark}&rlsname=${race.rlsname}&use_cache=1`}
                    aria-label="Simulate"
                >
                    <i className="icon-exchange"></i>
                </Link>
            </td>

            <td className="race-rlsname">
                <Link
                    to={`/race/${race.id}/log`}
                    style={{ color: 'inherit', textDecoration: 'none' }}
                >
                    <strong>{race.rlsname}</strong>
                </Link>

                <br />
                <RaceRowChain
                    chain={race.chain}
                    chainComplete={race.chain_complete}
                />
            </td>
            <td>
                <a title={race.created}>
                    <RaceRowCreated created={race.created} />
                </a>
            </td>

            <td style={{ textAlign: 'right' }}>
                <span
                    className="timing badge text-bg-secondary"
                    title="Race evaluation time"
                >
                    {roundDuration(race.log.duration)}
                </span>
                <span
                    className="timing badge text-bg-secondary"
                    title="Data lookup time"
                >
                    {roundDuration(race.log.dataLookupDuration)}
                </span>
            </td>
        </tr>
    )
}

export default () => {
    const params = useLocation()
    const p = new URLSearchParams(params.search)
    const { paging, races, dataSources, loadingRaces, tags, error } = useRaces(
        p.get('cursor') ?? '',
        p.get('tag') ?? ''
    )

    if (error) {
        return <Alert variant="danger">{error.message}</Alert>
    }

    if (!loadingRaces && !races?.length) {
        return <Alert variant="warning">No races yet!</Alert>
    }

    return (
        <div className="container-fluid">
            {loadingRaces ? (
                <Loading label="Loading races" />
            ) : (
                <RaceTable
                    races={races}
                    tags={tags}
                    paging={paging}
                    dataSources={dataSources}
                />
            )}
        </div>
    )
}
