import { Prebot, PrebotsData } from '@trd/shared/src/types'
import axios from 'axios'
import _ from 'lodash'
import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'

import { getJSON, postJSON } from '../helpers/api'
import useFetchDebounce, { SaveFunction } from '../hooks/useFetchDebounce'
import Loading from './Loading'
import PrebotsEditForm from './PrebotsEditForm'
import Title from './Title'

const load = async () => {
    return getJSON<PrebotsData>(`/prebots/list`)
}
const save: SaveFunction = async prebots => {
    try {
        await postJSON(`/prebots/save`, prebots)
        toast.success('Saved!', {
            toastId: 'saved',
        })
    } catch (e) {
        if (axios.isAxiosError(e)) {
            // @ts-ignore
            toast.error(e.response?.data.error)
        }
    }
}

export default () => {
    const [changed, setChanged] = useState<boolean>(false)
    const {
        loading,
        data: prebots,
        setData: setPrebots,
    } = useFetchDebounce<PrebotsData>(load, save, undefined, changed, 2500)

    const addBot = () => {
        if (!prebots) {
            return
        }

        const channel = prompt('Enter a regex to match the channel')
        const bot = prompt('Enter a regex to match the bot')
        const stringMatch = prompt('Enter a regex to match the rlsname')

        if (!channel?.length || !bot?.length || !stringMatch?.length) {
            return alert("You didn't fill in all information")
        }

        const exists = prebots?.some(
            prebot => prebot.channel === channel && prebot.bot === bot
        )

        if (exists) {
            return alert('Looks like this bot is already added')
        }

        var newObj = [...prebots]
        newObj.push({
            channel,
            bot,
            string_match: stringMatch,
        })
        setPrebots(newObj)
    }

    const onChange = (idx: number, field: string, value: string) => {
        if (!changed) {
            setChanged(true)
        }

        if (!prebots) {
            return
        }

        const newState = [...prebots]
        if (newState[idx]) {
            const newPrebot = {
                ...newState[idx],
                [field]: value,
            }
            newState[idx] = newPrebot
            setPrebots(newState)
        }
    }

    const onDelete = (idx: number) => {
        if (!changed) {
            setChanged(true)
        }

        if (!prebots) {
            return
        }

        const newPrebots = [...prebots]
        newPrebots.splice(idx, 1)
        setPrebots([...newPrebots])
    }

    if (loading) {
        return <Loading />
    }

    const list = (prebots ?? []).map((prebot, idx) => (
        <div className="row" key={`prebot${idx}`}>
            <PrebotsEditForm
                index={idx}
                data={prebot}
                onChange={onChange}
                onDelete={onDelete}
            />
        </div>
    ))

    return (
        <>
            <Title
                title="Prebots"
                rightChildren={
                    <>
                        {' '}
                        <a
                            data-trd-help="prebots"
                            className="btn btn-outline-secondary"
                        >
                            <i className="icon-question-circle"></i> Help
                        </a>{' '}
                        <a className="btn btn-primary" onClick={addBot}>
                            + Add prebot
                        </a>
                    </>
                }
            />
            <br />
            <div className="alert alert-warning">
                Make sure your regexes are valid using{' '}
                <a
                    target="_blank"
                    rel="noreferrer nofollow"
                    href="https://regex101.com"
                >
                    regex101.com
                </a>
            </div>
            {list}
        </>
    )
}
