import React from 'react'
import Button from 'react-bootstrap/Button'
import Spinner from 'react-bootstrap/Spinner'

interface LoadingProps {
    label?: string
}

const Loading = ({ label = 'Loading' }: LoadingProps) => (
    <Button variant="primary" disabled>
        <Spinner
            as="span"
            animation="border"
            size="sm"
            role="status"
            aria-hidden="true"
        />
        {` ${label}...`}
    </Button>
)

export default Loading
