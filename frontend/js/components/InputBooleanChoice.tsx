import React, { useState } from 'react'

import SettingsEditorFormRow from './SettingsEditorFormRow'

interface InputBooleanChoiceProps {
    name: string
    label: string
    defaultValue: boolean
    onChange: ChangeEvent
    description?: string
}

type ChangeEvent = (key: string, event: React.FormEvent) => void

export default ({
    name,
    label,
    defaultValue,
    onChange
}: InputBooleanChoiceProps) => {
    const [selectedOption, setSelectedOption] = useState<string>(
        defaultValue ? 'true' : 'false'
    )

    const handleOptionChange = (e: React.FormEvent<HTMLInputElement>) => {
        var val = e.currentTarget.value
        setSelectedOption(val)
        onChange(name, e)
    }

    return (
        <SettingsEditorFormRow label={label}>
            <div className="form-check form-check-inline">
                <input
                    name={name}
                    id={`booleanChoiceTrue${name}`}
                    type="radio"
                    value="true"
                    checked={selectedOption === 'true'}
                    onChange={handleOptionChange}
                    className="form-check-input"
                />
                <label
                    className="check-label"
                    htmlFor={`booleanChoiceTrue${name}`}
                >
                    {' '}
                    True
                </label>
            </div>
            <div className="form-check form-check-inline">
                <input
                    name={name}
                    id={`booleanChoiceFalse${name}`}
                    type="radio"
                    value="false"
                    checked={selectedOption === 'false'}
                    onChange={handleOptionChange}
                    className="form-check-input"
                />
                <label
                    className="form-check-label"
                    htmlFor={`booleanChoiceFalse${name}`}
                >
                    {' '}
                    False
                </label>
            </div>
        </SettingsEditorFormRow>
    )
}
