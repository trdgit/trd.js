import React, { useEffect, useState } from 'react'

import { getJSON } from '../../helpers/api'
import Loading from '../Loading'
import Title from '../Title'

export default () => {
    const [tags, setTags] = useState({})
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function fetchData() {
            setLoading(true)
            const response = await getJSON('/tools/tag_overview')
            setTags(response)
            setLoading(false)
        }
        fetchData()
    }, [])

    if (loading) {
        return <Loading />
    }

    return (
        <>
            <Title title="Tag overview" />
            <table className="table table-striped table-bordered">
                <tbody>
                    {Object.entries(tags).map(([tagName, sites]) => (
                        <tr>
                            <td width="10%">
                                <code>{tagName}</code>
                            </td>
                            <td>
                                {sites.map(s => (
                                    <>
                                        <span className="badge badge-default">
                                            {s}
                                        </span>{' '}
                                    </>
                                ))}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}
