import React, { useState } from 'react'
import * as yup from 'yup'

import { postForm } from '../../helpers/api'
import Loading from '../Loading'
import RaceLog from '../RaceLog'
import Title from '../Title'
import Checkbox from '../form/Checkbox'
import Form from '../form/Form'
import Input from '../form/Input'

const schema = yup.object().shape({
    rlsname: yup.string().required()
})

const FinderForm = ({
    onSubmit
}: {
    onSubmit: React.FormEventHandler<HTMLFormElement>
}) => {
    return (
        <Form
            onSubmit={onSubmit}
            className="needs-validation"
            novalidate={true}
            schema={schema}
        >
            <Input label="Rlsname" name="rlsname" />
            <input type="submit" className="btn btn-primary" />
        </Form>
    )
}

const TagResultTable = ({ title, rows }) => {
    const mappedRows = Object.entries(rows).filter(
        ([key, value]) => value.length > 0
    )
    return (
        <div>
            <h3>{title}</h3>
            <table className="table table-striped table-bordered">
                <tbody>
                    {mappedRows.map(([key, value]) => (
                        <tr>
                            <td>{key}</td>
                            <td>
                                <code>
                                    {value.map(val => (
                                        <div>{val}</div>
                                    ))}
                                </code>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

const TagsMatched = ({ tags }) => (
    <div>
        <h3>Tags matched</h3>
        <table className="table table-striped table-bordered">
            <tbody>
                {tags.map(tag => (
                    <tr>
                        <td>{tag}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    </div>
)

const TagResult = ({ response }) => {
    console.log('result', response.results)
    return (
        <div>
            {response.tags?.length && <TagsMatched tags={response.tags} />}
            {Object.keys(response.results.requires).length > 0 && (
                <TagResultTable
                    title="Requires failed matches"
                    rows={response.results.requires}
                />
            )}
            {Object.keys(response.results.skiplist).length > 0 && (
                <TagResultTable
                    title="Skiplist matches"
                    rows={response.results.skiplist}
                />
            )}
        </div>
    )
}

export default () => {
    const [error, setError] = useState()
    const [tagResult, setTagResult] = useState()

    const onSubmit = async data => {
        setError(undefined)
        setTagResult(undefined)

        const payload = new FormData()
        payload.append('rlsname', data.rlsname)
        const response = await postForm(`/tools/tag_finder`, payload)

        if (response.error) {
            setError(response.error)
        } else if (response.data) {
            setTagResult(response.data)
        }
    }

    return (
        <div>
            <FinderForm onSubmit={onSubmit} />
            {tagResult && (
                <div>
                    <br />
                    <br />
                    <TagResult response={tagResult} />
                </div>
            )}
            {error && <p>{error}</p>}
        </div>
    )
}
