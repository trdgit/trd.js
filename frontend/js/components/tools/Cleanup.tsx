import React, { useEffect, useState } from 'react'

import { getJSON, postJSON } from '../../helpers/api'
import Loading from '../Loading'
import Title from '../Title'

export default () => {
    const [result, setResult] = useState(undefined)

    const run = async () => {
        setResult(undefined)
        try {
            await postJSON('/tools/cleanup')
            setResult('Done')
        } catch (e) {
            setResult(e.message)
        }
    }

    return (
        <>
            <Title title="Cleanup" />
            <ul>
                <li>Deletes races that have gone wrong and has log as NULL</li>
            </ul>
            <p>
                <button className="btn btn-primary" onClick={() => run()}>
                    Run
                </button>
            </p>
            {result && <p>{result}</p>}
        </>
    )
}
