import React, { useEffect, useState } from 'react'

import { getJSON } from '../../helpers/api'
import Loading from '../Loading'
import Title from '../Title'

const joinReducerBr = (accu: React.JSX.Element[], elem: React.JSX.Element) => {
    return accu === null ? [elem] : [...accu, <br />, elem]
}

export default () => {
    const [tags, setTags] = useState({})
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function fetchData() {
            setLoading(true)
            const response = await getJSON('/tools/cbftp_tags')
            setTags(response)
            setLoading(false)
        }
        fetchData()
    }, [])

    if (loading) {
        return <Loading />
    }

    return (
        <>
            <Title title="CBFTP Missing tags" />
            {tags
                .map((row, index) => (
                    <div className="card">
                        <div className="card-header">Site: {row.site}</div>
                        <div className="card-body">
                            <table className="table table-bordered">
                                <tbody>
                                    {row.absent.map(bncRow => (
                                        <tr>
                                            <td width="1%">{bncRow.bnc}</td>
                                            <td key={bncRow.bnc}>
                                                {bncRow.sections
                                                    .map(x => x)
                                                    .join(', ')}
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                ))
                .reduce(joinReducerBr, null)}
        </>
    )
}
