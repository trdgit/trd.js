import React, { useEffect, useState } from 'react'

import { getJSON, postJSON } from '../../helpers/api'
import Loading from '../Loading'
import Title from '../Title'

const resetImmutableData = async k => {
    await postJSON('/tools/data_immutable', { k: k, action: 'reset' })
    window.location.reload()
}

export default () => {
    const [immutables, setImmutables] = useState({})
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        async function fetchData() {
            setLoading(true)
            const response = await getJSON('/tools/data_immutable')
            setImmutables(response)
            setLoading(false)
        }
        fetchData()
    }, [])

    if (loading) {
        return <Loading />
    }

    return (
        <>
            <Title title="Data Immutable" />
            <table className="table table-striped table-bordered">
                <thead className="table-light">
                    <tr>
                        <th>Key</th>
                        <th>Adjustments</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {immutables.map((row, index) => (
                        <tr key={row.k}>
                            <td width="30%">
                                <code>{row.k}</code>
                            </td>
                            <td>
                                {row.fixes.map(fieldRow => (
                                    <li key={row.k + fieldRow.field}>
                                        <strong>{fieldRow.field} </strong>
                                        <s>
                                            <code>{fieldRow.from}</code>
                                        </s>{' '}
                                        {' -> '}
                                        <code style={{ color: '#25a14e' }}>
                                            {fieldRow.to}
                                        </code>
                                    </li>
                                ))}
                            </td>
                            <td width="10%">
                                <a
                                    onClick={() => {
                                        resetImmutableData(row.k)
                                    }}
                                    className="btn btn-danger"
                                >
                                    Reset All
                                </a>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}
