import React from 'react'
import { Link } from 'react-router-dom'

interface ToolProps {
    title: string
    description: string
    path: string
}

const Tool = ({ title, description, path }: ToolProps) => (
    <div className="card">
        <div className="card-body">
            <h5 className="card-title">{title}</h5>
            <p className="card-text">{description}</p>
            <p className="align-self-end" style={{ marginTop: 'auto' }}>
                <Link className="btn btn-primary" to={path}>
                    Go &raquo;
                </Link>
            </p>
        </div>
    </div>
)

export default () => (
    <div className="container-lg">
        <div className="row row-cols-1 row-cols-md-2 g-4">
            <div className="col">
                <Tool
                    title="Simulator"
                    description="Simulate a race and get all debug info"
                    path="/tools/simulate"
                />
            </div>
            <div className="col">
                <Tool
                    title="Tag overview"
                    description="Get an overview of all tags in use, and which sites have them."
                    path="/tools/tag_overview"
                />
            </div>
            <div className="col">
                <Tool
                    title="Data immutable"
                    description={`Show all data lookups than have been "fixed".`}
                    path="/tools/data_immutable"
                />
            </div>
            <div className="col">
                <Tool
                    title="CBFTP Raw"
                    description={
                        'Send a raw command to a selection of sites and see the nerdy response.'
                    }
                    path="/tools/cbftp_raw"
                />
            </div>
            <div className="col">
                <Tool
                    title="CBFTP missing section finder"
                    description={
                        'Find sites where sections have not been added.'
                    }
                    path="/tools/cbftp_tags"
                />
            </div>
            <div className="col">
                <Tool
                    title="Tag finder"
                    description={
                        'Find out which tag will be used based off only rlsname'
                    }
                    path="/tools/tag_finder"
                />
            </div>
            <div className="col">
                <Tool
                    title="Cleanup"
                    description={
                        'Sometimes bad things happen. This tries to fix them.'
                    }
                    path="/tools/cleanup"
                />
            </div>
        </div>
    </div>
)
