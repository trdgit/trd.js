import React, { useEffect, useState, useCallback } from 'react'
import * as yup from 'yup'

import { postForm } from '../../helpers/api'
import Loading from '../Loading'
import Title from '../Title'
import Form from '../form/Form'
import Input from '../form/Input'

const joinReducerBr = (accu: React.JSX.Element[], elem: React.JSX.Element) => {
    return accu === null ? [elem] : [...accu, <br />, elem]
}

const schema = yup.object().shape({
    siteList: yup.string(),
    command: yup.string().required(),
    path: yup.string().required()
})

const CBFTPRawForm = ({
    onSubmit
}: {
    onSubmit: React.FormEventHandler<HTMLFormElement>
}) => {
    return (
        <Form
            onSubmit={onSubmit}
            className="needs-validation"
            novalidate={true}
            schema={schema}
        >
            <Input label="Site List" name="siteList" placeholder="e.g. A,B,C" />
            <Input
                label="Command"
                name="command"
                placeholder="e.g. site stat"
            />
            <Input label="Path" name="path" defaultValue="/" />

            <input type="submit" className="btn btn-primary" />
        </Form>
    )
}

const RawResults = ({ results }) => {
    let successes = null
    if (results.successes.length) {
        successes = (
            <div className="card bg-light">
                <div className="card-header">Successes</div>
                <div className="card-body">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <td className="5%">Site</td>
                                <td>Response</td>
                            </tr>
                        </thead>
                        <tbody>
                            {results.successes.map(row => (
                                <tr key={row.name}>
                                    <td>{row.name}</td>
                                    <td>
                                        {row.result
                                            .split('\r\n')
                                            .map(statRow => (
                                                <span>{statRow}</span>
                                            ))
                                            .reduce(joinReducerBr, null)}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

    let failures = null
    if (results.failures.length) {
        failures = (
            <div className="card bg-light">
                <div className="card-header">Failures</div>
                <div className="card-body">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <td className="5%">Site</td>
                                <td>Reason</td>
                            </tr>
                        </thead>
                        <tbody>
                            {results.failures.map(row => (
                                <tr>
                                    <td>{row.name}</td>
                                    <td>
                                        <code>{row.reason}</code>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

    return (
        <>
            <h3>Results</h3>
            {successes}
            {failures}
        </>
    )
}

export default () => {
    const [results, setResults] = useState(null)
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(false)

    const onSubmit = async data => {
        setError(null)
        setLoading(true)
        const payload = new FormData()
        payload.append('siteList', data.siteList)
        payload.append('command', data.command)
        payload.append('path', data.path)
        try {
            const response = await postForm(`/tools/cbftp_raw`, payload)
            if (response['successes']) {
                setResults(response)
            }
            setLoading(false)
        } catch (e) {
            let msg = 'Something went wrong'
            if (e.response.data?.error?.length) {
                msg = e.response.data.error
            }
            setError(msg)
            setLoading(false)
        }
    }

    if (loading) {
        return <Loading label="Retrieving results" />
    }

    const hasResults = results && Object.keys(results).length
    return (
        <>
            <Title title="CBFTP Raw" />
            {error && <div className="alert alert-danger">{error}</div>}
            {!hasResults && <CBFTPRawForm onSubmit={onSubmit} />}
            {hasResults && <RawResults results={results} />}
        </>
    )
}
