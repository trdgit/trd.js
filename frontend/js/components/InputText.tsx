import classNames from 'classnames'
import React from 'react'

import SettingsEditorFormRow from './SettingsEditorFormRow'

interface InputTextProps {
    name: string
    defaultValue: string
    label: string
    onChange: ChangeEvent
    classes?: string
    description?: string
}

type ChangeEvent = (
    name: string,
    event: React.FormEvent<HTMLInputElement>
) => void

export default ({
    name,
    label,
    defaultValue,
    classes,
    onChange
}: InputTextProps) => {
    const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
        onChange(name, e)
    }

    const classList = classNames('form-control', classes)

    return (
        <SettingsEditorFormRow label={label}>
            <input
                name={name}
                type="text"
                className={classList}
                defaultValue={defaultValue}
                onChange={handleChange}
            />
        </SettingsEditorFormRow>
    )
}
