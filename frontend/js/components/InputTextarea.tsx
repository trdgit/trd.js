import classNames from 'classnames'
import React from 'react'

import SettingsEditorFormRow from './SettingsEditorFormRow'

interface InputTextAreaProps {
    name: string
    label?: string
    defaultValue: string
    onChange: ChangeEvent
    classes?: string
    rows?: number
}

type ChangeEvent = (
    name: string,
    event: React.FormEvent<HTMLTextAreaElement>
) => void

export default ({
    name,
    label,
    defaultValue,
    onChange,
    classes,
    rows = 5,
}: InputTextAreaProps) => {
    const handleChange = (e: React.FormEvent<HTMLTextAreaElement>) => {
        onChange(name, e)
    }

    var classList = classNames('form-control', classes)

    if (label) {
        return (
            <SettingsEditorFormRow label={label}>
                <textarea
                    name={name}
                    className={classList}
                    defaultValue={defaultValue}
                    onChange={handleChange}
                    rows={rows}
                />
            </SettingsEditorFormRow>
        )
    }

    return (
        <div className="form-group">
            <textarea
                name={name}
                className={classList}
                defaultValue={defaultValue}
                onChange={handleChange}
                rows={rows}
            />
        </div>
    )
}
