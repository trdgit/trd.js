import { TagOption } from '@trd/shared/src/types'
import React from 'react'

import InputText from './InputText'
import InputTextarea from './InputTextarea'

type ChangeEvent = (key: string, event: React.FormEvent) => void

interface InputTagOptionsProps {
    name: string
    tag: string
    onChange: ChangeEvent
    info: TagOption
}

export default ({ tag, info, onChange }: InputTagOptionsProps) => {
    const onChangeSetting = (
        key: string,
        e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => onChange(`tag_options.${tag}.${key}`, e)

    let data_sources: string = ''
    if (info.data_sources) {
        data_sources = info.data_sources.sort().join(' ')
    }

    let allowed_groups: string = ''
    if (info.allowed_groups) {
        allowed_groups = info.allowed_groups.sort().join(' ')
    }

    let tag_requires: string = ''
    if (info.tag_requires) {
        tag_requires = info.tag_requires.sort().join('\n')
    }

    let tag_skiplist: string = ''
    if (info.tag_skiplist) {
        tag_skiplist = info.tag_skiplist.sort().join('\n')
    }

    return (
        <>
            <InputText
                name="data_sources"
                label="Data providers (space-separated)"
                defaultValue={data_sources}
                onChange={onChangeSetting}
            />
            <InputText
                name="allowed_groups"
                label="Allowed groups (space-separated"
                defaultValue={allowed_groups}
                onChange={onChangeSetting}
            />
            <InputTextarea
                name="tag_requires"
                label="Tag requires (one regex p/line)"
                defaultValue={tag_requires}
                onChange={onChangeSetting}
                rows={10}
                classes="code"
            />
            <InputTextarea
                name="tag_skiplist"
                label="Tag skiplist (one regex p/line)"
                defaultValue={tag_skiplist}
                onChange={onChangeSetting}
                rows={10}
                classes="code"
            />
        </>
    )
}

/*
export default class InputTagOption extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChange(changeEvent) {
        this.props.onChange(this.props.name, changeEvent);
    }

    onChangeSetting(key, event) {
      this.props.onChange("tag_options." + this.props.tag + "." + key, event);
    }

    render() {

        let data_sources = [];
        if(this.props.info.data_sources) {
          data_sources = this.props.info.data_sources
              .sort()
              .join(" ");
        }

        let allowed_groups = [];
        if(this.props.info.allowed_groups) {
          allowed_groups = this.props.info.allowed_groups
              .sort()
              .join(" ");
        }

        let tag_requires = [];
        if(this.props.info.tag_requires) {
          tag_requires = this.props.info.tag_requires
              .sort()
              .join("\n");
        }

        let tag_skiplist = [];
        if(this.props.info.tag_skiplist) {
          tag_skiplist = this.props.info.tag_skiplist
              .sort()
              .join("\n");
        }

        return (
            <div>
              <InputText
                  name="data_sources"
                  label="Data providers (space-separated)"
                  description="Bla bla"
                  defaultValue={data_sources}
                  onChange={this.onChangeSetting.bind(this)}
              />
            <InputText
                  name="allowed_groups"
                  label="Allowed groups (space-separated"
                  description="Bla bla"
                  defaultValue={allowed_groups}
                  onChange={this.onChangeSetting.bind(this)}
              />
              <InputTextarea
                  name="tag_requires"
                  label="Tag requires (one regex p/line)"
                  description="Bla bla"
                  defaultValue={tag_requires}
                  onChange={this.onChangeSetting.bind(this)}
                  rows={10}
                  classes="code"
              />
              <InputTextarea
                  name="tag_skiplist"
                  label="Tag skiplist (one regex p/line)"
                  description="Bla bla"
                  defaultValue={tag_skiplist}
                  onChange={this.onChangeSetting.bind(this)}
                  rows={10}
                  classes="code"
              />
            </div>
        );
    }
}
*/
