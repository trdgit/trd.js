import React from 'react'
import { useFormContext } from 'react-hook-form'

interface CheckboxProps {
    name: string
    label: string
    errors: Record<string, Error>
    checked?: boolean
}

interface Error {
    message: string
}

const Checkbox = ({ label, checked = false, name, ...rest }: CheckboxProps) => {
    const {
        register,
        formState: { errors }
    } = useFormContext()

    const fieldError =
        Object.keys(errors).length > 0
            ? errors[name]?.message?.toString()
            : undefined

    return (
        <div className="mb-3">
            <div className="form-check">
                <input
                    {...register(name)}
                    name={name}
                    type="checkbox"
                    className={`form-check-input ${
                        errors[name] ? 'is-invalid' : ''
                    }`}
                    defaultChecked={checked}
                    {...rest}
                />
                <label className="form-check-label">{label}</label>
                {fieldError && (
                    <div className="invalid-feedback">{fieldError}</div>
                )}
            </div>
        </div>
    )
}

export default Checkbox
