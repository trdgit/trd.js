import React from 'react'
import { FieldValues, useFormContext, UseFormRegister } from 'react-hook-form'

interface SelectProps {
    name: string
    label: string
    options: { key: string; value: string }[]
}

export function Select({ label, options, name, ...rest }: SelectProps) {
    const { register } = useFormContext()
    return (
        <div className="mb-3">
            <label className="form-label">{label}</label>
            <select {...register(name)} {...rest} className="form-select">
                {options.map(option => (
                    <option key={option.key} value={option.key}>
                        {option.value}
                    </option>
                ))}
            </select>
        </div>
    )
}
