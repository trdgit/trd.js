import React from 'react'
import { useFormContext } from 'react-hook-form'

interface InputProps {
    label: string
    name: string
    defaultValue?: string
    autocomplete?: boolean
    placeholder?: string
}

export default ({
    autocomplete = true,
    label,
    name,
    defaultValue,
    ...rest
}: InputProps) => {
    const {
        register,
        formState: { errors }
    } = useFormContext()

    const fieldError =
        Object.keys(errors).length > 0
            ? errors[name]?.message?.toString()
            : undefined

    return (
        <div className="mb-3">
            <label className="form-label">{label}</label>
            <input
                {...register(name)}
                className={`form-control ${errors[name] ? 'is-invalid' : ''}`}
                autoComplete={autocomplete ? 'on' : 'off'}
                defaultValue={defaultValue}
                {...rest}
            />
            {fieldError && <div className="invalid-feedback">{fieldError}</div>}
        </div>
    )
}
