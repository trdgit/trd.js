import { yupResolver } from '@hookform/resolvers/yup'
import React from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import Yup from 'yup'

export default function Form({
    schema,
    className,
    defaultValues,
    children,
    onSubmit,
    novalidate = false
}: {
    schema: Yup.AnyObjectSchema
    onSubmit: React.FormEventHandler<HTMLFormElement>
    className: string
    novalidate: boolean
    children: React.JSX.Element[]
    defaultValues?: any
}) {
    const methods = useForm({
        defaultValues,
        resolver: yupResolver(schema)
    })
    const {
        handleSubmit,
        formState: { errors }
    } = methods

    return (
        <FormProvider {...methods}>
            <form onSubmit={handleSubmit(onSubmit)} className={className}>
                {React.Children.map(children, child => {
                    return child.props.name
                        ? React.createElement(child.type, {
                              ...{
                                  ...child.props,
                                  key: child.props.name
                              }
                          })
                        : child
                })}
            </form>
        </FormProvider>
    )
}
