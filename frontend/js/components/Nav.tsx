import React from 'react'
import Container from 'react-bootstrap/Container'
import BSNav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'

import { Link } from 'react-router-dom'

import logo from '../../img/favicon.png'

const Nav = () => (
    <Navbar bg="body-tertiary" sticky="top" expand="md">
        <Container fluid={true}>
            <Navbar.Brand as={Link} to="/">
                <img src={logo} width="30" height="30" alt="" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <BSNav className="me-auto">
                    <BSNav.Link as={Link} to="/race/list">
                        Races
                    </BSNav.Link>
                    <BSNav.Link as={Link} to="/site/list">
                        Sites
                    </BSNav.Link>
                    <BSNav.Link as={Link} to="/skiplists">
                        Skiplists
                    </BSNav.Link>
                    <BSNav.Link as={Link} to="/prebots">
                        Prebots
                    </BSNav.Link>
                    {window._env.AUTOTRADING_ENABLED == 1 && (
                        <BSNav.Link as={Link} to="/autorules">
                            Autotrading rules
                        </BSNav.Link>
                    )}
                    <BSNav.Link as={Link} to="/tools">
                        Tools
                    </BSNav.Link>
                </BSNav>
                <BSNav>
                    <BSNav.Link as={Link} to="/settings">
                        <i className="icon-cog"></i> Settings
                    </BSNav.Link>
                </BSNav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
)

export default Nav
