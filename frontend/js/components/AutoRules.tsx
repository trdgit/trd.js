import _ from 'lodash'
import React, { useState, useEffect } from 'react'

import { getJSON, postJSON } from '../helpers/api'
import Loading from './Loading'
import RulesEditor from './RulesEditor'
import Title from './Title'
import { AutoRules } from '@trd/shared/src/types'

const AutoRulesComponent: React.FC = () => {
    const [rules, setRules] = useState<AutoRules['rules']>([])
    const [schedule, setSchedule] = useState<{ [key: string]: string[] }>({
        1: ['00:00', '23:59'],
        2: ['00:00', '23:59'],
        3: ['00:00', '23:59'],
        4: ['00:00', '23:59'],
        5: ['00:00', '23:59'],
        6: ['00:00', '23:59'],
        7: ['00:00', '23:59']
    })
    const [loaded, setLoaded] = useState<boolean>(false)

    useEffect(() => {
        const fetchRules = async () => {
            const response = (await getJSON('/autorules/list')) as {
                data: AutoRules
                currentTime: string
            }
            setRules(response.data.rules)
            setSchedule(response.data.schedule)
            setLoaded(true)
        }

        fetchRules()
    }, [])

    const onChangeRules = (value: string) => {
        setRules(
            value
                .trim()
                .split('\n')
                .filter(rule => rule)
        )
    }

    const save = async () => {
        try {
            await postJSON(`/autorules/save`, {
                rules,
                schedule
            })
        } catch {
            alert("Can't save autorules")
        }
    }

    useEffect(() => {
        if (loaded) save()
    }, [rules, schedule, loaded])

    const onChangeSchedule = (
        dayIndex: number,
        dayIndexKey: number,
        e: React.ChangeEvent<HTMLInputElement>
    ) => {
        var newSchedule = JSON.parse(JSON.stringify(schedule))
        _.set(newSchedule, dayIndex + '.[' + dayIndexKey + ']', e.target.value)
        setSchedule(newSchedule)
    }

    if (!loaded) {
        return <Loading />
    }

    const days = Object.keys(schedule).map((key, idx) => (
        <tr key={key}>
            <td>
                {
                    [
                        'Monday',
                        'Tuesday',
                        'Wednesday',
                        'Thursday',
                        'Friday',
                        'Saturday',
                        'Sunday'
                    ][idx]
                }
            </td>
            <td>
                <div className="row row-cols-md-auto g-3 align-items-center">
                    <div className="col-6">
                        <input
                            onChange={onChangeSchedule.bind(null, idx + 1, 0)}
                            type="text"
                            className="form-control input-sm"
                            style={{ width: '70px', marginRight: 0 }}
                            defaultValue={schedule[`${key}`][0]}
                        />
                    </div>
                    <div className="col-6">
                        <input
                            onChange={onChangeSchedule.bind(null, idx + 1, 1)}
                            type="text"
                            className="form-control input-sm"
                            style={{ width: '70px' }}
                            defaultValue={schedule[`${key}`][1]}
                        />
                    </div>
                </div>
            </td>
        </tr>
    ))

    return (
        <>
            <Title
                title="Autotrading Rules"
                description="Anything that matches rules on this page will be marked as approved, and possibly autotraded"
            />
            <div className="row">
                <div className="col-md-3 form-inline">
                    <p>Schedule:</p>
                    <table className="table table-bordered">
                        <tbody>{days}</tbody>
                    </table>
                    <p>Current time is: {new Date().toLocaleTimeString()}</p>
                </div>
                <div className="col-md-9">
                    <p>One rule per line</p>
                    <div className="form-group">
                        <RulesEditor
                            keyRef="rules"
                            defaultValue={rules.join(`\n`)}
                            onChange={onChangeRules}
                            autocomplete={true}
                        />
                    </div>
                    <div className="alert alert-warning">
                        You can use the special variable <code>[chain]</code>{' '}
                        only on this page to write auto rules based on which
                        sites are in the chain.
                    </div>
                </div>
            </div>
        </>
    )
}

export default AutoRulesComponent
