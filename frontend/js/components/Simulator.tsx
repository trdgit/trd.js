import React, { useEffect, useState, useCallback } from 'react'
import { Link, useNavigate, useSearchParams } from 'react-router-dom'
import * as yup from 'yup'

import { getJSON, postForm } from '../helpers/api'
import Loading from './Loading'
import RaceLog from './RaceLog'
import Title from './Title'
import Checkbox from './form/Checkbox'
import Form from './form/Form'
import Input from './form/Input'
import { Select } from './form/Select'

const schema = yup.object().shape({
    tag: yup.string().required(),
    rlsname: yup.string().required()
})

const SimulatorForm = ({ tags, onSubmit }) => {
    const tagOptions = tags.map(tag => ({
        key: tag,
        value: tag
    }))

    return (
        <Form
            onSubmit={onSubmit}
            className="needs-validation"
            novalidate={true}
            schema={schema}
        >
            <Select label="Tag" name="tag" options={tagOptions} />
            <Input label="Rlsname" name="rlsname" />
            <Checkbox
                label="Use cache? (if possible)"
                name="use_cache"
                checked
            />

            <input type="submit" className="btn btn-primary" />
        </Form>
    )
}

export default () => {
    const navigate = useNavigate()
    const [queryString] = useSearchParams()

    const onSubmit = async data => {
        const useCache = data.use_cache == 1 ? 1 : 0
        navigate(
            `/tools/simulate?tag=${data.tag}&rlsname=${data.rlsname}&use_cache=${useCache}`
        )
    }

    const [results, setResults] = useState(null)
    const [tags, setTags] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const response = await getJSON('/settings/list')
            setTags(Object.keys(response.tag_options).sort())
        }
        fetchData()
    }, [])

    useEffect(() => {
        setResults(null)
        const simulate = async (tag, rlsname, useCache) => {
            const payload = new FormData()
            payload.append('tag', tag)
            payload.append('rlsname', rlsname)
            payload.append('use_cache', useCache)
            const result = await postForm(`/tools/simulate`, payload)
            setResults(result)
        }
        const ready =
            tags.length > 0 &&
            queryString.has('rlsname') &&
            queryString.has('tag')
        if (ready) {
            simulate(
                queryString.get('tag'),
                queryString.get('rlsname'),
                queryString.get('use_cache')
            )
        }
    }, [tags, location.search])

    if (location.search.length && !results) {
        return <Loading label="Simulating" />
    }

    if (!tags.length) {
        return <Loading />
    }

    return (
        <>
            <Title title="Simulator" />
            {!results && <SimulatorForm tags={tags} onSubmit={onSubmit} />}
            {results && <RaceLog race={results} />}
        </>
    )
}
