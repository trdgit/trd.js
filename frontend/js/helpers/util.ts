export const splitArray = (str: string, seperator = ' '): string[] =>
    str
        .trim()
        .split(seperator)
        .filter(val => val.length)
