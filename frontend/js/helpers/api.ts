import { APIError } from '@trd/shared/src/types'
import axios from 'axios'

export const API_URL = `http://${window._env.API_WEB_HOST}:${window._env.API_WEB_PORT}`

export const getJSON = async <T>(path: string): Promise<T> => {
    const response = await axios.get<T>(`${API_URL}${path}`)
    return response.data
}

export const postJSON = async <T>(
    path: string,
    payload: any,
    form = false
): Promise<T> => {
    const contentType = form
        ? 'multipart/form-data'
        : 'application/json;charset=UTF-8'

    const response = await axios.post<T>(`${API_URL}${path}`, payload, {
        headers: {
            'Content-Type': contentType,
        },
    })
    return response.data
}

export const postForm = async <T>(
    path: string,
    payload: FormData
): Promise<T> => {
    const response = await postJSON<T>(path, payload, true)
    return response
}

export const errorHandler = (error: unknown): Error => {
    if (axios.isAxiosError<APIError>(error)) {
        return new Error(error.response?.data.error)
    } else if (error instanceof Error) {
        return error
    }
    return new Error('Unkonwn error')
}
