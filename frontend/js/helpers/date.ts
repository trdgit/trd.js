import humanizeDuration from 'humanize-duration'
import { DateTime, Interval } from 'luxon'

export const formatDistance = (start: DateTime, end: DateTime): string =>
    humanizeDuration(Interval.fromDateTimes(start, end).toDuration().valueOf())
