import react from '@vitejs/plugin-react'
import { defineConfig } from 'vite'
import { createHtmlPlugin } from 'vite-plugin-html'

export default defineConfig(({ command }) => {
    return {
        css: {
            preprocessorOptions: {
                scss: {
                    api: 'modern-compiler',
                    silenceDeprecations: [
                        'mixed-decls',
                        'color-functions',
                        'global-builtin',
                        'import'
                    ]
                }
            }
        },
        plugins: [
            react(),
            createHtmlPlugin({
                minify: false,
                entry: '/js/components/TRD.tsx',
                template: '/index-vite.html',
                inject: {
                    data: {
                        basePath:
                            command === 'serve' ? 'http://localhost:1338' : ''
                    }
                }
            })
        ],
        server: {
            port: 1400,
            host: '127.0.0.1'
        },
        build: {
            outDir: `${__dirname}/../.build/frontend`,
            emptyOutDir: true,
            rollupOptions: {
                onwarn(warning, warn) {
                    if (
                        warning.code &&
                        ['MODULE_LEVEL_DIRECTIVE'].includes(warning.code)
                    ) {
                        return
                    }
                    warn(warning)
                }
            }
        }
    }
})
