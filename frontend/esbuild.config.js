const esbuild = require('esbuild')
const sassPlugin = require('esbuild-plugin-sass')

const successMessage = '> Build successful ⚡'

const watchMode = process.env.ESBUILD_WATCH === 'true' || false
let nodeEnv = 'production'
if (watchMode) {
    console.log('Running in watch mode 👀...\n')
    nodeEnv = 'development'
} else {
    console.log('Creating production build 🏭...')
}

const watchModeObject = {
    onRebuild: (error, result) => {
        if (error) console.error('Build failure:', error)
        else console.log(successMessage)
    },
}

esbuild
    .build({
        entryPoints: ['./js/components/TRD.tsx'],
        bundle: true,
        minify: true,
        sourcemap: true,
        watch: watchMode ? watchModeObject : false,
        platform: 'browser',
        outfile: '../public/dist/bundle.esbuild.js',
        define: { 'process.env.NODE_ENV': `"${nodeEnv}"` },
        target: ['es2020'],
        plugins: [sassPlugin()],
        loader: { '.woff': 'file', '.png': 'dataurl' },
    })
    .then(result => {
        console.log(successMessage)
    })
    .catch(e => {
        console.log(e)
        process.exit(1)
    })
