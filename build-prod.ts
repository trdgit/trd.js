import * as esbuild from 'esbuild'
;(async () => {
    const entryPoints = [
        './packages/api/src/start.ts',
        './packages/daemon/src/start.ts',
        './packages/gui/src/start.ts'
    ]

    await esbuild.build({
        entryPoints,
        bundle: true,
        minify: true,
        sourcemap: true,
        platform: 'node',
        target: 'es2020',
        outdir: '.esbuild',
        external: ['mysql' /*, 'sqlite3', 'better-sqlite3'*/]
    })
})()
