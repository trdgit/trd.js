#!/bin/sh

if [ -z "${DEBUG+x}" ]; then
  DEBUG="*,-vite:*,-express:*,-connect:*,-babel,-send,-body-parser:*"
fi

export DEBUG  # Ensure it is available to child processes

# cleanup any hanging
ps aux | grep "pnpm run" | grep node | awk '{print $2}' | xargs kill

cmdApi="pnpm run --filter=@trd/api start"
cmdDaemon="pnpm run --filter=@trd/daemon start"
cmdGui="pnpm run --filter=@trd/gui start"
cmdFrontend="pnpm run --filter=@trd/frontend start"

(trap 'kill -9 0' SIGINT; $cmdApi & $cmdDaemon & $cmdGui & $cmdFrontend)