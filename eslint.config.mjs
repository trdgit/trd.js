import unusedImports from 'eslint-plugin-unused-imports'

// @ts-check

import eslint from '@eslint/js'
import tseslint from 'typescript-eslint'

export default tseslint.config(
    eslint.configs.recommended,
    tseslint.configs.recommendedTypeChecked,
    {
        ignores: [
            '**/._*',
            '**/node_modules',
            '**/dist',
            '**/doc/',
            'frontend/',
            '**/manual/',
            '**/public/',
            '**/test/',
            '**/.esbuild/',
            '**/.build/',
            '**/_src/',
            '**/coverage/'
        ]
    },
    {
        files: ['**/*.ts'],
        plugins: {
            'unused-imports': unusedImports
        },
        rules: {
            'no-useless-escape': 'off',
            // 'jest/no-focused-tests': 'error',
            //'jest/no-identical-title': 'error',
            '@typescript-eslint/array-type': 'error',
            '@typescript-eslint/no-require-imports': 'warn',
            '@typescript-eslint/no-unused-vars': [
                'error',
                {
                    argsIgnorePattern: '^_',
                    caughtErrorsIgnorePattern: '^_',
                    ignoreRestSiblings: true
                }
            ]
        }
    }
)
