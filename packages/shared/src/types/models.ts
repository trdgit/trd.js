export interface TagOption {
    data_sources: string[]
    tag_requires: string[]
    tag_skiplist: string[]
    allowed_groups?: string[]
}

interface AutoSetting {
    allowed_classifications: string[]
    allowed_countries: string[]
}

type ScheduleDay = '1' | '2' | '3' | '4' | '5' | '6' | '7'

type Schedule = [number, number]

export interface SettingsModelType {
    require_pretime: boolean
    data_exchange_channel: string
    always_add_affils: boolean
    pre_retention: string
    baddir: string
    baddir_skip_pre: boolean
    tags: string[]
    ignore_tags: string[]
    tag_options: Record<string, TagOption>
    default_skiplists: string[]
    banned_groups: string[]
    auto_settings: Record<string, AutoSetting>
    children_networks: string[]
    refresh_ended_shows: boolean
    cbftp_host: string
    cbftp_port: string
    cbftp_password: string
    approved_straight_to_cbftp: boolean
    schedule: Record<ScheduleDay, Schedule>
    cbftp_api_port: string
    hide_credits_from_site_list?: boolean
    full_dates_on_race_list?: boolean
}
