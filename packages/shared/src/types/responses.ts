import { ReleaseName, Site } from './'

export interface GetSitesResponse {
    [k: string]: Site
}

export type RaceListResponse = {
    id: string
    bookmark: string
    chain: string | null
    chain_complete: string | null
    valid_sites: string | null
    rlsname: string
    started: 1 | 0
    created: string
    updated: string | null
    log: string
}

export type RaceListResponseIdOnly = {
    id: string
}

export interface Paging {
    next?: string
    prev?: string
    total: number
    length: number
}

export interface GetRacesResponse {
    data: {
        races: RaceListResponse[]
        dataSources: Record<string, number>
        tags: string[]
    }
    paging: Paging
}

export interface AddSiteResponse {
    success: 1
}

export interface APIError {
    error: string
}

export interface TestExtractionResponse {
    matched: boolean
    rlsname: ReleaseName | null
    section: string | null
}
