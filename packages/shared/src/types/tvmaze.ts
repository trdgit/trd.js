export interface TVMazeShow {
    id: number
    url: string
    name: string
    type: string
    language: string
    genres: string[]
    status: string
    runtime: number
    premiered: string
    officialSite: string
    schedule: TVMazeSchedule
    rating: TVMazeRating
    weight: number
    network: TVMazeNetwork
    webChannel: TVMazeWebNetwork | null
    externals: TVMazeExternals
    image: TVMazeImage
    summary: string
    updated: number
    _links: TVMazeLinks
}

export interface TVMazeRating {
    average: number
}

export interface TVMazeSchedule {
    time: string
    days: string[]
}

export interface TVMazeNetwork {
    id: number
    name: string
    country: TVMazeCountry
}
export interface TVMazeWebNetwork {
    id: number
    name: string
    country: TVMazeCountry | null
    officialSite: string | null
}

export interface TVMazeExternals {
    tvrage: number
    thetvdb: number
    imdb: string
}

export interface TVMazeImage {
    medium: string
    original: string
}

export interface TVMazeCountry {
    name: string
    code: string
    timezone: string
}

export interface TVMazeSelf {
    href: string
}

type TVMazePreviousEpisode = TVMazeSelf

type TVMazeShowLink = TVMazeSelf

type TVMazeCharacterLink = TVMazeSelf

export interface TVMazeLinks {
    self?: TVMazeSelf
    previousepisode?: TVMazePreviousEpisode
    show?: TVMazeShowLink
    character?: TVMazeCharacterLink
}

export type TVMazeEpisodes = TVMazeEpisode[]
export type TVMazeSeasons = TVMazeSeason[]

interface TVMazeEpisode {
    id: number
    url: string
    name: string
    season: number
    number: number
    airdate: string
    airtime: string
    airstamp: string
    runtime: number
    image: TVMazeImage
    summary: string
    _links: TVMazeLinks
}

interface TVMazeSeason {
    id: number
    url: string
    number: number
    name: string
    episodeOrder: number
    premiereDate: string
    endDate: string
    network: TVMazeNetwork
    webChannel: string | null
    image: TVMazeImage
    summary: string
    _links: TVMazeLinks
}
