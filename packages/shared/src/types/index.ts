import { Brand } from './helpers'

export * from './helpers'
export * from './tvmaze'
export * from './models'
export * from './responses'
export * from './race'

export type ReleaseName = Brand<string, 'Normal'>
export type ReleaseNameFullTitle = Brand<string, 'FullTitle'>
export type ReleaseNameTitle = Brand<string, 'TitleOnly'>

export type AutoRule = string
type Day = 1 | 2 | 3 | 4 | 5 | 6 | 7
type TimeStart = string
type TimeEnd = string
type Times = [TimeStart, TimeEnd]

export interface AutoRules {
    rules: AutoRule[]
    schedule: Record<Day, Times>
}

export type DataProviderLogs = string[]

export type Lists = Record<ListName, List>

type ListName = string
export type ListItems = string[]
export type ListRegex = string

export interface List {
    items?: ListItems
    regex?: ListRegex
    shared: boolean
}

type PrebotRegex = string

export type PrebotsData = Prebot[]

export interface Prebot {
    channel: PrebotRegex
    bot: PrebotRegex
    string_match: PrebotRegex
}

export interface Site {
    irc: IRC
    enabled: boolean
    affils: string[]
    sections: SiteSection[]
    banned_groups: string[]
    credits: SiteCredits
}

interface IRC {
    strings: SiteString
    channel: string
    channel_key: string
    bot: string
}

export interface SiteString {
    newstring: string
    'newstring-isregex': boolean
    'newstring-section': string
    'newstring-rls': string
    endstring: string
    'endstring-isregex': boolean
    'endstring-section': string
    'endstring-rls': string
    prestring: string
    'prestring-isregex': boolean
    'prestring-section': string
    'prestring-rls': string
    nukestring?: string
    'nukestring-isregex'?: boolean
    'nukestring-section'?: string
    'nukestring-rls'?: string
    unnukestring?: string
    'unnukestring-isregex'?: boolean
    'unnukestring-section'?: string
    'unnukestring-rls'?: string
}

export type Credits = number

type SiteCredits = Record<SiteBNC, Credits>

export type SiteBNC = string

export interface SiteSection {
    name: string
    pretime: number
    bnc: SiteBNC
    tags: SiteSectionTag[]
    rules: SiteSectionRules
    skiplists: SiteSectionSkiplists
    dupeRules: SiteSectionDupeRules
    downloadOnly: boolean
}

interface SiteSectionTag {
    tag: Tag
    trigger: string
    rules?: SiteTagRules
}

type SiteSectionRules = string[]
type SiteTagRules = string[]
type SiteSectionSkiplists = string[]

export interface SiteSectionDupeRules {
    'source.firstWins'?: boolean
    'source.priority'?: string
    'range.firstWins'?: boolean
    'range.priority'?: string
}

export type Tag = string

export interface ResultLog {
    namespace: string
    logs: DataProviderLogs
}
