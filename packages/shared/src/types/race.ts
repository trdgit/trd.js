export type ResultLogType = 'catastrophe' | 'warning' | 'debug'

export type RaceResultLog = {
    type: ResultLogType
    message: string
    details: Record<string, unknown>
}
export type RaceResultLogs = RaceResultLog[]
