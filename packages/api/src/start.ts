import 'reflect-metadata'
import cors from 'cors'
import express, { Express } from 'express'

import multer from 'multer'
const upload = multer()

import routeDashboard from './routes/index'
import routeAutorulesList from './routes/autorules/list'
import routeAutorulesSave from './routes/autorules/save'
import routePrebotsList from './routes/prebots/list'
import routePrebotsSave from './routes/prebots/save'
import routeRaceList from './routes/race/list'
import routeRaceLog from './routes/race/log'
import routeRaceLookup from './routes/race/lookup'
import routeSettingsList from './routes/settings/list'
import routeSettingsSave from './routes/settings/save'
import routeSiteAdd from './routes/site/add'
import routeSiteGet from './routes/site/get'
import routeSitesList from './routes/site/list'
import routeSiteSave from './routes/site/save'
import routeSiteTestString from './routes/site/testString'
import routeSkiplistList from './routes/skiplist/list'
import routeSkiplistSave from './routes/skiplist/save'
import routeToolsCbftpRaw from './routes/tools/cbftp_raw'
import routeToolsCbftpTags from './routes/tools/cbftp_tags'
import routeToolsCleanup from './routes/tools/cleanup'
import {
    routeToolsImmutablesList,
    routeToolsImmutablesReset
} from './routes/tools/data_immutable'
import routeToolsSimulate from './routes/tools/simulate'
import routeToolsTagFinder from './routes/tools/tag_finder'
import routeToolsTagOverview from './routes/tools/tag_overview'
import * as logger from '@trd/core/src/helpers/logger'

export const serveApi = () => {
    const api: Express = express()

    // log route changes
    api.use((req, _res, next) => {
        logger.api(`Navigating to: ${req.originalUrl}`)
        next()
    })

    api.use(cors())
    api.use(express.json({ limit: process.env.BODY_POST_LIMIT || '5mb' }))

    api.post('/autorules/save', routeAutorulesSave)
    api.get('/race/list', routeRaceList)
    api.get('/race/:id(\\d+)/log', routeRaceLog)
    api.post('/race/lookup', routeRaceLookup)

    api.get('/settings/list', routeSettingsList)
    api.post('/settings/save', routeSettingsSave)

    api.get('/site/list', routeSitesList)
    api.post('/site/:name/save', routeSiteSave)
    api.post('/site/add', upload.none(), routeSiteAdd)
    api.get('/site/:name', routeSiteGet)
    api.post('/site/:name/testString', routeSiteTestString)

    api.get('/skiplist/list', routeSkiplistList)
    api.post('/skiplist/save', routeSkiplistSave)

    api.get('/prebots/list', routePrebotsList)
    api.post('/prebots/save', routePrebotsSave)

    api.post('/tools/simulate', upload.none(), routeToolsSimulate)
    api.get('/tools/tag_overview', routeToolsTagOverview)
    api.post('/tools/cbftp_raw', upload.none(), routeToolsCbftpRaw)
    api.post('/tools/cleanup', routeToolsCleanup)
    api.get('/tools/cbftp_tags', routeToolsCbftpTags)
    api.get('/tools/data_immutable', routeToolsImmutablesList)
    api.post('/tools/data_immutable', routeToolsImmutablesReset)
    api.post('/tools/tag_finder', upload.none(), routeToolsTagFinder)

    api.get('/autorules/list', routeAutorulesList)

    api.get('/', routeDashboard)

    api.listen(
        Number(process.env.API_BIND_PORT),
        process.env.API_BIND_HOST,
        () => {
            logger.api(
                `API started listening @ http://${process.env.API_BIND_HOST}:${process.env.API_BIND_PORT}`
            )
        }
    )
}

serveApi()
