import { ReleaseName } from '@trd/shared/src/types'
import { Request, Response } from 'express'

import SitesModel from '@trd/core/src/models/sites'
import Race from '@trd/core/src/race/race'
import { raceResultToLog } from '@trd/core/src/helpers/race'

export default async (req: Request, res: Response, _next) => {
    const data = req.body

    const siteList = []
    for (const [siteName] of Object.entries(SitesModel.getData())) {
        siteList.push(siteName)
    }

    const race = new Race(String(data['use_cache']) == '1' ? true : false)
    race.addSites(siteList)
    const result = await race.race(
        String(data['tag']),
        data['rlsname'] as ReleaseName
    )

    res.json(raceResultToLog(result))
}
