import { Request, Response, NextFunction } from 'express'

import { db } from '@trd/core/src/helpers/db'
import { unserializeDataCache } from '@trd/core/src/helpers/serialize.data_cache'
import { resetDataCacheImmutableData } from '@trd/core/src/repository/data'

import phpUnserialize from 'locutus/php/var/unserialize'

const isUsingJSONForCache = () => process.env.DATACACHE_JSON === '1'

const unserialize = isUsingJSONForCache()
    ? unserializeDataCache
    : phpUnserialize

const unserializeEntry = entry => {
    const original = unserialize(entry.data)
    const immutable = entry.data_immutable?.length
        ? unserialize(entry.data_immutable)
        : undefined
    let fixedFields = []

    for (const [k, v] of Object.entries(immutable)) {
        fixedFields = [
            ...fixedFields,
            {
                field: k,
                from: original[k],
                to: v
            }
        ]
    }

    return {
        k: entry.k,
        fixes: fixedFields
    }
}

export const routeToolsImmutablesList = async (
    req: Request,
    res: Response,
    _next: NextFunction
) => {
    // TODO: move to repository layer
    const data = await db.fetchAll(
        `SELECT k,data,data_immutable FROM data_cache WHERE data_immutable IS NOT NULL ORDER BY k ASC`
    )
    res.json(data.map(unserializeEntry))
}

export const routeToolsImmutablesReset = async (
    req: Request,
    res: Response,
    _next: NextFunction
) => {
    if (!req.is('json')) {
        res.sendStatus(400)
    }

    const body = req.body
    if (body.action == 'reset' && body.k) {
        await resetDataCacheImmutableData({ k: body.k })
        res.sendStatus(200)
    }
}
