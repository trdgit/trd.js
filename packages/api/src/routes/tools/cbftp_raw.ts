import { Request, Response } from 'express'

import SettingsModel from '@trd/core/src/models/settings'
import CBFTP from '@trd/core/src/utility/cbftp'

import { is_string } from '@trd/core/src/helpers/data_types'

export default async (req: Request, res: Response) => {
    const host = SettingsModel.get('cbftp_host')
    const port = SettingsModel.get('cbftp_api_port')
    const password = SettingsModel.get('cbftp_password')

    if (!host.length || !port.length || !password.length) {
        return res.json({ error: 'Missing cbftp info in settings' })
    }

    const fields = req.body

    let sites = []
    if (fields.siteList.length) {
        sites = is_string(fields.siteList)
            ? fields.siteList.split(',')
            : fields.siteList
    }

    const cb = new CBFTP(host, port, password)
    try {
        const results = await cb.rawCapture(
            String(fields['command']),
            sites,
            String(fields['path'])
        )
        res.json(results)
    } catch (e) {
        return res.status(400).json({ error: e.message })
    }
}
