import { Request, Response } from 'express'

import { sortObjectByKeys } from '@trd/core/src/helpers/data_types'
import SettingsModel from '@trd/core/src/models/settings'
import SitesModel from '@trd/core/src/models/sites'

export default async (_req: Request, res: Response) => {
    const tags = {}
    for (const tag of Object.keys(SettingsModel.get('tag_options'))) {
        tags[tag] = []
    }

    for (const [siteName, siteInfo] of Object.entries(SitesModel.getData())) {
        for (const sectionInfo of siteInfo['sections']) {
            for (const tag of sectionInfo.tags) {
                const value =
                    (sectionInfo.bnc?.length ? sectionInfo.bnc : siteName) +
                    ' - ' +
                    sectionInfo.name
                if (!(tag.tag in tags)) {
                    throw new Error(`Tag ${tag.tag} does not exist`)
                }
                tags[tag.tag].push(value)
            }
        }
    }

    for (const [k, v] of Object.entries(tags)) {
        Array(v).sort()
        tags[k] = v
    }

    res.json(sortObjectByKeys(tags))
}
