import { Request, Response } from 'express'

import { guessTagFromReleaseName } from '@trd/core/src/utility/tag'

export default async (req: Request, res: Response) => {
    const { rlsname } = req.body

    try {
        const tagResult = guessTagFromReleaseName(rlsname)
        res.json({ data: tagResult })
    } catch (e) {
        console.log(e)
        return res.json({ error: 'unknown error' })
    }
}
