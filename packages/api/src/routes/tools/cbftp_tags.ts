import { Request, Response } from 'express'

import SettingsModel from '@trd/core/src/models/settings'
import SitesModel from '@trd/core/src/models/sites'
import CBFTP from '@trd/core/src/utility/cbftp'

export default async (_req: Request, res: Response) => {
    const host = SettingsModel.get('cbftp_host')
    const port = SettingsModel.get('cbftp_api_port')
    const password = SettingsModel.get('cbftp_password')

    if (!host.length || !port.length || !password.length) {
        return res.json({ error: 'Missing cbftp info in settings' })
    }
    const cb = new CBFTP(host, port, password)

    const results = []
    for (const [siteName, siteInfo] of Object.entries(SitesModel.getData())) {
        const ringResults = []
        const bncTags = {}

        for (const sectionInfo of siteInfo['sections']) {
            // if not bnc, set siteName as bnc
            if (!sectionInfo.bnc) {
                sectionInfo.bnc = siteName
            }
            // add empty array for bnc in the object
            if (!bncTags[sectionInfo.bnc]) {
                bncTags[sectionInfo.bnc] = []
            }
            for (const tag of sectionInfo.tags) {
                bncTags[sectionInfo.bnc].push(tag.tag)
            }
        }

        for (const bnc of Object.keys(bncTags)) {
            // type sections so we can Array.filter
            const sections: string[] = bncTags[bnc]
            const cbSiteInfo = await cb.getSiteInfo(bnc)
            if (!cbSiteInfo['sections']) {
                continue
            }
            const cbSections = []
            cbSiteInfo['sections'].forEach(s => {
                cbSections.push(s.name)
            })
            // Filter sections we have and leave the ones we do not
            const missing = sections.filter(x => cbSections.indexOf(x) < 0)
            ringResults.push({ bnc: bnc, sections: missing })
        }
        results.push({ site: siteName, absent: ringResults })
    }

    res.json(results)
}
