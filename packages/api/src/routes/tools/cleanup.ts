import { Request, Response } from 'express'

import { db } from '@trd/core/src/helpers/db'

export default async (_req: Request, res: Response) => {
    await db.query('DELETE FROM race WHERE log IS NULL')
    res.status(200).json({ success: true })
}
