import { Request, Response } from 'express'

import PrebotsModel from '@trd/core/src/models/prebots'

export default async (req: Request, res: Response) => {
    res.json(PrebotsModel.getData())
}
