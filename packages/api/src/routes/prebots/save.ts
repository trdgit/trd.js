import { Request, Response } from 'express'

import PrebotsModel from '@trd/core/src/models/prebots'
import { handleAPISave } from '../../helpers/persist'

export default async (req: Request, res: Response) => {
    handleAPISave(req, res, PrebotsModel)
}
