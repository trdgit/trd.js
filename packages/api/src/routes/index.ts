import { Request, Response } from 'express'

import SitesModel from '@trd/core/src/models/sites'
import SettingsModel from '@trd/core/src/models/settings'
import { db } from '@trd/core/src/helpers/db'

export default async (req: Request, res: Response) => {
    const activeSites = SitesModel.getSitesArray()
    const totalAffils = SitesModel.getAllAffils().size
    const mostPopularAffil = Object.entries(SitesModel.getAffilCounts()).reduce(
        (a, b) => (a[1] > b[1] ? a : b),
        [null, -Infinity] // Default value if map is empty
    )[0]
    const totalRaces = await db.fetchColumn(
        'SELECT COUNT(*) as total FROM race'
    )
    const oldestRace = await db.fetchColumn(
        'SELECT DATEDIFF(NOW(), created) as daysAgo FROM race ORDER BY id ASC LIMIT 1'
    )
    const topTags = await db.fetchAll(
        'SELECT bookmark, COUNT(*) as total FROM race GROUP BY bookmark ORDER BY total desc'
    )
    const tags = SettingsModel.get('tags')

    const totalEntries = await db.fetchColumn('SELECT COUNT(*) from data_cache')
    const fixedEntries = await db.fetchColumn(
        'SELECT COUNT(*) FROM data_cache WHERE data_immutable IS NOT NULL'
    )

    const totalPres = await db.fetchColumn('SELECT COUNT(*) as total FROM pre')
    const firstPre = await db.fetchColumn(
        'SELECT DATEDIFF(NOW(), created) as daysAgo FROM pre ORDER BY id ASC LIMIT 1'
    )

    res.json({
        races: {
            totalRaces,
            mostRacedTag: topTags.length ? topTags[0].bookmark : 'n/a',
            leastRacedTag: topTags.length
                ? topTags[topTags.length - 1].bookmark
                : 'n/a',
            oldestRace: oldestRace ?? 'n/a'
        },
        sites: {
            totalSites: activeSites.length,
            totalAffils,
            mostPopularAffil
        },
        tags: {
            totalTags: tags.length
        },
        data: {
            totalEntries,
            fixedEntries
        },
        pre: {
            totalPres,
            firstPre
        }
    })
}
