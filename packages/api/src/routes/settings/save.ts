import { Request, Response } from 'express'

import SettingsModel from '@trd/core/src/models/settings'
import { handleAPISave } from '../../helpers/persist'

export default async (req: Request, res: Response) => {
    handleAPISave(req, res, SettingsModel)
}
