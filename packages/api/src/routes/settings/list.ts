import { Request, Response } from 'express'

import SettingsModel from '@trd/core/src/models/settings'

export default async (req: Request, res: Response) => {
    res.json(SettingsModel.getData())
}
