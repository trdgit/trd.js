import { Request, Response } from 'express'

import SkiplistsModel from '@trd/core/src/models/skiplists'

export default async (req: Request, res: Response) => {
    res.json(SkiplistsModel.getData())
}
