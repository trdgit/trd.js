import { Request, Response } from 'express'

import SkiplistsModel from '@trd/core/src/models/skiplists'
import { handleAPISave } from '../../helpers/persist'

export default async (req: Request, res: Response) => {
    handleAPISave(req, res, SkiplistsModel)
}
