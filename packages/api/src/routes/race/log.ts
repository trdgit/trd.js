import { Request, Response } from 'express'

import { unserializeRaceResult } from '@trd/core/src/helpers/serialize.data'
import { upgradeRaceResult } from '@trd/core/src/race/result'
import { getRaceById } from '@trd/core/src/repository/race'
import { raceResultToLog } from '@trd/core/src/helpers/race'

export default async (req: Request, res: Response) => {
    const id = req.params.id

    let race
    if (id?.length) {
        race = await getRaceById(id)
    }

    if (!race) {
        return res.status(404).send()
    }

    const raceResult = upgradeRaceResult(unserializeRaceResult(race.log))
    res.json(raceResultToLog(raceResult))
}
