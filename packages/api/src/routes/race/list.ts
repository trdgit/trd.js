import { RaceListResponse, RaceListResponseIdOnly } from '@trd/shared/src/types'
import { Request, Response } from 'express'

import { db } from '@trd/core/src/helpers/db'
import SettingsModel from '@trd/core/src/models/settings'

const fixStructure = race => {
    race.log = JSON.parse(race.log)
    return race
}

export default async (req: Request, res: Response) => {
    const cursor = req.query.cursor ? Number(req.query.cursor) : 99999999999
    const tag = req.query.tag
    const rlsname = req.query.rlsname

    // injected params
    const conditions = []
    const params = []
    if (tag) {
        conditions.push('bookmark = ?')
        params.push(tag)
    }
    if (rlsname) {
        conditions.push('rlsname = ?')
        params.push(rlsname)
    }

    const conditionsString = conditions.length
        ? `AND ${conditions.join(' AND ')}`
        : ''

    let next
    let prev
    const limit = 70

    // TODO: move to repository layer
    const totalPages = await db.fetchColumn(
        `SELECT COUNT(*) as total FROM race WHERE 1 = 1 ${conditionsString} LIMIT ${limit}`,
        params
    )

    const races = await db.fetchAll<RaceListResponse>(
        `SELECT * FROM race WHERE id <= ? ${conditionsString} ORDER BY id DESC LIMIT ${
            limit + 1
        }`,
        [cursor, ...params]
    )

    if (races.length - 1 === limit) {
        next = races.pop().id
    }
    if (races.length) {
        // TODO: move to repository layer
        const prevSlices = await db.fetchAll<RaceListResponseIdOnly>(
            `SELECT id FROM race WHERE id > ? ${conditionsString} ORDER BY id ASC LIMIT ${limit}`,
            [cursor, ...params]
        )
        if (prevSlices.length) {
            prev = prevSlices.pop().id
        }
    }

    const dataSourceMap: Record<string, number> = {}
    for (const [t, options] of Object.entries(
        SettingsModel.get('tag_options')
    )) {
        dataSourceMap[t] = options['data_sources']?.length
    }

    const tags = Object.keys(SettingsModel.get('tag_options')).sort()

    res.status(200).json({
        data: {
            races: races.map(fixStructure),
            dataSources: dataSourceMap,
            tags
        },
        paging: {
            next,
            prev,
            total: totalPages,
            length: races.length - 1
        }
    })
}
