import { ReleaseName } from '@trd/shared/src/types'
import { Request, Response } from 'express'

import { getRace } from '@trd/core/src/repository/race'

export default async (req: Request, res: Response) => {
    const rlsname = req.body.rlsname

    let id
    if (rlsname?.length) {
        const race = await getRace(rlsname as ReleaseName)
        if (race) {
            id = race.id
        }
    }

    if (!id) {
        return res.status(404).send()
    }

    res.status(200).json({
        id
    })
}
