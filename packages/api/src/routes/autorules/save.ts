import { Request, Response } from 'express'

import AutoRulesModel from '@trd/core/src/models/autorules'
import { handleAPISave } from '../../helpers/persist'

export default async (req: Request, res: Response) => {
    handleAPISave(req, res, AutoRulesModel)
}
