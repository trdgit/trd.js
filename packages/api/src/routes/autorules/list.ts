import { Request, Response } from 'express'

import { getCurrentTimeFull } from '@trd/core/src/helpers/date'
import AutoRulesModel from '@trd/core/src/models/autorules'

export default async (req: Request, res: Response) => {
    res.json({
        currentTime: getCurrentTimeFull(),
        data: AutoRulesModel.getData()
    })
}
