import { TestExtractionResponse } from '@trd/shared/src/types'
import { Request, Response } from 'express'

import SitesModel from '@trd/core/src/models/sites'
import { extract } from '@trd/core/src/utility/extractor'

export default async (req: Request, res: Response) => {
    const site = SitesModel.getSite(req.params.name)
    const data = req.body

    const testString = data['testString']
    const keyBits = data['key'].split('.')
    const endBit = keyBits.pop()

    const extraction = extract(site.irc.strings, [endBit], testString)
    const section = extraction.section
    const rlsname = extraction.rlsname

    const response: TestExtractionResponse = {
        matched: section !== null || rlsname !== null,
        section: section,
        rlsname: rlsname
    }

    return res.json(response)
}
