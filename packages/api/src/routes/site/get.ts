import { Request, Response } from 'express'

import SitesModel from '@trd/core/src/models/sites'

export default async (req: Request, res: Response) => {
    res.json(SitesModel.getSite(req.params.name))
}
