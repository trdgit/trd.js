import { Request, Response } from 'express'

import SitesModel from '@trd/core/src/models/sites'

export default async (req: Request, res: Response) => {
    try {
        SitesModel.addSite(req.body.name)
        return res.status(201).json({ success: 1 })
    } catch (e) {
        return res.status(400).json({ error: e.message })
    }
}
