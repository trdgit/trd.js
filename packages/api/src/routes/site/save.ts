import { Request, Response } from 'express'

import SitesModel from '@trd/core/src/models/sites'

export default async (req: Request, res: Response) => {
    const name = req.params.name
    const data = req.body

    try {
        SitesModel.replaceSite(name, data)
        return res.json({ success: 1 })
    } catch (e) {
        return res.status(400).json({ error: e.message })
    }
}
