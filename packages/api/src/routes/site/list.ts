import { GetSitesResponse } from '@trd/shared/src/types'
import { Request, Response } from 'express'

import SitesModel from '@trd/core/src/models/sites'

const sortEnabledThenName = (a, b) => {
    const rdiff = b[1]['enabled'] - a[1]['enabled']
    if (rdiff) {
        return rdiff
    }
    return a[0].localeCompare(b[0])
}

export default async (_req: Request, res: Response) => {
    const sites = SitesModel.getData()
    const sortedSites = Object.entries(sites).sort(sortEnabledThenName)
    const response: GetSitesResponse = Object.fromEntries(sortedSites)
    res.json(response)
}
