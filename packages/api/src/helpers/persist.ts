import { Request, Response } from 'express'

import Model from '@trd/core/src/models/model'

export const handleAPISave = <ModelType>(
    req: Request,
    res: Response,
    model: Model<ModelType>
) => {
    if (req.is('json')) {
        try {
            const newData = req.body
            model.setData(newData)
            model.save()
            return res.json({ success: 1 })
        } catch (e) {
            return res.status(400).json({ error: e.message })
        }
    }
    return res.status(500).json({ error: 1 })
}
