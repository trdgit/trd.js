import { randomBytes } from 'crypto'
import { Settings as LuxonSettings } from 'luxon'
import * as net from 'net'
import 'reflect-metadata'

import App from './app'
import { db } from '@trd/core/src/helpers/db'
import * as logger from '@trd/core/src/helpers/logger'
import Socket from './socket'

import { getVersion } from './version'

const asciiLogo = `
████████╗██████╗ ██████╗ 
╚══██╔══╝██╔══██╗██╔══██╗
   ██║   ██████╔╝██║  ██║
   ██║   ██╔══██╗██║  ██║
   ██║   ██║  ██║██████╔╝
   ╚═╝   ╚═╝  ╚═╝╚═════╝ 
`

if (!process.env.TZ) {
    throw new Error('Invalid timezone set process.env.TZ')
}
LuxonSettings.defaultZone = process.env.TZ

if (!process.env.DB_ENGINE) {
    throw new Error('Please configure process.env.DB_ENGINE to `mysql`')
}

const app = new App()

;(async () => {
    // check we have index
    // TODO: remove this maybe mid-2023
    if (process.env.DB_ENGINE === 'mysql') {
        const indexCheck = await db.fetchAssoc(
            "SHOW INDEX FROM race WHERE key_name = 'rlsname' AND non_unique = 0;"
        )
        if (!indexCheck) {
            console.error(
                'Go to misc/ section of manual and run the queries to update your indexes'
            )
            process.exit()
        }
    }
})()

const port = process.env.PORT

let clients: Socket[] = []
const server = new net.Server()
server.listen(
    {
        port,
        host: process.env.HOST
    },
    () => {
        logger.server(asciiLogo.trim())
        logger.server(
            `(v${getVersion()}) listening for connection requests on socket localhost:${port}.`
        )
    }
)

server.on('connection', function (socket: Socket) {
    socket.id = '' + randomBytes(16).toString('hex')

    // push clients to
    clients.push(socket)
    app.setClients(clients)

    socket.write(
        JSON.stringify({
            command: 'VERSION',
            version: getVersion()
        }) + '\n'
    )

    logger.server(`Client <${socket.id}> (${socket.remoteAddress}) connected`)

    // socket.write('Hello, client.\n')

    // The server can also receive data from the client by reading from its socket.
    socket.on('data', function (chunk) {
        const msg = chunk.toString()
        app.process(msg)
    })

    socket.on('end', function () {
        logger.server('Closing connection with the client')
        clients = clients.filter(c => c.id !== socket.id)
        app.setClients(clients)
    })

    // Don't forget to catch error, for your own sake.
    socket.on('error', function (err) {
        logger.server(`Socket error: ${err}`)
    })
})
