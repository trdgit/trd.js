import { DateTime } from 'luxon'

import { addDays, toMySQL } from '@trd/core/src/helpers/date'
import ProcessorResponse from '../processors/response'
import ProcessorResponseCommand from '../processors/response-command'
import { addApproval } from '@trd/core/src/repository/approval'
import Handler from './handler'

export default class ApprovalHandler extends Handler {
    public async handle(response: ProcessorResponse) {
        // TODO: check the rule doesn't exist already

        // expiry
        const expires = addDays(DateTime.now(), 1)

        // insert the rule
        await addApproval({
            bookmark: response.data['tag'],
            chain: response.data['chain'],
            pattern: response.data['pattern'],
            type: response.data['type'],
            maxlimit: 24,
            expires: toMySQL(expires)
        })

        // TODO: handle response communication better
        response.response = '1'

        const command = new ProcessorResponseCommand('APPROVALRESULT')
        response.setCommand(command)

        return response
    }
}
