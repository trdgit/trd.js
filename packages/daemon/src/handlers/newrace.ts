import { ReleaseName, TagOption } from '@trd/shared/src/types'
import { DateTime } from 'luxon'
import * as util from 'util'

import { clone } from '@trd/core/src/helpers/data_types'
import {
    getCurrentDayOfWeek,
    getCurrentHoursFull,
    getCurrentMinutesFull,
    toMySQL
} from '@trd/core/src/helpers/date'
import * as logger from '@trd/core/src/helpers/logger'
import { regexMatches } from '@trd/core/src/helpers/regex'
import { serializeRaceResult } from '@trd/core/src/helpers/serialize.data'
import AutoRulesModel from '@trd/core/src/models/autorules'
import SettingsModel from '@trd/core/src/models/settings'
import SitesModel from '@trd/core/src/models/sites'
import SkiplistsModel from '@trd/core/src/models/skiplists'
import ProcessorResponse from '../processors/response'
import ProcessorResponseCommand from '../processors/response-command'
import Race from '@trd/core/src/race/race'
import RaceResult from '@trd/core/src/race/result'
import { getPre } from '@trd/core/src/repository/pre'
import { getPretip, removePretip } from '@trd/core/src/repository/pretips'
import {
    addRace,
    addRaceSite,
    getRace,
    logRace,
    logRaceStarted
} from '@trd/core/src/repository/race'
import CBFTP from '@trd/core/src/utility/cbftp'
import { extract } from '@trd/core/src/utility/extractor'
import {
    getReleaseNameFromDirectory,
    passesRegexSkiplists,
    passesRequirements
} from '@trd/core/src/utility/release'
import Handler from './handler'

export default class NewRaceHandler extends Handler {
    public async handle(response: ProcessorResponse) {
        return await handleNewRace(response)
    }
}

const filterTags = (
    siteName: string,
    tags: string[],
    tagOptions: Record<string, TagOption>,
    rlsname: ReleaseName,
    section: string
): string[] => {
    return tags
        .filter(tag => {
            if (tagOptions[tag]?.tag_requires) {
                const passes = passesRequirements(
                    rlsname,
                    tagOptions[tag].tag_requires,
                    SkiplistsModel
                )
                if (passes !== true) {
                    logger.debug(
                        util.format(
                            'Site %s filtered tag "%s" from section "%s" for race %s because it did not meet tag requirements: %s',
                            siteName,
                            tag,
                            section,
                            rlsname,
                            Array(passes).join(' , ')
                        )
                    )
                    return false
                }
                return true
            }
        })
        .filter(tag => {
            if (tagOptions[tag]?.tag_skiplist) {
                const passes = passesRegexSkiplists(
                    rlsname,
                    tagOptions[tag].tag_skiplist,
                    SkiplistsModel
                )
                if (passes !== true) {
                    logger.debug(
                        util.format(
                            'Site %s filtered tag "%s" from section "%s" for race %s because it matched tag skiplist: %s',
                            siteName,
                            tag,
                            section,
                            rlsname,
                            passes
                        )
                    )
                    return false
                }
                return true
            }
        })
}

const handleNewRace = async (
    response: ProcessorResponse
): Promise<ProcessorResponse> => {
    if (!response.data.src) {
        return response
    }

    const { msg: str, src: siteName } = response.data
    const siteInfo = SitesModel.getSite(siteName)

    // get irc strings
    const irc = siteInfo.irc.strings

    const { section, rlsname: rlsnameString } = extract(
        irc,
        ['newstring', 'prestring'],
        str
    )

    if (!section || !rlsnameString) {
        return response
    }

    // we have a match
    let rlsname = getReleaseNameFromDirectory(rlsnameString)
    if (regexMatches(rlsname, SettingsModel.get('baddir'))) {
        logger.debugIrc(
            util.format(
                'Site %s found rlsname %s in section %s but matched the baddir setting',
                siteName,
                rlsname,
                section
            )
        )
        rlsname = null
    }

    if (!section || !rlsname) {
        return response
    }

    logger.debugIrc(
        util.format(
            'Site %s found new race string for section %s with rlsname %s',
            siteName,
            section,
            rlsname
        )
    )

    // add a new section if we come across it
    SitesModel.addSection(siteName, section)

    // check pretime
    const pretime = await getPre(rlsname)

    if (SettingsModel.get('require_pretime') == true && !pretime) {
        logger.debug(
            util.format(
                "Site %s didn't start race %s because no pre time",
                siteName,
                rlsname
            )
        )
        return response
    }

    // find the tag
    let validTags = SitesModel.findValidTags(siteName, section, rlsname)

    // filter based off tag requirements + skiplist
    const tagOptions = SettingsModel.get('tag_options')

    validTags = filterTags(siteName, validTags, tagOptions, rlsname, section)

    // see what we are left with
    let tag = null
    if (validTags.length > 1) {
        logger.error(
            util.format(
                "Site %s didn't start race %s because two tags matched: " +
                    validTags.join(' '),
                siteName,
                rlsname
            )
        )
        return response
    } else if (validTags.length === 1) {
        tag = validTags[0]
    }

    // last measures
    if (tag === null || !(typeof tag === 'string')) {
        logger.debug(
            util.format(
                "Site %s didn't start race %s/%s because no tag was found",
                siteName,
                section,
                rlsname
            )
        )
        return response
    }
    if (SettingsModel.get('ignore_tags').includes(tag)) {
        logger.debug(
            util.format(
                "Site %s didn't start race %s because either we ignore this tag (%s) in settings file",
                siteName,
                rlsname,
                tag
            )
        )
        return response
    }

    let raceResult: RaceResult

    // check pretips
    const tip = getPretip(rlsname)
    if (tip) {
        logger.debug(
            util.format('Pretip found in NewRaceHandler for %s', rlsname)
        )
        raceResult = clone(tip).result
        removePretip(rlsname)
    }

    // check if it's raced already
    const now = toMySQL()
    const raceId = await logNewRace(tag, rlsname, now)

    // log to race_site if already exists and exit
    if (raceId === 0) {
        const { id: existingRaceId } = await getRace(rlsname)
        logRaceSite(existingRaceId, siteName, now)
        return response
    }

    logRaceSite(raceId, siteName, now)

    logger.debug(
        util.format(
            'New race in %s / %s from %s with tag %s',
            section,
            rlsname,
            siteName,
            tag
        )
    )

    if (!raceResult) {
        const race = new Race()
        raceResult = await race.race(
            tag,
            rlsname,
            pretime ? DateTime.fromSQL(pretime.created) : undefined
        )
    } else {
        logger.debug(
            util.format(
                'Skipping race evaluation for %s because we already have the result from a pretip',
                rlsname
            )
        )
    }

    const isRace = raceResult.isRace()

    await logRace(
        {
            log: serializeRaceResult(raceResult),
            updated: now,
            ...(isRace && {
                valid_sites: raceResult.validSites.join(','),
                chain: raceResult.chain.join(','),
                chain_complete:
                    raceResult.affilSites.length > 0
                        ? raceResult.affilSites.join(',')
                        : null
            })
        },
        rlsname,
        tag
    )

    if (isRace) {
        const {
            chain,
            data: resultData,
            affilSites,
            preDifferenceInMinutes
        } = raceResult

        let command = new ProcessorResponseCommand('TRADE')

        // handle approvals
        // const approvedRow = await this.isApproved(tag, rlsname)
        // if (approvedRow !== false) {
        //     command = new ProcessorResponseCommand('APPROVED')
        //     chain = approvedRow.chain.split(',')
        //     await logApprovalHit(approvedRow.id, approvedRow.hits + 1)
        // }

        if (process.env.AUTOTRADING_ENABLED) {
            const autoResponse = AutoRulesModel.evaluate(chain, resultData)
            if (autoResponse !== false) {
                raceResult.autotraded = true
                command = new ProcessorResponseCommand('APPROVED')
                await logRaceStarted(rlsname, tag)
            }
        }

        sendToCb(command, raceResult)

        command.setDataArray({
            chain,
            affilSites,
            bookmark: tag,
            rlsname: rlsname,
            preDifferenceInMinutes,
            id: raceId
        })

        response.setCommand(command)
        response.setMetaData(resultData)
        response.terminate = false

        // $prefix = sprintf('%s', substr(ucfirst($command->getCommand()), 0, 4));
        // $color = 'green';
        // if ($command->getCommand() === 'APPROVED') {
        //     $color = 'blue';
        // }

        // ConsoleDebug::command($prefix, 'black', $color, $tag, $rlsname, $chain);

        logger.info(
            util.format(
                '%s race %s with validSites %s',
                command.getCommand(),
                rlsname,
                chain.join(',')
            )
        )
    }

    return response
}

const sendToCb = (
    command: ProcessorResponseCommand,
    raceResult: RaceResult
): void => {
    const { tag, rlsname, affilSites } = raceResult
    let { chain } = raceResult

    if (
        command.getCommand() !== 'APPROVED' ||
        SettingsModel.get('approved_straight_to_cbftp') !== true
    ) {
        return
    }

    if (!scheduleIsOk()) {
        return
    }

    const cb = new CBFTP(
        SettingsModel.get('cbftp_host'),
        SettingsModel.get('cbftp_port'),
        SettingsModel.get('cbftp_password')
    )
    let downloadOnly = null
    if (raceResult.hasAffils()) {
        chain = raceResult.getChainWithoutAffils()
        downloadOnly = affilSites
    }
    cb.race(tag, rlsname, chain, downloadOnly)
    logger.debug(
        util.format('Sent UDP packet to cbftp for rlsname %s', rlsname)
    )
}

const scheduleIsOk = (): boolean => {
    const currentDay = getCurrentDayOfWeek()
    const schedule = SettingsModel.get('schedule')[currentDay]
    const start = parseInt(schedule[0].replace(':', ''))
    const end = parseInt(schedule[1].replace(':', ''))
    const currentTime = parseInt(
        `${getCurrentHoursFull()}${getCurrentMinutesFull()}`
    )
    return currentTime >= start && currentTime <= end
}

const logRaceSite = async (
    id: number,
    name: string,
    now: string
): Promise<void> => {
    try {
        await addRaceSite({
            race_id: String(id),
            site: name,
            created: now
        })
    } catch (_e) {
        // just ignore it, it's no big deal, we probably matched something as new dir by accident
    }
}

const logNewRace = async (
    tag: string,
    rlsname: ReleaseName,
    now: string
): Promise<number> => {
    const id = await addRace({
        bookmark: tag,
        rlsname: rlsname,
        created: now
    })
    return Number(id)
}
