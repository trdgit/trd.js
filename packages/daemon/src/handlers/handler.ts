import ProcessorResponse from '../processors/response'

export default abstract class Handler {
    public abstract handle(response: ProcessorResponse)
}
