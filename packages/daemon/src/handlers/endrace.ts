import * as util from 'util'

import * as logger from '@trd/core/src/helpers/logger'
import SitesModel from '@trd/core/src/models/sites'
import ProcessorResponse from '../processors/response'
import ProcessorResponseCommand from '../processors/response-command'
import {
    getRace,
    logRaceEnded,
    logRaceSiteEnded
} from '@trd/core/src/repository/race'
import { extract } from '@trd/core/src/utility/extractor'
import { getReleaseNameFromDirectory } from '@trd/core/src/utility/release'
import Handler from './handler'

export default class EndRaceHandler extends Handler {
    public async handle(response: ProcessorResponse) {
        if (!response.data.src) {
            return response
        }

        const { msg: str, src: siteName } = response.data

        // check each sites strings
        const siteInfo = SitesModel.getSite(siteName)

        const extraction = extract(siteInfo.irc.strings, ['endstring'], str)
        const { section } = extraction
        let { rlsname } = extraction

        if (!rlsname?.length) {
            return response
        }

        //ConsoleDebug::debug("End race in $section / $rlsname from " . $response->data['src']);

        rlsname = getReleaseNameFromDirectory(rlsname)

        let currentlyComplete = new Set<string>()

        const raceInfo = await getRace(rlsname)
        if (!raceInfo?.chain?.length) {
            logger.debugIrc(
                util.format(
                    `Site %s found end race string for rlsname %s but no race was found`,
                    siteName,
                    rlsname
                )
            )
            return response
        }

        if (raceInfo.chain_complete?.length) {
            currentlyComplete = new Set<string>(
                raceInfo.chain_complete.trim().split(',')
            )
        }

        // get the bnc from ths section if we can
        // TODO can probably clean this up with a reduce()
        const possibleBNCs: string[] = []
        if (section !== null && siteInfo.sections[section]) {
            const siteSection = siteInfo.sections[section]
            for (const t of siteSection.tags) {
                if (t.tag == raceInfo.bookmark && siteSection.bnc?.length) {
                    possibleBNCs.push(siteSection.bnc)
                } else {
                    possibleBNCs.push(siteName)
                }
            }
        } else {
            // try and get it by iterating over sections with tags that match
            for (const s of siteInfo.sections) {
                if (s.tags?.length) {
                    for (const t of s.tags) {
                        if (t.tag == raceInfo.bookmark) {
                            if (s.bnc?.length) {
                                possibleBNCs.push(s.bnc)
                            } else {
                                possibleBNCs.push(siteName)
                            }
                        }
                    }
                }
            }
        }

        logger.debugIrc(
            util.format(
                `Site %s found end race string for rlsname %s and bookmark %s with possible BNC\'s "%s"`,
                siteName,
                rlsname,
                raceInfo['bookmark'],
                possibleBNCs.join(',')
            )
        )

        if (possibleBNCs.length !== 1) {
            return response
        }

        currentlyComplete.add(possibleBNCs[0])

        await logRaceEnded(rlsname, Array.from(currentlyComplete).join(','))
        await logRaceSiteEnded(raceInfo.id, siteName)

        const command = new ProcessorResponseCommand('RACECOMPLETESTATUS')
        command.setData('id', raceInfo.id)
        command.setData('tag', raceInfo.bookmark)
        command.setData('rlsname', rlsname)
        command.setData('chain_complete', Array.from(currentlyComplete))
        response.setCommand(command)

        return response
    }
}
