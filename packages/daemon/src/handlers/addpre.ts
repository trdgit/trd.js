import { ReleaseName } from '@trd/shared/src/types'

import { toMySQL } from '@trd/core/src/helpers/date'
import { attachExtraDupeInfo, isBotChannelMatch } from '../helpers/handler'
import { PCRE2JS, regexMatches } from '@trd/core/src/helpers/regex'
import PreBotsModel from '@trd/core/src/models/prebots'
import SettingsModel from '@trd/core/src/models/settings'
import ProcessorResponse from '../processors/response'
import { addPre } from '@trd/core/src/repository/pre'
import Handler from './handler'

const getRlsnameFromPrebotRegex = (
    str: string,
    regex: RegExp
): ReleaseName | undefined => {
    const result = str.match(regex)
    if (result && result[1]?.length) {
        return result[1] as ReleaseName
    }
    return
}

export default class AddPreHandler extends Handler {
    public async handle(response: ProcessorResponse) {
        const { msg: str, channel, nick } = response.data

        const prebotData = PreBotsModel.getData()
        for (const bot of prebotData) {
            if (isBotChannelMatch(bot, channel, nick)) {
                const rlsname = getRlsnameFromPrebotRegex(
                    str,
                    PCRE2JS(bot.string_match)
                )

                // check the string matches
                if (rlsname) {
                    if (SettingsModel.get('baddir_skip_pre') === true) {
                        const baddir = SettingsModel.get('baddir')
                        if (regexMatches(rlsname, baddir)) {
                            return response
                        }
                    }

                    const id = await addPre({
                        rlsname,
                        created: toMySQL(),
                        ...attachExtraDupeInfo(rlsname)
                    })

                    // exit if we have inserted
                    if (Number(id) > 0) {
                        response.terminate = true
                    }
                }
            }
        }

        return response
    }
}
