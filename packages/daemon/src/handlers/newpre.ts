import * as util from 'util'

import { toMySQL } from '@trd/core/src/helpers/date'
import { attachExtraDupeInfo } from '../helpers/handler'
import * as logger from '@trd/core/src/helpers/logger'
import SitesModel from '@trd/core/src/models/sites'
import ProcessorResponse from '../processors/response'
import { addPre } from '@trd/core/src/repository/pre'
import { extract } from '@trd/core/src/utility/extractor'
import { getGroup } from '@trd/core/src/utility/release'
import Handler from './handler'

export class NewPreHandler extends Handler {
    public async handle(response: ProcessorResponse) {
        if (!response.data.src) {
            return response
        }

        const { msg: str, src: site } = response.data

        // extraction
        const { section, rlsname } = extract(
            SitesModel.getSite(site)?.irc.strings,
            ['prestring'],
            str
        )

        // we have a match
        if (!!section && !!rlsname) {
            logger.debug(
                util.format(
                    'Site %s found pre string for section %s with rlsname %s',
                    site,
                    section,
                    rlsname
                )
            )
            // add a new affil if we come across it
            const group = getGroup(rlsname)
            if (SitesModel.addAffil(site, group)) {
                logger.debug(
                    util.format('New affil %s found on %s', group, site)
                )
            }

            const id = await addPre({
                rlsname,
                created: toMySQL(),
                ...attachExtraDupeInfo(rlsname)
            })

            // exit if we have inserted
            if (Number(id) > 0) {
                logger.debug(
                    util.format('New pre %s found on %s', rlsname, site)
                )
            }
        }

        return response
    }
}
