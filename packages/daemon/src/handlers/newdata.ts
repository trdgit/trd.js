import { ReleaseNameFullTitle } from '@trd/shared/src/types'
import * as c from 'irc-colors'
import * as util from 'util'

import IMDBDataProvider from '@trd/core/src/dataprovider/imdb'
import TVMazeDataProvider, {
    TVMazeData
} from '@trd/core/src/dataprovider/tvmaze'
import { getKeyByValue } from '@trd/core/src/helpers/data_types'
import * as logger from '@trd/core/src/helpers/logger'
import {
    serializeDataCache,
    unserializeDataCache
} from '@trd/core/src/helpers/serialize.data_cache'
import SettingsModel from '@trd/core/src/models/settings'
import ProcessorResponse from '../processors/response'
import ProcessorResponseCommand from '../processors/response-command'
import {
    getDataCacheByIdAndNamespace,
    setDataCacheImmutableData
} from '@trd/core/src/repository/data'
import { COUNTRIES } from '@trd/core/src/utility/locale'
import Handler from './handler'

import phpUnserialize from 'locutus/php/var/unserialize'
import phpSerialize from 'locutus/php/var/serialize'

const validCommandsWithoutPrefix = [
    'tvmaze',
    'imdb',
    'ctvmaze',
    'cimdb',
    'tvmazef',
    'imdbf'
]
const validCommands = validCommandsWithoutPrefix.map(cmd => `!${cmd}`)

const isUsingJSONForCache = () => process.env.DATACACHE_JSON === '1'

const serialize = isUsingJSONForCache() ? serializeDataCache : phpSerialize
const unserialize = isUsingJSONForCache()
    ? unserializeDataCache
    : phpUnserialize

export default class NewDataHandler extends Handler {
    public async handle(response: ProcessorResponse) {
        const { channel, msg } = response.data

        if (channel != SettingsModel.get('data_exchange_channel')) {
            return response
        }

        const bits = msg.split(' ')

        if (validCommands.includes(bits[0])) {
            logger.debug(
                util.format(
                    `Data-exchange message: <%s> detected in %s`,
                    msg,
                    channel
                )
            )
        } else {
            logger.debug(
                util.format(
                    `Data-exchange message: <*CENSORED*> detected in %s`,
                    channel
                )
            )
        }

        if (bits.length < 2) {
            return response
        }

        switch (bits[0]) {
            case '!tvmaze': {
                const data = {
                    namespace: bits[0],
                    key: bits.slice(1, -1).join(' '),
                    id: bits[bits.length - 1]
                }

                const command = new ProcessorResponseCommand('IRCREPLY')
                command.setData('channel', channel)

                const tvmaze = new TVMazeDataProvider()
                try {
                    const tvmazeData = await tvmaze.lookupById(Number(data.id))
                    if (tvmazeData?.id && tvmazeData.id == data.id) {
                        tvmaze.save(data.key, tvmazeData, true)
                        command.setData(
                            'msg',
                            `${c.yellow('TVMaze')} :: ${c.green(
                                'Updated'
                            )} :: ${tvmazeData['title']} ${util.format(
                                '(%d/%s/%s/%s)',
                                tvmazeData.id,
                                tvmazeData.classification,
                                tvmazeData.country,
                                tvmazeData.language
                            )}`
                        )
                        response.setCommand(command)
                        response.terminate = true
                    } else {
                        command.setData('msg', `TVMaze :: Invalid id`)

                        response.setCommand(command)
                        response.terminate = true
                    }
                } catch (e) {
                    command.setData('msg', `TVMaze :: Failure: ${e.message}`)

                    response.setCommand(command)
                    response.terminate = true
                }

                break
            }

            case '!tvmazef': {
                const value = bits.slice(3).join(' ')

                let castedValue: unknown = value
                if (bits[2] === 'genres') {
                    castedValue = value.split(',')
                } else if (['daily', 'web'].includes(bits[2])) {
                    castedValue = value === 'true' ? true : false
                }

                const data = {
                    namespace: 'tvmaze',
                    id: bits[1],
                    field: bits[2],
                    value: castedValue
                }

                const existing = await getDataCacheByIdAndNamespace(
                    data.id,
                    data.namespace
                )

                if (!existing?.data?.length) {
                    const command = new ProcessorResponseCommand('IRCREPLY')
                    command.setData('channel', channel)
                    command.setData(
                        'msg',
                        `${c.yellow('TVMaze')} :: ${c.red(
                            'Failed'
                        )} :: No data found for this show`
                    )
                    response.setCommand(command)
                    response.terminate = true
                    return response
                }

                let immutableData = {}

                if (existing && !!existing.data_immutable) {
                    immutableData = unserialize(existing.data_immutable)
                }

                // check field is valid
                let validField = false
                const existingData = unserialize(existing.data)
                for (const [k] of Object.entries(existingData)) {
                    if (k == data.field) {
                        validField = true
                    }
                }

                if (validField) {
                    immutableData[data.field] = data.value

                    if (data.field === 'country') {
                        const key = getKeyByValue(COUNTRIES, data.value)
                        if (key) {
                            immutableData['country_code'] = key
                        }
                    } else if (data.field === 'country_code') {
                        if (COUNTRIES[String(data.value)]) {
                            immutableData['country'] =
                                COUNTRIES[String(data.value)]
                        }
                    }

                    await setDataCacheImmutableData({
                        data_immutable: serialize(immutableData),
                        namespace: data.namespace,
                        id: data.id
                    })

                    const command = new ProcessorResponseCommand('IRCREPLY')
                    command.setData('channel', channel)
                    command.setData(
                        'msg',
                        `${c.yellow('TVMaze')} :: ${c.green('Updated')} :: ${
                            existingData['title']
                        } ${util.format('%s: %s', data.field, value)}`
                    )
                    response.setCommand(command)
                    response.terminate = true
                } else {
                    const command = new ProcessorResponseCommand('IRCREPLY')
                    command.setData('channel', channel)
                    command.setData(
                        'msg',
                        `${c.yellow('TVMaze')} :: ${c.red(
                            'Failed'
                        )} :: invalid field`
                    )
                    response.setCommand(command)
                    response.terminate = true
                }

                break
            }

            case '!imdb': {
                const data = {
                    namespace: 'imdb',
                    key: bits.slice(1, -1).join(' '),
                    id: bits[bits.length - 1]
                }

                const imdb = new IMDBDataProvider()
                const imdbData = await imdb.extractDataFromIMDBId(data.id)
                if (imdbData.id && imdbData.id == data.id) {
                    imdb.save(data.key, imdbData, true)

                    const command = new ProcessorResponseCommand('IRCREPLY')
                    command.setData('channel', channel)
                    command.setData(
                        'msg',
                        `${c.yellow('IMDB')} :: ${c.green('Updated')} :: ${
                            imdbData.title
                        } ${util.format(
                            '(%s/%s/%s/%s/%s)',
                            imdbData.id,
                            imdbData.year,
                            imdbData.genres.join(','),
                            imdbData.country,
                            imdbData.language_primary
                        )}`
                    )
                    response.setCommand(command)
                    response.terminate = true
                }
                break
            }

            case '!imdbf': {
                const value = bits.slice(3).join(' ')

                const data = {
                    namespace: 'imdb',
                    id: bits[1],
                    field: bits[2],
                    value: ['genres', 'countries', 'languages'].includes(
                        bits[2]
                    )
                        ? value.split(',')
                        : value
                }

                const existing = await getDataCacheByIdAndNamespace(
                    data.id,
                    data.namespace
                )

                if (!existing?.data?.length) {
                    const command = new ProcessorResponseCommand('IRCREPLY')
                    command.setData('channel', channel)
                    command.setData(
                        'msg',
                        `${c.yellow('IMDB')} :: ${c.red(
                            'Failed'
                        )} :: No data found for this movie`
                    )
                    response.setCommand(command)
                    response.terminate = true
                    return response
                }

                let immutableData = {}

                if (existing && !!existing.data_immutable) {
                    immutableData = unserialize(existing.data_immutable)
                }

                // check field is valid
                let validField = false
                const existingData = unserialize(existing.data)
                for (const [k] of Object.entries(existingData)) {
                    if (k == data.field) {
                        validField = true
                    }
                }

                if (validField) {
                    immutableData[data.field] = data.value

                    await setDataCacheImmutableData({
                        data_immutable: serialize(immutableData),
                        namespace: data.namespace,
                        id: data.id
                    })

                    const command = new ProcessorResponseCommand('IRCREPLY')
                    command.setData('channel', channel)
                    command.setData(
                        'msg',
                        `${c.yellow('IMDB')} :: ${c.green('Updated')} :: ${
                            existingData['title']
                        } ${util.format('%s: %s', data.field, value)}`
                    )
                    response.setCommand(command)
                    response.terminate = true
                } else {
                    const command = new ProcessorResponseCommand('IRCREPLY')
                    command.setData('channel', channel)
                    command.setData(
                        'msg',
                        `${c.yellow('IMDB')} :: ${c.red(
                            'Failed'
                        )} :: invalid field`
                    )
                    response.setCommand(command)
                    response.terminate = true
                }

                break
            }

            case '!ctvmaze': {
                const tvmaze = new TVMazeDataProvider()
                const title = bits.slice(1).join(' ')
                const tvmazeDataResponse = await tvmaze.lookupByRlsnameCleaned(
                    title as ReleaseNameFullTitle
                )

                const command = new ProcessorResponseCommand('IRCREPLY')
                command.setData('channel', channel)
                const tvmazeData =
                    (await tvmazeDataResponse.getData()) as TVMazeData
                if (tvmazeDataResponse.result) {
                    command.setData(
                        'msg',
                        `${c.yellow('TVMaze')} :: ${
                            tvmazeData.title
                        } ${util.format(
                            '(%d/%s/%s/%s)',
                            tvmazeData.id,
                            tvmazeData.classification,
                            tvmazeData.country,
                            tvmazeData.language
                        )}`
                    )
                } else {
                    command.setData(
                        'msg',
                        `TVMaze :: Nothing found for: ${title}`
                    )
                }
                response.setCommand(command)
                response.terminate = true

                break
            }

            case '!cimdb': {
                const imdb = new IMDBDataProvider()
                const title = bits.slice(1).join(' ')
                const imdbDataResponse = await imdb.lookupByRlsnameCleaned(
                    title as ReleaseNameFullTitle
                )
                const imdbData = imdbDataResponse.getData()

                const command = new ProcessorResponseCommand('IRCREPLY')
                command.setData('channel', channel)
                if (imdbDataResponse.result) {
                    command.setData(
                        'msg',
                        `${c.yellow('IMDB')} :: ${
                            imdbData.title
                        }  ${util.format(
                            '(%s/%s/%s/%s/%s)',
                            imdbData.id,
                            imdbData.year,
                            imdbData.genres.join(','),
                            imdbData.country,
                            imdbData.language_primary
                        )}`
                    )
                } else {
                    command.setData(
                        'msg',
                        `IMDB :: Nothing found for: ${title}`
                    )
                }
                response.setCommand(command)
                response.terminate = true

                break
            }
        }

        return response
    }
}
