import { ReleaseName } from '@trd/shared/src/types'
import util from 'util'

import * as logger from '@trd/core/src/helpers/logger'
import SettingsModel from '@trd/core/src/models/settings'
import ProcessorResponse from '../processors/response'
import ProcessorResponseCommand from '../processors/response-command'
import Race from '@trd/core/src/race/race'
import { addPretip } from '@trd/core/src/repository/pretips'
import CBFTP from '@trd/core/src/utility/cbftp'
import Handler from './handler'

export default class PretipHandler extends Handler {
    public async handle(response: ProcessorResponse) {
        const { tag, rlsname } = response.data

        logger.debug(
            util.format('Pretip about to be handled %s/%s', tag, rlsname)
        )

        const race = new Race()
        const result = await race.race(tag, rlsname as ReleaseName)

        const isRace = result.isRace()

        if (isRace) {
            addPretip(rlsname as ReleaseName, result)
            const cb = new CBFTP(
                SettingsModel.get('cbftp_host'),
                SettingsModel.get('cbftp_api_port'),
                SettingsModel.get('cbftp_password')
            )
            cb.idle(result.chain)
            logger.debug(
                util.format(
                    'Pretip for %s/%s receieved and valid race found',
                    tag,
                    rlsname
                )
            )
        } else {
            logger.debug(
                util.format(
                    'Pretip for %s/%s receieved and no valid race found',
                    tag,
                    rlsname
                )
            )
        }

        const command = new ProcessorResponseCommand('PRETIPRESULT')
        command.setDataArray({
            isRace
        })
        response.setCommand(command)

        return response
    }
}
