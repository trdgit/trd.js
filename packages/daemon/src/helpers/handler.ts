import { ReleaseName, Prebot } from '@trd/shared/src/types'

import ReleaseNameDataProvider from '@trd/core/src/dataprovider/release'
import { AddPreRequestDupeData } from '@trd/core/src/repository/pre'
import { getTitle } from '@trd/core/src/utility/release'
import { regexMatches } from '@trd/core/src/helpers/regex'

export const attachExtraDupeInfo = (
    rlsname: ReleaseName
): AddPreRequestDupeData => {
    const isTv = ReleaseNameDataProvider.isTvShow(rlsname)
    const isMovie = /\d{4}[\.\-_]\S+[\.\-\_][xh]26[45][\.\-_]/i.test(rlsname)
    const isTvOrMovie = isTv || isMovie

    const data: AddPreRequestDupeData = {}
    if (isTvOrMovie) {
        data.dupe_k = getTitle(rlsname).fullTitle
        data.dupe_resolution =
            ReleaseNameDataProvider.extractResolution(rlsname)
        data.dupe_source = ReleaseNameDataProvider.extractTVSource(rlsname)
        data.dupe_codec = ReleaseNameDataProvider.extractCodec(rlsname)
    }
    if (isTv) {
        data.dupe_season_episode =
            ReleaseNameDataProvider.extractSeason(rlsname) +
            '_' +
            ReleaseNameDataProvider.extractEpisode(rlsname)
    }

    return data
}

export const isBotChannelMatch = (bot: Prebot, channel: string, nick: string) =>
    regexMatches(channel, bot.channel) && regexMatches(nick, bot.bot)
