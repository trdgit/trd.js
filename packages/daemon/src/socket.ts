import * as net from 'net'

export default class Socket extends net.Socket {
    id?: string
}
