import ProcessorResponse from '../processors/response'

export default abstract class Filter {
    public abstract filter(response: ProcessorResponse): ProcessorResponse
}
