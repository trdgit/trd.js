import * as util from 'util'

import { clone } from '@trd/core/src/helpers/data_types'
import * as logger from '@trd/core/src/helpers/logger'
import SettingsModel from '@trd/core/src/models/settings'
import ProcessorResponse from '../processors/response'
import Filter from './filter'
import PrebotFilter from './prebot'
import SiteFilter from './site'

export default class RelevantChannelFilter extends Filter {
    public filter(response: ProcessorResponse) {
        const siteFilter = new SiteFilter()
        const siteFilterResponse = siteFilter.filter(clone(response)) // object clone hack

        const prebotFilter = new PrebotFilter()
        const prebotFilterResponse = prebotFilter.filter(clone(response))

        const isDataChannel =
            response.data['channel'] ===
            SettingsModel.get('data_exchange_channel')

        // only terminate if both responses indicate
        // that neither channel is worth looking at
        if (
            siteFilterResponse.terminate &&
            prebotFilterResponse.terminate &&
            !isDataChannel
        ) {
            response.terminate = true
        }

        if (siteFilterResponse.data.src?.length) {
            response.data.src = siteFilterResponse.data['src']
            logger.debugIrc(
                util.format(
                    'Channel %s and Bot %s matched site %s',
                    response.data['channel'],
                    response.data['nick'],
                    siteFilterResponse.data['src']
                )
            )
        }

        return response
    }
}
