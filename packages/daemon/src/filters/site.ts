import { regexMatches } from '@trd/core/src/helpers/regex'
import SitesModel from '@trd/core/src/models/sites'
import ProcessorResponse from '../processors/response'
import Filter from './filter'

export default class SiteFilter extends Filter {
    public filter(response: ProcessorResponse) {
        const siteData = SitesModel.getData()

        const { channel, nick } = response.data

        for (const [siteName, siteInfo] of Object.entries(siteData)) {
            if (!siteInfo.enabled) {
                continue
            }

            const {
                irc: { bot: botRegex, channel: channelRegex }
            } = siteInfo

            const channelAndBotMatches =
                regexMatches(channel, channelRegex) &&
                regexMatches(nick, botRegex)

            if (channelAndBotMatches) {
                response.data.src = siteName
                return response
            }
        }

        response.terminate = true
        return response
    }
}
