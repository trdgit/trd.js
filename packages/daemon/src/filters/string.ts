import ProcessorResponse from '../processors/response'
import Filter from './filter'

const isIRCChannel = (str: string): boolean => str.slice(0, 1) === '#'

export default class IncomingStringFilter extends Filter {
    public filter(response: ProcessorResponse) {
        if (!isIRCChannel(response.data.channel)) {
            response.terminate = true
        }

        return response
    }
}
