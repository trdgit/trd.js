import { isBotChannelMatch } from '../helpers/handler'
import PrebotsModel from '@trd/core/src/models/prebots'
import ProcessorResponse from '../processors/response'
import Filter from './filter'

export default class PrebotFilter extends Filter {
    public filter(response: ProcessorResponse) {
        const prebotData = PrebotsModel.getData()
        if (Array.isArray(prebotData)) {
            const { channel, nick } = response.data
            for (const botInfo of prebotData) {
                // let it through :)
                if (isBotChannelMatch(botInfo, channel, nick)) {
                    return response
                }
            }
        }

        response.terminate = true
        return response
    }
}
