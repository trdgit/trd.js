import RelevantChannelFilter from './filters/channel'
import IncomingStringFilter from './filters/string'
import AddPreHandler from './handlers/addpre'
import ApprovalHandler from './handlers/approval'
import EndRaceHandler from './handlers/endrace'
import NewDataHandler from './handlers/newdata'
import { NewPreHandler } from './handlers/newpre'
import NewRaceHandler from './handlers/newrace'
import PretipHandler from './handlers/pretip'
import { toMySQL } from '@trd/core/src/helpers/date'
import * as logger from '@trd/core/src/helpers/logger'
import AutorulesModel from '@trd/core/src/models/autorules'
import PrebotsModel from '@trd/core/src/models/prebots'
import SettingsModel from '@trd/core/src/models/settings'
import SitesModel from '@trd/core/src/models/sites'
import SkiplistsModel from '@trd/core/src/models/skiplists'
import ApprovalProcessor from './processors/approval'
import IRCProcessor from './processors/irc'
import PretipProcessor from './processors/pretip'
import Processor from './processors/processor'
import ProcessorResponse from './processors/response'
import {
    getIrcMessageFromQueue,
    processIrcMessage
} from '@trd/core/src/repository/irc'
import Socket from './socket'
import ByteArray from '@trd/core/src/utility/byte-array'

export default class App {
    private clients: Socket[]
    private buffer: ByteArray

    constructor() {
        this.buffer = new ByteArray('')
    }

    process(msg: string): void {
        this.buffer.append(msg)

        let incomingMsg
        while ((incomingMsg = this.buffer.pickUntilByte('\n'))) {
            this.handleMessage(incomingMsg)
        }

        // try and refresh models
        this.refreshModels()

        // handle irc message
        this.processIRCMessageQueue()
    }

    setClients(clients: Socket[]) {
        this.clients = clients
    }

    reply(message: string): void {
        this.clients.forEach(client => {
            logger.server(
                `Sending to client [${client.id}] message [${message}]`
            )
            client.write(`${message}\n`)
        })
    }

    async handleMessage(msg: string): Promise<void> {
        const bits = msg.trim().split(' ')
        const command = bits.slice(1).join(' ')

        let processor: Processor
        let response: ProcessorResponse | undefined

        switch (bits[0]) {
            case 'IRC':
                processor = new IRCProcessor()
                processor.setCommand(command)

                // filters
                processor.addFilter(new IncomingStringFilter())
                processor.addFilter(new RelevantChannelFilter())

                // handlers
                processor.addHandler(new NewPreHandler())
                processor.addHandler(new NewRaceHandler())
                processor.addHandler(new EndRaceHandler())
                processor.addHandler(new AddPreHandler())
                processor.addHandler(new NewDataHandler())

                response = await processor.process()

                // if we have no response, leave
                if (!response) {
                    return
                }

                if (response.command) {
                    this.reply(response.toJSON())
                }
                break

            case 'PRETIP':
                processor = new PretipProcessor()
                processor.setCommand(command)
                processor.addHandler(new PretipHandler())

                response = await processor.process()
                if (!response) {
                    return
                }

                if (response.response) {
                    this.reply(response.toJSON())
                }
                break

            case 'APPROVE':
                processor = new ApprovalProcessor()
                processor.setCommand(command)
                processor.addHandler(new ApprovalHandler())

                response = await processor.process()
                if (!response) {
                    return
                }

                if (response.response) {
                    this.reply(response.toJSON())
                }
                break

            case 'DATE':
                this.reply(`${toMySQL()}`)
        }
    }

    refreshModels(): void {
        const models = [
            AutorulesModel,
            PrebotsModel,
            SettingsModel,
            SitesModel,
            SkiplistsModel
        ]
        models.forEach(model => {
            if (typeof model.refresh === 'function') {
                model.refresh()
            }
        })
    }

    async processIRCMessageQueue(): Promise<void> {
        const message = await getIrcMessageFromQueue()
        if (message) {
            this.reply(
                `IRCREPLY ${SettingsModel.get('data_exchange_channel')} ${
                    message.message
                }`
            )
            await processIrcMessage(message.id)
        }
    }
}
