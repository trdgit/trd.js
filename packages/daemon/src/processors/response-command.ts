export default class ProcessorResponseCommand {
    private data: Record<string, unknown> = {}
    private command: string

    constructor(command: string) {
        this.command = command
    }

    public getCommand() {
        return this.command
    }

    public setCommand(command: string) {
        this.command = command
    }

    // TODO: actually now an object => rename
    public setDataArray(arr) {
        this.data = arr
    }

    public setData(k: string, v) {
        this.data[k] = v
    }

    public getData(k: string) {
        return this.data[k]
    }
}
