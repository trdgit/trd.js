import Processor from './processor'

export default class PretipProcessor extends Processor {
    public setCommand(str: string) {
        this.str = str.trim()

        const bits = this.str.split(' ')

        if (bits.length == 2) {
            this.data = {
                tag: bits[0],
                rlsname: bits[1],
            }
        }
    }
}
