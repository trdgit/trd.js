import Filter from '../filters/filter'
import Handler from '../handlers/handler'
import ProcessorResponse, { ProcessorData } from './response'

export default abstract class Processor {
    private handlers: (Filter | Handler)[] = []
    protected data: ProcessorData
    protected str: string | null

    public abstract setCommand(str: string): void

    public addFilter(filter: Filter) {
        this.handlers.push(filter)
    }

    public addHandler(handler: Handler) {
        this.handlers.push(handler)
    }

    public getData(): ProcessorData {
        return this.data
    }

    public async process(): Promise<ProcessorResponse | undefined> {
        if (!this.str) {
            return
        }

        let response
        if (this.data) {
            response = new ProcessorResponse(false, this.str, this.data)

            for (const handler of this.handlers) {
                if (handler instanceof Filter) {
                    response = await handler.filter(response)
                } else if (handler instanceof Handler) {
                    response = await handler.handle(response)
                }

                if (response.terminate) {
                    return response
                }
            }
        }

        return response
    }
}
