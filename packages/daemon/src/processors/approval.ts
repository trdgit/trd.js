import Processor from './processor'

export default class ApprovalProcessor extends Processor {
    public str: string | null = null

    /*
    Format: APPROVE <tag> <type> <pattern> <chain>
    */
    public setCommand(str: string) {
        this.str = str.trim()
        const bits = this.str.split(' ')

        if (bits.length == 4) {
            this.data = {
                tag: bits[0],
                type: bits[1],
                pattern: bits[2],
                chain: bits[3]
            }
        }
    }
}
