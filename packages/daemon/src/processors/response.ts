import RaceResultData from '@trd/core/src/race/data'
import ProcessorResponseCommand from './response-command'

export interface ProcessorData {
    src?: string
    channel?: string
    nick?: string
    msg?: string
    [key: string]: string
}

export default class ProcessorResponse {
    public terminate: boolean
    public str: string
    public data: ProcessorData
    public response
    public command: ProcessorResponseCommand
    private metaData = null

    constructor(terminate: boolean, str: string, data: ProcessorData = {}) {
        this.terminate = terminate
        this.str = str
        this.data = data
    }

    public setCommand(command: ProcessorResponseCommand): void {
        this.command = command
    }

    public setMetaData(data: RaceResultData): void {
        this.metaData = data
    }

    public getMetaData() {
        return this.metaData
    }

    public toJSON(): string {
        const cmd = this.command.getCommand()
        switch (cmd) {
            case 'TRADE':
            case 'APPROVED':
                return JSON.stringify({
                    command: cmd,
                    tag: this.command.getData('bookmark'),
                    chain: this.command.getData('chain'),
                    affilSites: this.command.getData('affilSites'),
                    rlsname: this.command.getData('rlsname'),
                    data: this.getMetaData().all(),
                    id: this.command.getData('id'),
                    preDifferenceInMinutes: this.command.getData(
                        'preDifferenceInMinutes'
                    )
                })

            case 'IRCREPLY':
                return JSON.stringify({
                    command: cmd,
                    msg: this.command.getData('msg'),
                    channel: this.command.getData('channel')
                })

            case 'RACECOMPLETESTATUS':
                return JSON.stringify({
                    command: cmd,
                    id: this.command.getData('id'),
                    tag: this.command.getData('tag'),
                    rlsname: this.command.getData('rlsname'),
                    chain_complete: this.command.getData('chain_complete')
                })

            case 'SIMULATIONRESULT':
                return JSON.stringify({
                    command: cmd,
                    payload: this.response
                })

            case 'APPROVALRESULT':
                return JSON.stringify({
                    success: this.response
                })

            case 'PRETIPRESULT':
                return JSON.stringify({
                    success: this.command.getData('isRace')
                })
        }
    }
}
