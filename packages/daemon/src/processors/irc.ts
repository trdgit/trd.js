import Processor from './processor'

export default class IRCProcessor extends Processor {
    public setCommand(str: string) {
        this.str = str.trim()

        const bits = this.str.split(' ')

        if (bits.length >= 3) {
            this.data = {
                channel: bits[0],
                nick: bits[1],
                msg: bits.slice(2).join(' '),
            }
        }
    }
}
