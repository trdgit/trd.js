import { SettingsModelType } from '@trd/shared/src/types'
import express from 'express'

import * as logger from '@trd/core/src/helpers/logger'
import SettingsModel from '@trd/core/src/models/settings'
import path from 'path'

const frontendPath = `${__dirname}/../../../.build/frontend`

export const serveGui = () => {
    const web = express()
    web.use(express.static(path.resolve(frontendPath)))
    web.get('/env.js', async (_req, res) => {
        const retParts = ['window._env = {};']
        const keys = ['API_WEB_HOST', 'API_WEB_PORT', 'AUTOTRADING_ENABLED']

        for (const [k, v] of Object.entries(process.env)) {
            if (keys.includes(k)) {
                retParts.push(`window._env.${k} = '${v}';`)
            }
        }
        res.setHeader('Content-Type', 'application/javascript')
        res.send(retParts.join('\n'))
    })
    web.get('/settings.js', async (_req, res) => {
        const retParts = ['window._settings = {};']
        const keys: (keyof SettingsModelType)[] = [
            'hide_credits_from_site_list',
            'full_dates_on_race_list'
        ]

        keys.forEach(k => {
            const v = SettingsModel.get(k)

            let renderedValue: string | boolean = `'${v}'`
            if (v === true || v === false) {
                renderedValue = v
            }
            retParts.push(`window._settings.${k} = ${renderedValue};`)
        })
        res.setHeader('Content-Type', 'application/javascript')
        res.send(retParts.join('\n'))
    })
    web.use((_req, res) =>
        res.sendFile(path.resolve(`${frontendPath}/index.html`))
    )
    web.listen(
        Number(process.env.WEB_BIND_PORT),
        process.env.WEB_BIND_HOST,
        () => {
            logger.web(
                `Web (gui) started listening @ http://${process.env.WEB_BIND_HOST}:${process.env.WEB_BIND_PORT}`
            )
        }
    )
}

serveGui()
