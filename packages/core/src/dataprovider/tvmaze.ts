import {
    ReleaseName,
    ReleaseNameFullTitle,
    ReleaseNameTitle,
    TVMazeShow,
    TVMazeEpisodes,
    TVMazeSeasons
} from '@trd/shared/src/types'
import { DateTime } from 'luxon'
import * as util from 'util'

import { array_last } from '../helpers/data_types'
import { getCurrentYear, subMonths } from '../helpers/date'
import SettingsModel from '../models/settings'
import { COUNTRIES } from '../utility/locale'
import { getTitle, GetTitleResponse, transliterate } from '../utility/release'
import { DataProvider } from './data-provider'
import DataProviderResponse from './response'

export interface TVMazeData {
    id: number
    url: string
    title: string
    classification: string
    genres: string[]
    language: string
    status: string
    runtime: number
    premiered: number
    year: number
    total_seasons: number
    latest_season: number
    current_season: number
    last_season: number
    rating: number
    aired_in_last_6_months: boolean
    recent_seasons: number[]
    daily: boolean
    country: string
    country_code: string
    network: string
    web: boolean
    is_children_network: boolean
}

const threshhold = process.env.TVMAZE_SCORE_THRESHOLD
    ? parseFloat(process.env.TVMAZE_SCORE_THRESHOLD)
    : undefined

export default class TVMazeDataProvider extends DataProvider {
    protected namespace = 'tvmaze'
    protected expiry = 2592000 // 30 days

    static readonly MIN_SCORE_THRESHOLD = threshhold ?? 0.9
    static readonly minYear = 1928

    public getDefaults(_existing = {}) {
        return {}
    }

    protected getDeprecated(): string[] {
        return []
    }

    public async lookupByRlsnameCleaned(
        rlsnameFullTitle: ReleaseNameFullTitle,
        releaseTitleData?: GetTitleResponse,
        forceRefresh = false
    ): Promise<DataProviderResponse<unknown>> {
        if (!releaseTitleData) {
            releaseTitleData = {
                title: rlsnameFullTitle as unknown as ReleaseNameTitle,
                fullTitle: rlsnameFullTitle,
                country: null,
                year: null
            }
        }

        // TODO: try and extract year, and even country to help better lookup
        const cacheResponse = await super.getCacheResponse(
            releaseTitleData.fullTitle,
            forceRefresh,
            this.getGeneratedFields
        )
        if (cacheResponse?.result) {
            return cacheResponse
        }

        let rawData

        try {
            if (cacheResponse.approved) {
                rawData = await this.lookupById(cacheResponse.rawData['id'])
            } else {
                rawData = await this.findByTitle(releaseTitleData)
            }

            if (rawData?.id) {
                await this.save(releaseTitleData.fullTitle, rawData)

                return new DataProviderResponse(
                    true,
                    rawData,
                    cacheResponse.dataImmutable,
                    this.getGeneratedFields(rawData),
                    cacheResponse.approved,
                    this.logs
                )
            }
        } catch (_e) {
            // do nothing and revert to failure below
        }

        return new DataProviderResponse(
            false,
            null,
            cacheResponse.dataImmutable,
            undefined,
            cacheResponse.approved,
            this.logs
        )
    }

    public async lookup(
        rlsname: ReleaseName,
        forceRefresh = false
    ): Promise<DataProviderResponse<unknown>> {
        const releaseTitleData = getTitle(rlsname)
        return await this.lookupByRlsnameCleaned(
            releaseTitleData.fullTitle,
            releaseTitleData,
            forceRefresh
        )
    }

    public async lookupById(id: number) {
        let info = {}

        const show = await DataProvider.load<TVMazeShow>(
            'https://api.tvmaze.com/shows/' + encodeURIComponent(id)
        )
        if (show.id) {
            info = await this.extractData(show)
            return this.mergeData(info, this.getGeneratedFields(info), true)
        }

        return null
    }

    // TODO not needed?
    public static getStrictTitleFromTitleData(
        titleData: GetTitleResponse
    ): string {
        const { country } = titleData
        let { title: strictTitle }: { title: string } = titleData

        const tokens = strictTitle.split(' ')
        const lastToken = array_last(tokens)

        // remove years
        if (
            parseInt(lastToken) > TVMazeDataProvider.minYear &&
            parseInt(lastToken) <= getCurrentYear()
        ) {
            strictTitle = tokens.slice(0, -1).join(' ')
        }

        // remove country if it's there
        if (country?.length) {
            const tokens = strictTitle.split(' ')
            const lastToken = array_last(tokens)
            if (lastToken.toLowerCase() === country.toLowerCase()) {
                strictTitle = tokens.slice(0, -1).join(' ')
            }
        }

        return strictTitle
    }

    public async findByTitle(titleData: GetTitleResponse) {
        const { country } = titleData
        let { year } = titleData
        if (
            year &&
            (year < TVMazeDataProvider.minYear || year > getCurrentYear() + 1)
        ) {
            year = null
        }

        const strictTitle =
            TVMazeDataProvider.getStrictTitleFromTitleData(titleData)
        let strictTitleWithCountry = strictTitle
        let strictTitleWithFullCountry
        if (titleData.country) {
            strictTitleWithCountry = `${titleData.title} ${titleData.country}`
            if (titleData.country in COUNTRIES) {
                strictTitleWithFullCountry = `${titleData.title} ${
                    COUNTRIES[titleData.country]
                }`
            }
        }

        const url =
            'https://api.tvmaze.com/search/shows?q=' +
            encodeURIComponent(strictTitle)
        let response = undefined
        try {
            response = await DataProvider.load(url)
        } catch (_e) {
            response = undefined
        }

        let shows = response ?? []
        this.log(
            util.format('Searching tvmaze with strict title [%s]', strictTitle)
        )

        // if we are dealing with a country version - let's check it also...
        if (country?.length) {
            let response
            try {
                response = await DataProvider.load(
                    'https://api.tvmaze.com/search/shows?q=' +
                        encodeURIComponent(`${strictTitle} ${country}`)
                )
            } catch (_e) {
                response = undefined
            }
            const extraShows = response ?? []
            shows = [...shows, ...extraShows]
            this.log(
                util.format(
                    'Adding results from tvmaze due to found country of [%s]',
                    country
                )
            )
        }

        if (shows.length) {
            for (const show of shows) {
                const realTitleString = this.realTitle(show['show']['name'])

                if (!TVMazeDataProvider.passesScoreThreshold(show['score'])) {
                    this.log(
                        util.format(
                            'Checking show title [%s] but has too low score %s (required: %f)',
                            realTitleString,
                            show['score'],
                            TVMazeDataProvider.MIN_SCORE_THRESHOLD
                        )
                    )
                    continue
                }

                const showInfo = await this.extractData(show['show'])

                if (country) {
                    this.log(
                        util.format(
                            'Checking show title [%s] against [%s,%s,%s] (with country)',
                            realTitleString,
                            strictTitle,
                            strictTitleWithCountry,
                            strictTitleWithFullCountry
                        )
                    )
                    if (
                        TVMazeDataProvider.titlesMatch(realTitleString, [
                            strictTitle,
                            strictTitleWithCountry,
                            ...(strictTitleWithFullCountry
                                ? strictTitleWithFullCountry
                                : [])
                        ]) &&
                        showInfo.country_code == country
                    ) {
                        this.log(
                            util.format(
                                'Match (country): for show title [%s] against [%s,%s,%s]',
                                realTitleString,
                                strictTitle,
                                strictTitleWithCountry,
                                strictTitleWithFullCountry
                            )
                        )
                        return showInfo
                    }
                } else if (year) {
                    this.log(
                        util.format(
                            'Checking show title [%s] against [%s] (with year %d)',
                            realTitleString,
                            strictTitle,
                            year
                        )
                    )
                    if (
                        TVMazeDataProvider.titlesMatch(realTitleString, [
                            strictTitle
                        ]) &&
                        showInfo.premiered === year
                    ) {
                        this.log(
                            util.format(
                                'Match (year): for show title [%s] against [%s]',
                                realTitleString,
                                strictTitle
                            )
                        )
                        return showInfo
                    }
                } else {
                    this.log(
                        util.format(
                            'Checking show title [%s] against [%s]',
                            realTitleString,
                            strictTitle
                        )
                    )

                    if (
                        TVMazeDataProvider.titlesMatch(realTitleString, [
                            strictTitle
                        ])
                    ) {
                        this.log(
                            util.format(
                                'Match: for show title [%s] against [%s]',
                                realTitleString,
                                strictTitle
                            )
                        )
                        return await this.extractData(shows[0]['show'])
                    }
                }
            }
        }

        this.log('No results from tvmaze')

        return null
    }

    private realTitle(title: string): string {
        return transliterate(title.toLowerCase())
            .replace(/&/gi, 'and')
            .replace(/[^\w\d_\.\s-]+/giu, '')
    }

    private static passesScoreThreshold(score: number): boolean {
        return score > this.MIN_SCORE_THRESHOLD
    }

    private static titlesMatch(
        realTitle: string,
        tvmazeTitles: string[] = []
    ): boolean {
        for (const t of tvmazeTitles) {
            if (realTitle.toLowerCase() === t.toLowerCase()) {
                return true
            }
        }

        return false
    }

    private async extractData(showInfo: TVMazeShow): Promise<TVMazeData> {
        const premiered = showInfo.premiered
            ? parseInt(showInfo.premiered.slice(0, 4))
            : 0

        // defaults
        const info: TVMazeData = {
            daily: false,
            country: '',
            country_code: '',
            network: '',
            id: showInfo.id,
            url: showInfo.url,
            title: showInfo.name,
            classification: showInfo.type,
            genres: showInfo.genres,
            language: showInfo.language,
            status: showInfo.status,
            runtime: showInfo.runtime,
            premiered,
            year: premiered,
            total_seasons: 0,
            latest_season: 0,
            current_season: 0,
            last_season: 0,
            rating: 0,
            aired_in_last_6_months: false,
            recent_seasons: [],
            web: false,
            is_children_network: false
        }

        if (showInfo.schedule?.days && showInfo.schedule.days.length > 2) {
            info.daily = true
        }

        if (showInfo.rating?.average) {
            info.rating = showInfo.rating.average
        }

        // basic country guessing
        if (showInfo.network?.country) {
            info.network = showInfo.network.name
            info.country = showInfo.network.country.name
            info.country_code = showInfo.network.country.code
        } else if (showInfo.webChannel?.country) {
            info.network = showInfo.webChannel.name
            info.country = showInfo.webChannel.country.name
            info.country_code = showInfo.webChannel.country.code
        } else if (showInfo.webChannel?.name) {
            info.network = showInfo.webChannel.name

            // fall back to wikidata
            const endpointUrl = 'https://query.wikidata.org/sparql'
            const sparqlQuery = `
              SELECT ?item ?itemLabel ?country ?countryLabel ?ISO (COUNT(?countryReference) AS ?referenceCount) WHERE {
                ?item wdt:P31 wd:Q5398426;
                      rdfs:label "${showInfo.name}"@en.
                OPTIONAL {
                  ?item p:P495 ?countryStatement.
                  ?countryStatement ps:P495 ?country.
                  ?country wdt:P297 ?ISO.
                  OPTIONAL { ?countryStatement prov:wasDerivedFrom ?countryReference. }
                }
                SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
              }
              GROUP BY ?item ?itemLabel ?country ?countryLabel ?ISO
              ORDER BY DESC(?referenceCount)
            `

            let response
            try {
                response = await DataProvider.load(
                    endpointUrl +
                        '?query=' +
                        encodeURIComponent(sparqlQuery) +
                        '&format=json'
                )
            } catch (e) {
                this.log(`Failed to query wikidata: ${e.message}`)
            }
            if (
                response?.results?.bindings &&
                response?.results?.bindings?.[0]?.countryLabel &&
                response?.results?.bindings?.[0]?.ISO
            ) {
                let country =
                    response['results']['bindings'][0]['countryLabel']['value']
                switch (country) {
                    case 'United States of America':
                        country = 'United States'
                        break
                    default:
                }
                info.country = country
                info.country_code =
                    response['results']['bindings']['0']['ISO']['value']
            }
        }

        // clean up shit country codes...
        if (info.country_code === 'GB') {
            info.country_code = 'UK'
        }

        // TODO: run these two http in Promise.all for speed bonus
        // check current season
        const episodesResponse = DataProvider.load<TVMazeEpisodes>(
            'https://api.tvmaze.com/shows/' +
                encodeURIComponent(info.id) +
                '/episodes'
        )
        const seasonsResponse = DataProvider.load<TVMazeSeasons>(
            'https://api.tvmaze.com/shows/' +
                encodeURIComponent(info.id) +
                '/seasons'
        )

        let episodes: TVMazeEpisodes = [],
            seasons: TVMazeSeasons = []
        try {
            const promiseResult = await Promise.all([
                episodesResponse,
                seasonsResponse
            ])
            ;[episodes, seasons] = promiseResult
        } catch (_e) {
            //
        }

        if (episodes.length) {
            const mostRecentEpisodeData = episodes[episodes.length - 1]
            info.latest_season = mostRecentEpisodeData.season

            if (mostRecentEpisodeData['airdate']?.length) {
                const mostRecentEpisodeAirDate = DateTime.fromISO(
                    mostRecentEpisodeData.airdate
                )
                const sixMonthsAgo = subMonths(DateTime.now(), 6)
                if (mostRecentEpisodeAirDate > sixMonthsAgo) {
                    info.aired_in_last_6_months = true
                }
            }

            if (
                [
                    'To Be Determined',
                    'In Development',
                    'Running',
                    'Ending',
                    'Ended'
                ].includes(info.status) &&
                info.aired_in_last_6_months
            ) {
                info.current_season = mostRecentEpisodeData.season
                info.last_season =
                    info.current_season == 1 ? 0 : info.current_season - 1
            }
        }

        if (seasons.length) {
            info.total_seasons = seasons[seasons.length - 1].number

            // get recent seasons
            const nineMonthsAgo = subMonths(DateTime.now(), 9)
            for (const season of seasons) {
                if (season.endDate !== null) {
                    const ended = DateTime.fromISO(season.endDate)
                    if (ended > nineMonthsAgo) {
                        info.recent_seasons.push(season.number)
                    }
                }
            }
        }

        // check web show
        info.web = false
        if (showInfo.network == null && showInfo.webChannel?.id) {
            info.web = true
        }

        // Handle children's networks as tvmaze does not provide adequate information
        if (SettingsModel.exists('children_networks')) {
            const childrenNetworks = SettingsModel.get('children_networks').map(
                network => network.toLowerCase()
            )
            if (childrenNetworks.includes(info.network.toLowerCase())) {
                info.is_children_network = true
                info.genres.push('Children')
            }
        }

        return info
    }

    public getGeneratedFields(data) {
        const newFields = {
            is_scripted_english: false,
            is_children_network: false
        }
        const countries = [
            'United States',
            'Canada',
            'New Zealand',
            'United Kingdom'
        ]
        if (
            data['classification'] == 'Scripted' &&
            countries.includes(data.country) &&
            data['language'] == 'English'
        ) {
            newFields['is_scripted_english'] = true
        }
        if (SettingsModel.exists('children_networks')) {
            const childrenNetworks = SettingsModel.get('children_networks').map(
                network => network.toLowerCase()
            )
            if (childrenNetworks.includes(data['network'].toLowerCase())) {
                newFields['is_children_network'] = true
            }
        }

        return newFields
    }
}
