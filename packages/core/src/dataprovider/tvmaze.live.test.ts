import { ReleaseName, ReleaseNameFullTitle } from '@trd/shared/src/types'

import TVMazeDataProvider from './tvmaze'

const tv = new TVMazeDataProvider()

describe('Test TVMazeDataProvider', () => {
    describe('lookupByName', () => {
        test('id + classification + country #1', async () => {
            const result = await tv.lookupByRlsnameCleaned(
                'The Mindy Project' as ReleaseNameFullTitle,
                undefined,
                true
            )
            expect(result.get('id')).toBe(75)
            expect(result.get('classification')).toBe('Scripted')
            expect(result.get('country')).toBe('United States')
        })
        test('Show with country in name (US)', async () => {
            const result = await tv.lookupByRlsnameCleaned(
                'The Apprentice US' as ReleaseNameFullTitle,
                undefined,
                true
            )
            expect(result.get('id')).toBe(18754)
            expect(result.get('classification')).toBe('Reality')
            expect(result.get('country')).toBe('United States')
        })
        test('Show with country in name (US) #2', async () => {
            const result = await tv.lookupByRlsnameCleaned(
                'The Librarians US' as ReleaseNameFullTitle,
                undefined,
                true
            )
            expect(result.get('id')).toBe(340)
            expect(result.get('classification')).toBe('Scripted')
            expect(result.get('country')).toBe('United States')
        })
        test('Show with country in name (UK) #1', async () => {
            const result = await tv.lookupByRlsnameCleaned(
                'The Apprentice UK' as ReleaseNameFullTitle,
                undefined,
                true
            )
            expect(result.get('id')).toBe(2958)
            expect(result.get('classification')).toBe('Reality')
            expect(result.get('country')).toBe('United Kingdom')
        })
        test('Show with country in name (UK) #2', async () => {
            const result = await tv.lookupByRlsnameCleaned(
                'The Voice UK' as ReleaseNameFullTitle,
                undefined,
                true
            )
            expect(result.get('id')).toBe(4859)
            expect(result.get('classification')).toBe('Reality')
            expect(result.get('country')).toBe('United Kingdom')
        })
        test('Season special', async () => {
            const result = await tv.lookup(
                'Bates.Motel.S04.Catch.Up.Special.720p.HDTV.x264-Group' as ReleaseName,
                true
            )
            expect(result.get('id')).toBe(195)
        })
        test('Show with year in name', async () => {
            const result = await tv.lookupByRlsnameCleaned(
                'The Odd Couple 2015' as ReleaseNameFullTitle,
                undefined,
                true
            )
            expect(result.get('id')).toBe(739)
        })
        test('HD show', async () => {
            const result = await tv.lookup(
                'Gilmore.Girls.S01E01.720p.HDTV.x264-group' as ReleaseName,
                true
            )
            expect(result.get('id')).toBe(525)
        })
        test('Obscure lookup #1', async () => {
            const result = await tv.lookup(
                '60.Days.In.S01E01.720p.HDTV.x264-group' as ReleaseName,
                true
            )
            expect(result.get('id')).toBe(13105)
        })
        test('Obscure lookup #2', async () => {
            const result = await tv.lookup(
                'Hooten.And.The.Lady.S01E01.1080p.HDTV.x264-group' as ReleaseName,
                true
            )
            expect(result.get('id')).toBe(19616)
        })
        test('Obscure lookup #3', async () => {
            const result = await tv.lookup(
                'Crazy.Ex-Girlfriend.S01E01.1080p.HDTV.X264-group' as ReleaseName,
                true
            )
            expect(result.get('id')).toBe(2260)
        })
        test('Obscure lookup #4', async () => {
            const result = await tv.lookup(
                'Seaquest.DSV.S02E08.PROPER.720p.BluRay.x264-group' as ReleaseName,
                true
            )
            expect(result.get('id')).toBe(3292)
        })
        test('Current season for an ended show', async () => {
            const result = await tv.lookup(
                'Deadbeat.S03E13.720p.HDTV.x264-test' as ReleaseName,
                true
            )
            expect(result.get('current_season')).toBe(0)
        })
        test('Test transliteration', async () => {
            const result = await tv.lookup(
                'Renees.Brygga.S01E01.SWEDiSH.720p.HDTV.x264-test' as ReleaseName,
                true
            )
            expect(result.get('id')).toBe(25567)
        })
        test('Test netflix show country', async () => {
            const result = await tv.lookup(
                'The.Crown.S02E01.720p.WEBRIP.x264-test' as ReleaseName,
                true
            )
            expect(result.get('country')).toBe('United Kingdom')
            expect(result.get('country_code')).toBe('UK')
        })
        test('Test foreign netflix show', async () => {
            const result = await tv.lookup(
                'El.Chapo.S02E01.720p.WEBRIP.x264-test' as ReleaseName,
                true
            )
            expect(result.get('country')).toBe('United States')
            expect(result.get('country_code')).toBe('US')
        })
        test('Test another netflix show', async () => {
            const result = await tv.lookup(
                'Orange.Is.The.New.Black.S02E01.720p.WEBRIP.x264-test' as ReleaseName,
                true
            )
            expect(result.get('country')).toBe('United States')
            expect(result.get('country_code')).toBe('US')
        })
        test('Test another netflix show again', async () => {
            const result = await tv.lookup(
                'Stranger.Things.S02E01.720p.WEBRIP.x264-test' as ReleaseName,
                true
            )
            expect(result.get('country')).toBe('United States')
            expect(result.get('country_code')).toBe('US')
            // Test extract votes, floating point number
            expect(result.get('rating')).toBeGreaterThan(0.1)
            // Test children network to be false by default
            expect(result.get('is_children_network')).toBe(false)
            expect(result.get('genres')).not.toContain('Children')
        })
        test('Test extract rating from show with too few votes', async () => {
            const result = await tv.lookup(
                'Corto.Maltes.S01E01.720p.WEBRIP.x264-test' as ReleaseName,
                true
            )
            // rating on the show might changed at a later date, but hopefully obscure enough not to
            expect(result.get('rating')).toBe(0)
        })

        // COMMENTED OUT AS THIS DOESNT WORK
        // test('Show that ends in something that looks like a country', async () => {
        //     const result = await tv.lookup(
        //         'Helicopter.ER.S03E03.720p.WEB.x264-UNDERBELLY',
        //         true
        //     )
        //     expect(result.get('id')).toBe(21760)
        // })
    })

    describe('lookupById', () => {
        test('pass in id to lookup', async () => {
            const result = await tv.lookupById(5496)
            expect(result['id']).toBe(5496)
            expect(result['is_scripted_english']).toBe(true)
        })
        test('Test children network CBeeBies to set children network to true and push children to genres', async () => {
            const result = await tv.lookupById(15666)
            expect(result['is_children_network']).toBe(true)
            expect(result['genres']).toContain('Children')
        })
    })
})
