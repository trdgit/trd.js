import { ReleaseName } from '@trd/shared/src/types'

import {
    getCurrentDayOfWeek,
    getCurrentHours,
    getCurrentMinutes,
    getCurrentYear,
    getLastYear,
    getNextYear
} from '../helpers/date'
import { DataProvider } from './data-provider'
import DataProviderResponse from './response'

export interface DateTimeData {
    year: number
    last_year: number
    last_2_years: [number, number]
    last_3_years: [number, number, number]
    last_4_years: [number, number, number, number]
    last_5_years: [number, number, number, number, number]
    next_year: number
    hour: number
    minute: number
    day_of_week: number
}

export default class DateTimeDataProvider extends DataProvider {
    protected namespace = 'datetime'

    public getDefaults(existing = {}): DateTimeData {
        return {
            ...existing,
            year: null,
            last_year: null,
            last_2_years: null,
            last_3_years: null,
            last_4_years: null,
            last_5_years: null,
            next_year: null,
            hour: null,
            minute: null,
            day_of_week: null
        }
    }

    public getDeprecated(): string[] {
        return []
    }

    public lookup(
        rlsname: ReleaseName,
        _forceRefresh = false
    ): Promise<DataProviderResponse<DateTimeData>> {
        const info = this.getDefaults()

        info['this_year'] = getCurrentYear()
        info['year'] = info['this_year']
        info['last_year'] = getLastYear()
        info['last_2_years'] = [info['this_year'], info['last_year']]
        info['last_3_years'] = [
            info['this_year'],
            info['last_year'],
            info['last_year'] - 1
        ]
        info['last_4_years'] = [
            info['this_year'],
            info['last_year'],
            info['last_year'] - 1,
            info['last_year'] - 2
        ]
        info['last_5_years'] = [
            info['this_year'],
            info['last_year'],
            info['last_year'] - 1,
            info['last_year'] - 2,
            info['last_year'] - 3
        ]
        info['next_year'] = getNextYear()

        info['hour'] = getCurrentHours()
        info['minute'] = getCurrentMinutes()
        info['day_of_week'] = getCurrentDayOfWeek()

        return new Promise(resolve =>
            resolve(new DataProviderResponse(true, info))
        )
    }
}
