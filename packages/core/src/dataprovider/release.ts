import { ReleaseName } from '@trd/shared/src/types'

import { getGroup, getTitle } from '../utility/release'
import { DataProvider } from './data-provider'
import DataProviderResponse from './response'

export interface ReleaseData {
    episode: number
    season: number
    season_episode: string
    codec: string
    source: string
    resolution: string
    language: string
    multi: boolean
    internal: boolean
    range: string
    cleaned: string
    cleaned_joined: string
    group: string
}

export default class ReleaseNameDataProvider extends DataProvider {
    private static EPISODE_FORM1 = /[\s._-](S\d+E(\d+)-?E(\d+))[\s._-]/i
    private static EPISODE_FORM2 =
        /[\s._-]((?:S\d+)?(?:Episode|E|Part)\.?(\d+))[\s._-]/i
    private static EPISODE_FORM3 = /[\s._-](\d+x(\d+))[\s._-]/i
    private static EPISODE_FORM4 = /[\s._-](\d{4}\.(\d{2}\.\d{2}))[\s._-]/i
    private static LANGUAGES =
        'Afar|Abkhaz|Avestan|Afrikaans|Akan|Amharic|Aragonese|Arabic|Assamese|Avaric|Aymara|Azerbaijani|Bashkir|Belarusian|Bulgarian|Bihari|Bislama|Bambara|Bengali|Tibetan|Breton|Bosnian|Catalan|Valencian|Chechen|Chamorro|Corsican|Cree|Czech|Chuvash|Welsh|Danish|German|Divehi|Dhivehi|Maldivian|Dzongkha|Ewe|Greek|English|Esperanto|Spanish|Castilian|Estonian|Basque|Persian|Fula|Fulah|Pulaar|Pular|Finnish|Fijian|Faroese|French|Frisian|Irish|Gaelic|Galician|Gujarati|Manx|Hausa|Hebrew|Hindi|Hiri Motu|Croatian|Haitian|Hungarian|Armenian|Herero|Interlingua|Indonesian|Interlingue|Igbo|Nuosu|Inupiaq|Ido|Icelandic|Italian|Inuktitut|Japanese|Javanese|Georgian|Kongo|Kikuyu|Gikuyu|Kwanyama|Kuanyama|Kazakh|Kalaallisut|Greenlandic|Khmer|Kannada|Korean|Kanuri|Kashmiri|Kurdish|Komi|Cornish|Kirghiz|Kyrgyz|Latin|Luxembourgish|Letzeburgesch|Luganda|Limburgish|Limburgan|Limburger|Lingala|Lao|Lithuanian|Luba-Katanga|Latvian|Malagasy|Marshallese|Maori|Macedonian|Malayalam|Mongolian|Malay|Maltese|Burmese|Nauru|Nepali|Ndonga|Dutch|Norwegian|Nynorsk|Navajo|Navaho|Chichewa|Chewa|Nyanja|Occitan|Ojibwe|Ojibwa|Oromo|Oriya|Ossetian|Ossetic|Panjabi|Punjabi|Pali|Polish|Pashto|Pushto|Portuguese|Quechua|Romansh|Kirundi|Romanian|Moldavian|Moldovan|Russian|Kinyarwanda|Sanskrit|Sardinian|Sindhi|Sami|Sango|Sinhala|Sinhalese|Slovak|Slovene|Samoan|Shona|Somali|Albanian|Serbian|Swati|Sundanese|Swedish|Swahili|Tamil|Telugu|Tajik|Thai|Tigrinya|Turkmen|Tagalog|Tswana|Turkish|Tsonga|Tatar|Twi|Tahitian|Uighur|Uyghur|Ukrainian|Urdu|Uzbek|Venda|Vietnamese|Walloon|Wolof|Xhosa|Yiddish|Yoruba|Zhuang|Chuang|Chinese|Zulu'

    protected namespace = 'rlsname'
    protected static defaults = {
        episode: null,
        season: null,
        season_episode: null,
        codec: null,
        source: null,
        resolution: null,
        language: null,
        multi: false,
        internal: false,
        range: null,
        cleaned: null,
        cleaned_joined: null,
        group: null
    }

    public static isTvShow(rlsname: ReleaseName): boolean {
        const regex = new RegExp(
            ReleaseNameDataProvider.EPISODE_FORM1.source +
                '|' +
                ReleaseNameDataProvider.EPISODE_FORM2.source +
                '|' +
                ReleaseNameDataProvider.EPISODE_FORM3.source +
                '|' +
                ReleaseNameDataProvider.EPISODE_FORM4.source
        )
        return regex.test(rlsname)
    }

    public getDefaults(existing: object = {}) {
        return {
            ...ReleaseNameDataProvider.defaults,
            ...existing
        }
    }

    public static getDefaultsStatic(existing: object = {}) {
        return {
            ...ReleaseNameDataProvider.defaults,
            ...existing
        }
    }

    protected getDeprecated(): string[] {
        return []
    }

    public async lookup(
        rlsname: ReleaseName,
        _forceRefresh = false
    ): Promise<DataProviderResponse<ReleaseData>> {
        let info = this.getDefaults()
        info['episode'] = ReleaseNameDataProvider.extractEpisode(rlsname)
        info['season'] = ReleaseNameDataProvider.extractSeason(rlsname)
        info['season_episode'] = `${info['season']}x${info['episode']}`

        // media data
        info['codec'] = ReleaseNameDataProvider.extractCodec(rlsname)
        info['source'] = ReleaseNameDataProvider.extractTVSource(rlsname)
        info['resolution'] = ReleaseNameDataProvider.extractResolution(rlsname)
        info['range'] = ReleaseNameDataProvider.extractRange(rlsname)

        // try and get the country
        info = ReleaseNameDataProvider.extractOther(rlsname, info)

        // dupe stuff
        info['repeat'] = ReleaseNameDataProvider.extractRepeatExtras(rlsname)

        // meta-data
        info.cleaned = getTitle(rlsname).fullTitle
        info.cleaned_joined = info['cleaned'].split(' ').join('.')
        info.group = getGroup(rlsname)

        return new Promise(resolve =>
            resolve(new DataProviderResponse(true, info))
        )
    }

    public static lookupStatic(rlsname: ReleaseName): ReleaseData {
        let info = ReleaseNameDataProvider.getDefaultsStatic()
        info['episode'] = ReleaseNameDataProvider.extractEpisode(rlsname)
        info['season'] = ReleaseNameDataProvider.extractSeason(rlsname)
        info['season_episode'] = `${info['season']}x${info['episode']}`

        // media data
        info['codec'] = ReleaseNameDataProvider.extractCodec(rlsname)
        info['source'] = ReleaseNameDataProvider.extractTVSource(rlsname)
        info['resolution'] = ReleaseNameDataProvider.extractResolution(rlsname)
        info['range'] = ReleaseNameDataProvider.extractRange(rlsname)

        // try and get the country
        info = ReleaseNameDataProvider.extractOther(rlsname, info)

        // dupe stuff
        info['repeat'] = ReleaseNameDataProvider.extractRepeatExtras(rlsname)

        // meta-data
        info['cleaned'] = getTitle(rlsname).fullTitle
        info['cleaned_joined'] = info['cleaned'].split(' ').join('.')
        info['group'] = getGroup(rlsname)

        return info
    }

    // TODO: implement this
    // private extractRegion(rlsname: ReleaseName) {}

    public static extractOther(rlsname: ReleaseName, otherInfo) {
        const episodeToken =
            ReleaseNameDataProvider.extractEpisodeToken(rlsname)
        if (episodeToken !== null && episodeToken !== '') {
            // Split on dot, space and underscore
            const firstSplit = rlsname.split(
                new RegExp(episodeToken + '[\\s_.]', 'i')
            )

            if (firstSplit[1] === undefined) {
                return otherInfo
            }

            // Remove the group
            let cleanedRelease = firstSplit[1].replace(new RegExp('-\\w+$'), '')

            // Replace already mapped with nothing
            if (otherInfo['codec']) {
                cleanedRelease = cleanedRelease.replace(
                    new RegExp(otherInfo['codec'], 'i'),
                    ''
                )
            }
            if (otherInfo['source']) {
                cleanedRelease = cleanedRelease.replace(
                    new RegExp(otherInfo['source'], 'i'),
                    ''
                )
            }
            if (otherInfo['resolution']) {
                cleanedRelease = cleanedRelease.replace(
                    new RegExp(otherInfo['resolution'], 'i'),
                    ''
                )
            }

            const possibleLanguages: string[] =
                this.LANGUAGES.toUpperCase().split('|')
            const finalSplit = cleanedRelease.split(/[\s_.]+/i)

            for (let x of finalSplit) {
                x = x.toUpperCase()

                if (!otherInfo['language'] && possibleLanguages.includes(x)) {
                    otherInfo['language'] = x
                }
                if (x === 'INTERNAL' || x == 'INT') {
                    otherInfo['internal'] = true
                }
            }
            //     $possibleLanguages = explode('|', strtoupper(ReleaseName::LANGUAGES));
            //     $finalSplit = preg_split('/[\s_.]+/i', $cleanedRelease);
            //     // try and get the language now
            //     foreach ($finalSplit as $tok) {
            //         $ut = strtoupper($tok);
            //         if (empty($otherInfo['language']) and in_array($ut, $possibleLanguages)) {
            //             $otherInfo['language'] = $tok;
            //         }
            //         if ($ut === 'INTERNAL' or $ut == 'INT') {
            //             $otherInfo['internal'] = true;
            //         }
            //     }
        }

        if (rlsname.toUpperCase().includes('MULTI')) {
            otherInfo['multi'] = true
        }

        if (getGroup(rlsname).toUpperCase().endsWith('_INT')) {
            otherInfo['internal'] = true
        }

        return otherInfo
    }

    public static extractResolution(rlsname: ReleaseName) {
        const matches = rlsname.match(
            /[\s._-](720P|1080P|1280P|1440P|1920P|2160P|2300P|2700P|2880P)[\s._-]/i
        )
        if (matches && matches[1]) {
            return matches[1].toUpperCase()
        }

        return null
    }

    public static extractCodec(rlsname: ReleaseName) {
        const matches = rlsname.match(/[\s._-]([xh]26[45]|xvid|VP[89])[\s._-]/i)
        if (matches && matches[1]) {
            return matches[1].toUpperCase()
        }
        return null
    }

    public static extractRange(rlsname: ReleaseName) {
        const matches = rlsname.match(/[\s._-](DV\.HDR|HDR|DV|HLG)[\s._-]/i)
        if (matches && matches[1]) {
            return matches[1].toUpperCase()
        }

        return null
    }

    public static extractTVSource(rlsname: ReleaseName) {
        let matches = rlsname.match(
            /[\s._-]([AU]?HDTV|AUHDTV|PDTV|DSR|WEBRIP|WEB)[\s._-]/i
        )
        if (matches && matches[1]) {
            return matches[1].toUpperCase()
        } else if (
            rlsname.match(
                /[\s._-](((720p|1080p)\.(PURE\.)?M?BLURAY)|COMPLETE(\.PURE)?\.M?BLURAY)/i
            )
        ) {
            return 'BLURAY'
        } else if (
            rlsname.match(
                /[\s._-]((2160p\.UHD\.M?BLURAY)|COMPLETE(\.UHD)?\.M?BLURAY)/i
            )
        ) {
            return 'UHD.BLURAY'
        } else {
            matches = rlsname.match(/[\s._-](DVDRIP|BDRIP)/i)
            if (matches && matches[1]) {
                return matches[1].toUpperCase()
            }
        }

        return null
    }

    public static extractEpisode(rlsname: ReleaseName) {
        let matches = rlsname.match(this.EPISODE_FORM1)
        if (matches && matches[2] && matches[3]) {
            if (matches[2][0] == '0' && matches[3]) {
                // E01-02 -> 0102
                return matches[2].toString() + matches[3].toString()
            }
            // otherwise treat as integer
            return parseInt(matches[2].toString() + matches[3].toString())
        }

        matches = rlsname.match(this.EPISODE_FORM2)
        if (matches && matches[2]) {
            return parseInt(matches[2])
        }

        matches = rlsname.match(this.EPISODE_FORM3)
        if (matches && matches[2]) {
            return matches[2]
        }

        matches = rlsname.match(this.EPISODE_FORM4)
        if (matches && matches[2]) {
            return matches[2]
        }

        return null
    }

    public static extractEpisodeToken(rlsname: ReleaseName) {
        let matches = rlsname.match(this.EPISODE_FORM1)
        if (matches && matches[1]) {
            return matches[1]
        }
        matches = rlsname.match(this.EPISODE_FORM2)
        if (matches && matches[1]) {
            return matches[1]
        }
        matches = rlsname.match(this.EPISODE_FORM3)
        if (matches && matches[1]) {
            return matches[1]
        }
        matches = rlsname.match(this.EPISODE_FORM4)
        if (matches && matches[1]) {
            return matches[1]
        }

        return null
    }

    public static extractSeason(rlsname: ReleaseName) {
        let matches = rlsname.match(
            /[\s\._]S(\d+)?(?:Episode|E)\d+(-?E\d+)?[\s\._]/i
        )
        if (matches && matches[1]) {
            return parseInt(matches[1])
        }

        matches = rlsname.match(/[\s\._](\d+)x\d+[\s\._]/i)
        if (matches && matches[1]) {
            return parseInt(matches[1])
        }

        matches = rlsname.match(/[\s\._](\d{4})\.\d{2}\.\d{2}[\s\._]/i)
        if (matches && matches[1]) {
            return parseInt(matches[1])
        }

        matches = rlsname.match(/[\s\._]S(\d+)[\s\._]/i)
        if (matches && matches[1]) {
            return parseInt(matches[1])
        }

        return null
    }

    public static extractRepeatExtras(rlsname: ReleaseName) {
        const lookup = ['REAL.PROPER', 'PROPER', 'RERIP', 'REPACK']

        const rlsnameWithoutGroup = rlsname
            .split(/[_.-]+/)
            .slice(0, -1)
            .join('.')

        for (const x of lookup) {
            if (rlsnameWithoutGroup.toUpperCase().includes(x)) {
                return x
            }
        }

        return null
    }
}
