import { ReleaseNameFullTitle } from '@trd/shared/src/types'

import * as dataCacheRepository from '../repository/data'
import { DataProvider } from './data-provider'
import IMDBDataProvider, { IMDBData } from './imdb'
import { serializeDataCache } from '../helpers/serialize.data_cache'

const mockDataCache = (response: dataCacheRepository.GetDataCacheResponse) => {
    jest.spyOn(dataCacheRepository, 'getDataCache').mockResolvedValueOnce(
        response,
    )
}

const testImdbId = 'tt0120689'
const imdbFixture: IMDBData = {
    id: testImdbId,
    imageurl: 'foo',
    imdbid: testImdbId,
    title: 'Fake movie',
    url: 'abc',
    genres: ['Drama'],
    language: 'English',
    language_primary: 'English',
    languages: ['English'],
    countries: ['Canada'],
    country: 'Canada',
    rating: 7.1,
    votes: 1000,
    stv: false,
    limited: false,
    series: false,
    runtime: 'Forever',
    aka: 'aka',
    year: 1999,
    poster_hash: 'hash',
    screens_uk: 1000,
    screens_us: 1000,
    layout: 1,
    type: '',
}

describe('IMDB fixtures', () => {
    beforeEach(() => {
        jest.spyOn(DataProvider.prototype, 'save').mockImplementation()
        jest.spyOn(DataProvider.prototype, 'log').mockImplementation()
        jest.spyOn(
            IMDBDataProvider.prototype,
            'findPossibleIMDBIDs',
        ).mockResolvedValue(['tt0120689'])
    })

    afterEach(() => {
        jest.clearAllMocks()
    })

    describe('with a cache', () => {
        describe('approved = 1', () => {
            let extractDataFromIMDBIdSpy

            beforeEach(() => {
                mockDataCache({
                    data: serializeDataCache(imdbFixture),
                    data_immutable: '',
                    approved: 1,
                })

                extractDataFromIMDBIdSpy = jest
                    .spyOn(IMDBDataProvider.prototype, 'extractDataFromIMDBId')
                    .mockResolvedValue(imdbFixture)
            })

            it('should not extract from imdb.com when forceRefresh = false', async () => {
                const imdb = new IMDBDataProvider()
                const response = await imdb.lookupByRlsnameCleaned(
                    'the green mile 1999' as ReleaseNameFullTitle,
                    undefined,
                    false,
                )

                expect(extractDataFromIMDBIdSpy).not.toHaveBeenCalled()
                expect(response).toMatchSnapshot()
            })
            it('should extract from imdb.com when forceRefresh = true', async () => {
                const imdb = new IMDBDataProvider()
                const response = await imdb.lookupByRlsnameCleaned(
                    'the green mile 1999' as ReleaseNameFullTitle,
                    undefined,
                    true,
                )

                expect(extractDataFromIMDBIdSpy).toBeCalledWith(testImdbId)
                expect(response).toMatchSnapshot()
            })
        })

        describe('approved = 0', () => {
            let extractDataFromIMDBIdSpy

            beforeEach(() => {
                mockDataCache({
                    data: serializeDataCache(imdbFixture),
                    data_immutable: '',
                    approved: 0,
                })

                extractDataFromIMDBIdSpy = jest
                    .spyOn(IMDBDataProvider.prototype, 'extractDataFromIMDBId')
                    .mockResolvedValue(imdbFixture)
            })

            it('should not extract from imdb.com when forceRefresh = false', async () => {
                const imdb = new IMDBDataProvider()
                const response = await imdb.lookupByRlsnameCleaned(
                    'the green mile 1999' as ReleaseNameFullTitle,
                    undefined,
                    false,
                )

                expect(extractDataFromIMDBIdSpy).not.toHaveBeenCalled()
                expect(response).toMatchSnapshot()
            })
            it('should extract from imdb.com when forceRefresh = true', async () => {
                const imdb = new IMDBDataProvider()
                const response = await imdb.lookupByRlsnameCleaned(
                    'the green mile 1999' as ReleaseNameFullTitle,
                    undefined,
                    true,
                )

                expect(extractDataFromIMDBIdSpy).toBeCalledWith(testImdbId)
                expect(response).toMatchSnapshot()
            })
        })
    })
})

type TestGroup = { title: string; expectation: string }
const expectations: TestGroup[] = [
    { title: 'The Green Mile', expectation: 'the green mile' },
    { title: 'Whack-a-mole', expectation: 'whack-a-mole' },
    { title: "C'mon c'mon", expectation: 'cmon cmon' },
    {
        title: 'Once Upon a Time... in Hollywood',
        expectation: 'once upon a time in hollywood',
    },
    {
        title: 'The Hobbit: An Unexpected Journey',
        expectation: 'the hobbit an unexpected journey',
    },
    {
        title: '¡Casa Bonita Mi Amor!',
        expectation: 'casa bonita mi amor',
    },
    {
        title: 'Have Rocket -- Will Travel',
        expectation: 'have rocket will travel',
    },
    {
        title: 'The 4:30 Movie',
        expectation: 'the 4 30 movie',
    },
]
test.each(expectations)(
    'IMDBDataProvider.clean_movie_title($title)',
    ({ title, expectation }) => {
        expect(IMDBDataProvider._clean_movie_title(title)).toBe(expectation)
    },
)
