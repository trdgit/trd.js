import { ReleaseName } from '@trd/shared/src/types'

import MusicDataProvider from './music'

describe('Test static methods of MusicDataProvider', () => {
    describe('Test extractLanguage', () => {
        test('SP', () => {
            expect(
                MusicDataProvider.extractLanguage(
                    'ARTIST_-_ALBUM_TITLE-(12345678)-SINGLE-WEB-SP-2021-XxXx' as ReleaseName
                )
            ).toBe('SP')
        })
        test('Default to English', () => {
            expect(
                MusicDataProvider.extractLanguage(
                    'ARTIST_-_ALBUM_TITLE-(12345678)-SINGLE-WEB-2021-XxXx' as ReleaseName
                )
            ).toBe('English')
        })
    })
    describe('Test extractSource', () => {
        test('WEB', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST_-_ALBUM_TITLE-(12345678)-SINGLE-WEB-SP-2021-XxXx' as ReleaseName
                )
            ).toBe('WEB')
        })
        test('SAT -> Live', () => {
            expect(
                MusicDataProvider.extractSource(
                    'Test-Thing_FG-SAT-04-26-2021-GRP' as ReleaseName
                )
            ).toBe('Live')
        })
        test('CDEP -> CD', () => {
            expect(
                MusicDataProvider.extractSource(
                    'Absolute_Test_Artist-Test_Album-Limited_Edition-CDEP-2021-GRP' as ReleaseName
                )
            ).toBe('CD')
        })
        test('LINE -> Live', () => {
            expect(
                MusicDataProvider.extractSource(
                    'Dj_Dummy-Dummy_Sessions-LINE-04-24-2021-GRP' as ReleaseName
                )
            ).toBe('Live')
        })
        test('12INCH -> Vinyl', () => {
            expect(
                MusicDataProvider.extractSource(
                    'Dj_Dummy-Dummy_Sessions-12INCH_VINYL-04-24-2021-GRP' as ReleaseName
                )
            ).toBe('Vinyl')
            expect(
                MusicDataProvider.extractSource(
                    'Dj_Dummy-Dummy_Sessions-(12INCH-VINYL)-04-24-2021-GRP' as ReleaseName
                )
            ).toBe('Vinyl')
        })
        test('(WEB) -> Web', () => {
            expect(
                MusicDataProvider.extractSource(
                    'Dj_Dummy-Dummy_Sessions-(WEB)-2017-GRP' as ReleaseName
                )
            ).toBe('WEB')
        })
        test('3BD -> Bluray', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST_-_ALBUM_TITLE-(12345678)-DELUXE_EDITION_BOXSET-3BD-2016-GRP' as ReleaseName
                )
            ).toBe('Bluray')
        })
        test('3CDR -> CD', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST_-_ALBUM_TITLE-(12345678)-DELUXE_EDITION_BOXSET-3CDR-2016-GRP' as ReleaseName
                )
            ).toBe('CD')
        })
        test('DVDS -> DVD', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTST_-_ALBUM_TITLE-Ahmad_1981-DVDS-2004-GRP' as ReleaseName
                )
            ).toBe('DVD')
        })
        test('3K7 -> Tape', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST-ALBUM_TITLE-3K7-2004-GRP' as ReleaseName
                )
            ).toBe('Tape')
        })
        test('7.inch.vinyl -> Vinyl', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST_-_ALBUM_TITLE-7.Inch.Vinyl-2016-GRP' as ReleaseName
                )
            ).toBe('Vinyl')
        })
        test('2EP-FLAC -> Vinyl', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST_-_ALBUM_TITLE-2EP-FLAC-2016-GRP' as ReleaseName
                )
            ).toBe('Vinyl')
        })
        test('Flash -> Flash', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST-ALBUM_TITLE-FLASH-2022-GRP' as ReleaseName
                )
            ).toBe('Flash')
        })
        test('DAT -> Other', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST_-_ALBUM_TITLE-REPACK-DAT-2016-GRP' as ReleaseName
                )
            ).toBe('Other')
        })
        test('No match -> CD', () => {
            expect(
                MusicDataProvider.extractSource(
                    'ARTIST_-_ALBUM_TITLE-2016-GRP' as ReleaseName
                )
            ).toBe('CD')
        })
    })

    describe('Test extractYear', () => {
        test('2021', () => {
            expect(
                MusicDataProvider.extractYear(
                    'ARTIST_-_ALBUM_TITLE-(12345678)-SINGLE-WEB-SP-2021-XxXx' as ReleaseName
                )
            ).toBe(2021)
            expect(
                MusicDataProvider.extractYear(
                    'ARTIST-1999-(12345678)-WEB-2021-XxXx' as ReleaseName
                )
            ).toBe(2021)
        })
        test('No year', () => {
            expect(
                MusicDataProvider.extractYear(
                    'ARTIST_-_ALBUM_TITLE-(12345678)-SINGLE-WEB-XxXx' as ReleaseName
                )
            ).toBe(null)
        })
    })

    describe('Test extractDiskCount', () => {
        test('4 disks', () => {
            expect(
                MusicDataProvider.extractDiskCount(
                    'Mr_Dummy-My_Nonexistent_Album-Remastered_Deluxe_Edition_Boxset-4CD-2020-GRP' as ReleaseName
                )
            ).toBe(4)
        })
        test('No CD count', () => {
            expect(
                MusicDataProvider.extractDiskCount(
                    'Mr_Dummy-My_Nonexistent_Album-Remastered_Deluxe_Edition_Boxset-2020-GRP' as ReleaseName
                )
            ).toBe(1)
        })
        test('3CDR -> 3 disks', () => {
            expect(
                MusicDataProvider.extractDiskCount(
                    'ARTIST_-_ALBUM_TITLE-(12345678)-DELUXE_EDITION_BOXSET-3CDR-2016-GRP' as ReleaseName
                )
            ).toBe(3)
        })
    })
})
