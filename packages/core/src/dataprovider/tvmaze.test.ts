import { ReleaseNameFullTitle } from '@trd/shared/src/types'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import * as dataCacheRepository from '../repository/data'
import { DataProvider } from './data-provider'
import TVMazeDataProvider, { TVMazeData } from './tvmaze'
import { serializeDataCache } from '../helpers/serialize.data_cache'

const mockDataCache = (response: dataCacheRepository.GetDataCacheResponse) => {
    jest.spyOn(dataCacheRepository, 'getDataCache').mockResolvedValueOnce(
        response
    )
}

const testTvmazeId = 82
const tvmazeFixture: TVMazeData = {
    id: testTvmazeId,
    title: 'Fake show',
    url: 'abc',
    genres: ['Drama'],
    language: 'English',
    country: 'Canada',
    rating: 7.1,
    year: 1999,
    premiered: 1999,
    classification: 'Scripted',
    status: 'Ended',
    runtime: 1234,
    total_seasons: 8,
    current_season: 0,
    latest_season: 8,
    last_season: 8,
    recent_seasons: [],
    aired_in_last_6_months: false,
    network: 'HBO',
    daily: false,
    country_code: 'US',
    web: false,
    is_children_network: false
}

const tv = new TVMazeDataProvider()

describe('Test TVMazeDataProvider', () => {
    describe('error handling', () => {
        beforeEach(() => {
            jest.spyOn(DataProvider.prototype, 'save').mockImplementation()
            jest.spyOn(DataProvider.prototype, 'log').mockImplementation()
            jest.spyOn(
                TVMazeDataProvider.prototype,
                'getGeneratedFields'
            ).mockReturnValue({
                is_scripted_english: true,
                is_children_network: false
            })
            mockDataCache({
                data: '',
                data_immutable: '',
                approved: 0
            })
        })

        afterEach(() => {
            jest.restoreAllMocks()
            jest.clearAllMocks()
        })

        test('timeout', async () => {
            const mock = new MockAdapter(axios)
            mock.onGet().timeout()

            expect(async () => {
                await tv.lookupByRlsnameCleaned(
                    'Fake' as ReleaseNameFullTitle,
                    undefined,
                    true
                )
            }).not.toThrow()
        })

        test('networkerror for lookupById', async () => {
            const mock = new MockAdapter(axios)
            mock.onGet().networkError()

            await expect(async () => {
                await tv.lookupById(12345)
            }).rejects.toThrow()
        })

        test('not found', async () => {
            const mock = new MockAdapter(axios)
            mock.onGet().reply(404)

            expect(async () => {
                await tv.lookupByRlsnameCleaned(
                    'Fake' as ReleaseNameFullTitle,
                    undefined,
                    true
                )
            }).not.toThrow()
        })

        test('server error', async () => {
            const mock = new MockAdapter(axios)
            mock.onGet().reply(500)

            expect(async () => {
                await tv.lookupByRlsnameCleaned(
                    'Fake' as ReleaseNameFullTitle,
                    undefined,
                    true
                )
            }).not.toThrow()
        })
    })

    describe('fixtures', () => {
        beforeEach(() => {
            jest.spyOn(DataProvider.prototype, 'save').mockImplementation()
            jest.spyOn(DataProvider.prototype, 'log').mockImplementation()
            jest.spyOn(
                TVMazeDataProvider.prototype,
                'getGeneratedFields'
            ).mockReturnValue({
                is_scripted_english: true,
                is_children_network: false
            })
        })

        afterEach(() => {
            jest.restoreAllMocks()
            jest.clearAllMocks()
        })

        describe('with a cache', () => {
            describe('approved = 1', () => {
                let lookupByIdSpy
                let findByTitleSpy

                beforeEach(() => {
                    mockDataCache({
                        data: serializeDataCache(tvmazeFixture),
                        data_immutable: '',
                        approved: 1
                    })

                    lookupByIdSpy = jest
                        .spyOn(TVMazeDataProvider.prototype, 'lookupById')
                        .mockResolvedValue(tvmazeFixture)
                    findByTitleSpy = jest
                        .spyOn(TVMazeDataProvider.prototype, 'findByTitle')
                        .mockResolvedValue(tvmazeFixture)
                })

                afterEach(() => {
                    jest.clearAllMocks()
                })

                it('should not extract from tvmaze.com when forceRefresh = false', async () => {
                    const response = await tv.lookupByRlsnameCleaned(
                        'game of thrones' as ReleaseNameFullTitle,
                        undefined,
                        false
                    )

                    expect(lookupByIdSpy).not.toHaveBeenCalled()
                    expect(findByTitleSpy).not.toHaveBeenCalled()
                    expect(response).toMatchSnapshot()
                })
                it('should extract from tvmaze.com when forceRefresh = true', async () => {
                    const response = await tv.lookupByRlsnameCleaned(
                        'game of thrones' as ReleaseNameFullTitle,
                        undefined,
                        true
                    )

                    expect(lookupByIdSpy).toHaveBeenCalledWith(testTvmazeId)
                    expect(findByTitleSpy).not.toHaveBeenCalled()
                    expect(response).toMatchSnapshot()
                })
            })

            describe('approved = 0', () => {
                let lookupByIdSpy
                let lookupByNameSpy

                beforeEach(() => {
                    mockDataCache({
                        data: serializeDataCache(tvmazeFixture),
                        data_immutable: '',
                        approved: 0
                    })

                    lookupByIdSpy = jest
                        .spyOn(TVMazeDataProvider.prototype, 'lookupById')
                        .mockResolvedValue(tvmazeFixture)
                    lookupByNameSpy = jest
                        .spyOn(TVMazeDataProvider.prototype, 'findByTitle')
                        .mockResolvedValue(tvmazeFixture)
                })

                it('should not extract from tvmaze.com when forceRefresh = false', async () => {
                    const response = await tv.lookupByRlsnameCleaned(
                        'game of thrones' as ReleaseNameFullTitle,
                        undefined,
                        false
                    )

                    expect(lookupByIdSpy).not.toHaveBeenCalled()
                    expect(lookupByNameSpy).not.toHaveBeenCalled()
                    expect(response).toMatchSnapshot()
                })
                it('should extract from tvmaze.com when forceRefresh = true', async () => {
                    const response = await tv.lookupByRlsnameCleaned(
                        'game of thrones' as ReleaseNameFullTitle,
                        undefined,
                        true
                    )
                    expect(lookupByIdSpy).not.toHaveBeenCalled()
                    expect(lookupByNameSpy).toHaveBeenCalled()
                    expect(response).toMatchSnapshot()
                })
            })
        })
    })

    // describe('getStrictTitleFromTitleData', () => {
    //     it('should remove years', async () => {
    //         const data: GetTitleResponse = {
    //             title: 'tv show' as ReleaseNameTitle,
    //             fullTitle: 'tv show 2010' as ReleaseNameTitle,
    //             year: 2010,
    //             country: undefined,
    //         }

    //         expect(
    //             TVMazeDataProvider.getStrictTitleFromTitleData(data)
    //         ).toEqual('tv show')
    //     })
    // })
})
