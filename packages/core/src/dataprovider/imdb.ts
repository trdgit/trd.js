import {
    ReleaseName,
    ReleaseNameFullTitle,
    ReleaseNameTitle,
} from '@trd/shared/src/types'
import { decode } from 'html-entities'
import { parse } from 'node-html-parser'
import * as util from 'util'

import { array_unique, is_string, strip_tags } from '../helpers/data_types'
import * as logger from '../helpers/logger'
import { escape } from '../helpers/regex'
import * as search from '../utility/imdbSearch'
import { getTitle, GetTitleResponse } from '../utility/release'
import { DataProvider } from './data-provider'
import DataProviderResponse from './response'

import crypto from 'crypto'

export interface IMDBData {
    id: string
    imdbid: string
    url: string
    genres: string[]
    country: string
    countries: string[]
    language_primary: string
    language: string
    languages: string[]
    rating: number
    votes: number
    stv: boolean
    limited: boolean
    series: boolean
    runtime: string
    title: string
    aka: string
    year: number
    imageurl: string
    poster_hash: string
    screens_uk: number
    screens_us: number
    layout: number
    type: string
}

type IMDBDataLanguage = Pick<
    IMDBData,
    'language' | 'language_primary' | 'languages'
>

const flattenId = id => id.replace(/tt0{0,}(\d+)/i, '$1')

const removeSimilar = (ids: string[], func): string[] => {
    if (ids.length <= 1) {
        return ids
    }

    const result = ids.reduce((ret, id) => {
        if (!ret[func(id)]) {
            ret[func(id)] = id
        }
        return ret
    }, {})

    return Object.values(result)
}

let searchFunction = search.bingSearch
if (process.env.IMDB_SEARCH_METHOD === 'imdb') {
    searchFunction = search.imdbSearch
}

const configuredSearch = async (query: string) =>
    await searchFunction(query, {
        headers: {
            'User-Agent':
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15',
            'Accept-Language': 'en-us',
        },
    })

export default class IMDBDataProvider extends DataProvider {
    protected namespace = 'imdb'

    private bingItems

    readonly IMDB_USER_AGENT =
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36'

    public getDefaults(existing): IMDBData {
        return {
            ...existing,
            id: existing['imdbid'],
            url: `https://www.imdb.com/title/${existing['imdbid']}/`,
            genres: [],
            language_primary: '',
            language: '',
            languages: [],
            country: 'n/a',
            countries: [],
            votes: 0,
            stv: false,
            limited: false,
            series: false,
            rating: 0,
            runtime: '',
            title: '',
            aka: '',
            year: 0,
            imageurl: '',
            poster_hash: '',
            screens_uk: 0,
            screens_us: 0,
            layout: 1,
            type: '',
        }
    }

    public getDeprecated(): string[] {
        return ['screens_us', 'screens_uk', 'limited']
    }

    public async lookup(
        rlsname: ReleaseName,
        forceRefresh = false,
    ): Promise<DataProviderResponse<unknown>> {
        const releaseTitleData = getTitle(rlsname)
        this.log(
            util.format(
                'Converted %s to %s',
                rlsname,
                releaseTitleData.fullTitle,
            ),
        )
        return await this.lookupByRlsnameCleaned(
            releaseTitleData.fullTitle,
            releaseTitleData,
            forceRefresh,
        )
    }

    public async lookupByRlsnameCleaned(
        rlsnameCleaned: ReleaseNameFullTitle,
        releaseTitleData?: GetTitleResponse,
        forceRefresh = false,
    ): Promise<DataProviderResponse<IMDBData>> {
        if (!releaseTitleData) {
            releaseTitleData = {
                title: rlsnameCleaned as unknown as ReleaseNameTitle,
                fullTitle: rlsnameCleaned,
                country: null,
                year: null,
            }
        }

        const cacheResponse = await super.getCacheResponse<IMDBData>(
            releaseTitleData.fullTitle,
            forceRefresh,
            this.getGeneratedFields,
        )

        let possibleIMDBIDs = []
        let rawData: IMDBData
        if (cacheResponse?.result) {
            this.log('Returning straight from the cache')
            return cacheResponse
        }

        // if it's approved and we got this far forceRefresh must be on
        // so we can only consider this and update the cache
        if (cacheResponse.approved) {
            rawData = await this.extractDataFromIMDBId(
                cacheResponse.rawData['id'],
            )
            if (rawData?.id?.length) {
                this.log(
                    util.format(
                        'Directly retrieved data from IMDB using ID %s for %s',
                        cacheResponse.rawData['id'],
                        rlsnameCleaned,
                    ),
                )
                await this.save(releaseTitleData.fullTitle, rawData)

                return new DataProviderResponse(
                    true,
                    rawData,
                    cacheResponse.dataImmutable,
                    this.getGeneratedFields(rawData),
                    cacheResponse.approved,
                    this.logs,
                )
            }
        }

        this.log(
            util.format(
                'Finding possible IMDB ids for %s',
                releaseTitleData.fullTitle,
            ),
        )
        possibleIMDBIDs = await this.findPossibleIMDBIDs(
            releaseTitleData.fullTitle,
        )
        this.log(
            util.format('Found %d possible IMDB ids', possibleIMDBIDs.length),
        )
        if (!possibleIMDBIDs.length) {
            this.log(
                util.format('No IMDB ids for %s', releaseTitleData.fullTitle),
            )
            return new DataProviderResponse(
                false,
                null,
                cacheResponse.dataImmutable,
                undefined,
                cacheResponse.approved,
                this.logs,
            )
        }

        if (possibleIMDBIDs.length == 1) {
            rawData = await this.extractDataFromIMDBId(possibleIMDBIDs[0])
        } else if (possibleIMDBIDs.length > 1) {
            rawData = await this.extractDataFromIMDBIDs(
                possibleIMDBIDs,
                releaseTitleData.fullTitle,
            )
        }

        if (rawData?.imdbid?.length) {
            await this.save(releaseTitleData.fullTitle, rawData)
            return new DataProviderResponse(
                true,
                rawData,
                cacheResponse.dataImmutable,
                this.getGeneratedFields(rawData),
                cacheResponse.approved,
                this.logs,
            )
        }

        return new DataProviderResponse(
            false,
            null,
            cacheResponse.dataImmutable,
            undefined,
            cacheResponse.approved,
            this.logs,
        )
    }

    public static _clean_movie_title(title: string): string {
        return (
            decode(title)
                .toLowerCase()
                .replace(/([.-])\1+/g, '$1') // Replace duplicates of '.' or '-' with a single instance
                .replace(' - IMDb', '')
                .replace('IMDb: ', '') // Remove " - IMDb" and "IMDb: "
                .replace('?', '') // Remove question mark

                // Remove year indicators in parentheses
                .replace(/-\s+(\d+)$/i, '$1') // Remove trailing number (e.g. "-1234")
                .replace(/\((\d{4})\)/i, '$1') // Replace year in parentheses with just the year
                .replace(/\(TV\s+Movie\s+\(\d{4}\)/i, '$1') // Handle TV Movie format

                // Remove trailing text and other unnecessary characters
                .replace(/\(.*?\)$/i, '') // Remove text in parentheses at end of title
                .replace('&', 'and ') // Replace "&" with "and "
                .replace(/&amp;/, 'and ')
                .replace(/&[a-z]+;/g, '') // Handle other ampersands

                // Remove special characters and punctuation
                .replace(/(:|!)/gi, ' ') // Replace colon or exclamation mark with space
                .replace(/([^\.\s]+)[\.,]/gi, '$1') // Remove trailing punctuation (e.g. "abc.")
                .replace(', ', ' ')
                .replace(' - ', ' ') // Replace comma and hyphen spaces with a single space

                // Clean up whitespace and remove any remaining characters
                .replace(/\s{2,}/, ' ') // Collapse multiple whitespace into a single space
                .replace(/[^\w\s-.]+/g, '') // Remove anything not a word character, space, period, or hyphen
                .trim()
                .replace(/'/g, '')
        ) // Trim the title and remove any remaining single quotes
    }

    public getBingItems() {
        return this.bingItems
    }

    public async findPossibleIMDBIDs(movieTitle: string): Promise<string[]> {
        const searchQuery = movieTitle

        this.log(util.format('Searching using qry %s', searchQuery))

        let results: search.SearchResult[]
        try {
            results = await configuredSearch(searchQuery)
        } catch (e) {
            this.log(util.format('Failed searching with err: %s', e))
            return []
        }

        // only take 3 results as most are shit after that
        const possible = []
        for (const result of results.slice(0, 3)) {
            const title = IMDBDataProvider._clean_movie_title(result['title'])
            const url = result['url']
            const idMatches = url.match(/imdb\.com\/title\/(tt\d+)\/?/i)
            if (idMatches) {
                const id = idMatches[1]

                this.log(
                    util.format(
                        'Examining serp title [%s] with IMDB id %s',
                        title,
                        id,
                    ),
                )

                if (title.match(new RegExp(`^${escape(movieTitle)}$`, 'i'))) {
                    this.log(
                        util.format(
                            'Examining title [%s] with IMDB id %s',
                            title,
                            id,
                        ),
                    )
                    possible.push(id)
                } else {
                    this.log(
                        util.format(
                            'Fail: Rlsname [%s] (ID %s) did not match serp title of [%s]',
                            movieTitle,
                            id,
                            title,
                        ),
                    )
                }
            }
        }

        // filter out dupes to do with leading zeros
        const possibleFiltered = removeSimilar(possible, flattenId).slice(0, 2)
        this.log(
            util.format(
                'Went from %d to %d ids with similarity removal check: %s',
                possible.length,
                possibleFiltered.length,
                possibleFiltered.join(','),
            ),
        )

        return array_unique(possibleFiltered)
    }

    private async extractDataFromIMDBIDs(
        ids: string[],
        title: string,
    ): Promise<IMDBData | null> {
        const data: Record<string, IMDBData> = {}

        // TODO: Promise.all this
        for (const id of ids) {
            data[id] = await this.extractDataFromIMDBId(id)
        }

        const matches: IMDBData[] = []
        for (const [id, imdbInfo] of Object.entries(data)) {
            const imdbTitle = IMDBDataProvider._clean_movie_title(
                imdbInfo.title,
            )
            const akaTarget = `${imdbInfo.aka} ${imdbInfo.year}`
            const normalTitleTarget = `${imdbTitle} ${imdbInfo.year}`
            if (
                akaTarget.match(new RegExp(`/^${escape(title)}$/i`)) ||
                normalTitleTarget.match(new RegExp(`/^${escape(title)}$/i`))
            ) {
                this.log(
                    util.format(
                        'Title or aka match on IMDB page for id %s',
                        id,
                    ),
                )
                matches.push(imdbInfo)
            }
        }

        if (matches.length == 1) {
            this.log(util.format('Choosing match %s', matches[0].id))
            return matches[0]
        }

        this.log('Will not match because found multiple matching IDs')
        return null
    }

    public async getIMDBHTML(imdbid: string): Promise<string> {
        return DataProvider.load<string>(
            `https://www.imdb.com/title/${imdbid}/`,
            this.IMDB_USER_AGENT,
            {
                //Cookie: 'beta-control="tmd=out"',
            },
        )
    }

    public async extractDataFromIMDBId(imdbid: string): Promise<IMDBData> {
        this.log(util.format('Extracting IMDB information for id %s', imdbid))

        const info = this.getDefaults({ imdbid: imdbid })

        let response
        try {
            response = await this.getIMDBHTML(imdbid)
        } catch (e) {
            this.log(util.format('Failed to check IMDB page with err: %s', e))
            return info
        }

        if (!is_string(response)) {
            return info
        }

        const root = parse(response)

        const jsonData = JSON.parse(
            root.querySelector('#__NEXT_DATA__').rawText,
        )

        // get real id
        const idMatches = response.match(
            /property="pageConst" content="(ttd+)"/i,
        )
        if (idMatches) {
            info.id = idMatches[1]
            info.imdbid = idMatches[1]
        }

        const cleanupText = (text: string): string => {
            return strip_tags(
                decode(
                    text
                        .trim()
                        .replace(/[\r\n]+/g, '')
                        .replace(/\s{2,}/gi, ' '),
                ),
            )
        }

        // get full title
        const titleJson =
            jsonData.props?.pageProps?.aboveTheFoldData?.titleText?.text
        const titleMatchHtml = root.querySelector('h1')
        if (titleJson) {
            info.title = titleJson
        } else if (titleMatchHtml) {
            info.title = decode(titleMatchHtml.text).trim()
            info.title = info.title.replace(' - IMDb', '')
            // clean up bits from title
            info.title = info.title
                .replace(/[\r\n]+/i, ' ')
                .replace(/\(\d+\)/i, '')
                .replace(/<span.*?>.*?<\/span>/is, '')
                .replace(/\(\d+-\d+\)/i, '')
                .replace(/[\r\n]+/i, '')
            info.title = strip_tags(info.title).trim()
        }

        // aka
        const akaJson = jsonData.props?.pageProps?.mainColumnData?.akas?.edges
        if (akaJson && akaJson.length) {
            info.aka = akaJson[0].node.text
        } else {
            const akaMatchHtml = root.querySelector(
                '[data-testid=title-details-akas] span',
            )
            if (akaMatchHtml) {
                info.aka = cleanupText(akaMatchHtml.text)
            }
        }

        // get STV
        const stvMatch = response.match(/<title>(.*?)<\/title>/is)
        if (stvMatch) {
            if (
                stvMatch[1].includes('(V)') ||
                stvMatch[1].includes('(TV)') ||
                stvMatch[1].includes('(TV Special') ||
                stvMatch[1].includes('(TV Movie') ||
                stvMatch[1].match(/\(Video \d+/is)
            ) {
                info.stv = true
            }
            if (stvMatch[1].includes('(TV Series')) {
                info.series = true
            }
        }

        // get the type: Movie, TV Series, TV Special, TV Movie, Short, etc..
        const imdbType =
            jsonData.props?.pageProps?.aboveTheFoldData?.titleType?.text
        if (imdbType) {
            info.type = imdbType
        }

        // get year
        const yearJson =
            jsonData.props?.pageProps?.aboveTheFoldData?.releaseYear?.year
        if (yearJson) {
            info.year = parseInt(yearJson)
        } else {
            const yearElement = root
                .querySelector('meta[property="og:title"]')
                .getAttribute('content')
            const yearMatchHtml = yearElement.match(/\((\d{4})\)/is)
            if (yearMatchHtml) {
                info.year = parseInt(yearMatchHtml[1])
            }
        }

        // get genres
        const genresJson =
            jsonData.props?.pageProps?.aboveTheFoldData?.genres?.genres
        if (genresJson && genresJson.length) {
            info.genres = genresJson.map(item => {
                return item.text
            })
        } else {
            const genresElementHtml = root.querySelectorAll(
                'li[data-testid=storyline-genres] div ul li',
            )
            if (genresElementHtml && genresElementHtml.length) {
                info.genres = genresElementHtml.map(item => {
                    return cleanupText(item.text)
                })
            }
        }

        // get rating + votes
        const ratingJson =
            jsonData.props?.pageProps?.aboveTheFoldData?.ratingsSummary
        // rating sometimes shows as null when there are no votes
        if (ratingJson && ratingJson.aggregateRating) {
            info.rating = ratingJson.aggregateRating
        }
        // votes defaults to 0
        if (ratingJson && ratingJson.voteCount) {
            info.votes = ratingJson.voteCount
        }

        // get image
        const imageJson =
            jsonData.props?.pageProps?.aboveTheFoldData?.primaryImage?.url
        if (imageJson) {
            info.imageurl = imageJson
            const bits = imageJson.split('/')
            const last = bits[bits.length - 1]
            if (last.substr(-3) === 'jpg') {
                const moreBits = last.split('.')
                info.poster_hash = crypto
                    .createHash('md5')
                    .update(moreBits[0])
                    .digest('hex')
            }
        }

        // get languages
        const languageJson =
            jsonData.props?.pageProps?.mainColumnData?.spokenLanguages
                ?.spokenLanguages
        if (languageJson) {
            info.languages = languageJson.map(item => {
                return item.text
            })
            if (info.languages.length) {
                info.language = info.languages[0]
                info.language_primary = info.languages[0]
            }
        } else {
            const languagesElement = root.querySelectorAll(
                'li[data-testid="title-details-languages"] div ul li',
            )
            if (languagesElement && languagesElement.length) {
                info.languages = languagesElement
                    .filter(languageDom => {
                        return languageDom.querySelector('a')
                    })
                    .map(item => {
                        return cleanupText(item.text)
                    })

                if (info.languages.length) {
                    info.language = info.languages[0]
                    info.language_primary = info.languages[0]
                }
            }
        }

        // get country
        const countryJson =
            jsonData.props?.pageProps?.mainColumnData?.countriesOfOrigin
                ?.countries
        if (countryJson && countryJson.length) {
            info.countries = countryJson.map(country => {
                if (country.text === 'United States') {
                    return 'USA'
                } else if (country.text === 'United Kingdom') {
                    return 'UK'
                }
                return country.text
            })
        } else {
            const countryElement = root.querySelectorAll(
                'li[data-testid="title-details-origin"] div ul li',
            )
            if (countryElement && countryElement.length) {
                info.countries = countryElement
                    .filter(countryDom => {
                        return countryDom.querySelector('a')
                    })
                    .map(item => {
                        return cleanupText(item.text)
                    })
                    .map(country => {
                        if (country === 'United States') {
                            return 'USA'
                        } else if (country === 'United Kingdom') {
                            return 'UK'
                        }
                        return country
                    })
            }
        }
        if (info.countries.length) {
            info.country = info.countries[0]
        }

        // runtime
        const runtimeJson =
            jsonData.props?.pageProps?.aboveTheFoldData?.runtime?.seconds
        if (runtimeJson) {
            const minutes = parseInt(runtimeJson) / 60
            if (minutes > 60) {
                const remainingMinutes = minutes % 60
                const hours = (minutes - remainingMinutes) / 60
                info.runtime = `${hours} hour ${remainingMinutes} minutes`
            } else {
                info.runtime = `${minutes} minutes`
            }
        } else {
            const runtimeMatchHtml = root.querySelector(
                'li[data-testid="title-techspec_runtime"] div',
            )
            if (runtimeMatchHtml) {
                info.runtime = cleanupText(runtimeMatchHtml.text)
            }
        }

        // handle some weird case with us movies not being english as primary
        const altLanguage = await this.findLanguageFromAlternativeSource(
            imdbid,
            info.country,
            info.language_primary,
            info.languages,
        )

        return {
            ...info,
            ...altLanguage,
        }
    }

    private async findLanguageFromAlternativeSource(
        id: string,
        existingCountry: string,
        existingLanguage: string,
        existingLanguages: string[],
    ): Promise<IMDBDataLanguage | undefined> {
        if (
            ['USA', 'UK'].includes(existingCountry) &&
            existingLanguage != 'English' &&
            existingLanguages?.includes('English')
        ) {
            logger.datalog(
                'Attempting to find language from wikidata due to missing data',
            )

            const endpointUrl = 'https://query.wikidata.org/sparql'
            const sparqlQuery = `
                SELECT ?item ?itemLabel ?lang ?langLabel WHERE {
                    ?item wdt:P31 wd:Q11424.
                    ?item wdt:P345 "${id}".
                    OPTIONAL {
                    ?item p:P364 ?langStatement.
                    ?langStatement ps:P364 ?lang.
                    }
                    SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
                }
            `

            let response

            try {
                response = await DataProvider.load(
                    `${endpointUrl}?query=${encodeURIComponent(
                        sparqlQuery,
                    )}&format=json`,
                )
            } catch (e) {
                logger.datalog(
                    util.format('Failed to query wikidata with err: %s', e),
                )
                return
            }
            if (response?.length) {
                const contents = JSON.parse(response)
                if (contents.results?.bindings) {
                    const language =
                        contents['results']['bindings'][0]['langLabel']['value']
                    if (language && language == 'English') {
                        return {
                            language_primary: 'English',
                            language: 'English',
                            languages: ['English'],
                        }
                    }
                }
            }
        }

        return
    }

    public getGeneratedFields(
        data,
    ): Pick<IMDBData, 'votes' | 'rating' | 'year'> {
        // fix for wrong data type in old data_cache entries
        return {
            votes: data.votes
                ? Number.isInteger(data.votes)
                    ? data.votes
                    : parseInt(data.votes)
                : 0,
            rating: data.rating ? parseFloat(data['rating']) : 0.0,
            year: data.year
                ? Number.isInteger(data.year)
                    ? data.year
                    : parseInt(data.year)
                : 0,
        }
    }
}
