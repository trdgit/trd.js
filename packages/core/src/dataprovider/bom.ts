import { ReleaseName, ReleaseNameFullTitle } from '@trd/shared/src/types'
import * as util from 'util'

import { strip_tags } from '../helpers/data_types'
import * as logger from '../helpers/logger'
import { getTitle } from '../utility/release'
import { DataProvider } from './data-provider'
import { IMDBData } from './imdb'
import DataProviderResponse from './response'

export interface BOMData {
    id: string
    year: number
    screens_us: number
    screens_uk: number
    has_screens: boolean
    limited: boolean
    wide: boolean
}

type PartialIMDB = Pick<IMDBData, 'id' | 'country'>

export default class BoxOfficeMojoDataProvider extends DataProvider {
    protected namespace = 'bom'
    protected imdbData: PartialIMDB

    public getDefaults(existing): BOMData {
        return {
            ...existing,
            id: existing.id ?? null,
            year: '',
            screens_us: 0,
            screens_uk: 0,
            has_screens: false,
            limited: false,
            wide: false,
        }
    }

    public getDeprecated() {
        return []
    }

    public async lookup(
        rlsname: ReleaseName,
        forceRefresh = false,
        imdbData?: PartialIMDB
    ): Promise<DataProviderResponse<unknown>> {
        const releaseTitleData = getTitle(rlsname)
        return await this.lookupByRlsnameCleaned(
            releaseTitleData.fullTitle,
            forceRefresh,
            imdbData
        )
    }

    public async lookupByRlsnameCleaned(
        rlsnameCleaned: ReleaseNameFullTitle,
        forceRefresh = false,
        imdbData?: PartialIMDB
    ): Promise<DataProviderResponse<BOMData>> {
        if (!imdbData || !imdbData['id']) {
            return new DataProviderResponse(false, null)
        }

        this.imdbData = imdbData

        // TODO: try and extract year, and even country to help better lookup
        const cacheResponse = await super.lookupCore<BOMData>(
            rlsnameCleaned,
            forceRefresh
        )
        if (cacheResponse.result) {
            return cacheResponse
        }

        const rawData = await this.lookupById(imdbData.id)

        if (rawData?.id) {
            await this.save(rlsnameCleaned, rawData)
            return new DataProviderResponse(
                true,
                rawData,
                cacheResponse.dataImmutable,
                undefined,
                cacheResponse.approved
            )
        } else {
            return new DataProviderResponse(
                false,
                this.getDefaults({}),
                cacheResponse.dataImmutable,
                undefined,
                cacheResponse.approved
            )
        }
    }

    public async lookupById(id: string): Promise<BOMData | undefined> {
        const url = `https://www.boxofficemojo.com/title/${encodeURIComponent(
            id
        )}/`

        let html: string
        try {
            html = await DataProvider.load<string>(url)
            const info = await this.extractData(html, id)
            return info
        } catch (e) {
            logger.datalog(
                util.format('Error checking bom url %s with err: %s', url, e)
            )
            //throw e
        }
    }

    private async extractData(html: string, id?: string): Promise<BOMData> {
        // Steps required
        // Navigate to page and go to "Original release"
        // Find the URLs for "Domestic" and "United Kingdom"
        // Extract "Widest release" total for both

        const info = this.getDefaults({
            id: id,
            url: `https://www.boxofficemojo.com/title/${id}/`,
        })

        const originalReleaseId = html.match(/\/releasegroup\/([a-z\d]+)/ims)
        if (!originalReleaseId) {
            return
        }

        // get year
        const yearMatch = html.match(/<h1.*?>(.*?)<\/h1>/ims)
        if (yearMatch) {
            const getYear = strip_tags(yearMatch[1])
            const actualYearMatch = getYear.match(/\((\d{4})\)/ims)
            if (actualYearMatch) {
                info.year = parseInt(actualYearMatch[1])
            }
        }

        const originalReleaseId2 = originalReleaseId[1]

        const url = `https://www.boxofficemojo.com/releasegroup/${encodeURIComponent(
            originalReleaseId2
        )}/`
        let newHtml: string
        try {
            newHtml = await DataProvider.load<string>(url)
        } catch (e) {
            logger.datalog(
                util.format('Failed to check BOM url %s with err: ', url, e)
            )
            return info
        }

        // get domestic and uk urls
        const domestic = newHtml.match(
            /value="\/release\/(.*?)\/"[^>]*?>Domestic<\/option>/ims
        )
        const uk = newHtml.match(
            /value="\/release\/(\S+)\/"[^>]*?>United Kingdom<\/option>/ims
        )
        if (domestic) {
            info.screens_us = await this.getScreensFromRelease(domestic[1])
        }
        if (uk) {
            info.screens_uk = await this.getScreensFromRelease(uk[1])
        }

        info.wide = isWide(info, this.imdbData)

        const limitedTuples = [
            [info.screens_uk, 250],
            [info.screens_us, 500],
        ]
        info.limited = limitedTuples.some(([screens, max]) => {
            return screens > 0 && screens < max
        })

        if (info.screens_us === 0 && info.screens_uk === 0) {
            info.limited = false
        } else {
            info.has_screens = true
        }

        return info
    }

    private async getScreensFromRelease(releaseId: string): Promise<number> {
        const url = `https://www.boxofficemojo.com/release/${encodeURIComponent(
            releaseId
        )}/weekend/`
        let html: string
        try {
            html = await DataProvider.load<string>(url)
        } catch (e) {
            logger.datalog(
                util.format('Failed to check BOM url %s with err: %s', url, e)
            )
        }
        const matches = html.match(/Widest\s+Release<\/span><span>([\d,]+)/ims)
        if (matches) {
            return parseInt(matches[1].replace(',', ''))
        }
        return 0
    }
}

const isWide = (info: BOMData, imdbData: PartialIMDB): boolean =>
    (imdbData.country === 'USA' && info.screens_us >= 500) ||
    (imdbData.country === 'UK' && info.screens_uk >= 250)
