import { readFileSync } from 'fs'

import IMDBDataProvider from './imdb'

const fixturesPath = './test/fixtures/imdb'
const newIMDBPath = `${fixturesPath}/imdb.new.html`

describe('IMDBDataProvider offline', () => {
    let imdb
    let newHTML
    beforeAll(async () => {
        newHTML = readFileSync(newIMDBPath).toString()
        imdb = new IMDBDataProvider()
        jest.spyOn(imdb, 'getIMDBHTML').mockImplementation(() => {
            return Promise.resolve(newHTML)
        })
    })
    //

    describe('new html', () => {
        test('should have stubbed correctly', async () => {
            const html = await imdb.getIMDBHTML('tt0120689')
            expect(html).toBe(newHTML)
        })
        test('should have correct info for basic case', async () => {
            const info = await imdb.extractDataFromIMDBId('tt0120689')
            expect(info['title']).toBe('The Green Mile')
            expect(info['aka']).toBe(`Stephen King's The Green Mile`)
            expect(typeof info['rating']).toBe('number')
            expect(info['votes']).toBeGreaterThan(0)
            expect(info['genres']).toStrictEqual([
                'Crime',
                'Drama',
                'Fantasy',
                'Mystery'
            ])
        })
    })
})
