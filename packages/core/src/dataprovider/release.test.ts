import { ReleaseName } from '@trd/shared/src/types'

import ReleaseNameDataProvider from './release'

describe('Test static methods of ReleaseNameDataProvider', () => {
    describe('Test extractCodec', () => {
        it('X264', () => {
            const result1 = ReleaseNameDataProvider.extractCodec(
                'Show.S01E01.720p.HDTV.x264-GROUP' as ReleaseName
            )
            expect(result1).toBe('X264')
        })
        it('H264', () => {
            const result2 = ReleaseNameDataProvider.extractCodec(
                'Show.S01E01.720p.HDTV.H264-GROUP' as ReleaseName
            )
            expect(result2).toBe('H264')
        })
        it('VP9', () => {
            const result3 = ReleaseNameDataProvider.extractCodec(
                'Show.S01E01.720p.HDTV.VP9-GROUP' as ReleaseName
            )
            expect(result3).toBe('VP9')
        })
    })
    describe('Test extractRepeatExtras', () => {
        it('extracts PROPER', () => {
            const result4 = ReleaseNameDataProvider.extractRepeatExtras(
                'TV.Show.S01E01.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result4).toBe('PROPER')
        })
        it('extracts REPACK', () => {
            const result5 = ReleaseNameDataProvider.extractRepeatExtras(
                'TV.Show.S01E01.REPACK.HDTV.x264-Group' as ReleaseName
            )
            expect(result5).toBe('REPACK')
        })
        it('extracts REAL.PROPER', () => {
            const result6 = ReleaseNameDataProvider.extractRepeatExtras(
                'TV.Show.S01E01.REAL.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result6).toBe('REAL.PROPER')
        })
        it('does not extract PROPER from group name', () => {
            const result = ReleaseNameDataProvider.extractRepeatExtras(
                'TV.Show.S01E01.HDTV.x264-GroupPROPER' as ReleaseName
            )
            expect(result).toBe(null)
        })
    })
    describe('Test extraEpisodeToken', () => {
        it('S01E01-E02', () => {
            const result7 = ReleaseNameDataProvider.extractEpisodeToken(
                'TV.Show.S01E01-E02.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result7).toBe('S01E01-E02')
        })
    })
    describe('Test extractOther', () => {
        it('INTERNAL', () => {
            const result = ReleaseNameDataProvider.extractOther(
                'TV.Show.S04E00.Inside.Keeper.of.the.Light.iNTERNAL.720p.WEB.x264-GROUP' as ReleaseName,
                ReleaseNameDataProvider.getDefaultsStatic({})
            )
            expect(result['internal']).toBe(true)
        })
        it('_iNT', () => {
            const result = ReleaseNameDataProvider.extractOther(
                'Abc_Cdefg-Abc_Cdefg_(1984)-Remastered-WEB-2017-AbC_iNT' as ReleaseName,
                ReleaseNameDataProvider.getDefaultsStatic({})
            )
            expect(result['internal']).toBe(true)
        })
        it('SWEDiSH', () => {
            const result = ReleaseNameDataProvider.extractOther(
                'TV.Show.S03E01-E02.SWEDISH.720p.HDTV.x264-Group' as ReleaseName,
                ReleaseNameDataProvider.getDefaultsStatic({})
            )
            expect(result['language']).toBe('SWEDISH')
        })

        it('invalid extractOther', () => {
            const result = ReleaseNameDataProvider.extractOther(
                'Test.Update-GROUP' as ReleaseName,
                {}
            )
            expect(result['codec']).toBe(undefined)
        })

        it('weird bookware bug', () => {
            const t = () => {
                ReleaseNameDataProvider.extractOther(
                    'OREILLY_SOMETHING_PART1-Group' as ReleaseName,
                    {}
                )
            }
            expect(t).not.toThrowError()
        })
    })

    describe('Test extractSeason', () => {
        it('S01E01', () => {
            expect(
                ReleaseNameDataProvider.extractSeason(
                    'TV.Show.S01E01.PROPER.HDTV.x264-Group' as ReleaseName
                )
            ).toBe(1)
        })
    })

    describe('Test extractEpisode', () => {
        // basic cases
        it('S01E00', () => {
            const result10 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.S01E00.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result10).toBe(0)
        })
        it('S01E01', () => {
            const result11 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.S01E01.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result11).toBe(1)
        })
        // only episode
        it('E01', () => {
            const result12 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.E01.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result12).toBe(1)
        })
        // part syntax
        it('Part10', () => {
            const result13 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.Part.S01.Part10.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result13).toBe(10)
        })
        it('Part.10', () => {
            const result14 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.Part.S01.Part.10.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result14).toBe(10)
        })
        // episode syntax
        it('Episode10', () => {
            const result15 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.Part.S01.Episode10.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result15).toBe(10)
        })
        it('Episode.10', () => {
            const result16 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.Part.S01.Episode.10.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result16).toBe(10)
        })
        // yyyy.mm.dd syntax
        it('MM.DD', () => {
            const result17 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.2020.01.01.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result17).toBe('01.01')
        })
        // EXXEXX syntax
        it('E01E02', () => {
            const result18 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.S01E01E02.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result18).toBe('0102')
        })
        it('E01-E02', () => {
            const result19 = ReleaseNameDataProvider.extractEpisode(
                'TV.Show.S01E01-E02.PROPER.HDTV.x264-Group' as ReleaseName
            )
            expect(result19).toBe('0102')
        })
        it('Invalid', () => {
            const result = ReleaseNameDataProvider.extractEpisode(
                'Test.Update-GROUP' as ReleaseName
            )
            expect(result).toBe(null)
        })
    })

    describe('Test extractRange', () => {
        it('Dolby Vision', () => {
            const result1 = ReleaseNameDataProvider.extractRange(
                'TV.Show.S01E01-E02.DV.2160p.WEB.h265-Group' as ReleaseName
            )
            expect(result1).toBe('DV')
        })
        it('High dynamic range', () => {
            const result2 = ReleaseNameDataProvider.extractRange(
                'TV.Show.S01E01-E02.HDR.2160p.WEB.h265-Group' as ReleaseName
            )
            expect(result2).toBe('HDR')
        })
        it('Standard dynamic range', () => {
            const result3 = ReleaseNameDataProvider.extractRange(
                'TV.Show.S01E01-E02.2160p.WEB.h265-Group' as ReleaseName
            )
            expect(result3).toBe(null)
        })
        it('Hybrid log-gamma', () => {
            const result4 = ReleaseNameDataProvider.extractRange(
                'TV.Show.S01E01-E02.HLG.2160p.WEB.h265-Group' as ReleaseName
            )
            expect(result4).toBe('HLG')
        })
        it('DV with High dynamic range fallback', () => {
            const result5 = ReleaseNameDataProvider.extractRange(
                'TV.Show.S02E06.DV.HDR.2160p.WEB.H265-Group' as ReleaseName
            )
            expect(result5).toBe('DV.HDR')
        })
    })

    describe('Test extractTVSource', () => {
        it('HDTV', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.HDTV.x264-Group' as ReleaseName
            )
            expect(result).toBe('HDTV')
        })
        it('AHDTV', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.AHDTV.x264-Group' as ReleaseName
            )
            expect(result).toBe('AHDTV')
        })
        it('AUHDTV', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.2160p.AUHDTV.h265-Group' as ReleaseName
            )
            expect(result).toBe('AUHDTV')
        })
        it('UHDTV', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.2160p.UHDTV.h265-Group' as ReleaseName
            )
            expect(result).toBe('UHDTV')
        })
        it('PDTV', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.PDTV.x264-Group' as ReleaseName
            )
            expect(result).toBe('PDTV')
        })
        it('DSR', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.DSR.x264-Group' as ReleaseName
            )
            expect(result).toBe('DSR')
        })
        it('BDRIP', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.BDRip.x264-Group' as ReleaseName
            )
            expect(result).toBe('BDRIP')
        })
        it('DVDRIP', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.DVDRip.x264-Group' as ReleaseName
            )
            expect(result).toBe('DVDRIP')
        })
        it('WEB', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.720p.WEB.x264-Group' as ReleaseName
            )
            expect(result).toBe('WEB')
        })
        it('WEBRIP', () => {
            const result = ReleaseNameDataProvider.extractTVSource(
                'TV.Show.S01E01.720p.WEBRiP.x264-Group' as ReleaseName
            )
            expect(result).toBe('WEBRIP')
        })
        it('BLURAY', () => {
            const bluray = ReleaseNameDataProvider.extractTVSource(
                'The.Movie.2022.1080p.BluRay.x264-Group' as ReleaseName
            )
            expect(bluray).toBe('BLURAY')

            const mbluray = ReleaseNameDataProvider.extractTVSource(
                'The.Movie.2022.1080p.MBLURAY.x264-Group' as ReleaseName
            )
            expect(mbluray).toBe('BLURAY')

            const purembluray = ReleaseNameDataProvider.extractTVSource(
                'The.Movie.2022.1080p.PURE.MBLURAY.x264-Group' as ReleaseName
            )
            expect(purembluray).toBe('BLURAY')

            const complete = ReleaseNameDataProvider.extractTVSource(
                'The.Movie.2022.COMPLETE.BLURAY-Group' as ReleaseName
            )
            expect(complete).toBe('BLURAY')

            const completembluray = ReleaseNameDataProvider.extractTVSource(
                'Some.Music.2022.COMPLETE.MBLURAY-Group' as ReleaseName
            )
            expect(completembluray).toBe('BLURAY')

            const completepurembluray = ReleaseNameDataProvider.extractTVSource(
                'Some.Music.2022.COMPLETE.PURE.MBLURAY-Group' as ReleaseName
            )
            expect(completepurembluray).toBe('BLURAY')
        })
        it('UHD.BLURAY', () => {
            const completeuhd = ReleaseNameDataProvider.extractTVSource(
                'The.Movie.2022.COMPLETE.UHD.BLURAY-Group' as ReleaseName
            )
            expect(completeuhd).toBe('UHD.BLURAY')

            const completeuhdmbluray = ReleaseNameDataProvider.extractTVSource(
                'Some.Music.2022.COMPLETE.UHD.MBLURAY-Group' as ReleaseName
            )
            expect(completeuhdmbluray).toBe('UHD.BLURAY')
        })
    })

    describe('Test extractResolution', () => {
        it('Test resolutions', () => {
            const hdtv720p = ReleaseNameDataProvider.extractResolution(
                'TV.Show.S01E01.720p.HDTV.x264-Group' as ReleaseName
            )
            expect(hdtv720p).toBe('720P')
            const hdtv1080p = ReleaseNameDataProvider.extractResolution(
                'TV.Show.S01E01.1080p.HDTV.x264-Group' as ReleaseName
            )
            expect(hdtv1080p).toBe('1080P')
            const hdtv2160p = ReleaseNameDataProvider.extractResolution(
                'TV.Show.S01E01.2160p.UHDTV.x264-Group' as ReleaseName
            )
            expect(hdtv2160p).toBe('2160P')

            const web720p = ReleaseNameDataProvider.extractResolution(
                'TV.Show.S01E01.720p.WEB.x264-Group' as ReleaseName
            )
            expect(web720p).toBe('720P')
            const web1080p = ReleaseNameDataProvider.extractResolution(
                'TV.Show.S01E01.1080p.WEB.x264-Group' as ReleaseName
            )
            expect(web1080p).toBe('1080P')
            const web2160p = ReleaseNameDataProvider.extractResolution(
                'TV.Show.S01E01.2160p.WEB.x264-Group' as ReleaseName
            )
            expect(web2160p).toBe('2160P')

            const mbluray720p = ReleaseNameDataProvider.extractResolution(
                'Some.720p.MBLURAY.x264-Group' as ReleaseName
            )
            expect(mbluray720p).toBe('720P')
            const mbluray1080p = ReleaseNameDataProvider.extractResolution(
                'Some.1080p.MBLURAY.x264-Group' as ReleaseName
            )
            expect(mbluray1080p).toBe('1080P')
            const mbluray2160p = ReleaseNameDataProvider.extractResolution(
                'Some.2160p.UHD.MBLURAY.h265-Group' as ReleaseName
            )
            expect(mbluray2160p).toBe('2160P')

            const completembluray = ReleaseNameDataProvider.extractResolution(
                'Some.COMPLETE.MBLURAY-Group' as ReleaseName
            )
            expect(completembluray).toBe(null)
            const completeuhdmbluray =
                ReleaseNameDataProvider.extractResolution(
                    'Some.COMPLETE.UHD.MBLURAY-Group' as ReleaseName
                )
            expect(completeuhdmbluray).toBe(null)

            const bluraycomplete = ReleaseNameDataProvider.extractResolution(
                'Some.2024.COMPLETE.BLURAY-Group' as ReleaseName
            )
            expect(bluraycomplete).toBe(null)
            const completeuhdbluray = ReleaseNameDataProvider.extractResolution(
                'Some.2024.COMPLETE.UHD.BLURAY-Group' as ReleaseName
            )
            expect(completeuhdbluray).toBe(null)
        })
    })
})
