import { DataProviderLogs } from '@trd/shared/src/types'

import { is_object } from '../helpers/data_types'

export default class DataProviderResponse<T> {
    public result = false
    public rawData: T = null
    public dataImmutable
    public generatedFields
    public approved = false
    public debug: DataProviderLogs = []

    constructor(
        result: boolean,
        rawData,
        dataImmutable = {},
        generatedFields?,
        approved = false,
        debug: DataProviderLogs = undefined
    ) {
        this.result = result
        this.rawData = rawData
        this.dataImmutable = dataImmutable
        this.generatedFields = generatedFields
        this.approved = approved
        this.debug = debug
    }

    public getRawData(): T {
        return this.rawData
    }

    public setDebug(debug) {
        this.debug = debug
    }

    public getDebug() {
        return this.debug
    }

    public getData(): T {
        let data = this.mergeData(this.rawData, this.dataImmutable)
        if (
            this.generatedFields &&
            Object.entries(this.generatedFields).length
        ) {
            data = this.mergeData(data, this.generatedFields, true)
        }
        return data
    }

    public get(key) {
        const data = this.getData()
        if (is_object(data)) {
            return data && key in data ? data[key] : null
        }
        return null
    }

    public getImmutableData() {
        return this.dataImmutable
    }

    private mergeData(current, overrides, addNew = false) {
        const fixed = { ...current }
        if (Object.entries(overrides).length) {
            for (const [k, v] of Object.entries(overrides)) {
                if (k in fixed || addNew) {
                    fixed[k] = v
                }
            }
        }
        return fixed
    }

    public setGeneratedFields(fields) {
        this.generatedFields = fields
    }
}
