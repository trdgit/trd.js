import { ReleaseNameFullTitle } from '@trd/shared/src/types'

import DataProviderResponse from '../dataprovider/response'
import * as dataCacheRepository from '../repository/data'
import { DataProvider } from './data-provider'
import IMDBDataProvider from './imdb'

jest.setTimeout(60000)

const imdb = new IMDBDataProvider()

describe('IMDBDataProvider live', () => {
    beforeEach(() => {
        jest.spyOn(dataCacheRepository, 'getDataCache').mockResolvedValueOnce(
            null
        )
        jest.spyOn(DataProvider.prototype, 'save').mockImplementation()
    })

    test('testBasicLookup', async () => {
        const result = await imdb.lookupByRlsnameCleaned(
            'The Green Mile 1999' as ReleaseNameFullTitle,
            undefined,
            true
        )
        expect(result).toBeInstanceOf(DataProviderResponse)

        const data = result.getData()

        expect(Object.entries(data).length).toBeGreaterThan(0)
        expect(data.id).toMatch(/tt\d+/i)
        expect(data.year).toBe(1999)
        expect(data.url).toBe('https://www.imdb.com/title/tt0120689/')
        expect(data.stv).toBe(false)
        expect(data.country).toBe('USA')
        expect(data.genres.length).toBe(4)
        expect(data.languages.length).toBe(2)
        expect(data.rating).toBeGreaterThan(0)
        expect(data.votes).toBeGreaterThan(0)
        expect(data.type).toBe('Movie')
    })

    test('testImdbTypes', async () => {
        const short = await imdb.extractDataFromIMDBId('tt9603082')
        expect(short['type']).toBe('Short')

        const tvmovie = await imdb.extractDataFromIMDBId('tt1785302')
        expect(tvmovie['type']).toBe('TV Movie')

        const tvspecial = await imdb.extractDataFromIMDBId('tt7924798')
        expect(tvspecial['type']).toBe('TV Special')
    })

    // test('testBasicLookup', async () => {
    //     const id = 'tt0266543'
    //     const ids = await imdb.findPossibleIMDBIDs('Finding Nemo 2003')
    //     expect(ids).toBeInstanceOf(Array)
    //     expect(ids.length).toBeGreaterThan(0)
    //     expect(ids.includes(id)).toBe(true)
    // })

    // test('testGetCorrectLanguage', async () => {
    //     const result = await imdb.lookup(
    //         'The.Passionate.Friends.1949.720p.BluRay.x264-Group',
    //         true
    //     )

    //     const data = result.getData()
    //     expect(data).not.toBeNull()
    //     expect(data['imdbid']).toBe('tt0041735')
    //     expect(data['languages'].includes('English')).toBe(true)
    // })

    // test('testSeriesLookup', async () => {
    //     const data = await imdb.extractDataFromIMDBId('tt2193021')
    //     expect(data['imdbid']).toBe('tt2193021')
    //     expect(data['series']).toBe(true)

    //     const movie = await imdb.extractDataFromIMDBId('tt1492179')
    //     expect(movie['imdbid']).toBe('tt1492179')
    //     expect(movie['series']).toBe(true)
    // })

    // test('testLookupConsistencyAndEdgeCases', async () => {
    //     let result = await imdb.lookup(
    //         'X-Men.Apocalypse.2016.1080p.BluRay.x264-Group',
    //         true
    //     )
    //     let data = result.getData()
    //     expect(data['imdbid']).toBe('tt3385516')

    //     result = await imdb.lookup(
    //         'Independence.Day.Resurgence.2016.1080p.BluRay.x264-Group',
    //         true
    //     )

    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt1628841')

    //     result = await imdb.lookup(
    //         'Mechanic.Resurrection.2016.720p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt3522806')

    //     result = await imdb.lookup(
    //         'Kubo.And.The.Two.Strings.2016.1080p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt4302938')

    //     result = await imdb.lookup(
    //         'Nerve.2016.PROPER.1080p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt3531824')

    //     result = await imdb.lookup(
    //         'Star.Trek.Beyond.2016.1080p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt2660888')

    //     result = await imdb.lookup('The.BFG.2016.1080p.BluRay.x264-Group', true)
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt3691740')

    //     result = await imdb.lookup('Mother.2017.720p.BluRay.x264-Group', true)
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt5109784')

    //     result = await imdb.lookup(
    //         'Hail.Caesar.2016.720p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt0475290')

    //     result = await imdb.lookup(
    //         'Star.Trek.2009.2160p.UHD.BluRay.X265-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt0796366')

    //     result = await imdb.lookup(
    //         'Angels.and.Demons.2009.2160p.UHD.BluRay.X265-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt0808151')

    //     result = await imdb.lookup(
    //         'Hancock.2008.2160p.UHD.BluRay.X265-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt0448157')

    //     result = await imdb.lookup(
    //         'Blade.Runner.2049.2017.PROPER.720p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt1856101')

    //     result = await imdb.lookup(
    //         'Keeping.Up.with.the.Joneses.2016.2160p.UHD.BluRay.x265-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['language_primary']).toBe('English')
    //     expect(data['languages'][0]).toBe('English')

    //     result = await imdb.lookup(
    //         'Mike.And.Dave.Need.Wedding.Dates.2016.2160p.UHD.BluRay.x265-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['language_primary']).toBe('English')
    //     expect(data['languages'][0]).toBe('English')

    //     result = await imdb.lookup(
    //         'A.New.Leaf.1971.REMASTERED.1080p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt0067482')

    //     result = await imdb.lookup(
    //         'Roman.J.Israel.Esq.2017.720p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt6000478')

    //     result = await imdb.lookup(
    //         'Just.Getting.Started.2017.720p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt5721088')

    //     result = await imdb.lookup(
    //         'The.Ballad.of.Lefty.Brown.2017.720p.BluRay.X264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt4400994')

    //     result = await imdb.lookup(
    //         'The.27th.Day.1957.1080p.BluRay.x264-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt0050085')

    //     result = await imdb.lookup(
    //         'The.Hunger.Games.Mockingjay.Part.2.2015.2160p.UHD.BluRay.x265-Group',
    //         true
    //     )
    //     data = result.getData()
    //     expect(data['imdbid']).toBe('tt1951266')
    // })

    // test('Test imdb.stv', async () => {
    //     // No gross income
    //     let data = await imdb.extractDataFromIMDBId('tt19266900')
    //     expect(data['stv']).toBe(true)
    //     // gross income
    //     data = await imdb.extractDataFromIMDBId('tt5463162')
    //     expect(data['stv']).toBe(false)
    //     // TV Movie
    //     data = await imdb.extractDataFromIMDBId('tt16294738')
    //     expect(data['stv']).toBe(true)
    // })
})
