import { DataProviderLogs } from '@trd/shared/src/types'
import * as util from 'util'

import axios from '../helpers/axios'
import { toMySQL } from '../helpers/date'
import * as logger from '../helpers/logger'
import {
    serializeDataCache,
    unserializeDataCache
} from '../helpers/serialize.data_cache'
import {
    createDataCache,
    CreateDataCacheRequest,
    getDataCache,
    getDataCacheById,
    updateDataCache,
    UpdateDataCacheRequest
} from '../repository/data'
import DataProviderResponse from './response'

import phpUnserialize from 'locutus/php/var/unserialize'
import phpSerialize from 'locutus/php/var/serialize'

const isUsingJSONForCache = () => process.env.DATACACHE_JSON === '1'

const serialize = isUsingJSONForCache() ? serializeDataCache : phpSerialize
const unserialize = isUsingJSONForCache()
    ? unserializeDataCache
    : phpUnserialize

export class DataProviderLookupError extends Error {
    constructor(message: string) {
        super(message)

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, DataProviderLookupError.prototype)
    }
}

export abstract class DataProvider {
    protected dispatcher = null
    protected db = null

    protected namespace = ''
    protected logs: DataProviderLogs = []

    public __construct(db, dispatcher) {
        this.db = db
        this.dispatcher = dispatcher
    }

    public abstract getDefaults(existing)
    protected abstract getDeprecated(): string[]
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    protected static getGeneratedFields(data): object {
        return {}
    }

    public async lookupCore<T>(
        key: string,
        forceRefresh = false
    ): Promise<DataProviderResponse<T>> {
        const k = `${this.namespace}:${key}`.toLowerCase()

        const data = await getDataCache(k)
        if (data && data.data?.length) {
            const rawData = unserialize(data['data'])
            if (data['data_immutable']?.length) {
                return new DataProviderResponse(
                    !forceRefresh,
                    rawData,
                    unserialize(data['data_immutable']),
                    undefined,
                    data['approved'] == 1,
                    this.logs
                )
            }
            return new DataProviderResponse(
                !forceRefresh,
                rawData,
                {},
                undefined,
                data['approved'] == 1,
                this.logs
            )
        }

        return new DataProviderResponse(
            false,
            null,
            {},
            undefined,
            false,
            this.logs
        )
    }

    public async lookupId(id: string) {
        const data = await getDataCacheById(id)
        if (data['data'] && data['data'].length) {
            const existingData = unserialize(data['data'])
            if (data['data_immutable'] && data['data_immutable'].length) {
                return this.mergeData(
                    existingData,
                    unserialize(data['immutable_data'])
                )
            }
            return existingData
        }
        return false
    }

    public async exists(key: string) {
        const k = `${this.namespace}:${key}`.toLowerCase()
        const data = await getDataCache(k)
        return data.data && data.data.length
    }

    public async save(key: string, data: object, manuallyApproved = null) {
        const now = toMySQL()

        if (!key || !key.length) {
            return
        }

        const k = `${this.namespace}:${key}`.toLowerCase()

        // check
        const exists = await getDataCache(k)

        if (!exists) {
            const row: CreateDataCacheRequest = {
                k,
                data: serialize(data),
                namespace: this.namespace,
                id: data['id'],
                updated: now
            }

            if (manuallyApproved === true || manuallyApproved === false) {
                row.approved = manuallyApproved ? 1 : 0
            }
            await createDataCache(row)

            // TODO: re-implement
            //const $event = new CacheMutatedEvent($this->namespace, $key, null, $data);
        } else {
            const row: UpdateDataCacheRequest = {
                data: serialize(data),
                id: data['id'],
                updated: now
            }

            if (manuallyApproved === true || manuallyApproved === false) {
                row['approved'] = manuallyApproved ? 1 : 0
            }

            await updateDataCache(k, row)

            // TODO: re-implement
            //$event = new CacheMutatedEvent($this->namespace, $key, unserialize($exists['data']), $data);
        }

        // TODO: reimplement
        //this.dispatcher.dispatch(event);
    }

    public async getCacheResponse<RT>(
        key: string,
        forceRefresh: boolean,
        generatedFieldsFunction
    ): Promise<null | DataProviderResponse<RT>> {
        const cacheResponse = await this.lookupCore<RT>(key, forceRefresh)
        if (cacheResponse.result === true) {
            if (forceRefresh === false) {
                this.log(util.format('Cache hit for [%s]', key))
                const generatedFields = generatedFieldsFunction(
                    cacheResponse.getData()
                )
                cacheResponse.setGeneratedFields(generatedFields)

                return cacheResponse
            }
        }

        return cacheResponse
    }

    public static async load<T>(
        url: string,
        userAgent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/36.0',
        extraHeaders = {}
    ): Promise<T | undefined> {
        const headers = {
            'User-Agent': userAgent,
            'Accept-Language': 'en-US',
            ...extraHeaders
        }

        logger.datalog(`Visiting url: ${url}`)

        try {
            const response = await axios.get<T>(url, {
                headers
            })
            if (response.status === 200) {
                return response.data
            }
        } catch (e) {
            if (axios.isAxiosError(e)) {
                if (e.response) {
                    throw new DataProviderLookupError(
                        `Error with statusCode [${e.response.status}] @ url: [${url}] with message: ${e.message}`
                    )
                }
            }
            throw new DataProviderLookupError(
                `Error @ url: [${url}] with message: ${e.message}`
            )
        }
        return
    }

    protected mergeData(current, overrides, addNew = false) {
        const fixed = { ...current }
        for (const [k, v] of Object.entries(overrides)) {
            if (fixed[k] || addNew) {
                fixed[k] = v
            }
        }
        return fixed
    }

    public log(msg: string): void {
        this.logs.push(msg)
        logger.datalog(msg)
    }

    public getLogs() {
        return this.logs
    }
}
