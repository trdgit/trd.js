import { ReleaseNameFullTitle } from '@trd/shared/src/types'

import * as dataCacheRepository from '../repository/data'
import BOMDataProvider, { BOMData } from './bom'
import { DataProvider } from './data-provider'
import { serializeDataCache } from '../helpers/serialize.data_cache'

const mockDataCache = (response: dataCacheRepository.GetDataCacheResponse) => {
    jest.spyOn(dataCacheRepository, 'getDataCache').mockResolvedValueOnce(
        response
    )
}

const testImdbId = 'tt0120689'
const bomFixture: BOMData = {
    id: testImdbId,
    year: 1999,
    screens_us: 500,
    screens_uk: 100,
    has_screens: true,
    limited: true,
    wide: false,
}

describe('BOM fixtures', () => {
    beforeEach(() => {
        jest.spyOn(DataProvider.prototype, 'save').mockImplementation()
        jest.spyOn(DataProvider.prototype, 'log').mockImplementation()
    })

    afterEach(() => {
        jest.clearAllMocks()
    })

    describe('with a cache', () => {
        let lookupByIdSpy

        // approved is irrelevent for bom
        describe('approved = 0', () => {
            beforeEach(() => {
                mockDataCache({
                    data: serializeDataCache(bomFixture),
                    data_immutable: '',
                    approved: 0,
                })

                lookupByIdSpy = jest
                    .spyOn(BOMDataProvider.prototype, 'lookupById')
                    .mockResolvedValue(bomFixture)
            })

            it('should not extract from bom when forceRefresh = false', async () => {
                const bom = new BOMDataProvider()
                const response = await bom.lookupByRlsnameCleaned(
                    'the green mile 1999' as ReleaseNameFullTitle,
                    false,
                    {
                        id: testImdbId,
                        country: 'USA',
                    }
                )

                expect(lookupByIdSpy).not.toHaveBeenCalled()
                expect(response).toMatchSnapshot()
            })
            it('should extract from bom when forceRefresh = true', async () => {
                const bom = new BOMDataProvider()
                const response = await bom.lookupByRlsnameCleaned(
                    'the green mile 1999' as ReleaseNameFullTitle,
                    true,
                    {
                        id: testImdbId,
                        country: 'USA',
                    }
                )

                expect(lookupByIdSpy).toBeCalledWith(testImdbId)
                expect(response).toMatchSnapshot()
            })
        })
    })
})
