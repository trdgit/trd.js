import { ReleaseName } from '@trd/shared/src/types'

import { DataProvider } from './data-provider'
import DataProviderResponse from './response'

export interface MusicData {
    language: string
    source: string
    year: number
    disk_count: number
}

export default class MusicDataProvider extends DataProvider {
    private static LANGUAGES =
        /\-(AA|AB|AF|AL|AM|AR|AS|AY|AT|AZ|BA|BE|BG|BH|BI|BN|BO|BR|BY|CA|CG|CH|CN|CO|CV|CS|CY|CZ|DA|DE|DD|DK|DZ|EE|EH|EL|EN|EO|ES|ET|EU|FA|FI|FJ|FO|FR|FY|GA|GD|GL|GN|GU|GR|HA|HE|HI|HL|HR|HU|HY|HT|IA|IE|IK|IL|IN|IS|IT|IW|JA|JI|JP|JW|KA|KK|KL|KM|KN|KO|KR|KS|KU|KY|LA|LB|LN|LO|LT|LV|LU|MA|MG|MI|MK|ML|MN|MO|MR|MS|MT|MY|MX|NA|NE|NL|NO|OC|OM|OR|PA|PH|PL|PS|PT|QU|RE|RM|RN|RS|RO|RU|RW|SA|SD|SE|SG|SH|SI|SK|SL|SM|SN|SO|SP|SQ|SR|SS|ST|SU|SY|SV|SW|TA|TE|TG|TH|TI|TK|TL|TN|TO|TR|TS|TT|TW|UA|UR|UZ|VI|VO|WO|XH|YO|YU|ZA|ZH|ZU|ZW|CAT|CRO|DUTCH|SLO|SYR|ESP|CPOP|KPOP)\-/i

    protected namespace = 'music'
    protected static defaults: MusicData = {
        language: null,
        source: null,
        year: null,
        disk_count: null
    }

    public getDefaults(existing: object = {}): MusicData {
        return {
            ...MusicDataProvider.defaults,
            ...existing
        }
    }

    public static getDefaultsStatic(existing: object = {}) {
        return {
            ...MusicDataProvider.defaults,
            ...existing
        }
    }

    protected getDeprecated(): string[] {
        return []
    }

    public async lookup(
        rlsname: ReleaseName,
        _forceRefresh = false
    ): Promise<DataProviderResponse<MusicData>> {
        const info = this.getDefaults()
        info['language'] = MusicDataProvider.extractLanguage(
            rlsname as ReleaseName
        )
        info['source'] = MusicDataProvider.extractSource(rlsname)
        info['year'] = MusicDataProvider.extractYear(rlsname)
        info['disk_count'] = MusicDataProvider.extractDiskCount(
            rlsname as ReleaseName
        )

        return new Promise(resolve =>
            resolve(new DataProviderResponse(true, info))
        )
    }

    public static lookupStatic(rlsname: ReleaseName) {
        const info = MusicDataProvider.getDefaultsStatic()
        info['language'] = MusicDataProvider.extractLanguage(rlsname)
        info['source'] = MusicDataProvider.extractSource(rlsname)
        info['year'] = MusicDataProvider.extractYear(rlsname)
        info['disk_count'] = MusicDataProvider.extractDiskCount(rlsname)

        return info
    }

    public static extractLanguage(rlsname: ReleaseName) {
        const matches = rlsname.match(this.LANGUAGES)
        if (matches && matches[1]) {
            return matches[1]
        }
        return 'English'
    }

    public static extractSource(rlsname: ReleaseName) {
        let matches = rlsname.match(/\-\d{0,3}(BD|BDR|BLURAY|HDDVD|HDDVDR)\-/i)
        if (matches && matches[1]) {
            return 'Bluray'
        }
        matches = rlsname.match(
            /\-(\d{0,3}(CDA|SACD|BONUS_CD|MCD|CDR|CD|CDS|CDM|CDEP|CDRS|CDREP)|(?:\d{1,3}x)?(\d{1,2}_?INCH.(CD|CDA|CDS|CDM|CDEP|MCD|CDR|CDRS|CDREP)))\-/i
        )
        if (matches && matches[1]) {
            return 'CD'
        }
        matches = rlsname.match(/\-\d{0,3}(DVD|DVDR|DVDA|DVDS)\-/i)
        if (matches && matches[1]) {
            return 'DVD'
        }
        matches = rlsname.match(
            /(\-\d{0,3}(?:VINYL|VLS|LP|MLP|FLEXI)\-|[\(_-](?:\d{1,3}x)?(\d{1,2}\S?INCH.VINYL)[\)_-]|-\d{0,3}EP-FLAC-)/i
        )
        if (matches && matches[1]) {
            return 'Vinyl'
        }
        matches = rlsname.match(/\-\d{0,3}(TAPE|K7)\-/i)
        if (matches && matches[1]) {
            return 'Tape'
        }
        matches = rlsname.match(
            /\-(AM|AUD|CABLE|DAB|DVBC|DVBS|DVBT|FM|LINE|MD|SAT|SBD|RADIO|FM|LIVE|STREAM)\-/i
        )
        if (matches && matches[1]) {
            return 'Live'
        }
        matches = rlsname.match(/-(\(WEB\)|WEB|WEBFLAC|WEB_FLAC)-/i)
        if (matches && matches[1]) {
            return 'WEB'
        }
        matches = rlsname.match(/(\-FLASH\-)/i)
        if (matches && matches[1]) {
            return 'Flash'
        }
        matches = rlsname.match(/\-\d{0,3}(DAT|VHS|MP3CD)\-/i)
        if (matches && matches[1]) {
            return 'Other'
        }

        return 'CD'
    }

    public static extractYear(rlsname: ReleaseName) {
        const years = [...rlsname.matchAll(/-(19\d{2}|20\d{2})-/g)]
        const last = years.pop()
        if (last && last[1]) {
            return parseInt(last[1])
        }
        return null
    }

    public static extractDiskCount(rlsname: ReleaseName) {
        const matches = rlsname.match(
            /-(\d{1,3})(BD|BDR|BLURAY|HDDVD|HDDVDR|CDA|SACD|BONUS_CD|MCD|CDR|CD|CDS|CDM|CDEP|CDRS|CDREP|VINYL|VLS|LP|MLP|FLEXI|x\d{1,2}_?INCH.(CD|CDA|CDS|CDM|CDEP|MCD|CDR|CDRS|CDREP|VINYL)|EP|TAPE|K7|DAT|VHS|MP3CD)-/i
        )
        if (matches && parseInt(matches[1])) {
            return parseInt(matches[1])
        }

        return 1
    }
}
