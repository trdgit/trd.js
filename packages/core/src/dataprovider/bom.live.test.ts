import { ReleaseNameFullTitle } from '@trd/shared/src/types'

import DataProviderResponse from '../dataprovider/response'
import BoxOfficeMojoDataProvider from './bom'

jest.setTimeout(60000)

const bom = new BoxOfficeMojoDataProvider()

describe('Test BoxOfficeMojoDataProvider', () => {
    test('testLookup', async () => {
        const result = await bom.lookupByRlsnameCleaned(
            'toy story 4' as ReleaseNameFullTitle,
            true,
            {
                id: 'tt1979376',
                country: 'UK'
            }
        )
        expect(result).toBeInstanceOf(DataProviderResponse)

        const data = result.getData()
        expect(data).not.toBeNull()
        expect(data['id']).toMatch(/tt\d+/i)
        expect(typeof data['screens_us']).toBe('number')
        expect(typeof data['screens_uk']).toBe('number')
        expect(data['screens_us']).toBeGreaterThan(0)
        expect(data['screens_uk']).toBeGreaterThan(0)
        expect(data['has_screens']).toBe(true)
        expect(data['limited']).toBe(false)
        expect(data['wide']).toBe(true)
    })
})
