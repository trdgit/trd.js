import DataProviderResponse from './response'

describe('DataProviderResponse', () => {
    describe('getData', () => {
        test('returns generated fields correctly', () => {
            const response = new DataProviderResponse(
                true,
                {
                    field1: 'value1',
                },
                {},
                {
                    generatedField: 'generatedFieldValue',
                }
            )

            expect(response.getData()).toMatchSnapshot()
        })

        test('returns immutable data correctly', () => {
            const response = new DataProviderResponse(
                true,
                {
                    field1: 'value1',
                },
                {
                    field1: 'overridden-value',
                }
            )

            expect(response.getData()).toMatchSnapshot()
        })
    })
})
