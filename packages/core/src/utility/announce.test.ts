import { isAnnounceString } from './announce'

test('Announce string matches test case 1', () => {
    const result = isAnnounceString(
        "-Box- [SECTION] + NEW SHIT IN: Releasename-Group pre'd 8s ago - (user: user)",
        "-&other- [&section] + NEW SHIT IN: &release pre'd"
    )

    expect(result).toBeInstanceOf(Array)
    expect(result[2]).toBe('SECTION')
    expect(result[3]).toBe('Releasename-Group')
})

test('Announce string matches test case 2', () => {
    const result = isAnnounceString(
        'IRC #channel something- [SECTION] + New Case: Section/Releasename-GROUP by user@FRiENDS',
        '[&section] + New Case: &other/&release'
    )

    expect(result).toBeInstanceOf(Array)
    expect(result[1]).toBe('SECTION')
    expect(result[3]).toBe('Releasename-GROUP')
})

test("Announce string doesn't match", () => {
    const result = isAnnounceString(
        'IRC #channel something- blablabla',
        '[&section] + New Case: &other/&release'
    )

    expect(result).toEqual(false)
})
