import {
    ReleaseName,
    ReleaseNameFullTitle,
    ReleaseNameTitle
} from '@trd/shared/src/types'
import { transliterate as tr } from 'transliteration'

import { array_last } from '../helpers/data_types'
import { regexDoesntMatch, regexMatches } from '../helpers/regex'
import SkiplistsModel from '../models/skiplists'

export interface GetTitlePreResponse {
    title: ReleaseNameTitle
    year: number
    country: string
}

export interface GetTitleResponse {
    title: ReleaseNameTitle
    fullTitle: ReleaseNameFullTitle
    year: number
    country: string
}

export const COUNTRY_TLDS =
    'AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CS|CU|CV|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|EH|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SU|SV|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|UM|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|YU|ZA|ZM|ZR|ZW'

export const getGroup = (rlsname: ReleaseName) =>
    rlsname.split('-').pop().toUpperCase()

// export const spacify = (rlsname: ReleaseName): ReleaseNameSpaced =>
//     rlsname
//         .replace(/[^\sa-z0-9]+/gi, ' ')
//         .replace(/\s{2,}/gi, ' ') as ReleaseNameSpaced

export const getReleaseNameFromDirectory = (
    rlsname: ReleaseName
): ReleaseName => {
    if (!rlsname || !rlsname.length || !rlsname.includes('/')) {
        return rlsname
    }

    return rlsname.split('/').pop() as ReleaseName
}

export const cleanTitle = (title): string => {
    return title.replaceAll(/[\._]/g, ' ') as string
}

export const getYear = (name: string) => {
    const lastToken = cleanTitle(name).split(' ').pop()

    const matches = lastToken.match(/(\d{4})/i)
    if (matches && matches[1]) {
        return parseInt(matches[1])
    }
    return null
}

export const getCountry = (name: string) => {
    const lastToken = cleanTitle(name).split(' ').pop()

    const matches = lastToken.match(new RegExp('^(' + COUNTRY_TLDS + ')$'))
    if (matches && matches[1]) {
        return matches[1]
    }
    return null
}

export const passesRequirements = (
    rlsname: ReleaseName,
    requirements: string[],
    regexes: typeof SkiplistsModel
): true | string[] => {
    const failedRequirements = []
    if (Array.isArray(requirements)) {
        for (const requirement of requirements) {
            if (!requirement.length) {
                continue
            }

            if (
                regexes !== null &&
                requirement.substr(0, 7) === '[regex.' &&
                requirement.substr(-1) === ']'
            ) {
                const regexMatch = requirement.match(/^\[regex\.(.*?)\]$/i)
                if (regexMatch && regexMatch.length) {
                    const getRegex = regexes.getSkiplistRegex(regexMatch[1])
                    if (
                        getRegex.length &&
                        regexDoesntMatch(rlsname, getRegex)
                    ) {
                        failedRequirements.push(requirement)
                    }
                }
            } else {
                if (regexDoesntMatch(rlsname, requirement)) {
                    failedRequirements.push(requirement)
                }
            }
        }
        if (failedRequirements.length) {
            return failedRequirements
        }
    }
    return true
}

export const passesRegexSkiplists = (
    rlsname: ReleaseName,
    skiplists,
    regexes = null
) => {
    if (!Array.isArray(skiplists)) {
        return true
    }

    for (const skiplistItem of skiplists) {
        if (!skiplistItem || !skiplistItem.length) {
            continue
        }

        if (
            regexes !== null &&
            skiplistItem.substr(0, 7) === '[regex.' &&
            skiplistItem.substr(-1) === ']'
        ) {
            const regexMatch = skiplistItem.match(/^\[regex\.(.*?)\]$/i)
            if (regexMatch && regexMatch.length) {
                const getRegex = regexes.getSkiplistRegex(regexMatch[1])
                if (getRegex?.length && regexMatches(rlsname, getRegex)) {
                    return skiplistItem
                }
            }
        } else {
            if (regexMatches(rlsname, skiplistItem)) {
                return skiplistItem
            }
        }
    }
    return true
}

const removeGroup = (rlsname: ReleaseName): string =>
    rlsname.replace('-' + getGroup(rlsname), '')

const removeSpecialTags = (rlsnamePartial: string): string =>
    rlsnamePartial
        .replace(
            /[\._]((extended\.)?directors[\._](cut|edition)|remastered|theatrical(\.cut)?|extended\.edition)/i,
            ''
        )
        .replace(/[\._](nfo|dir|sub|sample|proof|covers|cover|sync)fix/i, '')
        .replace(/[\._]real[\._]proper/i, '')
        .replace(/[\._](bdrip|dvdrip)/i, '')
        .replace(/[\._](repack)/i, '')
        .replace(/[_.]iNTERNAL/, '')

const splitRelease = (rlsnamePartial: string): string => {
    // strip by the obvious first!
    const matches = rlsnamePartial.match(
        /^(.*?)[\._](720p|1080p|1280p|1440p|1920p|2160p|2300p|2700p|2880p|S\d+E\d+|S\d+D\d+|E\d+|S\d+|Episode[\._]\d+|\d{1,3}x\d{1,3})/i
    )
    if (matches && matches[1]) {
        rlsnamePartial = matches[1]
    }

    // handle the more complicated cases now
    const splitted = rlsnamePartial.split(/[\._](xvid|[hx]26[45]|dvdr)/i)
    if (splitted.length) {
        rlsnamePartial = splitted[0]
    }

    return rlsnamePartial
}

const createGetTitleResponse = (
    title: string,
    year?: number,
    country?: string
): GetTitleResponse => ({
    title: title.toLowerCase() as ReleaseNameTitle,
    fullTitle: buildFullTitle(
        title,
        year,
        country
    ).toLowerCase() as ReleaseNameFullTitle,
    year,
    country
})

const buildFullTitle = (title: string, year?: number, country?: string) => {
    return [title, year, country].filter(val => val).join(' ')
}

export const getTitle = (incomingRlsname: ReleaseName): GetTitleResponse => {
    // fix for if we pass in with spaces
    const rlsname = incomingRlsname.trim() as unknown as ReleaseName

    // pop off the group
    let rlsnameCleaned = removeGroup(rlsname)
    rlsnameCleaned = removeSpecialTags(rlsnameCleaned)

    // strip by the obvious first!
    rlsnameCleaned = splitRelease(rlsnameCleaned)

    const tokens = cleanTitle(rlsnameCleaned).split(' ')
    const lastToken = array_last(tokens)

    // if the end is a date then strip it off and return
    if (rlsnameCleaned.match(/\d{4}\.\d{2}\.\d{2}/i)) {
        return createGetTitleResponse(
            cleanTitle(rlsnameCleaned.replace(/\.\d{4}\.\d{2}\.\d{2}.*?$/i, ''))
        )
    }

    // if the last token is a year after 1900 we can assume it has no shit
    // if (parseInt(lastToken) > 1900) {
    //     return createGetTitleResponse(
    //         cleanTitle(rlsnameCleaned),
    //         parseInt(lastToken)
    //     )
    // }

    // if one token is a year then let's just take everything before it
    // as this covers most cases
    const slicedTokens = tokens.slice(1)
    for (const [k, tok] of slicedTokens.entries()) {
        const potentialYear = parseInt(tok)
        if (potentialYear > 1900) {
            // fix for double year!
            if (slicedTokens[k + 1] && parseInt(slicedTokens[k + 1]) > 1900) {
                return createGetTitleResponse(
                    tokens.slice(0, k + 2).join(' '),
                    parseInt(tokens[k + 2])
                )
            }
            return createGetTitleResponse(
                tokens.slice(0, k + 1).join(' '),
                potentialYear
            )
        }
    }

    if (
        [
            'limited',
            'dc',
            'proper',
            'uncut',
            'extended',
            'chrono',
            'extras',
            'festival',
            'docu',
            'stv',
            'ntsc',
            'pal',
            'nordic',
            'dutch',
            'russian',
            'finnish',
            'swedish',
            'norwegian',
            'danish',
            'czech',
            'german',
            'dubbed',
            'subbed',
            'subpack',
            'french',
            'spanish',
            'portugese'
        ].includes(lastToken.toLowerCase())
    ) {
        return createGetTitleResponse(tokens.slice(0, -1).join(' '))
    }

    const countryMatches = lastToken.match(new RegExp(`^(${COUNTRY_TLDS})$`))
    if (countryMatches) {
        return createGetTitleResponse(
            tokens.slice(0, -1).join(' '), // remove country from the strict title
            undefined,
            countryMatches[1]
        )
    }

    return createGetTitleResponse(
        cleanTitle(rlsnameCleaned),
        undefined,
        countryMatches?.[1] ?? undefined
    )
}

export const transliterate = (str: string) => {
    // commented out as no idea what this does anymore
    // if (!preg_match('/[\x80-\xff]/', $str)) {
    //     return $str;
    // }

    return tr(str)
}
