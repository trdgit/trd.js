export default class ByteArray {
    private bytes: Buffer

    public constructor(str: string) {
        if (!str) {
            str = ''
        }
        this.bytes = this.fromString(str)
    }

    public fromString(str: string): Buffer {
        return Buffer.from(str, 'utf8')
    }

    public append(str: string) {
        this.bytes = Buffer.concat([this.bytes, this.fromString(str)])
    }

    public toEnd(startingByte: number): Buffer {
        return this.bytes.subarray(startingByte)
    }

    public static toString(bytes: Buffer): string {
        return bytes.toString('utf8')
    }

    public asString(): string {
        return ByteArray.toString(this.bytes)
    }

    public getBytes(): Buffer {
        return this.bytes
    }

    public findByte(byte: string): number {
        const target = byte.charCodeAt(0)
        return this.bytes.indexOf(target)
    }

    public findByteRange(byte: string): [number, number] {
        const target = byte.charCodeAt(0)
        return [this.bytes.indexOf(target), this.bytes.lastIndexOf(target)]
    }

    public split(byte: string) {
        const index = this.findByte(byte)
        if (index >= 0) {
            return this.bytes.subarray(0, index - 1)
        }
        return
    }

    public splitToString(byte: string) {
        return ByteArray.toString(this.split(byte))
    }

    private eraseUntil(index: number): void {
        this.bytes = this.bytes.subarray(index + 1)
    }

    public pickUntilByte(byte: string) {
        const index = this.findByte(byte)
        if (index >= 0) {
            const string = ByteArray.toString(this.bytes.subarray(0, index))

            // look for a sequence
            let end = index
            let c = index
            let done = false
            do {
                if (byte.charCodeAt(0) === this.bytes[c]) {
                    end = c
                    c++
                } else {
                    done = true
                }
            } while (!done)

            this.eraseUntil(end)
            return string
        }
        return false
    }
}
