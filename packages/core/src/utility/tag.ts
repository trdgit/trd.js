import { ReleaseName } from '@trd/shared/src/types'
import { Tag } from '@trd/shared/src/types'

import SettingsModel from '../models/settings'
import SkiplistsModel from '../models/skiplists'
import { passesRegexSkiplists, passesRequirements } from './release'

interface TagGuessResponseResults {
    requires: Record<string, string[]>
    skiplist: Record<string, string[]>
}

interface TagGuessResponse {
    tags: string[]
    results: TagGuessResponseResults
}

export const guessTagFromReleaseName = (
    rlsname: ReleaseName,
    tags?: Tag[]
): TagGuessResponse => {
    const tagOptions = SettingsModel.get('tag_options')

    if (!tags?.length) {
        tags = Object.keys(tagOptions)
    }

    const requiresMap = {}
    const skiplistMap = {}

    // iterate over all tags
    const possibleTags = tags.filter(tag => {
        if (!requiresMap[tag]) {
            requiresMap[tag] = []
        }
        if (!skiplistMap[tag]) {
            skiplistMap[tag] = []
        }

        const rlsPassesRequirements = passesRequirements(
            rlsname,
            tagOptions[tag].tag_requires,
            SkiplistsModel
        )
        if (rlsPassesRequirements !== true) {
            requiresMap[tag].push(rlsPassesRequirements)
            return false
        }

        const rlsPassesSkiplist = passesRegexSkiplists(
            rlsname,
            tagOptions[tag].tag_skiplist,
            SkiplistsModel
        )
        if (rlsPassesSkiplist !== true) {
            skiplistMap[tag].push(rlsPassesSkiplist)
            return false
        }

        return true
    })

    return {
        tags: possibleTags,
        results: { requires: requiresMap, skiplist: skiplistMap },
    }
}
