import { ReleaseName } from '@trd/shared/src/types'

import SkiplistsModel from '../models/skiplists'
import {
    getTitle,
    getReleaseNameFromDirectory,
    transliterate,
    passesRequirements,
} from './release'

describe('Test getReleaseNameFromDirectory', () => {
    beforeAll(async () => {
        return new Promise(resolve => {
            const interval = setInterval(() => {
                const data = SkiplistsModel.getData()
                if (data) {
                    clearInterval(interval)
                    resolve(null)
                }
            }, 500)
        })
    })

    test('Test with just rls', () => {
        const result = getReleaseNameFromDirectory(
            'foobar-group' as ReleaseName
        )
        expect(result).toBe('foobar-group')
    })

    test('Test with directory', () => {
        const result = getReleaseNameFromDirectory(
            'dir1/dir2/foobar-group' as ReleaseName
        )
        expect(result).toBe('foobar-group')
    })
})

describe('getTitle', () => {
    test('handles awkward name correctly', () => {
        const result = getTitle(
            'Hooten.And.The.Lady.S01E01.1080p.HDTV.x264-group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'hooten and the lady',
            fullTitle: 'hooten and the lady',
            year: undefined,
            country: undefined,
        })
    })

    test('handles country tld correctly', () => {
        const result = getTitle(
            'TV.Show.UK.S01E01.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'tv show',
            fullTitle: 'tv show uk',
            year: undefined,
            country: 'UK',
        })
    })

    test('handles year correctly', () => {
        const result = getTitle(
            'Movie.Title.2010.REMASTERED.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title',
            fullTitle: 'movie title 2010',
            year: 2010,
            country: undefined,
        })
    })

    test('handles double year correctly', () => {
        const result = getTitle(
            'Movie.Title.2049.2017.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title 2049',
            fullTitle: 'movie title 2049 2017',
            year: 2017,
            country: undefined,
        })
    })

    test('strips out remastered', () => {
        const result = getTitle(
            'Movie.Title.2010.REMASTERED.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title',
            fullTitle: 'movie title 2010',
            year: 2010,
            country: undefined,
        })
    })
    test('strips out directors edition', () => {
        const result = getTitle(
            'Movie.Title.2010.DIRECTORS.EDITION.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title',
            fullTitle: 'movie title 2010',
            year: 2010,
            country: undefined,
        })
    })
    test('strips out directors cut', () => {
        const result = getTitle(
            'Movie.Title.2010.DIRECTORS.CUT.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title',
            fullTitle: 'movie title 2010',
            year: 2010,
            country: undefined,
        })
    })
    test('strips out extended directors cut', () => {
        const result = getTitle(
            'Movie.Title.2010.EXTENDED.DIRECTORS.CUT.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title',
            fullTitle: 'movie title 2010',
            year: 2010,
            country: undefined,
        })
    })
    test('strips out theatrical', () => {
        const result = getTitle(
            'Movie.Title.2010.THEATRICAL.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title',
            fullTitle: 'movie title 2010',
            year: 2010,
            country: undefined,
        })
    })
    test('strips out theatrical cut', () => {
        const result = getTitle(
            'Movie.Title.2010.THEATRICAL.CUT.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title',
            fullTitle: 'movie title 2010',
            year: 2010,
            country: undefined,
        })
    })
    test('strips out theatrical cut internal', () => {
        const result = getTitle(
            'Movie.Title.2010.THEATRICAL.CUT.iNTERNAL.1080p.WEB.H264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'movie title',
            fullTitle: 'movie title 2010',
            year: 2010,
            country: undefined,
        })
    })

    test('handles yyyy.mm.dd syntax', () => {
        const result = getTitle(
            'Show.Name.2022.01.01.Something.1080p.WEB.h264-Group' as ReleaseName
        )
        expect(result).toStrictEqual({
            title: 'show name',
            fullTitle: 'show name',
            year: undefined,
            country: undefined,
        })
    })
})

describe('transliterate', () => {
    test('test transliteration', () => {
        const result = transliterate('renée')
        expect(result).toBe('renee')
    })
})

describe('testRegexSyntaxPassesRequirements', () => {
    test('testRegexSyntaxPassesRequirements', () => {
        const requirements = ['[regex.non_english_language]']

        const shouldPass = passesRequirements(
            'this.is.swedish-group' as ReleaseName,
            requirements,
            SkiplistsModel
        )
        expect(shouldPass).toBe(true)

        const shouldFail = passesRequirements(
            'this.is.nothing-group' as ReleaseName,
            requirements,
            SkiplistsModel
        )
        expect(shouldFail).not.toBe(true)
    })
})
