import ByteArray from './byte-array'

describe('Tests for ByteArray', () => {
    test('testAppendFromEmpty', () => {
        const bytes = new ByteArray('')
        bytes.append('IRC message\n')
        const ret = bytes.pickUntilByte('\n')
        expect(ret).toBe('IRC message')
    })

    test('testHasBytes', () => {
        const bytes = new ByteArray('IRC message\n')
        expect(bytes.getBytes().length).toBeGreaterThan(0)
        expect(bytes.getBytes().length).toBe(12)
    })

    test('testHasBytesMultipleMessages', () => {
        const bytes = new ByteArray('IRC message\nIRC message 2\n')
        expect(bytes.getBytes().length).toBeGreaterThan(0)
        expect(bytes.getBytes().length).toBe(26)
    })

    test('testAsString', () => {
        const string = 'Message'
        const bytes = new ByteArray(string)
        expect(bytes.asString()).toBe(string)
    })

    test('testBasicRead', () => {
        const string = 'IRC message\n'
        const bytes = new ByteArray(string)
        const ret = bytes.pickUntilByte('\n')
        expect(ret).not.toBe(false)
        expect(ret).toBe('IRC message')
    })

    test('testMultiRead', () => {
        const string = 'IRC message\nIRC message 2\n'
        const bytes = new ByteArray(string)
        const ret = bytes.pickUntilByte('\n')
        expect(ret).not.toBe(false)
        expect(ret).toBe('IRC message')
        const ret2 = bytes.pickUntilByte('\n')
        expect(ret2).not.toBe(false)
        expect(ret2).toBe('IRC message 2')
    })

    test('testLoop', () => {
        const string = 'IRC message\nIRC message 2\n'
        const bytes = new ByteArray(string)
        let iterations = 0
        const strings: (string | false)[] = []
        let str: string | false
        while ((str = bytes.pickUntilByte('\n'))) {
            strings.push(str)
            iterations++
        }
        expect(iterations).toBe(2)
        expect(strings).toStrictEqual(['IRC message', 'IRC message 2'])
    })

    test('testMultipleNewLines', () => {
        const string = 'IRC message\n\nIRC message 2\n\n'
        const bytes = new ByteArray(string)
        let iterations = 0
        const strings: (string | false)[] = []
        let str: string | false
        while ((str = bytes.pickUntilByte('\n'))) {
            strings.push(str)
            iterations++
        }
        expect(iterations).toBe(2)
        expect(strings).toStrictEqual(['IRC message', 'IRC message 2'])
    })

    test('testSplitRead', () => {
        const string = 'IRC message\nIRC mess'
        const bytes = new ByteArray(string)
        let iterations = 0
        const strings: (string | false)[] = []
        let str: string | false
        while ((str = bytes.pickUntilByte('\n'))) {
            strings.push(str)
            iterations++
        }
        expect(bytes.asString()).toBe('IRC mess')
        bytes.append('age 2\nIRC message 3\n')
        while ((str = bytes.pickUntilByte('\n'))) {
            strings.push(str)
            iterations++
        }
        expect(iterations).toBe(3)
        expect(strings).toStrictEqual([
            'IRC message',
            'IRC message 2',
            'IRC message 3',
        ])
    })
})
