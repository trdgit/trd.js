import { escape } from '../helpers/regex'

export const isAnnounceString = (str: string, matchString) => {
    const matchRegex =
        '[\\s]?' + escape(matchString).replace('/[\\s]+/', '\\s+') + '[\\s]?'

    const lookup =
        '&section|&rlsname|&release|&user|&group|&altbookmark|&folder|&size|&date|&multiplier|&reason|&nuker|&time|&other'.split(
            '|'
        )
    let newRegex = matchRegex
    for (const item of lookup) {
        newRegex = newRegex.replace(new RegExp(item, 'i'), '([^\\s]+)')
    }

    const matches = str.match(new RegExp(newRegex, 'i'))

    if (matches !== null) {
        return matches
    }
    return false
}
