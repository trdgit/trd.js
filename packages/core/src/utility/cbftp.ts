import { ReleaseName } from '@trd/shared/src/types'
import axios from 'axios'
import * as dgram from 'dgram'
import * as https from 'https'
import * as util from 'util'

import * as logger from '../helpers/logger'

export default class CBFTP {
    private host = null
    private port = null
    private password = null
    private agent: https.Agent

    constructor(host: string, port, password: string) {
        this.host = host
        this.port = port
        this.password = password
        this.agent = new https.Agent({
            rejectUnauthorized: false,
        })
    }

    public sendCommand(command: string, args: (string | number)[] = []) {
        const message = util.format(
            '%s %s %s',
            this.password,
            command,
            args.join(' '),
        )

        logger.cbftp(`Sent: "${command} ${args.join(' ')}" to cbftp`)

        const client = dgram.createSocket('udp4')
        const data = Buffer.from(message)
        client.send(data, this.port, this.host, function (_err, _bytes) {
            client.close()
        })
    }

    // public function prepare($tag, $rlsname, $chain, $downloadOnly = null)
    // {
    //     $args = [$tag, $rlsname, $this->joinChain($chain)];
    //     if ($downloadOnly !== null) {
    //         $args[] = $this->joinChain($downloadOnly);
    //     }
    //     $this->sendCommand('prepare', $args);
    // }

    public race(
        tag: string,
        rlsname: ReleaseName,
        chain: string[],
        downloadOnly: null | string[] = null,
    ): void {
        const args = [tag, rlsname, this.joinChain(chain)]
        if (downloadOnly !== null) {
            args.push(this.joinChain(downloadOnly))
        }
        this.sendCommand('race', args)
    }

    public idle(chain: string[], idleTime = 120): void {
        const args = [this.joinChain(chain), idleTime]
        this.sendCommand('idle', args)
    }

    public async rawCapture(command: string, sites: string[] = [], path = '/') {
        const payload = {
            command: command,
            sites_all: !sites.length, // run on all sites
            path: path, // the path to cwd to before running command
            timeout: 10, // max wait before failing
            async: false,
        }

        if (sites.length) {
            payload['sites'] = sites
        }

        const url = `https://${this.host}:${this.port}/raw`

        try {
            const response = await axios.post(url, payload, {
                httpsAgent: this.agent,
                timeout: 12000,
                auth: {
                    username: '',
                    password: this.password,
                },
            })

            if (response.status === 200) {
                return response.data
            }
        } catch (e) {
            logger.error(e.reseponse)
        }

        return []
    }

    public async getSiteInfo(sitename: string) {
        const url = `https://${this.host}:${this.port}/sites/${sitename}`
        try {
            const response = await axios.get(url, {
                httpsAgent: this.agent,
                auth: {
                    username: '',
                    password: this.password,
                },
            })
            return response.data
        } catch (_e) {
            logger.cbftp(`Failed to get site info for: ${sitename}`)
            return {}
        }
    }

    // public static function siteHasSection($siteSections, $section)
    // {
    //     foreach ($siteSections as $s) {
    //         if ($s['name'] === $section) {
    //             return true;
    //         }
    //     }
    //     return false;
    // }

    private joinChain(chain: string[]): string {
        return chain.join(',')
    }

    public parseStatToCredits(statResponse: string) {
        const matches = statResponse.match(
            /(?:Credits|C|Creds):\s{0,}([\d\.]+)\s{0,}(MB|MiB|GB|GiB|TB|TiB)/ims,
        )

        let multiplier = 1

        if (matches && matches[1]) {
            switch (matches[2]) {
                case 'MB':
                    multiplier = 1.024
                    break
                case 'GB':
                    multiplier = 1.024 * 1024
                    break
                case 'GiB':
                    multiplier = 1024
                    break
                case 'TB':
                    multiplier = 1.024 * 1024 * 1024
                    break
                case 'TiB':
                    multiplier = 1024 * 1024
                    break
                default:
                    break
            }

            return parseFloat(matches[1]) * multiplier
        }
        // Return 0 as fallback so that we can skip writing it to Site JSON
        return 0
    }
}
