import { db } from '../../helpers/db'
import * as logger from '../../helpers/logger'
import { convertLegacyData } from '../../helpers/serialize.data_cache'
import { getAllDataCache, updateDataCache } from '../../repository/data'

// const isUsingJSONForCache = () => process.env.DATACACHE_JSON === '1'

/*
const unserialize = isUsingJSONForCache()
    ? unserializeDataCache
    : phpUnSerialize

const adjust = (k: string, field: string, data: unknown): 1 | 0 => {
    if (is_string(data[field])) {
        const old = data[field]
        data[field] = parseInt(data[field])
        console.log(`Changing data types for ${k}`, {
            field,
            old,
            new: data[field],
        })
        return 1
    }
    return 0
}

const adjustToFloat = (k: string, field: string, data: unknown): 1 | 0 => {
    if (is_string(data[field])) {
        const old = data[field]
        data[field] = parseFloat(data[field])
        console.log(`Changing data types for ${k}`, {
            field,
            old,
            new: data[field],
        })
        return 1
    }
    return 0
}

interface FixBrokenDataCacheResponse {
    updated: number
    totalEntries: number
}

type DataCacheRow = {
    data: string
    k: string
}

const fixBrokenDataCache = async (): Promise<FixBrokenDataCacheResponse> => {
    const imdbCacheEntries = await fetchAll<DataCacheRow>(
        "SELECT data,k FROM data_cache WHERE namespace = 'imdb'",
    )

    const totalEntries = imdbCacheEntries.length
    let updated = 0
    imdbCacheEntries.forEach(entry => {
        const data = unserialize(entry.data)
        if (data) {
            updated += adjust(entry.k, 'year', data)
            updated += adjust(entry.k, 'votes', data)
            updated += adjustToFloat(entry['k'], 'rating', data)

            // save
            // update(
            //     'data_cache',
            //     {
            //         data: phpSerialize(data),
            //     },
            //     {
            //         k: data['k'],
            //     }
            // )
        }
    })

    return { updated, totalEntries }
}
*/

export const surgeon = async flags => {
    const simulate = !!flags.simulate
    const cleanupBad = !!flags.cleanupBad
    if (process.env.DATACACHE_JSON === '1') {
        logger.task('Attempting to upgrade data_cache to json format')
        const items = await getAllDataCache()
        if (items) {
            let updated = 0
            let bad = 0
            let deleted = 0
            for (const item of items) {
                let updatedDataString
                if (item.data?.length) {
                    updatedDataString = convertLegacyData(item.data)
                } else {
                    logger.task(
                        `Weird string encountered for ${item.k} data=${item.data}`
                    )
                }
                let updatedDataImmutableString
                if (item.data_immutable?.length) {
                    updatedDataImmutableString = convertLegacyData(
                        item.data_immutable
                    )
                }
                if (updatedDataString) {
                    if (!simulate) {
                        await updateDataCache(
                            item.k,
                            {
                                id: item.id,
                                updated: item.updated,
                                data: updatedDataString,
                                ...(updatedDataImmutableString && {
                                    data_immutable: updatedDataImmutableString
                                })
                            },
                            false
                        )
                    }
                    updated++
                } else {
                    try {
                        JSON.parse(item.data)
                    } catch (_e) {
                        if (cleanupBad) {
                            await db.deleteRows('data_cache', {
                                id: item.id
                            })
                            deleted++
                        } else {
                            bad++
                        }
                    }
                }
            }
            logger.task(
                `${simulate ? 'Would have... ' : ''}Updated ${updated} rows`
            )
            if (bad > 0) {
                logger.task(`Found ${bad} bad rows`)
                logger.task('Run again with --cleanupBad flag to delete')
            }
            if (deleted > 0) {
                logger.task(`Deleted ${deleted} bad rows`)
            }
        } else {
            logger.task('No rows found to update')
        }
    }
    // logger.task('Fixing broken data_cache entries for imdb')
    // const { updated, totalEntries } = await fixBrokenDataCache()
    // logger.task(
    //     `Fixed ${updated} out of ${totalEntries} data_cache entries for imdb`
    // )
}
