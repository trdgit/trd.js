import * as util from 'util'

import * as logger from '../../helpers/logger'
import { query } from '../../helpers/mysql'
import SettingsModel from '../../models/settings'

const msg = (table: string, rows: number): string =>
    `Cleaned up ${rows} rows from ${table}`

const deleteEntries = (
    sql: string,
    tableName: string,
    params = [],
): Promise<string> => {
    return query(sql, params).then(affectedRows => msg(tableName, affectedRows))
}

const deleteOldRaceSiteEntries = (): Promise<string> =>
    deleteEntries(
        'DELETE FROM race_site WHERE race_id IN ( SELECT id FROM race WHERE created < DATE_SUB(NOW(), INTERVAL 30 DAY))',
        'race_site',
    )

const deleteOldRaceEntries = (): Promise<string> =>
    deleteEntries(
        'DELETE FROM race WHERE created < DATE_SUB(NOW(), INTERVAL 30 DAY)',
        'race',
    )

const deleteOldPreEntries = (interval: number): Promise<string> => {
    logger.task(util.format('Deleting pres older than %d days', interval))
    return deleteEntries(
        `DELETE FROM pre WHERE created < DATE_SUB(NOW(), INTERVAL ${interval} DAY)`,
        'pre',
    )
}

export const cleanup = async () => {
    logger.task('Deleting races older than 30 days')

    const tasks: Promise<string>[] = []
    tasks.push(deleteOldRaceSiteEntries())
    tasks.push(deleteOldRaceEntries())

    const interval = parseInt(SettingsModel.get('pre_retention'))
    if (interval > 0) {
        tasks.push(deleteOldPreEntries(interval))
    } else {
        logger.task(
            util.format(
                'Not wiping any pres - interval setting is set to %d days',
                interval,
            ),
        )
    }

    const results = await Promise.all(tasks)
    results.map(message => logger.task(message))
}
