import { ReleaseName } from '@trd/shared/src/types'
import * as logger from '../../helpers/logger'
import Race from '../../race/race'
import { TaskFlags } from '../../task'
import SitesModel from '@trd/core/src/models/sites'

export const simulate = async (flags: TaskFlags) => {
    const params = flags._ as string[]
    if (!params || params.length != 2) {
        logger.task('Please supply a tag and rlsname')
        return
    }

    const [tag, rlsname] = params

    // data_cache
    logger.task('Running race sim')

    const siteList = []
    for (const [siteName] of Object.entries(SitesModel.getData())) {
        siteList.push(siteName)
    }

    const race = new Race(true)
    race.addSites(siteList)
    const result = await race.race(tag, rlsname as ReleaseName)
    logger.task(`isRace = ${result.isRace() ? 'yes' : 'no'}`)
    logger.task(`validSites = ${result.validSites.join(',')}`)
    logger.task(
        `invalidSites = ${result.invalidSites.map(iv => iv.site).join(',')}`
    )
}
