import axios from 'axios'
import CSV from 'csv-parser'
import { Readable, Transform } from 'stream'
import zlib from 'zlib'

import { TaskFlags } from './../../task'
import { db } from '../../helpers/db'

const ratingsURL = 'https://datasets.imdbws.com/title.ratings.tsv.gz'

export const updateIMDBRatingsFromDataFile = async (flags: TaskFlags) => {
    try {
        const sqls = await doUpdateIMDBRatingsFromDataFile()
        if (flags?.synchronous === true) {
            for (const sql of sqls) {
                await db.query(sql)
            }
        } else {
            await Promise.all(sqls.map(sql => db.query(sql)))
        }
    } catch (e) {
        console.error(e)
    }
}

export const doUpdateIMDBRatingsFromDataFile = async (): Promise<string[]> => {
    const stream = await readCsv(ratingsURL)

    const getIds = await db.fetchAll(
        `select id FROM data_cache where namespace = 'imdb'`
    )
    const existingIds = getIds.map(row => row.id)

    return new Promise((resolve, reject) => {
        const queries = []
        stream.on('data', data => {
            const { tconst: id, averageRating: rating, numVotes: votes } = data

            if (id && existingIds.includes(id) && rating > 0 && votes > 0) {
                queries.push(
                    `
                        UPDATE 
                            data_cache 
                        SET 
                            data = JSON_SET(data, 
                                "$.rating", ${rating}, 
                                "$.votes", ${votes}
                            ) 
                        WHERE id = '${id}'`
                )
            }
        })

        stream.on('end', () => {
            if (queries.length) {
                resolve(queries)
            } else {
                reject('Nothing to match')
            }
        })
    })
}

const getStream = async (url: string, headers = {}): Promise<Readable> => {
    try {
        const res = await axios.get<Readable>(url, {
            headers,
            responseType: 'stream' // we specify the response type
        })

        return res.data
    } catch (_err) {
        throw new Error('Error in get request')
    }
}

const getTransformStream = (): Transform => {
    const transform = new Transform({
        transform: (chunk, encoding, next) => {
            next(null, chunk)
        }
    })
    return transform
}

const readCsv = async (url: string): Promise<Readable> => {
    const httpStream = await getStream(url, {
        'Accept-Encoding': 'gzip'
    })

    const transform = getTransformStream()

    const csvReadStream = httpStream
        .pipe(zlib.createGunzip())
        .pipe(transform)
        .pipe(
            CSV({
                separator: '\t'
            })
        ) as Readable

    return csvReadStream
}
