// import { DBParam } from '../../helpers/db'
// import { fetchAll as mysqlFetchAll } from '../../helpers/db'
// import * as logger from '../../helpers/logger'
// import sqlite from '../../helpers/sqlite'

// export const migrate = async () => {
//     // data_cache
//     logger.task('Find data_cache entries')

//     const dataCacheEntries = await mysqlFetchAll('SELECT * FROM data_cache')
//     logger.task(`Trying to migrate ${dataCacheEntries}`)
//     for (const row of dataCacheEntries) {
//         await sqlite.insert(
//             'data_cache',
//             row as unknown as Record<string, DBParam>, // hack :)
//         )
//     }

//     logger.task('Migrated data_cache entries - DONE!')

//     // pre
//     logger.task('Migrating pre entries')

//     const preEntries = await mysqlFetchAll('SELECT * FROM pre')
//     for (const row of preEntries) {
//         await sqlite.insert('pre', row as Record<string, DBParam>)
//     }

//     logger.task('Migrated pre entries - DONE!')
// }
