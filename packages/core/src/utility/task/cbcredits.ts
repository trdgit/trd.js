import * as util from 'util'

import * as logger from '../../helpers/logger'
import SitesModel from '../../models/sites'
import SettingsModel from '../../models/settings'
import CBFTP from '../cbftp'

export const cbcredits = async () => {
    logger.task('Refreshing cbftp credits')

    for (const target of SitesModel.getSitesArray()) {
        const bncs = SitesModel.findAllBNCs(target)

        const cb = new CBFTP(
            SettingsModel.get('cbftp_host'),
            SettingsModel.get('cbftp_api_port'),
            SettingsModel.get('cbftp_password')
        )

        const response = await cb.rawCapture(
            "site stat",
            bncs
        )
        if (response) {
            if (response.successes?.length > 0) {
                for (const row of response['successes']) {
                    const bnc = row.name
                    const result = row.result
                    const credits = cb.parseStatToCredits(result)
                    if (credits != 0) {
                        SitesModel.setCredits(target, bnc, credits)
                        logger.task(
                            util.format(
                                'Updated credits on: %s = %s',
                                bnc,
                                credits.toString()
                            )
                        )
                    }
                }
            }
        }
    }
    return 0;
}
