import cliProgress from 'cli-progress'
import * as util from 'util'

import TVMazeDataProvider from '../../dataprovider/tvmaze'
import axios from '../../helpers/axios'
import { DBRow, db } from '../../helpers/db'
import * as logger from '../../helpers/logger'
import { unserializeDataCache } from '../../helpers/serialize.data_cache'
import SettingsModel from '../../models/settings'
import { updateDataCacheId } from '../../repository/data'

import phpUnserialize from 'locutus/php/var/unserialize'

const isUsingJSONForCache = () => process.env.DATACACHE_JSON === '1'

const unserialize = isUsingJSONForCache()
    ? unserializeDataCache
    : phpUnserialize

const updateItems = async (items: DBRow[], progress = false) => {
    const interval = 1000

    let progressBar
    if (progress) {
        progressBar = new cliProgress.SingleBar(
            {},
            cliProgress.Presets.shades_classic
        )
        progressBar.start(items.length, 0)
    }

    const promises = items.map((row, i) => {
        return new Promise(resolve => {
            setTimeout(
                async () => {
                    const result = await refreshItem(row)
                    if (progressBar) {
                        progressBar.update(i + 1)
                    }
                    resolve(result)
                },
                interval * (i + 1)
            )
        })
    })

    const results = await Promise.all(promises)
    if (progressBar) {
        progressBar.stop()
    }
    return results.filter(res => res).length
}

export const refreshUpdatedShows = async () => {
    const url = 'https://api.tvmaze.com/updates/shows?since=day'
    const response = await axios.get(url)
    if (response.status === 200) {
        const ids = Object.keys(response.data)
        const items = await db.fetchAll(
            `
            SELECT 
                * 
            FROM 
                data_cache 
            WHERE
                namespace='tvmaze' AND 
                    id IN ('${ids.join("','")}')
        `
        )

        logger.task(
            util.format(
                'Found %d items that need updating in cache',
                items.length
            )
        )

        return updateItems(items)
    }
}

export const refreshCache = async flags => {
    const progress = !!flags.progress

    // clean out any shit while we're here
    await db.query("DELETE FROM data_cache WHERE k LIKE '%HDTV%'")

    // TODO: move to repository layer
    const items = await db.fetchAll(`
            SELECT * FROM data_cache WHERE
            namespace = 'tvmaze'
            AND (updated < DATE_SUB(NOW(), INTERVAL 14 DAY)
            OR updated IS NULL)
        `)

    logger.task(
        util.format(
            'Found %d items that potentially need updating in cache',
            items.length
        )
    )

    if (!items.length) {
        return
    }

    const result = await updateItems(items, progress)
    logger.task(util.format('Updated %d items', items.length))

    return result
}

const refreshItem = async (row): Promise<boolean> => {
    const dataProvider = new TVMazeDataProvider()

    const currentData = unserialize(row['data'])
    if (
        SettingsModel.get('refresh_ended_shows') === false &&
        currentData['status'] == 'Ended'
    ) {
        return false
    }

    const showName = row['k'].replace(`${row['namespace']}:`, '')
    if (!row.id) {
        return false
    }

    try {
        const data = await dataProvider.lookupById(row['id'])
        if (data?.id) {
            if (!row['id']) {
                updateDataCacheId(row['k'], data['id'])
            }
            await dataProvider.save(showName, data)
            return true
        } else {
            db.deleteRows('data_cache', {
                id: row['id']
            })
        }
    } catch (_e) {
        // lookup failed, do nothing
    }

    return false
}
