import { DateTime } from 'luxon'

import { TaskFlags } from './../../task'
import { addSeconds, toMySQL } from '../../helpers/date'
import { db } from '../../helpers/db'
import * as logger from '../../helpers/logger'
import { unserializeDataCache } from '../../helpers/serialize.data_cache'
import SettingsModel from '../../models/settings'
import { updateDataCacheUpdated } from '../../repository/data'

import phpUnserialize from 'locutus/php/var/unserialize'

const now = DateTime.now()
const fourteenDaysInSeconds = 60 * 60 * 24 * 14

const isUsingJSONForCache = () => process.env.DATACACHE_JSON === '1'

const unserialize = isUsingJSONForCache()
    ? unserializeDataCache
    : phpUnserialize

const shouldSkipEnded = (status: string): boolean =>
    SettingsModel.get('refresh_ended_shows') === false && status === 'Ended'

export const redistributeCache = async (flags: TaskFlags) => {
    const isDryRun = flags?.dryRun === true
    const dryRunString = isDryRun ? ` (dry-run)` : ''

    logger.task(`Redistributing cache entries${dryRunString}`)

    // TODO: move to repository layer
    const items = await db.fetchAll<{ id: number; data: string }>(`
            SELECT id,data FROM data_cache WHERE
            namespace = 'tvmaze'
        `)

    const progressionInSeconds = Math.floor(
        fourteenDaysInSeconds / items.length
    )

    let c = 0
    for (const row of items) {
        const currentData = unserialize(row['data'])
        if (shouldSkipEnded(currentData['status'])) {
            continue
        }

        const updated = toMySQL(addSeconds(now, progressionInSeconds * c))

        if (!isDryRun) {
            updateDataCacheUpdated(row.id, updated)
        } else {
            logger.task(`[${currentData['title']}] would go to [${updated}]`)
        }
        c++
    }

    if (!isDryRun) {
        logger.task(`Done! Updated ${c} entries`)
    }
}
