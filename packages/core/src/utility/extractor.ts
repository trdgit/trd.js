import { ReleaseName, SiteString } from '@trd/shared/src/types'

import { PCRE2JS } from '../helpers/regex'
import { isAnnounceString } from './announce'

interface ExtractionResult {
    section: string | null
    rlsname: ReleaseName | null
}

export const extract = (
    strings: Partial<SiteString>,
    checkPrefixes: (keyof SiteString)[],
    target
): ExtractionResult => {
    for (const prefix of checkPrefixes) {
        if (!strings[prefix]) {
            continue
        }

        const isRegexPrefix = `${prefix}-isregex`
        const sectionPrefix = `${prefix}-section`
        const rlsPrefix = `${prefix}-rls`

        if (strings[isRegexPrefix] && strings[isRegexPrefix] == 1) {
            const matches = target.match(PCRE2JS(String(strings[prefix])))
            if (matches !== null) {
                const ret = {
                    rlsname: matches[strings[rlsPrefix]] as ReleaseName,
                    section: null,
                }
                if (strings[sectionPrefix] && matches[strings[sectionPrefix]]) {
                    ret['section'] = matches[strings[sectionPrefix]]
                }

                return ret
            }
        } else {
            const announceString = isAnnounceString(target, strings[prefix])
            if (
                announceString !== false &&
                announceString[strings[rlsPrefix]]
            ) {
                const ret = {
                    rlsname: announceString[strings[rlsPrefix]] as ReleaseName,
                    section: null,
                }
                if (
                    strings[sectionPrefix] &&
                    announceString[strings[sectionPrefix]]
                ) {
                    ret['section'] = announceString[strings[sectionPrefix]]
                }
                return ret
            }
        }
    }
    return {
        section: null,
        rlsname: null,
    }
}
