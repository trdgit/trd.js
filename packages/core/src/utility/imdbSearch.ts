import { AxiosRequestConfig } from 'axios'
import { parse } from 'node-html-parser'

import axios from '../helpers/axios'

type FilterFunction = (input: string) => boolean

export interface SearchResult {
    title: string
    url: string
}

const imdbQueryTypes: string[] = ['tvSpecial', 'short', 'movie']

interface SearchDefinition {
    (
        query: string,
        config: AxiosRequestConfig,
        titleFilter?: FilterFunction,
        urlFilter?: FilterFunction
    ): Promise<SearchResult[]>
}

const searchIMDB: SearchDefinition = async (
    query,
    config,
    titleFilter?,
    urlFilter?
) => {
    config.params = { includeVideos: 1 }
    const res = await axios.get(
        `https://v3.sg.media-imdb.com/suggestion/x/${query.toLowerCase()}.json`,
        config
    )
    if (res.status === 200) {
        const results = res.data.d ?? []
        return results
            .map(result => {
                if (
                    result.id &&
                    result.y &&
                    result.qid &&
                    imdbQueryTypes.includes(result.qid)
                ) {
                    return {
                        title: `${result.l} (${result.y.toString()})`,
                        url: `https://www.imdb.com/title/${result.id}/`
                    }
                }
            })
            .filter(result => {
                if (result === undefined) {
                    return false
                }
                if (titleFilter && !titleFilter(result.title)) {
                    return false
                }
                if (urlFilter && !urlFilter(result.url)) {
                    return false
                }
                return true
            })
    }
}

const searchBing: SearchDefinition = async (
    query,
    config,
    titleFilter?,
    urlFilter?
) => {
    query = `${query} site:imdb.com -intitle:"cast & crew" -intitle:"Release Info" -intitle:"Company credits" -intitle:"plot summary"`
    const res = await axios.get(
        `https://www.bing.com/search?q=${query}&count=30`,
        config
    )
    if (res.status === 200) {
        const root = parse(res.data)
        const serps = root.querySelectorAll('ol#b_results > li')
        if (serps.length) {
            return serps
                .map(serp => {
                    const a = serp.querySelector('h2 a')
                    if (a) {
                        return {
                            title: a.text,
                            url: a.getAttribute('href')
                        }
                    }
                })
                .filter(serp => {
                    if (serp === undefined) {
                        return false
                    }
                    if (titleFilter && !titleFilter(serp.title)) {
                        return false
                    }
                    if (urlFilter && !urlFilter(serp.url)) {
                        return false
                    }
                    return true
                })
        }
    }
}

const titleFilter = title => /\d{4}\)/i.test(title)
const urlFilter = url => /\/title\/tt\d+\/?$/i.test(url)

export const bingSearch = async (query: string, config: AxiosRequestConfig) => {
    const results = await searchBing(query, config, titleFilter, urlFilter)
    return results
}

export const imdbSearch = async (query: string, config: AxiosRequestConfig) => {
    const results = await searchIMDB(query, config, titleFilter, urlFilter)
    return results
}
