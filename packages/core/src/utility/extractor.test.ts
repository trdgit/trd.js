import { SiteString } from '@trd/shared/src/types'

import { extract } from './extractor'

const basicStrings: SiteString = {
    newstring: 'New: &rlsname in &section',
    'newstring-rls': '1',
    'newstring-section': '2',
    'newstring-isregex': false,
    prestring: 'Pre: &rlsname in &section',
    'prestring-rls': '1',
    'prestring-section': '2',
    'prestring-isregex': false,
    endstring: 'End: &rlsname in &section',
    'endstring-rls': '1',
    'endstring-section': '2',
    'endstring-isregex': false,
}

describe('Testing basic extractions', () => {
    test('Test the most basic of new string extractions', () => {
        const result = extract(
            basicStrings,
            ['newstring'],
            'New: Hello-world in SECTION'
        )

        expect(result).toBeInstanceOf(Object)
        expect(result).toStrictEqual({
            rlsname: 'Hello-world',
            section: 'SECTION',
        })
    })

    test('Test the most basic of end string extractions', () => {
        const result = extract(
            basicStrings,
            ['endstring'],
            'End: Hello-world in SECTION'
        )

        expect(result).toBeInstanceOf(Object)
        expect(result).toStrictEqual({
            rlsname: 'Hello-world',
            section: 'SECTION',
        })
    })

    test('Test the most basic of pre string extractions', () => {
        const result = extract(
            basicStrings,
            ['prestring'],
            'Pre: Hello-world in SECTION'
        )

        expect(result).toBeInstanceOf(Object)
        expect(result).toStrictEqual({
            rlsname: 'Hello-world',
            section: 'SECTION',
        })
    })

    test('A more complicated example', () => {
        const basicStrings2: Partial<SiteString> = {
            newstring: '[+] &section / &release',
            'newstring-rls': '2',
            'newstring-section': '1',
            'newstring-isregex': false,
        }

        const result = extract(
            basicStrings2,
            ['newstring'],
            '[+] SECTION / Hello-world started by user/iND'
        )

        expect(result).toBeInstanceOf(Object)
        expect(result).toStrictEqual({
            rlsname: 'Hello-world',
            section: 'SECTION',
        })
    })
})

const regexStrings: SiteString = {
    newstring: '/New\\:\\s+(.*?)\\s+in\\s+(.*?)$/i',
    'newstring-rls': '1',
    'newstring-section': '2',
    'newstring-isregex': true,
    prestring: '/Pre\\:\\s+(.*?)\\s+in\\s+(.*?)$/i',
    'prestring-rls': '1',
    'prestring-section': '2',
    'prestring-isregex': true,
    endstring: '/End\\:\\s+(.*?)\\s+in\\s+(.*?)$/i',
    'endstring-rls': '1',
    'endstring-section': '2',
    'endstring-isregex': true,
}

describe('Testing regex extractions', () => {
    test('Test new string regex extraction', () => {
        const result = extract(
            regexStrings,
            ['newstring'],
            'New: Hello-world in SECTION'
        )

        expect(result).toBeInstanceOf(Object)
        expect(result).toStrictEqual({
            rlsname: 'Hello-world',
            section: 'SECTION',
        })
    })

    test('Test the most basic of end string extractions', () => {
        const result = extract(
            regexStrings,
            ['endstring'],
            'End: Hello-world in SECTION'
        )

        expect(result).toBeInstanceOf(Object)
        expect(result).toStrictEqual({
            rlsname: 'Hello-world',
            section: 'SECTION',
        })
    })

    test('Test the most basic of pre string extractions', () => {
        const result = extract(
            regexStrings,
            ['prestring'],
            'Pre: Hello-world in SECTION'
        )

        expect(result).toBeInstanceOf(Object)
        expect(result).toStrictEqual({
            rlsname: 'Hello-world',
            section: 'SECTION',
        })
    })
})
