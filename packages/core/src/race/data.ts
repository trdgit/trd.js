import { Transform, Type, plainToClass } from 'class-transformer'

import DataProviderResponse from '../dataprovider/response'
import Source from '../dupe-engine/source'
import { array_unique } from '../helpers/data_types'
import RuleData from '../parser/rule-data'

export default class RaceResultData {
    private data = {}

    @Transform(
        value => {
            const map = {}
            for (const entry of Object.entries(value.value)) {
                map[entry[0]] = plainToClass(DataProviderResponse, entry[1])
            }
            return map
        },
        { toClassOnly: true }
    )
    private dataProviderData: { [key: string]: DataProviderResponse<unknown> } =
        {}

    @Type(() => Source)
    private dupeData: Source[] = []

    constructor(initialValues) {
        if (initialValues) {
            for (const [k, v] of Object.entries(initialValues)) {
                this.attachData(k, v)
            }
        }
    }

    public attachDataProviderResponse<T>(
        namespace: string,
        dataProviderResponse: DataProviderResponse<T>
    ) {
        this.dataProviderData[namespace] = dataProviderResponse
    }

    public attachData(k, v) {
        this.data[k] = v
    }

    public getDataProviderData() {
        return this.dataProviderData
    }

    public attachDupeSource(source: Source) {
        this.dupeData.push(source)
    }

    public toRuleData(): RuleData {
        const ruleData = new RuleData()
        for (const [k, v] of Object.entries(this.data)) {
            ruleData.set(k, v)
        }
        for (const [namespace, response] of Object.entries(
            this.dataProviderData
        )) {
            ruleData.setData(namespace, response.getData())
        }

        const dupeGroups: string[] = []
        const dupeGroupsInternal: string[] = []
        const dupeGroupsNonInternal: string[] = []
        for (const source of this.dupeData) {
            dupeGroups.push(source.getGroup())
            if (source.isInternal()) {
                dupeGroupsInternal.push(source.getGroup())
            } else {
                dupeGroupsNonInternal.push(source.getGroup())
            }
        }

        const groups = array_unique(dupeGroups)
        ruleData.set('dupe.groups', groups)
        ruleData.set('dupe.groups_total', groups.length)

        const groupsInternal = array_unique(dupeGroupsInternal)
        ruleData.set('dupe.groups_internal', groupsInternal)
        ruleData.set('dupe.groups_internal_total', groupsInternal.length)

        const groupsNonInternal = array_unique(dupeGroupsNonInternal)
        ruleData.set('dupe.groups_non_internal', groupsNonInternal)
        ruleData.set('dupe.groups_non_internal_total', groupsNonInternal.length)

        return ruleData
    }

    public all() {
        return this.flatten()
    }

    public flatten() {
        return this.toRuleData().all()
    }

    public getRawData(): RuleData {
        const ruleData = new RuleData()
        for (const [namespace, response] of Object.entries(
            this.dataProviderData
        )) {
            ruleData.setData(namespace, response.getRawData())
        }
        return ruleData
    }

    public getImmutableData(): RuleData {
        const ruleData = new RuleData()
        for (const [namespace, response] of Object.entries(
            this.dataProviderData
        )) {
            ruleData.setData(namespace, response.getImmutableData())
        }
        return ruleData
    }
}
