import { ReleaseName } from '@trd/shared/src/types'
import { DateTime } from 'luxon'

import BoxOfficeMojoDataProvider from '../dataprovider/bom'
import DateTimeDataProvider from '../dataprovider/datetime'
import IMDBDataProvider, { IMDBData } from '../dataprovider/imdb'
import MusicDataProvider from '../dataprovider/music'
import ReleaseNameDataProvider from '../dataprovider/release'
import TVMazeDataProvider from '../dataprovider/tvmaze'
import Engine, { DupeEngineRuleString } from '../dupe-engine/engine'
import Source from '../dupe-engine/source'
import { clone } from '../helpers/data_types'
import { differenceInMinutes } from '../helpers/date'
import * as logger from '../helpers/logger'
import { PCRE2JS, regexMatches } from '../helpers/regex'
import SettingsModel from '../models/settings'
import SitesModel from '../models/sites'
import SkiplistsModel from '../models/skiplists'
import RuleResponse from '../parser/responses'
import Rules from '../parser/rules'
import { getPossibleDupeSources } from '../repository/pre'
import {
    getGroup,
    getTitle,
    passesRegexSkiplists,
    passesRequirements
} from '../utility/release'
import RaceResultData from './data'
import RaceResult from './result'

const hasNoPreTime = section => !section.pretime || section.pretime == 0

const hasAcceptablePretime = (section, preDiffMins: number) =>
    section.pretime && section.pretime > 0 && preDiffMins < section.pretime

interface ChainItem {
    name: string
    section: string
}
type Chain = ChainItem[]

export default class Race {
    private sites: string[] = []
    private useCache: boolean

    constructor(useCache = true) {
        this.useCache = useCache
    }

    public addSites(sites) {
        this.sites.push(...sites)
    }

    private addAllSites() {
        for (const [siteName] of Object.entries(SitesModel.getData())) {
            this.sites.push(siteName)
        }
    }

    private getChain(
        raceResult: RaceResult,
        tag: string,
        rlsname: ReleaseName,
        preDifferenceInMinutes: 0 | number = 0
    ): Chain {
        const chain: Chain = []

        const filteredSites = this.sites
            .map(siteName => {
                return {
                    siteName,
                    info: SitesModel.getSite(siteName)
                }
            })
            .filter(site => {
                const result =
                    site.info.sections &&
                    Array.isArray(site.info.sections) &&
                    site.info.enabled

                if (!result) {
                    raceResult.addLog(
                        'debug',
                        `${site.siteName} not considered due to having no sections or being disabled`
                    )
                }

                return result
            })

        for (const site of filteredSites) {
            const {
                siteName,
                info: { sections }
            } = site
            for (const ss of sections) {
                if (ss.tags?.length) {
                    for (const st of ss.tags) {
                        if (
                            st.tag == tag &&
                            regexMatches(rlsname, st.trigger)
                        ) {
                            const noPretimeKey = hasNoPreTime(ss)
                            const preDiffIsGood = hasAcceptablePretime(
                                ss,
                                preDifferenceInMinutes
                            )

                            if (!preDiffIsGood) {
                                raceResult.addLog(
                                    'debug',
                                    `${siteName} section ${ss.name} not added due to bad pre time (${preDifferenceInMinutes})`
                                )
                            }

                            // check pretime
                            if (noPretimeKey || preDiffIsGood) {
                                chain.push({
                                    name: siteName,
                                    section: ss.name
                                })
                            }
                        }
                    }
                }
            }
        }
        return chain
    }

    public async race(
        tag: string,
        rlsname: ReleaseName,
        preTime: undefined | DateTime = undefined
    ): Promise<RaceResult> {
        logger.debug(`Evaluting isRace() for ${tag}/${rlsname}`)

        const result = new RaceResult()
        result.tag = tag
        result.rlsname = rlsname

        // add all sites if we don't add manually
        if (!this.sites.length) {
            this.addAllSites()
        }

        let preDifferenceInMinutes = 0
        if (preTime) {
            preDifferenceInMinutes = differenceInMinutes(
                preTime,
                DateTime.now()
            )
        }
        result.preDifferenceInMinutes = preDifferenceInMinutes

        const addAffils =
            SettingsModel.exists('always_add_affils') &&
            SettingsModel.get('always_add_affils') === true
        const group = getGroup(rlsname)
        const chain = this.getChain(
            result,
            tag,
            rlsname,
            preDifferenceInMinutes
        )

        // rule parser..
        const parser = new Rules()
        const raceData = new RaceResultData({
            rlsname: rlsname,
            tag: tag
        })

        // Attach release-name meta-data
        const releaseNameDataProvider = new ReleaseNameDataProvider()
        const releaseNameDataProviderResponse =
            await releaseNameDataProvider.lookup(rlsname)
        raceData.attachDataProviderResponse(
            'rlsname',
            releaseNameDataProviderResponse
        )

        // Attach date-time meta-data
        const dateTimeDataProvider = new DateTimeDataProvider()
        const dateTimeDataProviderResponse =
            await dateTimeDataProvider.lookup(rlsname)
        raceData.attachDataProviderResponse(
            'datetime',
            dateTimeDataProviderResponse
        )

        // global bad dir
        if (regexMatches(rlsname, SettingsModel.get('baddir'))) {
            const message = `Bad dir match based on regex: ${SettingsModel.get(
                'baddir'
            )}`
            return RaceResult.factoryCatastrophe(result, message, raceData)
        }

        // check global settings before any complex data lookups
        if (SettingsModel.exists('banned_groups')) {
            const bannedGroups = SettingsModel.get('banned_groups').map(grp =>
                grp.toUpperCase()
            )
            if (bannedGroups.includes(group)) {
                return RaceResult.factoryCatastrophe(
                    result,
                    `Banned group: ${group}`,
                    raceData
                )
            }
        }

        const tagOptions = SettingsModel.get('tag_options')

        if (!tagOptions[tag]) {
            logger.error(`An invalid tag ${tag} was passed into race.race()`)
            return RaceResult.factoryCatastrophe(
                result,
                `Tag ${tag} does not exist`,
                raceData
            )
        }

        // Passes allowed_groups check
        if (
            tagOptions !== null &&
            tagOptions[tag] &&
            tagOptions[tag].allowed_groups?.length
        ) {
            const allowedGroups = tagOptions[tag].allowed_groups.map(grp =>
                grp.trim().toUpperCase()
            )
            if (!allowedGroups.includes(group)) {
                const message = `${group} is not in allowed list of groups for this tag: ${tagOptions[
                    tag
                ].allowed_groups.join(',')}`
                return RaceResult.factoryCatastrophe(result, message, raceData)
            }
        }

        // Passes tag_requirements check
        if (
            tagOptions[tag].tag_requires &&
            Array.isArray(tagOptions[tag].tag_requires)
        ) {
            const passes = passesRequirements(
                rlsname,
                tagOptions[tag].tag_requires,
                SkiplistsModel
            )
            if (passes !== true) {
                const message = `Tag requirements were not met: ${passes.join(
                    ','
                )}`
                return RaceResult.factoryCatastrophe(result, message, raceData)
            }
        }

        // Passes tag_skiplist check
        if (
            tagOptions[tag].tag_skiplist &&
            Array.isArray(tagOptions[tag].tag_skiplist)
        ) {
            const passes = passesRegexSkiplists(
                rlsname,
                tagOptions[tag].tag_skiplist,
                SkiplistsModel
            )
            if (passes !== true) {
                const message = `Tag skiplist item matched: ${passes}`
                return RaceResult.factoryCatastrophe(result, message, raceData)
            }
        }

        // check tag options for data sources
        const datalookupStart = Number(process.hrtime.bigint())
        const tagDataSources = tagOptions[tag].data_sources?.length
            ? tagOptions[tag].data_sources
            : null
        let isEligibleForDupeCheck = false
        let activeDataProvider
        if (tagDataSources !== null) {
            for (const dataSource of tagDataSources) {
                activeDataProvider = dataSource
                switch (dataSource) {
                    case 'tvmaze': {
                        isEligibleForDupeCheck = true
                        const tvMazeDataProvider = new TVMazeDataProvider()
                        const tvMazeDataResponse =
                            await tvMazeDataProvider.lookup(
                                rlsname,
                                !this.useCache
                            )
                        if (!tvMazeDataResponse.result) {
                            logger.datalog(
                                `No tvmaze info found for ${rlsname}`
                            )
                        }
                        raceData.attachDataProviderResponse(
                            'tvmaze',
                            tvMazeDataResponse
                        )
                        break
                    }
                    case 'music': {
                        const musicDataProvider = new MusicDataProvider()
                        const MusicDataResponse =
                            await musicDataProvider.lookup(
                                rlsname,
                                !this.useCache
                            )
                        if (!MusicDataResponse.result) {
                            logger.datalog(
                                `Failed to extract music info for ${rlsname}`
                            )
                        }
                        raceData.attachDataProviderResponse(
                            'music',
                            MusicDataResponse
                        )
                        break
                    }
                    case 'imdb': {
                        isEligibleForDupeCheck = true
                        const imdbDataProvider = new IMDBDataProvider()
                        const imdbResponse = await imdbDataProvider.lookup(
                            rlsname,
                            !this.useCache
                        )
                        if (!imdbResponse.result) {
                            logger.datalog(`No imdb info found for ${rlsname}`)
                        }
                        raceData.attachDataProviderResponse(
                            'imdb',
                            imdbResponse
                        )
                        const bomDataProvider = new BoxOfficeMojoDataProvider()
                        const imdbData = imdbResponse.getData() as IMDBData
                        const bomResponse = await bomDataProvider.lookup(
                            rlsname,
                            !this.useCache,
                            imdbData
                                ? {
                                      id: imdbData.id,
                                      country: imdbData.country
                                  }
                                : undefined
                        )
                        if (!bomResponse.result) {
                            logger.datalog(`No bom info found for ${rlsname}`)
                        }
                        raceData.attachDataProviderResponse('bom', bomResponse)
                        break
                    }
                }
            }
        }
        result.dataLookupDuration =
            (Number(process.hrtime.bigint()) - datalookupStart) / 1000000

        // check dupe info
        let dupeEngine: Engine
        if (isEligibleForDupeCheck) {
            const possibleSources = await getPossibleDupeSources(
                getTitle(rlsname).fullTitle,
                releaseNameDataProviderResponse.getData(),
                activeDataProvider
            )
            dupeEngine = new Engine([])
            dupeEngine.addFilterRegex(PCRE2JS(SettingsModel.get('baddir')))
            // add tag_skiplist while we're here
            if (
                tagOptions[tag]?.tag_skiplist &&
                Array.isArray(tagOptions[tag].tag_skiplist)
            ) {
                const tagSkiplist = tagOptions[tag].tag_skiplist
                for (const sl of tagSkiplist) {
                    const regex =
                        sl.slice(0, 7) === '[regex.'
                            ? SkiplistsModel.getSkiplistRegex(
                                  sl.slice(0, -1).replace('[regex.', '')
                              )
                            : sl
                    dupeEngine.addFilterRegex(PCRE2JS(regex))
                }
            }
            for (const dupeSource of possibleSources) {
                const src = new Source(dupeSource['rlsname'])
                dupeEngine.addSource(src)
                result.dupeEngineSources.push(clone(dupeSource))
                raceData.attachDupeSource(src)
            }
        }

        // add our race data to the parser as rule data
        parser.addData(raceData.toRuleData())
        result.data = raceData

        // loop through the incoming sites and parse rules
        for (const site of chain) {
            // get skiplists + rules for this site
            const sectionRules = this._getSectionRules(site.name, site.section)
            const tagRules = this._getTagRules(site.name, site.section, tag)
            const sls = this._getSkiplists(site.name, site.section)
            const sectionDupeRules = this._getSectionDupeRules(
                site.name,
                site.section
            )
            const downloadOnly = this._getSectionDownloadOnly(
                site.name,
                site.section
            )

            let valid = true
            const invalidReasons: string[] = []

            // handle banned groups
            const sn = site.name
            const siteModel = SitesModel.getSite(sn)
            let isSiteBannedGroup = false
            if (
                siteModel.banned_groups?.length &&
                siteModel.banned_groups.includes(group.toUpperCase())
            ) {
                valid = false
                invalidReasons.push(`${group} is a banned group for this site`)
                isSiteBannedGroup = true
            }

            // handle section rules
            if (sectionRules !== null) {
                const sortedSectionRules = parser.sortRules(sectionRules)
                for (let rule of sortedSectionRules) {
                    rule = rule.trim()
                    if (rule.length) {
                        try {
                            if (
                                parser.parseRule(rule) === RuleResponse.IsFalse
                            ) {
                                valid = false
                                invalidReasons.push(
                                    `Failed section rule: ${rule}`
                                )
                            } else if (
                                parser.parseRule(rule) ===
                                    RuleResponse.IsExcept &&
                                !isSiteBannedGroup
                            ) {
                                valid = true
                                result.exceptions.push({
                                    site: site['name'],
                                    exception: rule
                                })
                                break
                            } else if (
                                parser.parseRule(rule) ===
                                RuleResponse.IsComment
                            ) {
                                continue
                            }
                        } catch (_e) {
                            valid = false
                            invalidReasons.push(`Invalid section rule: ${rule}`)
                        }
                    }
                }
            }

            // handle tag rules
            if (tagRules !== null) {
                const sortedTagRules = parser.sortRules(tagRules)
                for (let rule of sortedTagRules) {
                    rule = rule.trim()
                    if (rule.length) {
                        try {
                            if (
                                parser.parseRule(rule) === RuleResponse.IsFalse
                            ) {
                                valid = false
                                invalidReasons.push(`Failed tag rule: ${rule}`)
                            } else if (
                                parser.parseRule(rule) ===
                                    RuleResponse.IsExcept &&
                                !isSiteBannedGroup
                            ) {
                                valid = true
                                result.exceptions.push({
                                    site: site['name'],
                                    exception: rule
                                })
                                break
                            }
                        } catch (_e) {
                            valid = false
                            invalidReasons.push(`Invalid tag rule: ${rule}`)
                        }
                    }
                }
            }

            if (sectionRules === null && tagRules === null) {
                valid = false
                invalidReasons.push('No section or tag rules set')
            }

            // handle skiplists after rules, because skiplists can apply to exceptions
            if (sls !== null) {
                for (const skiplist of sls) {
                    const listResolution = SkiplistsModel.passesSkiplist(
                        skiplist,
                        rlsname
                    )
                    if (listResolution.passes !== true) {
                        valid = false
                        invalidReasons.push(listResolution.error)
                    }
                }
            }

            // dupe rules
            if (sectionDupeRules !== null && dupeEngine) {
                const dupeKeys = Object.keys(sectionDupeRules)
                for (const dupeKey of dupeKeys) {
                    if (
                        dupeKey.includes('firstWins') &&
                        sectionDupeRules[dupeKey] === true
                    ) {
                        const dupeResult = dupeEngine.isDupe(
                            rlsname,
                            site.name,
                            dupeKey as DupeEngineRuleString
                        )
                        if (dupeResult.isDupe()) {
                            valid = false
                            invalidReasons.push(
                                `Failed first format wins. Previous releases: ${dupeResult.getSourcesAsString()}`
                            )
                        }
                    } else if (
                        dupeKey.includes('priority') &&
                        sectionDupeRules[dupeKey]?.length
                    ) {
                        const dupeResult = dupeEngine.isDupe(
                            rlsname,
                            site.name,
                            dupeKey as DupeEngineRuleString,
                            {
                                priority: sectionDupeRules[dupeKey]
                            }
                        )
                        if (dupeResult.isDupe()) {
                            valid = false
                            invalidReasons.push(
                                `Failed dupe priority. Previous releases: ${dupeResult.getSourcesAsString()}`
                            )
                        }
                    }
                }
                result.dupeEngineLogs = dupeEngine.getLogs()
            }

            // add site/bnc to affils chain if possible. Add dlonly sites as well
            if (
                downloadOnly ||
                (addAffils &&
                    SitesModel.getAffils(site['name']).includes(
                        group.toUpperCase(),
                    ))
            ) {
                valid = true
                const bnc = this._getBNC(site['name'], site['section'])
                if (bnc !== null) {
                    result.affilSites.push(bnc)
                } else {
                    result.affilSites.push(site['name'])
                }
                if (downloadOnly) {
                    result.addLog(
                        'debug',
                        `${site.name} has section ${site.section} marked as download only`
                    )
                }
            }

            // determine if it's valid or not!
            if (valid) {
                result.validSites.push(site['name'])
                const bnc = this._getBNC(site['name'], site['section'])
                if (bnc !== null) {
                    result.chain.push(bnc)
                } else {
                    result.chain.push(site['name'])
                }
                // This array is only populated if we add an affil to a race regardless of the fact it broke the rules
                if (invalidReasons.length) {
                    result.invalidSitesOverrides.push({
                        site: site['name'],
                        section: site['section'],
                        invalidReasons: invalidReasons
                    })
                }
            } else {
                result.invalidSites.push({
                    site: site['name'],
                    section: site['section'],
                    invalidReasons: invalidReasons
                })
            }
        }

        // check that the race doesn't consist solely of affil sites
        if (
            result.affilSites.length &&
            result.affilSites.length === result.validSites.length
        ) {
            const message =
                'Only affil sites were available to complete this race'
            return RaceResult.factoryCatastrophe(result, message, raceData)
        }
        result.endRace()

        return result
    }

    private _getSectionRules(site: string, section: string) {
        for (const s of SitesModel.getSite(site).sections) {
            if (s.name == section && s.rules?.length) {
                return s.rules
            }
        }
        return null
    }

    private _getSectionDupeRules(site: string, section: string) {
        for (const s of SitesModel.getSite(site).sections) {
            if (s.name == section && s.dupeRules) {
                return s.dupeRules
            }
        }
        return null
    }

    private _getSectionDownloadOnly(site: string, section: string) {
        for (const s of SitesModel.getSite(site).sections) {
            if (s.name == section && s.downloadOnly) {
                return s.downloadOnly
            }
        }
        return false
    }

    private _getTagRules(site: string, section: string, tag: string) {
        for (const s of SitesModel.getSite(site).sections) {
            if (s.name == section && s.tags?.length) {
                for (const [, ti] of Object.entries(s.tags)) {
                    if (ti['tag'] === tag && ti['rules']?.length) {
                        return ti['rules']
                    }
                }
            }
        }
        return null
    }

    private _getSkiplists(site: string, section: string) {
        for (const s of SitesModel.getSite(site).sections) {
            if (s.name == section && s.skiplists?.length) {
                return s.skiplists
            }
        }
        return null
    }

    private _getBNC(site: string, section: string) {
        for (const s of SitesModel.getSite(site).sections) {
            if (s.name == section && s.bnc?.length) {
                return s.bnc
            }
        }
        return null
    }
}
