import { RaceResultLogs, ReleaseName, ResultLog } from '@trd/shared/src/types'
import { Type } from 'class-transformer'

import RaceResultData from './data'

interface InvalidSite {
    site: string
    section: string
    invalidReasons: string[]
}
type InvalidSites = InvalidSite[]

interface RaceException {
    site: string
    exception: string
}

export default class RaceResult {
    public tag: string = null
    public rlsname: ReleaseName = null

    @Type(() => RaceResultData)
    public data: RaceResultData = null

    public catastrophes: string[] = []
    public validSites: string[] = []
    public invalidSites: InvalidSites = []
    public invalidSitesOverrides: InvalidSites = []
    public affilSites: string[] = []
    public exceptions: RaceException[] = []
    public chain: string[] = []
    private start: number
    public dataLookupDuration = 0
    private duration = 0
    public dupeEngineSources = []
    public autotraded = false
    public preDifferenceInMinutes = 0
    public dupeEngineLogs: string[] = []
    private resultLogs: RaceResultLogs = []

    constructor() {
        this.start = Number(process.hrtime.bigint())
    }

    public isRace(): boolean {
        return this.validSites.length > 1 && !this.hasCatastrophe()
    }

    public hasCatastrophe(): boolean {
        return this.resultLogs.some(log => log.type === 'catastrophe')
    }

    public addLog(
        logType: 'catastrophe' | 'warning' | 'debug',
        message: string,
        details?: Record<string, unknown>
    ): void {
        this.resultLogs.push({
            type: logType,
            message,
            details,
        })
    }

    public endRace(): void {
        const end = Number(process.hrtime.bigint())
        const diff = end - this.start
        this.duration = Number(diff) / 1000000
        this.chain.sort()
        this.validSites.sort()
    }

    public getDuration() {
        return this.duration
    }

    public getDataLookupDuration() {
        return this.dataLookupDuration
    }

    public hasAffils(): boolean {
        return this.affilSites.length > 0
    }

    public getChainWithoutAffils() {
        return this.chain.filter(v => {
            return !this.affilSites.includes(v)
        })
    }

    public logs(): ResultLog[] {
        return Object.entries(this.data.getDataProviderData())
            .filter(([, data]) => data.getDebug())
            .map(([namespace, data]) => {
                return {
                    namespace,
                    logs: data.getDebug(),
                }
            })
    }

    public static factoryCatastrophe(
        existingResult: RaceResult,
        message: string,
        data: RaceResultData
    ): Promise<RaceResult> {
        existingResult.addLog('catastrophe', message)
        existingResult.data = data
        existingResult.endRace()
        return new Promise(resolve => {
            resolve(existingResult)
        })
    }

    public getLogsNew() {
        return this.resultLogs
    }

    public toJSON() {
        const {
            rlsname,
            tag,
            data,
            catastrophes,
            validSites,
            invalidSites,
            invalidSitesOverrides,
            affilSites,
            exceptions,
            chain,
            dataLookupDuration,
            dupeEngineSources,
            autotraded,
            preDifferenceInMinutes,
            dupeEngineLogs,
            resultLogs,
            duration,
        } = this
        return {
            rlsname,
            tag,
            data,
            catastrophes,
            validSites,
            invalidSites,
            invalidSitesOverrides,
            affilSites,
            exceptions,
            chain,
            duration,
            dataLookupDuration,
            dupeEngineSources,
            autotraded,
            preDifferenceInMinutes,
            dupeEngineLogs,
            resultLogs,
        }
    }
}

export const upgradeRaceResult = (old: RaceResult): RaceResult => {
    if (old.getLogsNew().length) {
        return old
    }

    if (old.catastrophes.length) {
        const catastrophe = old.catastrophes[0] // we should only ever have one anyway
        old.addLog('catastrophe', catastrophe)
    }

    return old
}
