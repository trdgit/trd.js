import { DateTime } from 'luxon'

import { getCurrentMinutesFull, differenceInMinutes } from './date'

describe('Date helpers', () => {
    describe('getCurrentMinutesFull', () => {
        test('should return something stringy', () => {
            const mins = getCurrentMinutesFull()
            expect(typeof mins).toBe('string')
        })
    })
    describe('differenceInMinutes', () => {
        test('should return correct value', () => {
            const now = DateTime.now()
            const inOneMinute = now.plus({ minutes: 1 })
            expect(differenceInMinutes(now, inOneMinute)).toBe(1)
        })

        test('should handle db formatted strings', () => {
            const now = DateTime.now()
            const dbPreTime = DateTime.fromSQL('2022-07-09 13:59:25')
            expect(differenceInMinutes(dbPreTime, now)).toBeGreaterThan(1)
        })
    })
})
