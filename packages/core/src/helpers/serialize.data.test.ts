import { ReleaseName } from '@trd/shared/src/types'
import { readFileSync } from 'fs'

import { raceResultToLog } from './race'
import DataProviderResponse from '../dataprovider/response'
import Engine from '../dupe-engine/engine'
import Source from '../dupe-engine/source'
import RaceResultData from '../race/data'
import RaceResult from '../race/result'
import { serializeRaceResult, unserializeRaceResult } from './serialize.data'

const fixturesPath = './test/fixtures/race-result'

describe('DataTypes', () => {
    test('serializeRaceResult', () => {
        const raceResult = new RaceResult()
        raceResult.tag = 'tag'
        raceResult.rlsname = 'rlsname' as ReleaseName

        const raceData = new RaceResultData({
            rlsname: 'rlsname',
            tag: 'tag'
        })
        raceData.attachDataProviderResponse(
            'test',
            new DataProviderResponse(
                true,
                {
                    foo: 'bar'
                },
                {},
                false
            )
        )

        const dupeEngine = new Engine([])
        dupeEngine.addFilterRegex(/foobar/i)
        const src = new Source('fake-rls' as ReleaseName)
        dupeEngine.addSource(src)
        raceResult.dupeEngineSources.push({
            rlsname: 'fake-rls',
            otherProp: 'blah'
        })
        raceData.attachDupeSource(src)

        raceResult.data = raceData

        const serialized = serializeRaceResult(raceResult)
        const unserialized = unserializeRaceResult(serialized)

        expect(unserialized).toBeInstanceOf(RaceResult)
        expect(unserialized.tag).toBe(raceResult.tag)
        expect(unserialized.data).toBeInstanceOf(RaceResultData)
        expect(unserialized.data.getRawData().get('test.foo')).toBe('bar')
    })

    test('unserializeRaceResult with fixture', () => {
        const input = readFileSync(`${fixturesPath}/new.json`)
        const inputString = input.toString()
        const result = unserializeRaceResult(inputString)

        // run it through raceResultToLog as this was a bug
        raceResultToLog(result)

        expect(result).toBeInstanceOf(RaceResult)
    })
})
