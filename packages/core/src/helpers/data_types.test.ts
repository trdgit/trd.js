import { strip_tags } from './data_types'

describe('strip_tags', () => {
    it('removes tags and keeps text', () => {
        expect(strip_tags('<p>foo</p>')).toBe('foo')
    })
})
