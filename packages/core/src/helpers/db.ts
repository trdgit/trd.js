import { instance as mysql } from './mysql'

export type DBParam = string | number
export type DBRecord = Record<string, DBParam>
export type DBConditions = Record<string, DBParam>
export type DBRow = Record<string, unknown>

export type DBInsertResult = number
export type DBInsertFunction = (
    table: string,
    values: Record<string, DBParam>,
    withIgnore?: boolean
) => Promise<DBInsertResult>

export type DBUpdateResult = void
export type DBUpdateFunction = (
    table: string,
    values: Record<string, DBParam>,
    conditions?: DBConditions
) => Promise<DBUpdateResult>

export type DBDeleteResult = void
export type DBDeleteRowFunction = (
    table: string,
    conditions: DBConditions
) => Promise<DBDeleteResult>

export type DBFetchAssocFunction<T> = (
    sql: string,
    params: DBParam[]
) => Promise<T | undefined>

interface DB {
    insert(
        table: string,
        values: Record<string, DBParam>,
        withIgnore?: boolean
    ): Promise<DBInsertResult>
    update(
        table: string,
        values: Record<string, DBParam>,
        conditions?: DBConditions
    ): Promise<DBUpdateResult>
    deleteRows(table: string, conditions: DBConditions): Promise<DBDeleteResult>
    fetchAssoc<T>(sql: string, params?: DBParam[]): Promise<T | undefined>
    fetchColumn<T>(sql: string, params?: DBParam[]): Promise<T | undefined>
    fetchAll<T extends DBRow>(sql: string, params?: DBParam[]): Promise<T[]>
    query(sql: string, params?: DBParam[]): Promise<number>
    closeConnection(): Promise<void>
}

// const isMySQL = (): boolean => process.env.DB_ENGINE === 'mysql'

// const insert = isMySQL() ? mysql.insert : mysql.insert
// const update = isMySQL() ? mysql.update : mysql.update
// const deleteRows = isMySQL() ? mysql.deleteRows : mysql.deleteRows
// const fetchAssoc = isMySQL()
//     ? rest => mysql.fetchAssoc(...rest)
//     : mysql.fetchAssoc
// const fetchColumn = isMySQL() ? mysql.fetchColumn : mysql.fetchColumn
// const fetchAll = isMySQL() ? mysql.fetchAll : mysql.fetchAll
// const query = isMySQL() ? mysql.query : mysql.query
// const closeConnection = isMySQL()
//     ? mysql.closeConnection
//     : () => {
//           /**/
//       }

const db = mysql
export { DB, db }
