import { faker } from '@faker-js/faker'

import { array_random } from './data_types'

const resolutions = ['720p', '1080p', '2160p']
const codecs = ['h264', 'h265', 'x264']
const formats = ['Bluray', 'WEB', 'WEBRIP', 'HDTV']

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
}

function pad(num, size) {
    num = num.toString()
    while (num.length < size) num = '0' + num
    return num
}

function randomInt(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
}

const randomTitle = (totalWords = 5) =>
    faker.random
        .words(totalWords)
        .split(' ')
        .map(word => capitalizeFirstLetter(word))
        .join('.')
const randomSeasonEpisode = () =>
    `S${pad(randomInt(1, 5), 2)}E${pad(randomInt(1, 24), 2)}`

const randomGroup = () => faker.word.noun().toUpperCase()

export const generateTVRlsname = (
    resolution?: string,
    format?: string,
    codec?: string
) => {
    if (!resolution) {
        resolution = array_random<string>(resolutions)
    }
    if (!format) {
        format = array_random<string>(formats)
    }
    if (!codec) {
        codec = array_random<string>(codecs)
    }

    return `${randomTitle()}.${randomSeasonEpisode()}.${resolution}.${format}.${codec}-${randomGroup()}`
}

export const generateGameRlsname = () => {
    return `${randomTitle(randomInt(1, 5))}-${randomGroup()}`
}
