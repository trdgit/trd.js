import axios from 'axios'
import { SocksProxyAgent } from 'socks-proxy-agent'

axios.defaults.timeout = 10000
axios.defaults.maxRedirects = 1
axios.defaults.withCredentials = true

const env = process.env
if (env.PROXY_HOST) {
    const proxyOptions = `${env.PROXY_HOST}`
    const proxyAgent = new SocksProxyAgent(proxyOptions)
    axios.defaults.httpAgent = proxyAgent
    axios.defaults.httpsAgent = proxyAgent
}

export default axios
