import { phpSerialize, phpUnSerialize } from './serialize.php'

describe('PHP Serialization (data cache)', () => {
    test('phpSerialize', () => {
        const input = { foo: 'bar' }
        expect(phpSerialize(input)).toBe('a:1:{s:3:"foo";s:3:"bar";}')
    })

    test('phpUnserialize', () => {
        const input = 'a:1:{s:3:"foo";s:3:"bar";}'
        expect(phpUnSerialize(input)).toStrictEqual({ foo: 'bar' })
    })
})
