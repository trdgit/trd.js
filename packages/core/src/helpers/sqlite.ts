// import DatabaseConstructor, { Database } from 'better-sqlite3'

// import {
//     DB,
//     DBConditions,
//     DBDeleteResult,
//     DBInsertResult,
//     DBParam,
//     DBUpdateResult
// } from './db'

// class DBSqlite implements DB {
//     private db: Database

//     constructor() {
//         this.db = new DatabaseConstructor('trdjs.db')
//         this.db.pragma('journal_mode = WAL')
//     }

//     async insert(
//         table: string,
//         values: Record<string, DBParam>,
//         withIgnore?: boolean
//     ): Promise<DBInsertResult> {
//         const fields = Object.keys(values).join(',')
//         const questionMarks = Object.keys(values)
//             .map(_v => '?')
//             .join(',')
//         const realValues = Object.values(values)
//         const sql = `INSERT ${
//             withIgnore ? 'IGNORE' : ''
//         } INTO ${table} (${fields}) VALUES (${questionMarks})`
//         const result = this.db.prepare(sql).run(realValues)
//         return Number(result.lastInsertRowid)
//     }

//     async fetchAll<T>(sql: string, params?: DBParam[]): Promise<T[]> {
//         return this.db.prepare(sql).all(params ?? []) as T[]
//     }

//     async query(sql: string, params?: DBParam[]): Promise<number> {
//         const result = this.db.prepare(sql).run(params)
//         return result.changes
//     }

//     async update(
//         table: string,
//         values: Record<string, DBParam>,
//         conditions?: DBConditions
//     ): Promise<DBUpdateResult> {
//         const realValues = Object.keys(values)
//             .map(k => `${k} = ?`)
//             .join(',')

//         let realConditions = '1=1'
//         if (conditions) {
//             realConditions = Object.keys(conditions)
//                 .map(k => `${k} = ?`)
//                 .join(' AND ')
//         }

//         const sql = `UPDATE ${table} SET ${realValues} WHERE ${realConditions}`
//         this.db
//             .prepare(sql)
//             .run([...Object.values(values), ...Object.values(conditions ?? {})])
//     }

//     async deleteRows(
//         table: string,
//         conditions: DBConditions
//     ): Promise<DBDeleteResult> {
//         const realConditions = Object.keys(conditions)
//             .map(k => `${k} = ?`)
//             .join(' AND ')

//         const sql = `DELETE FROM ${table} WHERE ${realConditions}`
//         this.db.prepare(sql).run([...Object.values(conditions)])
//     }

//     async fetchAssoc<T>(
//         sql: string,
//         params?: DBParam[]
//     ): Promise<T | undefined> {
//         const row = this.db.prepare(sql).get(params ?? [])

//         if (!row) {
//             return
//         }
//         return row as T
//     }

//     async fetchColumn<T>(
//         sql: string,
//         params?: DBParam[]
//     ): Promise<T | undefined> {
//         const row = this.db.prepare(sql).get(params)

//         return Object.values(row).shift() as T
//     }

//     async closeConnection(): Promise<void> {
//         //
//     }
// }

// const foo = new DBSqlite()
// export default foo
