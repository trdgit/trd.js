import {
    serialize as serialize2,
    deserialize as deserialize2
} from 'class-transformer'

import RaceResult from '../race/result'

// for race results
export const serializeRaceResult = (data: unknown) => serialize2(data)
export const unserializeRaceResult = (data: string) =>
    deserialize2(RaceResult, data)
