import * as mysql from 'mysql2'

import {
    DB,
    DBConditions,
    DBDeleteResult,
    DBInsertResult,
    DBParam,
    DBRow,
    DBUpdateResult
} from './db'

// // Create the connection pool. The pool-specific settings are the defaults
// const pool = mysql.createPool({
//     host: process.env.DB_HOST,
//     port: parseInt(process.env.DB_PORT),
//     user: process.env.DB_USER,
//     password: process.env.DB_PASSWORD,
//     database: process.env.DB_NAME,
//     waitForConnections: true,
//     connectionLimit: 10,
//     queueLimit: 0,
//     dateStrings: true,
//     ...(process.env.DB_SOCKET && { socketPath: process.env.DB_SOCKET })
// })

// const db = pool.promise()

class DBMysql implements DB {
    private db

    constructor() {
        const pool = mysql.createPool({
            host: process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT),
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            waitForConnections: true,
            connectionLimit: 10,
            queueLimit: 0,
            dateStrings: true,
            ...(process.env.DB_SOCKET && { socketPath: process.env.DB_SOCKET })
        })
        const db = pool.promise()
        this.db = db
    }

    async insert(
        table: string,
        values: Record<string, DBParam>,
        withIgnore?: boolean
    ): Promise<DBInsertResult> {
        const fields = Object.keys(values).join(',')
        const questionMarks = Object.keys(values)
            .map(_v => '?')
            .join(',')
        const realValues = Object.values(values)

        const sql = `INSERT ${
            withIgnore ? 'IGNORE' : ''
        } INTO ${table} (${fields}) VALUES (${questionMarks})`
        const result = await this.db.execute(sql, realValues)

        return Number(result[0]['insertId'])
    }

    async fetchAll<T extends DBRow>(
        sql: string,
        params?: DBParam[]
    ): Promise<T[]> {
        const [rows] = await this.db.execute(sql, params)
        const tempResult = rows

        if (!tempResult) {
            return []
        }
        return tempResult as unknown as T[]
    }

    async query(sql: string, params?: DBParam[]): Promise<number> {
        const [res] = await this.db.execute(sql, params)
        const result = res as mysql.ResultSetHeader
        return result.affectedRows
    }

    async update(
        table: string,
        values: Record<string, DBParam>,
        conditions?: DBConditions
    ): Promise<DBUpdateResult> {
        const realValues = Object.keys(values)
            .map(k => `${k} = ?`)
            .join(',')
        const realConditions = Object.keys(conditions)
            .map(k => `${k} = ?`)
            .join(' AND ')

        const sql = `UPDATE ${table} SET ${realValues} WHERE ${realConditions}`
        await this.db.execute(sql, [
            ...Object.values(values),
            ...Object.values(conditions)
        ])
    }

    async deleteRows(
        table: string,
        conditions: DBConditions
    ): Promise<DBDeleteResult> {
        const realConditions = Object.keys(conditions)
            .map(k => `${k} = ?`)
            .join(' AND ')

        const sql = `DELETE FROM ${table} WHERE ${realConditions}`
        await this.db.execute(sql, [...Object.values(conditions)])
    }

    async fetchAssoc<T>(
        sql: string,
        params?: DBParam[]
    ): Promise<T | undefined> {
        const [rows] = await this.db.execute(sql, params)

        if (!rows) {
            return
        }
        return rows[0]
    }

    async fetchColumn<T>(
        sql: string,
        params?: DBParam[]
    ): Promise<T | undefined> {
        const [rows, fields] = await this.db.execute(sql, params)
        const tempResult: unknown = rows

        if (!tempResult || (Array.isArray(tempResult) && !tempResult.length)) {
            return
        }

        const firstField = fields.shift().name
        return rows[0][firstField]
    }

    async closeConnection(): Promise<void> {
        this.db.end()
    }
}

export const instance = new DBMysql()
