import { DateTime, Interval } from 'luxon'

export const toMySQL = (date?: DateTime) => {
    if (!date) {
        date = DateTime.now()
    }
    return date.toSQL({ includeOffset: false, includeZone: false })
}

export const addSeconds = (dt: DateTime, seconds: number): DateTime =>
    dt.plus({ seconds })

export const addDays = (dt: DateTime, days: number): DateTime =>
    dt.plus({ days })
export const subDays = (dt: DateTime, days: number): DateTime =>
    dt.minus({ days })

export const subMonths = (dt: DateTime, months: number): DateTime =>
    dt.minus({ months })

export const microtime = (): number => new Date().getTime()

export const getCurrentYear = (): number => DateTime.now().year
export const getLastYear = (): number => DateTime.now().minus({ years: 1 }).year
export const getNextYear = (): number => DateTime.now().plus({ years: 1 }).year

export const getCurrentHours = (): number => DateTime.now().hour
export const getCurrentMinutes = (): number => DateTime.now().minute
export const getCurrentDayOfWeek = (): number => DateTime.now().weekday

export const getCurrentHoursFull = (): string => DateTime.now().toFormat('HH')
export const getCurrentMinutesFull = (): string => DateTime.now().toFormat('mm')
export const getCurrentTimeFull = (): string =>
    `${getCurrentHoursFull()}:${getCurrentMinutesFull()}`

export const differenceInMinutes = (
    left: DateTime,
    right: DateTime
): number => {
    if (left > right) {
        return 0
    }
    return Interval.fromDateTimes(left, right).length('minutes')
}

export const differenceInMinutesFromNow = (dt: DateTime): number => {
    return Interval.fromDateTimes(dt, DateTime.now()).length('minutes')
}
