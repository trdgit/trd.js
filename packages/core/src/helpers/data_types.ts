import locutus_strip_tags from 'locutus/php/strings/strip_tags'

export const strip_tags = locutus_strip_tags

export const array_unique = <T>(array: T[]) =>
    array.filter((v, i, a) => a.indexOf(v) === i)

export const array_last = <T>(array: T[]) => array[array.length - 1]

export const is_string = (input: unknown): input is string =>
    typeof input === 'string' || input instanceof String

export const is_object = (input: unknown): input is object =>
    typeof input === 'object' && !Array.isArray(input) && input !== null

export const substr_count = (
    string: string,
    subString: string,
    allowOverlapping = false
): number => {
    string += ''
    subString += ''
    if (subString.length <= 0) return string.length + 1

    let n = 0,
        pos = 0
    const step = allowOverlapping ? 1 : subString.length

    while (true) {
        pos = string.indexOf(subString, pos)
        if (pos >= 0) {
            ++n
            pos += step
        } else break
    }
    return n
}

export const str_replace_first = (
    input: RegExp | string,
    replacement: string,
    target: string
): string => {
    if (input instanceof RegExp) {
        if (!input.flags.includes('g')) {
            throw new Error(
                'Invalid param, should not be global match when doing only first replacement'
            )
        }
    }
    return target.replace(input, replacement)
}

export const str_replace_all = (
    input: RegExp | string | RegExp[] | string[],
    replacement: string,
    target: string
) => {
    if (Array.isArray(input)) {
        for (const i of input) {
            target = target.replace(i, replacement)
        }
        return target
    }
    return target.replace(input, replacement)
}

export const xor = (logicFlip: boolean, statement: boolean): boolean => {
    if (!logicFlip) {
        return statement
    }
    return logicFlip && statement ? false : true
}

export const array_intersect = (a, b) => a.filter(Set.prototype.has, new Set(b))

export const clone = <T>(input: T): T =>
    Object.assign(Object.create(Object.getPrototypeOf(input)), input)

export const sortObjectByKeys = (input: object): object =>
    Object.keys(input)
        .sort()
        .reduce((obj, key) => {
            obj[key] = input[key]
            return obj
        }, {})

export const getKeyByValue = (obj: object, value) => {
    return Object.keys(obj).find(key => obj[key] === value)
}

export const fromEntries = iterable =>
    [...iterable].reduce((obj, [key, val]) => {
        obj[key] = val
        return obj
    }, {})

export const array_random = <T>(arr: T[]): T =>
    arr[Math.floor(Math.random() * arr.length)]
