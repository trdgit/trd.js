import * as logger from '../helpers/logger'
import {
    getFlags,
    getRegex,
    PCRE2JS,
    regexDoesntMatch,
    regexMatches
} from './regex'

describe('getFlags', () => {
    test('Gets flags /foobar/i', () => {
        expect(getFlags('/foobar/i')[0]).toBe('i')
    })

    test('errors when missing first boundary', () => {
        expect(() => {
            getFlags('foobar/i')
        }).toThrow(SyntaxError)
    })

    test('errors when passing in invalid flags', () => {
        expect(() => {
            getFlags('/foobar/x')
        }).toThrow(SyntaxError)
    })

    test('no errors when passing in no flags', () => {
        expect(() => {
            getFlags('/foobar/')
        }).not.toThrow(SyntaxError)
    })
})

describe('getRegex', () => {
    test('Gets actual regex from /foobar/i', () => {
        expect(getRegex('/foobar/i')).toBe('foobar')
    })
})

describe('PCRE2JS', () => {
    test('Gets actual regex from /foobar/i 2', () => {
        expect(PCRE2JS('/foobar/i').toString()).toBe('/foobar/i')
    })

    it('Matches are what we expect them to be', () => {
        const re = PCRE2JS('/\\[(.*?)\\]\\s+(.*?)\\s+/')
        const matches = '[FOO] bar lol'.match(re) ?? []
        expect(matches[1]).toBe('FOO')
        expect(matches[2]).toBe('bar')
    })

    it('testComplicatedRegex', () => {
        expect(() => {
            PCRE2JS('/(\\S+) :: NEW :: [\\d\\/\\-]{0,}\\/(\\S+) by/i')
        }).not.toThrow(SyntaxError)
    })

    it('testMissingFlags', () => {
        expect(PCRE2JS(`/a\.b/`).toString()).toBe('/a.b/')
    })

    it('Missing one boundary', () => {
        expect(() => {
            PCRE2JS('abc/i')
        }).toThrow(SyntaxError)
    })
})

describe('regexMatches', () => {
    let errorLoggerSpy: jest.SpyInstance

    beforeEach(() => {
        errorLoggerSpy = jest.spyOn(logger, 'error').mockImplementation()
    })
    afterEach(() => {
        jest.clearAllMocks()
    })

    describe('should match with valid regex', () => {
        const targetString = 'abcd'
        const validRegex = '/abcd/i'

        it('does not log an error', () => {
            regexMatches(targetString, validRegex)
            expect(errorLoggerSpy).not.toHaveBeenCalled()
        })

        it('result to be true', () => {
            const result = regexMatches(targetString, validRegex)
            expect(result).toBe(true)
        })
    })

    describe('should not match with valid but incorrect regex', () => {
        const targetString = 'abcd'
        const invalidRegex = '/1111/i'

        it('logs an error', () => {
            regexMatches(targetString, invalidRegex)
            expect(errorLoggerSpy).not.toHaveBeenCalled()
        })

        it('result to be false', () => {
            const result = regexMatches(targetString, invalidRegex)
            expect(result).toBeFalsy()
        })
    })

    describe('should not match with invalid regex', () => {
        const targetString = 'abcd'
        const invalidRegex = 'abcd/i'

        it('logs an error', () => {
            regexMatches(targetString, invalidRegex)
            expect(errorLoggerSpy).toHaveBeenCalled()
        })

        it('result to be false', () => {
            const result = regexMatches(targetString, invalidRegex)
            expect(result).toBeFalsy()
        })
    })
})

describe('regexDoesntMatch', () => {
    let errorLoggerSpy: jest.SpyInstance

    beforeEach(() => {
        errorLoggerSpy = jest.spyOn(logger, 'error').mockImplementation()
    })
    afterEach(() => {
        jest.clearAllMocks()
    })

    describe('should return true with incorrect regex', () => {
        const targetString = 'abcd'
        const regex = '/dddd/i'

        it('result to be true', () => {
            const result = regexDoesntMatch(targetString, regex)
            expect(result).toBe(true)
        })
    })

    describe('should return false with correct regex', () => {
        const targetString = 'abcd'
        const validRegex = '/abcd/i'

        it(`doesn't log an error`, () => {
            regexDoesntMatch(targetString, validRegex)
            expect(errorLoggerSpy).not.toHaveBeenCalled()
        })

        it('result to be false', () => {
            const result = regexDoesntMatch(targetString, validRegex)
            expect(result).toBeFalsy()
        })
    })

    describe('should return false with invalid regex', () => {
        const targetString = 'abcd'
        const invalidRegex = 'abcd/i'

        it(`logs an error`, () => {
            regexDoesntMatch(targetString, invalidRegex)
            expect(errorLoggerSpy).toHaveBeenCalled()
        })

        it('result to be false', () => {
            const result = regexDoesntMatch(targetString, invalidRegex)
            expect(result).toBeFalsy()
        })
    })
})
