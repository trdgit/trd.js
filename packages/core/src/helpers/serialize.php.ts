import serialize from 'locutus/php/var/serialize'
import unserialize from 'locutus/php/var/unserialize'

// backwards compatible for the data-cache
export const phpSerialize = (data: unknown) => serialize(data)
export const phpUnSerialize = (data: unknown) => unserialize(data)
