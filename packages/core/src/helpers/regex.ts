import * as util from 'util'

import * as logger from '../helpers/logger'

export const getFlags = (regex: string, boundary = '/'): string[] => {
    if (regex.slice(0, 1) !== boundary) {
        throw SyntaxError(`Missing first boundary ${regex}`)
    }

    const lastIndex = regex.lastIndexOf(boundary)
    const everythingAfterLast = regex.slice(lastIndex + 1)
    if (everythingAfterLast.length && everythingAfterLast.match(/[^igmsu]+/i)) {
        throw new SyntaxError(`Invalid flags for ${regex}`)
    }

    const split = regex.split(boundary)
    if (split) {
        return (split.pop() ?? '').split('')
    }
    return []
}

export const getRegex = (regex: string, boundary = '/'): string =>
    regex.slice(1).split(boundary).slice(0, -1).join(boundary)

export const PCRE2JS = (regex: string, boundary = '/'): RegExp => {
    try {
        const flags = getFlags(regex, boundary)

        const regexpFlags: string[] = []
        if (flags.includes('i')) {
            regexpFlags.push('i')
        }

        const regexString = getRegex(regex, boundary)
        regexString.replace('\\', '\\\\')

        return new RegExp(regexString, regexpFlags.join(''))
    } catch (e) {
        logger.error(e.message)
        throw e
    }
}

export const escape = (str: string) => {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
}

export const isValidRegex = (
    regex: string,
    letThroughRegexLists = false
): boolean => {
    let isValid = true

    if (letThroughRegexLists && regex.match(/\[regex\.[a-z_]+\]/i)) {
        return isValid
    }

    try {
        PCRE2JS(regex)
    } catch (_e) {
        isValid = false
    }
    return isValid
}

export const validateRegex = (regex: string, letThroughRegexLists = false) => {
    if (!isValidRegex(regex, letThroughRegexLists)) {
        throw new Error(util.format('Invalid regex: %s', regex))
    }
}

export const regexMatches = (targetString: string, regex: string): boolean => {
    try {
        const result = targetString.match(PCRE2JS(regex))
        if (result.length) {
            return true
        }
        return false
    } catch (_e) {
        return false
    }
}

export const regexDoesntMatch = (
    targetString: string,
    regex: string
): boolean => {
    try {
        const result = targetString.match(PCRE2JS(regex))
        if (!result?.length) {
            return true
        }
        return false
    } catch (_e) {
        return false
    }
}
