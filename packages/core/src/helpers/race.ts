import RaceResult from '../race/result'

export const raceResultToLog = (result: RaceResult) => {
    return {
        ...result,
        data: result.data.all(),
        rawData: result.data.getRawData().all(),
        logs: result.logs()
    }
}
