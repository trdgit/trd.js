import unserialize from 'locutus/php/var/unserialize'

export const serializeDataCache = (data: unknown): string =>
    JSON.stringify(data)
export const unserializeDataCache = <T>(data: string) => JSON.parse(data) as T

const isPhpSerialized = (input: string) => input.startsWith('a:')

export const convertLegacyData = (data: string): string | false => {
    if (!isPhpSerialized(data)) {
        try {
            JSON.parse(data)
        } catch (_e) {
            throw new Error(`Encountered bad serialized string: ${data}`)
        }
    }

    try {
        const unserializedData = unserialize(data)
        if (!unserializedData) {
            return false
        }
        return serializeDataCache(unserializedData)
    } catch (_e) {
        console.log('Bad serialized string', data)
    }
}
