import * as util from 'util'

import Rules from '../parser/rules'

export const validateRule = (rule: string): boolean => {
    try {
        const parser = new Rules()
        parser.parseRule(rule)
        return true
    } catch (e) {
        throw new Error(util.format('Invalid rule: %s (%s)', rule, e.message))
    }
}
