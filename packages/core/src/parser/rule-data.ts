export default class RuleData {
    private data: object = {}

    public set(key: string, val) {
        val = val === null ? '' : val // let's not accept null
        this.data[key] = val
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public setData(namespace: string, data: { [key: string]: any }) {
        if (data && Object.entries(data).length) {
            for (const [k, v] of Object.entries(data)) {
                this.set(`${namespace}.${k}`, v)
            }
        }
    }

    public get(key: string) {
        return this.data[key]
    }

    public has(key: string) {
        return Object.prototype.hasOwnProperty.call(this.data, key)
    }

    public remove(key: string) {
        delete this.data[key]
    }

    public all() {
        return this.data
    }
}
