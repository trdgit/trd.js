import RuleData from './rule-data'

describe('RuleData', () => {
    test('basic set operations', () => {
        const data = new RuleData()
        data.set('k', 'v')

        const result = data.get('k')
        expect(result).toBe('v')
    })

    test('basic setData operations', () => {
        const data = new RuleData()
        data.setData('namespace', {
            k: 'v',
        })

        const result = data.get('namespace.k')
        expect(result).toBe('v')
    })

    test('has', () => {
        const data = new RuleData()
        data.set('k', 'v')
        expect(data.has('k')).toBe(true)
    })

    test('delete', () => {
        const data = new RuleData()
        data.set('k', 'v')
        expect(data.get('k')).toBe('v')
        expect(data.has('k')).toBe(true)
        data.remove('k')
        expect(data.has('k')).toBe(false)
    })

    test('all', () => {
        const data = new RuleData()
        data.set('k', 'v')
        data.set('k2', 'v2')

        const result = data.all()
        expect(result).toStrictEqual({
            k: 'v',
            k2: 'v2',
        })
    })
})
