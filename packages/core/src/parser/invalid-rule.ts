export class InvalidRule extends Error {
    constructor(m: string) {
        super(m)

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, InvalidRule.prototype)
    }
}
