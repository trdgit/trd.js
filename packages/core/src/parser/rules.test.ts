import { InvalidRule } from './invalid-rule'
import RuleResponse from './responses'
import RuleData from './rule-data'
import Rules from './rules'

const rule = (rule, data?: RuleData) => {
    const parser = new Rules()
    if (data instanceof RuleData) {
        parser.addData(data)
    }
    const result = parser.parseRule(rule)
    return result
}

describe('Test parser', () => {
    test('Test keyword ALL', () => {
        expect(rule('ALL')).toBe(RuleResponse.IsTrue)
    })

    test('Test invalid rule missing logic word at end', () => {
        const t = () => {
            rule('INVALID RULE BECAUSE NO LOGIC WORD AT THE END')
        }
        expect(t).toThrow(InvalidRule)
    })

    test('testInvalidRuleLogicWordInWrongPlace', () => {
        const t = () => {
            rule('CONTAINS ALLOW IN THE WRONG PLACE')
        }
        expect(t).toThrow(InvalidRule)
    })

    test('testInvalidRuleMultipleLogicWords', () => {
        const t = () => {
            rule('CONTAINS ALLOW IN THE RIGHT PLACE BUT TWICE - ALLOW')
        }
        expect(t).toThrow(InvalidRule)
    })

    test('testInvalidRuleSpaceAroundOperators', () => {
        const t = () => {
            rule('1==1 DROP')
        }
        expect(t).toThrow(InvalidRule)
    })

    test('testOperatorLessThanEqualTo', () => {
        expect(rule('2 <= 1 ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('1 <= 2 ALLOW')).toBe(RuleResponse.IsTrue)
        expect(rule('2 <= 2 ALLOW')).toBe(RuleResponse.IsTrue)
    })

    test('testOperatorGreaterThanEqualTo', () => {
        expect(rule('2 >= 1 ALLOW')).toBe(RuleResponse.IsTrue)
        expect(rule('2 >= 2 ALLOW')).toBe(RuleResponse.IsTrue)
        expect(rule('1 >= 2 ALLOW')).toBe(RuleResponse.IsFalse)
    })

    test('testOperatorLessThan', () => {
        expect(rule('2 < 1 ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('2 < 2 ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('1 < 2 ALLOW')).toBe(RuleResponse.IsTrue)
    })

    test('testOperatorGreaterThan', () => {
        expect(rule('2 > 1 ALLOW')).toBe(RuleResponse.IsTrue)
        expect(rule('1 > 2 ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('2 > 2 ALLOW')).toBe(RuleResponse.IsFalse)
    })

    test('testOperatorEqualTo', () => {
        expect(rule('1 == 1 ALLOW')).toBe(RuleResponse.IsTrue)
        expect(rule('2 == 1 ALLOW')).toBe(RuleResponse.IsFalse)
    })

    test('testOperatorNotEqualTo', () => {
        expect(rule('1 != 1 ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('2 != 1 ALLOW')).toBe(RuleResponse.IsTrue)
    })

    test('testOperatorNotEqualTo2', () => {
        expect(rule('1 != 1 ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('2 != 1 ALLOW')).toBe(RuleResponse.IsTrue)

        const data = new RuleData()
        data.set('rlsname.group', 'SOMETHING')
        expect(rule('[rlsname.group] != SOMETHING DROP', data)).toBe(
            RuleResponse.IsTrue
        )
    })

    test('OR operator', () => {
        const data = new RuleData()
        data.set('tvmaze.country', 'United States')
        expect(
            rule(
                '[tvmaze.country] == United States OR [tvmaze.country] == Canada OR [tvmaze.country] == United Kingdom ALLOW',
                data
            )
        ).toBe(RuleResponse.IsTrue)
    })

    describe('isin', () => {
        test('list of strings', () => {
            const data = new RuleData()
            data.set('tvmaze.country', 'United States')
            expect(
                rule(
                    '[tvmaze.country] isin Australia,Canada,New Zealand,United States,United Kingdom ALLOW',
                    data
                )
            ).toBe(RuleResponse.IsTrue)
        })

        test('array of second params', () => {
            const data2 = new RuleData()
            data2.set('tvmaze.recent_seasons', [6])
            data2.set('rlsname.season', 6)

            expect(
                rule(
                    '[rlsname.season] isin [tvmaze.recent_seasons] ALLOW',
                    data2
                )
            ).toBe(RuleResponse.IsTrue)
        })

        test('data type mismatch potential', () => {
            const data3 = new RuleData()
            data3.set('tvmaze.id', 123)

            expect(rule('[tvmaze.id] isin 3,4,123 ALLOW', data3)).toBe(
                RuleResponse.IsTrue
            )
            expect(rule('[tvmaze.id] isin 3,4,5 ALLOW', data3)).toBe(
                RuleResponse.IsFalse
            )
        })
    })

    test('testOperatorIswm', () => {
        expect(rule('foobar iswm *oob* ALLOW')).toBe(RuleResponse.IsTrue)
        expect(rule('foobar.something.foobar iswm *foobar* ALLOW')).toBe(
            RuleResponse.IsTrue
        )
        expect(rule('foobar iswm *abc* ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('foobar iswm foo* ALLOW')).toBe(RuleResponse.IsTrue)
        expect(rule('foobar iswm *bar ALLOW')).toBe(RuleResponse.IsTrue)
        expect(
            rule(
                'foobar iswm *bar AND adventure iswm *ven* AND oranges iswm *rang* ALLOW'
            )
        ).toBe(RuleResponse.IsTrue)
        expect(rule('foobar iswm *null* OR adventure iswm *ven* ALLOW')).toBe(
            RuleResponse.IsTrue
        )
        expect(rule('foobar iswm *OOBA* ALLOW')).toBe(RuleResponse.IsTrue)
    })

    test('testOperatorMatches', () => {
        expect(rule('moo matches /moo/i ALLOW')).toBe(RuleResponse.IsTrue)
        expect(rule('foobar matches /moo/i ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('Moo matches /moo/ ALLOW')).toBe(RuleResponse.IsFalse)
    })

    test('testOperatorMatchesWithParentheses', () => {
        expect(
            rule(
                'This.Is.Norwegian matches /norwegian.1080p.Bluray.(x|h)264/i EXCEPT'
            )
        ).toBe(RuleResponse.IsTrue)
        expect(
            rule(
                'Kindred.Spirits.S01E01.Some.Episode.Name.1080p.WEB.x264-GROUP matches /swedish.(720p|1080p).(HDTV|HDTVRiP|WEBRIP|WEB).(x|h)264/i ALLOW'
            )
        ).toBe(RuleResponse.IsFalse)
    })

    test('testOperatorMatchesWithSquareBrackets', () => {
        expect(
            rule(
                'Apple.Homekit matches /^(Adobe|Ahead|Apple[._]|Autodesk|Cyberlink|Jasc|Symantec|Macromedia|Magix|McAfee|Microsoft|Pinnacle|Roxio|SUSE|TomTom|Ulead|VMware)/i ALLOW'
            )
        ).toBe(RuleResponse.IsTrue)
    })

    test('testOperatorInverseMatches', () => {
        expect(rule('moo !matches /moo/ ALLOW')).toBe(RuleResponse.IsFalse)
        expect(rule('Foo !matches /moo/ ALLOW')).toBe(RuleResponse.IsTrue)
    })

    test('testDataSubstitutions', () => {
        const data = new RuleData()
        data.set('imdb.votes', 1000)
        expect(rule('[imdb.votes] > 500 ALLOW', data)).toBe(RuleResponse.IsTrue)
        expect(rule('[imdb.votes] < 500 ALLOW', data)).toBe(
            RuleResponse.IsFalse
        )
    })

    test('testEmptyStatement', () => {
        const data = new RuleData()
        data.set('rlsname.language', '')
        expect(rule('empty([rlsname.language]) ALLOW', data)).toBe(
            RuleResponse.IsTrue
        )

        data.set('rlsname.language', 'Swedish')
        expect(rule('empty([rlsname.language]) ALLOW', data)).toBe(
            RuleResponse.IsFalse
        )

        // TODO: fix this rule
        // const data2 = new RuleData()
        // data.set('tvmaze.web', true)
        // data.set('tvmaze.country_code', '')

        // expect(
        //     rule(
        //         '[tvmaze.country_code] isin US,UK,CA OR ([tvmaze.web] == true AND empty([tvmaze.country_code])) ALLOW'
        //     )
        // ).toBe(RuleResponse.IsTrue)
    })

    test('testInvalidFunction', () => {
        const t = () => {
            rule('fake(1) ALLOW')
        }
        expect(t).toThrow(InvalidRule)
    })

    test('testBasicDroppingWithData', () => {
        const data = new RuleData()
        data.set('rlsname', 'anything')
        expect(rule('[rlsname] iswm * DROP', data)).toBe(RuleResponse.IsFalse)
    })

    test('double data', () => {
        const data = new RuleData()
        data.set('rlsname.season', 9)
        data.set('tvmaze.current_season', 9)
        expect(
            rule('[rlsname.season] == [tvmaze.current_season] ALLOW', data)
        ).toBe(RuleResponse.IsTrue)
    })

    test('simple parenthesis', () => {
        const data = new RuleData()
        data.set('rlsname.source', 'BLURAY')
        data.set('rlsname.codec', 'X264')
        data.set('imdb.language_primary', 'English')
        expect(
            rule(
                '[imdb.language_primary] == English AND ([rlsname.source] == BLURAY AND [rlsname.codec] == X264) ALLOW',
                data
            )
        ).toBe(RuleResponse.IsTrue)
        expect(
            rule(
                '([imdb.language_primary] == English) AND ([rlsname.source] == BLURAY) AND ([rlsname.codec] == X264) ALLOW',
                data
            )
        ).toBe(RuleResponse.IsTrue)
    })
})
