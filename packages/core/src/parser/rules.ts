import * as nm from 'nanomatch'
import * as util from 'util'

import {
    array_intersect,
    is_string,
    str_replace_all,
    str_replace_first,
    substr_count,
    xor
} from '../helpers/data_types'
import { PCRE2JS } from '../helpers/regex'
import { InvalidRule } from './invalid-rule'
import RuleResponse from './responses'
import RuleData from './rule-data'

const uppercaseArray = (input: unknown[]): unknown[] => input.map(uppercase)

const uppercase = (input: unknown): unknown =>
    is_string(input) ? input.toUpperCase() : input

export default class Rules {
    readonly STATEMENT_OPERATORS =
        /^(.*?)\s+(\!)?(iswm|containsany|matches|contains|isin|iswm|>=|<=|>|<|==|!=)\s+(.*?)$/
    readonly LOGICAL_OPERATORS = /\s+(?:AND|OR|and|or)\s+/

    readonly TOKEN_COMMENT = '#'
    readonly TOKEN_KEYWORD_ALL = 'ALL'
    readonly TOKEN_KEYWORD_ALLOW = 'ALLOW'
    readonly TOKEN_KEYWORD_DROP = 'DROP'
    readonly TOKEN_KEYWORD_EXCEPT = 'EXCEPT'
    readonly TOKEN_KEYWORDS = ['ALLOW', 'DROP', 'EXCEPT']
    readonly TOKEN_DATA_START = '['
    readonly TOKEN_DATA_END = ']'

    readonly TOKEN_LOGIC_FLIP = '!'

    readonly TOKEN_COMPARISON_OPERATOR_GREATER_THAN_OR_EQUAL_TO = '>='
    readonly TOKEN_COMPARISON_OPERATOR_LESS_THAN_OR_EQUAL_TO = '<='
    readonly TOKEN_COMPARISON_OPERATOR_GREATER_THAN = '>'
    readonly TOKEN_COMPARISON_OPERATOR_LESS_THAN = '<'
    readonly TOKEN_COMPARISON_OPERATOR_EQUAL_TO = '=='
    readonly TOKEN_COMPARISON_OPERATOR_NOT_EQUAL_TO = '!='
    readonly TOKEN_COMPARISON_OPERATOR_WILDCARD_MATCH = 'iswm'
    readonly TOKEN_COMPARISON_OPERATOR_ARRAY_CONTAINS = 'contains'
    readonly TOKEN_COMPARISON_OPERATOR_ITEM_IS_IN_ARRAY = 'isin'
    readonly TOKEN_COMPARISON_OPERATOR_ARRAY_CONTAINS_ANY = 'containsany'
    readonly TOKEN_COMPARISON_OPERATOR_REGEX_MATCH = 'matches'

    readonly VALID_FUNCTION_STATEMENT = /([a-z]+)\((.*?)\)/
    readonly FUNCTIONS_WHITELIST = ['empty']

    private rules: string[] = []
    private data = null

    public addData(data: RuleData) {
        this.data = data
    }

    public getData() {
        return this.data
    }

    public parseRule(rule: string) {
        rule = rule.trim()

        if (rule.slice(0, 1) === this.TOKEN_COMMENT) {
            return RuleResponse.IsComment
        }

        if (rule === this.TOKEN_KEYWORD_ALL) {
            return RuleResponse.IsTrue
        }

        // no keyword or too many
        const logic = this.multi_explode(this.TOKEN_KEYWORDS, rule)
        if (logic.length == 1 || logic.length > 2) {
            throw new InvalidRule(
                util.format(
                    'Make sure you use the %s keyword only once',
                    this.TOKEN_KEYWORDS.join(', ')
                )
            )
        }

        // keyword not at the end
        const keywordMatch = this.str_ends(this.TOKEN_KEYWORDS, rule)
        if (keywordMatch === false) {
            throw new InvalidRule(
                util.format(
                    'Make sure every rule ends with %s',
                    this.TOKEN_KEYWORDS.join(', ')
                )
            )
        }

        // split by logical operators
        // TODO: support && and ||
        const statements = logic[0]
            .split(this.LOGICAL_OPERATORS)
            .map(s => s.trim())

        let finalExpression = logic[0].trim()

        for (const statement of statements) {
            // split statement up into form X OPERATOR Y
            const statementParts = statement.match(this.STATEMENT_OPERATORS)

            // also see if it's a valid special statement
            const emptyParts = statement.match(this.VALID_FUNCTION_STATEMENT)

            // statement
            if (emptyParts && emptyParts.length === 3) {
                if (!this.FUNCTIONS_WHITELIST.includes(emptyParts[1])) {
                    throw new InvalidRule(
                        util.format(
                            'Only the following functions are supported: %s',
                            this.FUNCTIONS_WHITELIST.join(', ')
                        )
                    )
                }

                const part = this.evaluateStatementPart(emptyParts[2], false)
                finalExpression = this.transformExpression(
                    statement,
                    part === '' || part === null,
                    finalExpression,
                    false
                )
            }
            // normal statement
            else if (statementParts && statementParts.length === 5) {
                const tStatementParts = statementParts.map(s =>
                    s ? s.trim() : s
                )
                const operator = tStatementParts[3]
                let leftPart = this.evaluateStatementPart(tStatementParts[1])
                let rightPart = this.evaluateStatementPart(
                    tStatementParts[4],
                    operator !== this.TOKEN_COMPARISON_OPERATOR_REGEX_MATCH
                )
                const logicFlip = tStatementParts[2] === this.TOKEN_LOGIC_FLIP

                // if the data could not be evaluated we need to return false
                if (
                    (is_string(leftPart) &&
                        leftPart.substr(0, 1) === this.TOKEN_DATA_START &&
                        leftPart.substr(-1) === this.TOKEN_DATA_END) ||
                    (is_string(rightPart) &&
                        rightPart.substr(0, 1) === this.TOKEN_DATA_START &&
                        rightPart.substr(-1) === this.TOKEN_DATA_END)
                ) {
                    return RuleResponse.IsFalse
                }

                if (rightPart === 'true') {
                    rightPart = true
                }
                if (rightPart === 'false') {
                    rightPart = false
                }

                switch (operator) {
                    case this
                        .TOKEN_COMPARISON_OPERATOR_GREATER_THAN_OR_EQUAL_TO:
                        finalExpression = this.transformExpression(
                            statement,
                            leftPart >= rightPart,
                            finalExpression
                        )
                        break
                    case this.TOKEN_COMPARISON_OPERATOR_LESS_THAN_OR_EQUAL_TO:
                        finalExpression = this.transformExpression(
                            statement,
                            leftPart <= rightPart,
                            finalExpression
                        )
                        break
                    case this.TOKEN_COMPARISON_OPERATOR_GREATER_THAN:
                        finalExpression = this.transformExpression(
                            statement,
                            leftPart > rightPart,
                            finalExpression
                        )
                        break
                    case this.TOKEN_COMPARISON_OPERATOR_LESS_THAN:
                        finalExpression = this.transformExpression(
                            statement,
                            leftPart < rightPart,
                            finalExpression
                        )
                        break
                    case this.TOKEN_COMPARISON_OPERATOR_EQUAL_TO:
                        if (is_string(leftPart)) {
                            leftPart = leftPart.toUpperCase()
                        }
                        if (is_string(rightPart)) {
                            rightPart = rightPart.toUpperCase()
                        }

                        finalExpression = this.transformExpression(
                            statement,
                            leftPart == rightPart,
                            finalExpression
                        )
                        break
                    case this.TOKEN_COMPARISON_OPERATOR_NOT_EQUAL_TO:
                        if (is_string(leftPart)) {
                            leftPart = leftPart.toUpperCase()
                        }
                        if (is_string(rightPart)) {
                            rightPart = rightPart.toUpperCase()
                        }

                        finalExpression = this.transformExpression(
                            statement,
                            leftPart != rightPart,
                            finalExpression
                        )
                        break
                    case this.TOKEN_COMPARISON_OPERATOR_WILDCARD_MATCH:
                        if (substr_count(tStatementParts[1], '*') > 0) {
                            throw new InvalidRule(
                                'Wildcards go on the right side of the statement, not left.'
                            )
                        }

                        finalExpression = this.transformExpression(
                            statement,
                            xor(
                                logicFlip,
                                nm.isMatch(leftPart, rightPart, {
                                    nocase: true
                                })
                            ),
                            finalExpression
                        )
                        break
                    case this.TOKEN_COMPARISON_OPERATOR_ARRAY_CONTAINS: {
                        let contains = false
                        if (Array.isArray(leftPart)) {
                            if (
                                uppercaseArray(leftPart).includes(
                                    uppercase(rightPart)
                                )
                            ) {
                                contains = true
                            }
                        } else {
                            if (
                                is_string(leftPart) &&
                                is_string(rightPart) &&
                                leftPart
                                    .toUpperCase()
                                    .split(',')
                                    .includes(rightPart.toUpperCase())
                            ) {
                                contains = true
                            }
                        }

                        finalExpression = this.transformExpression(
                            statement,
                            xor(logicFlip, contains),
                            finalExpression
                        )

                        break
                    }
                    case this.TOKEN_COMPARISON_OPERATOR_ITEM_IS_IN_ARRAY: {
                        let isin = false
                        if (Array.isArray(rightPart)) {
                            if (
                                uppercaseArray(rightPart).includes(
                                    uppercase(leftPart)
                                )
                            ) {
                                isin = true
                            }
                        } else {
                            if (
                                rightPart
                                    .toUpperCase()
                                    .split(',')
                                    .includes(String(leftPart).toUpperCase())
                            ) {
                                isin = true
                            }
                        }

                        finalExpression = this.transformExpression(
                            statement,
                            xor(logicFlip, isin),
                            finalExpression
                        )

                        break
                    }
                    case this.TOKEN_COMPARISON_OPERATOR_ARRAY_CONTAINS_ANY: {
                        let containsAny = false
                        if (Array.isArray(leftPart)) {
                            if (
                                array_intersect(
                                    uppercaseArray(leftPart),
                                    rightPart.toUpperCase().split(',')
                                ).length > 0
                            ) {
                                containsAny = true
                            }
                        } else {
                            if (
                                array_intersect(
                                    leftPart.toUpperCase().split(','),
                                    rightPart.toUpperCase().split(',')
                                ).length > 0
                            ) {
                                containsAny = true
                            }
                        }

                        finalExpression = this.transformExpression(
                            statement,
                            xor(logicFlip, containsAny),
                            finalExpression
                        )

                        break
                    }
                    case this.TOKEN_COMPARISON_OPERATOR_REGEX_MATCH: {
                        let matches = false
                        if (is_string(leftPart) && is_string(rightPart)) {
                            try {
                                const regexMatch = leftPart.match(
                                    PCRE2JS(rightPart)
                                )
                                if (regexMatch && regexMatch.length) {
                                    matches = true
                                }
                            } catch (_e) {
                                //
                            }
                        }

                        finalExpression = this.transformExpression(
                            statement,
                            xor(logicFlip, matches),
                            finalExpression,
                            false
                        )

                        break
                    }
                }
            } else {
                throw new InvalidRule(
                    'Your rule statements should contain a left part, an operator and a right part: ' +
                        statement
                )
            }
        }

        let result = null

        // TODO: maybe find a cleaner place to do this
        finalExpression = finalExpression
            .replace(/\s+OR\s+/gi, ' || ')
            .replace(/\s+AND\s+/gi, ' && ')

        try {
            //result = eval(`${finalExpression}`)
            result = (0, eval)(finalExpression)
        } catch (_e) {
            throw new InvalidRule(
                util.format(
                    'Unable to evaluate rule %s - Final expression: %s',
                    rule,
                    finalExpression
                )
            )
        }

        if (keywordMatch == 'EXCEPT' && result) {
            return RuleResponse.IsExcept
        }

        if (keywordMatch == 'ALLOW' && !result) {
            return RuleResponse.IsFalse
        }

        if (keywordMatch == 'DROP' && result) {
            return RuleResponse.IsFalse
        }

        return RuleResponse.IsTrue
    }

    public transformExpression(
        statement: string,
        result: boolean,
        incoming: string,
        removeParentheses = true
    ): string {
        if (removeParentheses) {
            statement = str_replace_all(['(', ')'], '', statement)
        }
        return str_replace_first(
            statement,
            this.transformEvaluation(result),
            incoming
        )
    }

    public transformEvaluation(result: boolean): string {
        return result ? 'true == true' : 'false == true'
    }

    public evaluateStatementPart(part: string, removeParentheses = true) {
        if (removeParentheses) {
            part = str_replace_all(['(', ')'], '', part)
        }

        // check the total opening and closing tags match up
        const openingBrackets = substr_count(part, '[')
        const closingBrackets = substr_count(part, ']')

        // we have brackets but the total don't add up
        if (
            (openingBrackets > 0 || closingBrackets > 0) &&
            openingBrackets != closingBrackets
        ) {
            throw new InvalidRule(
                "Opening and closing bracket count doesn't match"
            )
        }

        // we have brackets, and they are the same count, but wrong order
        if (
            openingBrackets == closingBrackets &&
            part.indexOf('[') > part.indexOf(']')
        ) {
            throw new InvalidRule('Brackets appear to be the wrong way around')
        }

        //        // no brackets but we have dots
        //        if (substr_count($part, '.') > 0 and ($openingBrackets == 0 or $closingBrackets == 0)) {
        //            var_dump($part);
        //            throw new InvalidRule('Rule seems to contain data but no brackets');
        //        }

        if (part.substr(0, 1) === '[' && part.substr(-1) === ']') {
            const bit = this.get_string_between(part, '[', ']')
            if (
                bit &&
                bit.length &&
                this.data instanceof RuleData &&
                this.data.has(bit)
            ) {
                return this.data.get(bit)
            }
        }

        // if (preg_match('/^\[(.*?)\]$/i', $part, $matches)) {
        //     if ($this->data instanceof RuleData AND $this->data->has($matches[1])) {
        //         return $this->data->get($matches[1]);
        //     }
        // }

        return part
    }

    public evaluateWildcardToRegex(wildcard: string): string {
        return str_replace_all('*', '.*?', wildcard)
    }

    public sortRules(rules: string[]): string[] {
        const sortedRules: string[] = []
        for (const rule of rules) {
            if (rule.slice(-6) === 'EXCEPT') {
                sortedRules.unshift(rule)
            } else {
                sortedRules.push(rule)
            }
        }
        return sortedRules
    }

    public parse(): boolean {
        this.rules = this.sortRules(this.rules)

        for (const rule of this.rules) {
            if (this.parseRule(rule) === RuleResponse.IsExcept) {
                return true
            } else if (this.parseRule(rule) === RuleResponse.IsFalse) {
                return false
            }
        }

        return true
    }

    // optimisisations
    private multi_explode(delimiters: string[], str: string): string[] {
        const tempChar = delimiters[0] // We can use the first token as a temporary join character
        for (let i = 1; i < delimiters.length; i++) {
            str = str.split(delimiters[i]).join(tempChar)
        }
        const finalStr = str.split(tempChar)
        return finalStr
    }

    private str_ends(options: string[], str: string): string | false {
        for (const o of options) {
            if (str.substr(-1 * o.length).toLowerCase() === o.toLowerCase()) {
                return o
            }
        }
        return false
    }

    private get_string_between(s, a, b) {
        const p = s.indexOf(a) + a.length
        return s.substring(p, s.indexOf(b, p))
    }
}
