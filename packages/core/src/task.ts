import 'reflect-metadata'
import Yargs from 'yargs'

import * as logger from './helpers/logger'
import { refreshCache, refreshUpdatedShows } from './utility/task/cache'
import { cbcredits } from './utility/task/cbcredits'
import { cleanup } from './utility/task/cleanup'
import { updateIMDBRatingsFromDataFile } from './utility/task/imdb'
import { redistributeCache } from './utility/task/redistribute_cache'
import { surgeon } from './utility/task/surgeon'
import { simulate } from './utility/task/simulate'

interface TaskFunction {
    name: string
    func
    exitOnComplete: boolean
}

export type TaskFlags = Record<string, unknown>

const command = process.argv.slice(2, 3).pop()
const flags = Yargs(process.argv.slice(3)).argv as TaskFlags

const tasks: TaskFunction[] = [
    {
        name: 'refreshCache',
        func: refreshCache,
        exitOnComplete: true
    },
    {
        name: 'refreshUpdatedShows',
        func: refreshUpdatedShows,
        exitOnComplete: true
    },
    {
        name: 'cleanup',
        func: cleanup,
        exitOnComplete: true
    },
    {
        name: 'simulate',
        func: simulate,
        exitOnComplete: true
    },
    {
        name: 'credits',
        func: cbcredits,
        exitOnComplete: false
    },
    {
        name: 'redistributeCache',
        func: redistributeCache,
        exitOnComplete: true
    },
    {
        name: 'surgeon',
        func: surgeon,
        exitOnComplete: true
    },
    // {
    //     name: 'migrate',
    //     func: migrate,
    //     exitOnComplete: true
    // },
    {
        name: 'imdbDataUpdate',
        func: updateIMDBRatingsFromDataFile,
        exitOnComplete: true
    }
]

const run = async (func, exitAfter = true) => {
    setTimeout(async () => {
        await func()
        if (exitAfter) {
            process.exit()
        }
    }, 1000)
}

if (!command || !command.length) {
    const commands = tasks.map(task => task.name).join(', ')
    logger.task(`Valid commands: ${commands}`)
    process.exit()
}

const task = tasks.find(t => t.name === command)
if (!task) {
    logger.task('Invalid command')
    process.exit()
}

run(async () => {
    await task.func(flags)
}, task.exitOnComplete)
