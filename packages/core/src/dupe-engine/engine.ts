import { ReleaseName } from '@trd/shared/src/types'

import ReleaseNameDataProvider from '../dataprovider/release'
import { EngineResult } from './result'
import Source from './source'

export type DupeEngineRuleString =
    | 'source.firstWins'
    | 'source.priority'
    | 'range.firstWins'
    | 'range.priority'

export default class Engine {
    private sources: Source[] = []
    private filterRegex: RegExp[] = []
    private logs: string[] = []

    constructor(sources: Source[]) {
        sources.map(source => this.addSource(source))
    }

    private log(msg: string): void {
        this.logs.push(msg)
    }

    public getLogs() {
        return this.logs
    }

    public addFilterRegex(regex: RegExp): void {
        this.log(`Attaching /${regex.source}/ as a filter`)
        this.filterRegex.push(regex)
    }

    public addSource(source: Source): void {
        this.log(`Attaching ${source.getRlsname()} as a source`)
        this.sources.push(source)
    }

    public static getRepeatHierarchy(dupePart: string): number {
        const lookup = [
            'REAL.PROPER',
            'PROPER',
            'REAL.RERIP',
            'RERIP',
            'REAL.REPACK',
            'REPACK'
        ]

        const idx = lookup.indexOf(dupePart)
        if (idx > -1) {
            return lookup.length - idx
        }
        return 0
    }

    public static getValidDupeRuleStrings(): DupeEngineRuleString[] {
        return [
            'source.firstWins',
            'source.priority',
            'range.firstWins',
            'range.priority'
        ]
    }

    private filterSources(rlsname: ReleaseName, fields: object): Source[] {
        const sources = this.sources
        const releaseScore = Engine.getRepeatHierarchy(fields['repeat'])

        const filterLog = (source: Source, reason: string) =>
            this.log(
                `filterSources: Removing ${source.getRlsname()} Reason: ${reason}`
            )

        // TODO: lots of looping over the sources here - could be one loop

        // remove internals + existing rlsname + multis
        return sources.filter(source => {
            const sourceFields = source.getFields()

            if (sourceFields['internal'] === true) {
                filterLog(source, `it's internal`)
                return false
            }

            // remove multi
            if (sourceFields['multi'] === true) {
                filterLog(source, `it's multi`)
                return false
            }

            // remove rlsname that we're checking against
            if (source.getRlsname() === rlsname) {
                filterLog(source, `it's original rlsname`)
                return false
            }

            for (const re of this.filterRegex) {
                if (source.getRlsname().match(re)) {
                    filterLog(source, `it matches filter regex ${re.source}`)
                    return false
                }
            }

            // filter other language sources out as they cannot be dupe
            if (sourceFields['language'] != fields['language']) {
                filterLog(source, `it's a different language to source`)
                return false
            }

            // handle the score business
            if (source.getScore() < releaseScore) {
                filterLog(
                    source,
                    `sources score ${source.getScore()} is less than rls score ${releaseScore}`
                )
                return false
            }

            return true
        })
    }

    public isDupe(
        rlsname: ReleaseName,
        site: string,
        rules: DupeEngineRuleString = undefined,
        extra = {}
    ): EngineResult {
        this.log(`Evaluating isDupe() for site ${site}`)

        const releaseFields = ReleaseNameDataProvider.lookupStatic(rlsname)

        // filter sources
        const sources = this.filterSources(rlsname, releaseFields)

        const dupeLog = (reason: string) =>
            this.log(`isDupe: no because: ${reason}`)

        // internal is never a dupe
        if (releaseFields.internal === true) {
            dupeLog('internal release can never be duped')
            return new EngineResult(false, sources)
        }

        // no sources, no dupes
        if (!sources.length) {
            dupeLog('there are no sources')
            return new EngineResult(false, sources)
        }

        // if we have no rules it's never a dupe
        if (
            !rules?.length ||
            !Engine.getValidDupeRuleStrings().includes(rules)
        ) {
            dupeLog('there are no (valid) dupe rules set')
            return new EngineResult(false, sources)
        }

        // first wins and a source? it's a dupe
        if (rules.endsWith('.firstWins') && sources.length > 0) {
            dupeLog('firstWins rule and previous sources is always a dupe')
            return new EngineResult(true, sources)
        }

        // figure out the priority stuff
        if (rules.endsWith('.priority') && extra['priority']) {
            const fieldToCheck = rules.split('.').shift()
            const priorityList = extra['priority'].toUpperCase().split(',')
            if (priorityList.length > 1 && !!releaseFields[fieldToCheck]) {
                const dupeCheckField = releaseFields[fieldToCheck].toUpperCase()
                const releasePosition = priorityList.indexOf(dupeCheckField)
                if (releasePosition > -1) {
                    for (const source of sources) {
                        const sourcePosition = priorityList.indexOf(
                            source.getFields()[fieldToCheck]
                        )
                        if (sourcePosition > -1) {
                            if (releasePosition <= sourcePosition) {
                                dupeLog('encountered a priority rule failure')
                                return new EngineResult(true, sources)
                            }
                        }
                    }
                }
            }
        }

        return new EngineResult(false, sources)
    }
}
