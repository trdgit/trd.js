import { ReleaseName } from '@trd/shared/src/types'

import ReleaseNameDataProvider from '../dataprovider/release'
import { getGroup } from '../utility/release'
import Engine from './engine'

export default class Source {
    private rlsname: ReleaseName = null
    private _score = 0
    private _fields = {}

    constructor(rlsname: ReleaseName) {
        this.rlsname = rlsname
    }

    get fields(): object {
        return ReleaseNameDataProvider.lookupStatic(this.rlsname)
    }

    set fields(fields) {
        this._fields = fields
    }

    get score(): number {
        return Engine.getRepeatHierarchy(
            ReleaseNameDataProvider.extractRepeatExtras(this.rlsname)
        )
    }

    set score(score: number) {
        this._score = score
    }

    public getScore(): number {
        return this.score
    }

    public getRlsname(): string {
        return this.rlsname
    }

    public isInternal() {
        return this.fields['internal'] === true
    }

    public getGroup(): string {
        // split by space as we store pre data with spaces :/
        return getGroup(this.rlsname)
    }

    public getFields() {
        return this.fields
    }
}
