import Source from './source'

export class EngineResult {
    private dupe = false
    private sources: Source[] = []

    constructor(isDupe: boolean, sources: Source[]) {
        this.dupe = isDupe
        this.sources = sources
    }

    public getSources(): Source[] {
        return this.sources
    }

    public getSourcesAsString(): string {
        const collection = []
        for (const source of this.sources) {
            collection.push(source.getRlsname())
        }
        return collection.join(',')
    }

    public isDupe(): boolean {
        return this.dupe
    }
}
