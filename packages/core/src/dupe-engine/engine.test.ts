import { ReleaseName } from '@trd/shared/src/types'

import Engine from './engine'
import Source from './source'

const rls = (rlsname: string): ReleaseName => rlsname as ReleaseName

describe('DupeEngine tests', () => {
    describe('source.*', () => {
        test('testBasicCase', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.720p.WEBRip.x264-GRP1')),
            ]
            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.720p.WEBRip.x264-GRP2'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true)
        })

        test('testTargetIsNotSource', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.720p.WEBRip.x264-GRP1')),
            ]
            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.720p.WEBRip.x264-GRP1'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
        })

        test('testBasicFirstWins', () => {
            const engine = new Engine([
                new Source(rls('Some.Show.S01E01.HDTV.x264-Group')),
            ])
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.WEBRIP.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true) // dupe format
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.WEB.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true) // dupe format
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.HDTV.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true) // actual dupe
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.INTERNAL.HDTV.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false) // internal never dupes
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.FRENCH.HDTV.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false) // another language should not be a dupe
        })

        test('testProperRepack', () => {
            const engine = new Engine([
                new Source(rls('Some.Show.S01E01.HDTV.x264-Group')),
            ])
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.HDTV.PROPER.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.HDTV.REPACK.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.HDTV.RERIP.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls(
                            'Some.Show.S01E01.HDTV.REAL.PROPER.x264-AnotherGroup'
                        ),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
        })

        test('testLanguages', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.FRENCH.HDTV.x264-Group')),
                new Source(
                    rls(
                        'Some.Show.S01E03.Offen.fuer.alles.German.1080p.HDTV.x264-Group'
                    )
                ),
            ]

            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.WEBRIP.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.WEB.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.HDTV.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.INTERNAL.HDTV.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E03.1080p.BluRay.X264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
        })

        test('testComplicatedHierarchies', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.PROPER.HDTV.x264-Group')),
            ]

            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.REPACK.HDTV.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.RERIP.HDTV.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.PROPER.HDTV.x264-AnotherGroup'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true)
            expect(
                engine
                    .isDupe(
                        rls(
                            'Some.Show.S01E01.REAL.PROPER.HDTV.x264-AnotherGroup'
                        ),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
        })

        test('testRegexFilter', () => {
            const sources = [
                new Source(
                    rls('Some.Show.S01E01.CONVERT.1080p.HDTV.x264-Group')
                ),
            ]

            const engine = new Engine(sources)
            engine.addFilterRegex(/[_.]CONVERT[_.]/i)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.RERIP.1080p.HDTV.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls(
                            'Some.Show.S01E01.PROPER.1080p.HDTV.x264-AnotherGroup'
                        ),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls(
                            'Some.Show.S01E01.REAL.PROPER.1080p.HDTV.x264-AnotherGroup'
                        ),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)

            const result = engine.isDupe(
                rls('Some.Show.S01E01.1080p.HDTV.x264-Group'),
                'fakesite',
                'source.firstWins'
            )
            const newSources = result.getSources()
            expect(newSources.length).toBe(0)
        })

        test('testBasicPriority', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.1080p.HDTV.x264-Group')),
            ]

            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.WEB.x264-Group'),
                        'fakesite',
                        'source.priority',
                        { priority: 'hdtv,web' }
                    )
                    .isDupe()
            ).toBe(false)

            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group2'),
                        'fakesite',
                        'source.priority',
                        { priority: 'hdtv,web' }
                    )
                    .isDupe()
            ).toBe(true)

            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.WEB.x264-Group'),
                        'fakesite',
                        'source.priority',
                        { priority: 'web,hdtv' }
                    )
                    .isDupe()
            ).toBe(true)
        })

        test('testMorePriority', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.1080p.WEB.x264-Group')),
            ]

            const engine = new Engine(sources)

            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.WEB.x264-Group2'),
                        'fakesite',
                        'source.priority',
                        { priority: 'web,hdtv' }
                    )
                    .isDupe()
            ).toBe(true)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group'),
                        'fakesite',
                        'source.priority',
                        { priority: 'web,hdtv' }
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group'),
                        'fakesite',
                        'source.priority',
                        { priority: 'hdtv,web' }
                    )
                    .isDupe()
            ).toBe(true)
        })

        test('priority, should fail with 3 if has dupe', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.720p.WEB.x264-Group')),
            ]

            const engine = new Engine(sources)

            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group2'),
                        'fakesite',
                        'source.priority',
                        { priority: 'hdtv,webrip,web' }
                    )
                    .isDupe()
            ).toBe(true)
        })

        test('testPriorityRegex', () => {
            const sources = [
                new Source(
                    rls('Some.Show.S01E01.CONVERT.1080p.WEB.x264-Group')
                ),
            ]

            const engine = new Engine(sources)
            engine.addFilterRegex(new RegExp(/[_.]CONVERT[_.]/i))
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.WEB.x264-Group'),
                        'fakesite',
                        'source.priority',
                        { priority: 'web,hdtv' }
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group'),
                        'fakesite',
                        'source.priority',
                        { priority: 'web,hdtv' }
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group'),
                        'fakesite',
                        'source.priority',
                        { priority: 'hdtv,web' }
                    )
                    .isDupe()
            ).toBe(false)
        })

        test('testInternalSources', () => {
            const sources = [
                new Source(
                    rls('Some.Show.S01E01.INTERNAL.1080p.WEB.x264-Group')
                ),
            ]

            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.WEB.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.1080p.HDTV.x264-Group'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(false)
        })

        test('testReportedBug1', () => {
            const sources = [
                new Source(rls('Some.Show.S08E04.720p.WEBRip.x264-GRP1')),
            ]
            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S08E04.720p.HDTV.x264-GRP2'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S08E04.720p.HDTV.x264-GRP2'),
                        'fakesite',
                        'source.priority',
                        { priority: 'hdtv,web,webrip' }
                    )
                    .isDupe()
            ).toBe(true)
        })
    })

    describe('range.*', () => {
        test('basic actual dupe case', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.HDR.2160p.WEB.H265-GRP1')),
            ]
            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.HDR.2160p.WEB.H265-GRP2'),
                        'fakesite',
                        'source.firstWins'
                    )
                    .isDupe()
            ).toBe(true)
        })

        test('basic first wins', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.HDR.2160p.WEB.H265-GRP1')),
            ]
            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.DV.2160p.WEB.H265-GRP2'),
                        'fakesite',
                        'range.firstWins'
                    )
                    .isDupe()
            ).toBe(true)
        })

        test('testBasicPriority', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.HDR.2160p.WEB.H265-GRP1')),
            ]

            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.DV.2160p.WEB.H265-GRP2'),
                        'fakesite',
                        'range.priority',
                        { priority: 'hdr,dv' }
                    )
                    .isDupe()
            ).toBe(false)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.DV.2160p.WEB.H265-GRP2'),
                        'fakesite',
                        'range.priority',
                        { priority: 'dv,hdr' }
                    )
                    .isDupe()
            ).toBe(true)
        })

        test('testDV.HDR', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.DV.HDR.2160p.WEB.H265-GRP1')),
            ]

            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.DV.2160p.WEB.H265-GRP2'),
                        'fakesite',
                        'range.priority',
                        { priority: 'hdr,dv,dv.hdr' }
                    )
                    .isDupe()
            ).toBe(true)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.HDR.2160p.WEB.H265-GRP3'),
                        'fakesite',
                        'range.priority',
                        { priority: 'hdr,dv,dv.hdr' }
                    )
                    .isDupe()
            ).toBe(true)
        })

        test('testNotInPriority', () => {
            const sources = [
                new Source(rls('Some.Show.S01E01.HDR.2160p.WEB.H265-GRP1')),
            ]

            const engine = new Engine(sources)
            expect(
                engine
                    .isDupe(
                        rls('Some.Show.S01E01.DV.HDR.2160p.WEB.H265-GRP2'),
                        'fakesite',
                        'range.priority',
                        // dv.hdr absent, so won't be evaluated for dupe
                        { priority: 'hdr,dv' }
                    )
                    .isDupe()
            ).toBe(false)
        })
    })
})
