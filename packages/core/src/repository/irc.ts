import { db } from '../helpers/db'

interface GetIrcMessageFromQueueResponse {
    id: string
    message: string
}

export const processIrcMessage = async (id: string) => {
    await db.update('irc_message_queue', { processed: 1 }, { id })
}

export const getIrcMessageFromQueue =
    async (): Promise<GetIrcMessageFromQueueResponse> => {
        return await db.fetchAssoc(
            'SELECT id,message FROM irc_message_queue WHERE processed = 0 LIMIT 1'
        )
    }
