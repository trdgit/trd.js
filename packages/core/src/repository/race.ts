import { ReleaseName } from '@trd/shared/src/types'

import { toMySQL } from '../helpers/date'
import { db } from '../helpers/db'

export interface AddRaceRequest {
    bookmark: string
    rlsname: ReleaseName
    created: string
}

export interface AddRaceSiteRequest {
    race_id: string
    site: string
    created: string
}

export interface GetRaceResponse {
    id: number
    bookmark: string
    chain: string
    chain_complete: string
}

export const addRace = async (raceRequest: AddRaceRequest): Promise<number> => {
    const id = await db.insert(
        'race',
        {
            bookmark: raceRequest.bookmark,
            rlsname: raceRequest.rlsname,
            created: raceRequest.created
        },
        true
    )
    return id
}

export const addRaceSite = async (
    raceSiteRequest: AddRaceSiteRequest
): Promise<void> => {
    await db.insert('race_site', {
        race_id: raceSiteRequest.race_id,
        site: raceSiteRequest.site,
        created: raceSiteRequest.created
    })
}

type LogRaceRequest = {
    log: string
    valid_sites?: string
    chain?: string
    chain_complete?: string
    updated?: string
    rlsname?: string
}

export const logRace = async (
    logRaceRequest: LogRaceRequest,
    rlsname: ReleaseName,
    bookmark: string
) => {
    await db.update('race', logRaceRequest, { rlsname, bookmark })
}

export const logRaceStarted = async (
    rlsname: ReleaseName,
    bookmark: string
) => {
    return await db.update(
        'race',
        {
            started: 1
        },
        { rlsname, bookmark }
    )
}

export const logRaceEnded = async (
    rlsname: ReleaseName,
    chain_complete: string
) => {
    await db.update('race', { chain_complete }, { rlsname })
}

export const logRaceSiteEnded = async (raceId: number, site: string) => {
    await db.update(
        'race_site',
        {
            ended: toMySQL()
        },
        {
            race_id: raceId,
            site
        }
    )
}

export const getRace = async (
    rlsname: ReleaseName
): Promise<GetRaceResponse> => {
    return await db.fetchAssoc(
        'SELECT id, bookmark, chain, chain_complete FROM race WHERE rlsname = ?',
        [rlsname]
    )
}

export const getRaceById = async (id: string): Promise<GetRaceResponse> => {
    return await db.fetchAssoc(
        `SELECT 
            id, bookmark, chain, chain_complete,
            valid_sites, rlsname, log, started, created,
            updated
         FROM 
            race 
         WHERE 
            id = ?`,
        [id]
    )
}
