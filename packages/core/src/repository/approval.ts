import { db } from '../helpers/db'

export type AddApprovalRequest = {
    bookmark: string
    chain: string
    pattern: string
    type: string
    maxlimit: number
    expires: string
}

export type GetApproval = {
    id: string
    bookmark: string
    chain: string
    pattern: string
    type: string
    hits: number
    maxlimit: number
    created: string
    expires: string
}

export type GetApprovalsResponse = GetApproval[]

export const logApprovalHit = async (id: string, hits: number) => {
    await db.update('approved', { hits }, { id })
}

export const addApproval = async (addApprovalRequest: AddApprovalRequest) => {
    await db.insert('approved', addApprovalRequest)
}

export const getApprovals = async (
    tag: string
): Promise<GetApprovalsResponse> => {
    return await db.fetchAll(
        `
        SELECT * FROM approved
        WHERE
          bookmark = ? AND expires > now() AND (hits < maxlimit or maxlimit = 0)
    `,
        [tag]
    )
}
