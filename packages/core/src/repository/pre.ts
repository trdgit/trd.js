import { ReleaseName, ReleaseNameFullTitle } from '@trd/shared/src/types'

import { ReleaseData } from '../dataprovider/release'
import { db } from '../helpers/db'

export type AddPreRequestDupeData = {
    dupe_k?: string
    dupe_season_episode?: string
    dupe_resolution?: string
    dupe_source?: string
    dupe_codec?: string
}

export type AddPreRequest = {
    rlsname: ReleaseName
    created: string
} & AddPreRequestDupeData

export interface GetPreResponse {
    created: string
}

export const addPre = async (data: AddPreRequest) => {
    if (data.rlsname.includes(' ')) {
        throw new Error(
            `Addpre request with a spaced rlsname ${data.rlsname}. Fix prebots!`
        )
    }
    const id = await db.insert('pre', data, true)
    return id
}

export const getPre = async (rlsname: ReleaseName): Promise<GetPreResponse> => {
    return await db.fetchAssoc('SELECT created FROM pre WHERE rlsname = ?', [
        rlsname
    ])
}

type GetDupeSource = {
    rlsname: ReleaseName
    created: string
}
type DupeSourceResponse = GetDupeSource[]

export const getPossibleDupeSources = async (
    cleanedRlsName: ReleaseNameFullTitle,
    releaseNameDataProviderResponseData: ReleaseData,
    dataProvider: string
): Promise<DupeSourceResponse> => {
    let ffs = ''
    const rlsnameInfo = releaseNameDataProviderResponseData
    if (!rlsnameInfo.resolution?.length) {
        ffs = 'OR dupe_resolution IS NULL'
    }

    const seasonEpisode = `${rlsnameInfo['season']}_${rlsnameInfo['episode']}`

    switch (dataProvider) {
        case 'tvmaze': {
            const res = await db.fetchAll<GetDupeSource>(
                `
                                SELECT rlsname, created FROM pre WHERE
                                dupe_k = ? AND
                                dupe_season_episode IS NOT NULL AND
                                dupe_season_episode = ? AND
                                (dupe_resolution = ? ${ffs})
                            `,
                [cleanedRlsName, seasonEpisode, rlsnameInfo['resolution']]
            )
            return res
        }

        case 'imdb': {
            return await db.fetchAll(
                `
                                SELECT rlsname, created FROM pre WHERE
                                dupe_k = ? AND
                                dupe_season_episode IS NULL AND
                                (dupe_resolution = ? ${ffs})
                            `,
                [cleanedRlsName, rlsnameInfo['resolution']]
            )
        }
    }

    return []
}
