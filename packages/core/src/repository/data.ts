import { DateTime } from 'luxon'

import { toMySQL } from '../helpers/date'
import { db } from '../helpers/db'

export interface GetDataCacheResponse {
    data: string
    data_immutable: string
    approved: 1 | 0 // :(
}

export type CreateDataCacheRequest = {
    k: string
    namespace: string
    data: string
    id: string
    updated: string
    approved?: 1 | 0
}

export type UpdateDataCacheRequest = {
    id: string
    data: string
    data_immutable?: string
    updated: string
    approved?: 1 | 0
}

export interface ResetDataCacheImmutableDataRequest {
    k: string
}

export interface SetDataCacheImmutableDataRequest {
    id: string
    data_immutable: string
    namespace: string
}

export type GetAllDataCacheResponse = {
    id: string
    k: string
    namespace: string
    data: string
    data_immutable: string
    approved: 1 | 0
    updated: string
}

export const getAllDataCache = async (): Promise<
    GetAllDataCacheResponse[] | null
> => {
    const response = await db.fetchAll<GetAllDataCacheResponse>(
        `SELECT id, k, namespace, data, data_immutable, approved, updated FROM data_cache`
    )
    return response
}

export const getDataCache = async (
    k: string
): Promise<GetDataCacheResponse | null> => {
    return await db.fetchAssoc(
        `
    SELECT data, data_immutable, approved FROM data_cache WHERE \`k\` = ?
    `,
        [k]
    )
}

export const getDataCacheById = async (
    id: string
): Promise<GetDataCacheResponse> => {
    return await db.fetchAssoc(
        `
    SELECT data, data_immutable, approved FROM data_cache WHERE \`id\` = ?
    `,
        [id]
    )
}

export const getDataCacheByIdAndNamespace = async (
    id: string,
    namespace: string
): Promise<GetDataCacheResponse> => {
    return await db.fetchAssoc(
        'SELECT data,data_immutable FROM data_cache WHERE namespace = ? AND id = ?',
        [namespace, id]
    )
}

export const createDataCache = async (
    row: CreateDataCacheRequest
): Promise<void> => {
    await db.insert('data_cache', row)
}

export const updateDataCache = async (
    k: string,
    row: UpdateDataCacheRequest,
    updateField = true
): Promise<void> => {
    const updated = toMySQL(DateTime.now())
    await db.update(
        'data_cache',
        {
            ...row,
            ...(updateField && { updated })
        },
        {
            k: k
        }
    )
}

export const updateDataCacheId = async (
    k: string,
    id: string
): Promise<void> => {
    await db.update(
        'data_cache',
        {
            id
        },
        { k }
    )
}

export const updateDataCacheUpdated = async (
    id: number,
    updated: string
): Promise<void> => {
    await db.update(
        'data_cache',
        {
            updated
        },
        { id }
    )
}

export const resetDataCacheImmutableData = async (
    resetDataCacheDataImmutableRequest: ResetDataCacheImmutableDataRequest
) => {
    await db.update(
        'data_cache',
        { data_immutable: null },
        { k: resetDataCacheDataImmutableRequest.k }
    )
}

export const setDataCacheImmutableData = async (
    setDataCacheDataImmutableRequest: SetDataCacheImmutableDataRequest
) => {
    await db.update(
        'data_cache',
        {
            data_immutable: setDataCacheDataImmutableRequest.data_immutable
        },
        {
            namespace: setDataCacheDataImmutableRequest.namespace,
            id: setDataCacheDataImmutableRequest.id
        }
    )
}
