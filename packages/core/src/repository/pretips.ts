import { ReleaseName } from '@trd/shared/src/types'
import { DateTime, Interval } from 'luxon'

import RaceResult from '../race/result'

interface PreTip {
    result: RaceResult
    created: DateTime
}
type PretipsStore = Record<ReleaseName, PreTip>

const pretips: PretipsStore = {}

const cleanOldPretips = () => {
    for (const [rlsname, pretip] of Object.entries(pretips)) {
        const diff = Interval.fromDateTimes(
            DateTime.now(),
            pretip.created
        ).length('minutes')
        if (diff > 30) {
            delete pretips[rlsname]
        }
    }
}

export const addPretip = (rlsname: ReleaseName, result: RaceResult) => {
    pretips[rlsname] = { result, created: DateTime.now() }
    cleanOldPretips()
}

export const removePretip = (rlsname: ReleaseName): void => {
    delete pretips[rlsname]
}

export const getPretip = (rlsname: ReleaseName): PreTip | undefined => {
    return pretips[rlsname]
}
