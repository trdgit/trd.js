import { List, ListRegex, Lists } from '@trd/shared/src/types'
import * as nm from 'nanomatch'

import { regexMatches, validateRegex } from '../helpers/regex'
import Model from './model'

export interface ListResolution {
    passes: boolean
    error?: string
}

class Skiplist extends Model<Lists> {
    protected $refreshInterval = 60
    //protected data: Lists

    constructor() {
        super('skiplists')
    }

    public getData(): Lists {
        return this.data
    }

    public getSkiplist(name: string): List {
        return this.data[name] ? this.data[name] : null
    }

    public getSkiplistRegex(name: string): ListRegex {
        const skiplist = this.getSkiplist(name)
        if (skiplist && skiplist.regex) {
            return skiplist.regex
        }
        return null
    }

    public addItem(name: string, item: string): boolean {
        if (item.includes('*')) {
            let found = false
            for (const i of this.data[name].items) {
                if (i == item) {
                    found = true
                }
            }

            if (!found) {
                this.data[name].items.push(item)
                this.save()
            }
        }
        return false
    }

    public passesSkiplist(name: string, str: string): ListResolution {
        if (!this.data[name]) {
            return {
                passes: true
            }
        }

        if (this.data[name].regex) {
            const regexs = this.data[name].regex.split('\n')
            for (const regex of regexs) {
                if (regexMatches(str, regex)) {
                    return {
                        passes: false,
                        error: `Failed on skiplist '${name}' with regex: ${regex}`
                    }
                }
            }
        } else {
            const skiplist = this.data[name].items
            for (const item of skiplist) {
                if (
                    nm.isMatch(str, item, {
                        nocase: true
                    })
                ) {
                    return {
                        passes: false,
                        error: `Failed on skiplist '${name}' for item: ${item}`
                    }
                }
            }
        }

        return {
            passes: true
        }
    }

    validate(data) {
        Object.keys(data).forEach(name => {
            if (data[name].regex) {
                validateRegex(data[name].regex)
            }
        })
    }
}

export default new Skiplist()
