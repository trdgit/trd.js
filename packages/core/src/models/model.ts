import { readFile, writeFile } from '../helpers/fs'

export default abstract class Model<ModelData> {
    protected data: ModelData
    protected path = null
    public name: string
    protected format = 'json'

    protected cache = null

    protected refreshInterval = 0
    protected lastRefreshed = null
    protected needsRefresh = false

    constructor(name: string, cache = null, path = null) {
        this.name = name
        this.cache = cache
        if (!path) {
            this.path = process.env.DATA_PATH + '/' + this.name + '.json'
            this.load(this.path)
        } else {
            this.path = path
            this.load(path)
        }
        this.lastRefreshed = Math.floor(Date.now() / 1000)
    }

    public getData() {
        return this.data
    }

    public setData(data: ModelData): void {
        this.validate(data)
        this.data = data
    }

    public async refresh(): Promise<void> {
        if (this.refreshInterval > 0) {
            const now = Math.floor(Date.now() / 1000)
            if (
                now - this.lastRefreshed > this.refreshInterval ||
                this.needsRefresh
            ) {
                if (this.cache !== null) {
                    this.cache.delete(`trd:models:${this.name}`)
                }
                await this.load(this.path, false, true)
                this.lastRefreshed = now
                this.needsRefresh = false
            }
        }
    }

    protected async load(
        path,
        skipCache = false,
        acceptableFailure = false
    ): Promise<void> {
        // try and get the data from the cache first if possible
        if (this.cache !== null && !skipCache) {
            const model = this.cache.get(`trd:models:${this.name}`)
            if (model) {
                // var_dump(debug_backtrace());
                this.data = JSON.parse(model)
                return
            }
        }

        // pull it from the file system instead
        try {
            const fileContents = await readFile(path)
            const data = JSON.parse(fileContents.toString())
            this.data = data
            if (this.cache !== null) {
                this.cache.set(
                    `trd:models:${this.name}`,
                    JSON.stringify(this.data)
                )
            }
        } catch (e) {
            if (!acceptableFailure) {
                throw e
            }
        }
    }

    public abstract validate(data): void

    public save() {
        if (!this.data) {
            return
        }

        // persist to disk
        this.needsRefresh = true
        writeFile(
            process.env.DATA_PATH + '/' + this.name + '.json',
            JSON.stringify(this.data, null, 4)
        )
        if (this.cache !== null) {
            this.cache.set(`trd:models:${this.name}`, JSON.stringify(this.data))
        }
    }
}
