import { PrebotsData } from '@trd/shared/src/types'

import { validateRegex } from '../helpers/regex'
import Model from './model'

class Prebots extends Model<PrebotsData> {
    protected refreshInterval = 60
    protected data: PrebotsData = []

    constructor() {
        super('prebots')
    }

    validate(data) {
        data.forEach(info => {
            validateRegex(info.bot)
            validateRegex(info.channel)
            validateRegex(info.string_match)
        })
    }
}

export default new Prebots()
