import { ReleaseName, Credits, Site, SiteBNC } from '@trd/shared/src/types'
import { Tag } from '@trd/shared/src/types'
import * as fs from 'fs'
import { resolve } from 'path'

import { is_object } from '../helpers/data_types'
import { readFile, writeFile } from '../helpers/fs'
import { regexMatches, validateRegex } from '../helpers/regex'
import { validateRule } from '../helpers/rules'
import Model from './model'

const isJsonFile = (entity: fs.Dirent) =>
    entity.isFile() && entity.name.endsWith('.json')

type SiteModel = Record<string, Site>

class Sites extends Model<SiteModel> {
    protected refreshInterval = 60
    //protected data: SiteModel

    constructor() {
        super('sites', null, process.env.DATA_PATH + '/sites')
    }

    public getSite(siteName: string): Site {
        return this.data[siteName]
    }

    public replaceSite(siteName: string, data): void {
        this.data[siteName] = data
        this.validateSite(siteName)
        this.save()
    }

    public getSitesArray(returnDisabled = false): string[] {
        const sites = []
        for (const [site, info] of Object.entries(this.data)) {
            if (info['enabled'] || returnDisabled === true) {
                sites.push(site)
            }
        }
        return sites
    }

    public isRing(siteName: string): boolean {
        const site = this.getSite(siteName)
        return site.sections.some(section => section.bnc.length)
    }

    public getAffils(siteName): string[] {
        return this.data[siteName].affils
    }

    protected async load(path, skipCache = false, acceptableFailure = false) {
        // try and get the data from the cache first if possible
        if (this.cache !== null && !skipCache) {
            const model = this.cache.get('trd:models:' + this.name)
            if (!model) {
                this.data = JSON.parse(model)
                return
            }
        }

        // pull it from the file system
        try {
            const dir = await fs.promises.opendir(path)
            const data: Record<string, Site> = {}
            for await (const dirent of dir) {
                if (isJsonFile(dirent)) {
                    const siteName = dirent.name.split('.').shift()
                    const fileContents = await readFile(
                        resolve(path, dirent.name)
                    )
                    data[siteName] = JSON.parse(fileContents.toString())
                }
            }
            this.data = data

            if (this.cache !== null) {
                this.cache.set(
                    'trd:models:' + this.name,
                    JSON.stringify(this.data)
                )
            }
        } catch (e) {
            if (!acceptableFailure) {
                throw e
            }
        }
    }

    public save() {
        this.needsRefresh = true
        for (const [siteName, siteInfo] of Object.entries(this.data)) {
            writeFile(
                `${process.env.DATA_PATH}/sites/${siteName}.json`,
                JSON.stringify(siteInfo, null, 4)
            )
        }
        if (this.cache !== null) {
            this.cache.set(`trd:models:${this.name}`, JSON.stringify(this.data))
        }
    }

    public findValidTags(
        siteName: string,
        section: string,
        rlsname: ReleaseName
    ): Tag[] {
        const site = this.getSite(siteName)

        const validTags: Tag[] = []
        for (const ss of site.sections) {
            // need to check isset here as there is a race condition
            // where newly found sections will not be present in the $site object
            // yet for some reason
            if (ss.name === section) {
                for (const tag of ss.tags) {
                    if (regexMatches(rlsname, tag.trigger)) {
                        validTags.push(tag.tag)
                    }
                }
            }
        }
        return validTags
    }

    public addSite(siteName) {
        if (siteName && siteName.length && !this.data[siteName]) {
            this.data[siteName] = {
                enabled: false,
                irc: {
                    channel: '',
                    channel_key: '',
                    bot: '',
                    strings: {
                        newstring: '',
                        'newstring-rls': '',
                        'newstring-section': '',
                        'newstring-isregex': false,
                        endstring: '',
                        'endstring-rls': '',
                        'endstring-section': '',
                        'endstring-isregex': false,
                        prestring: '',
                        'prestring-rls': '',
                        'prestring-section': '',
                        'prestring-isregex': false
                    }
                },
                sections: [],
                affils: [],
                banned_groups: [],
                credits: {}
            }
            this.validateSite(siteName)
            this.save()

            return true
        }

        return false
    }

    public getAllAffils(): Set<string> {
        return Object.entries(this.data).reduce((acc, [_name, info]) => {
            if (info.enabled) {
                info.affils.forEach(affil => acc.add(affil))
            }
            return acc
        }, new Set<string>())
    }

    public getAffilCounts(): Map<string, number> {
        return Object.entries(this.data).reduce((acc, [_name, info]) => {
            if (info.enabled) {
                info.affils.forEach(
                    affil => (acc[affil] = acc[affil] ? acc[affil] + 1 : 1)
                )
            }
            return acc
        }, new Map<string, number>())
    }

    public addAffil(siteName, newAffil): boolean {
        // loop through existing and exit if we match
        const currentAffils = this.data[siteName].affils
        for (const affil of currentAffils) {
            if (affil == newAffil.toUpperCase()) {
                return false
            }
        }

        // otherwise it's new!
        this.data[siteName].affils.push(newAffil.toUpperCase())
        this.save()
        return true
    }

    public addSection(siteName: string, sectionName: string): boolean {
        sectionName = sectionName.trim()

        // check site exists
        if (!this.data[siteName]) {
            return false
        }

        if (!sectionName.length) {
            return false
        }

        // skip stupid sections
        if (
            sectionName.match(
                /(Req|Backfill|freezone|arch|upload|default|temp|IRC)/i
            )
        ) {
            return false
        }

        // skip bad charactered sections
        if (sectionName.match(/[^a-z0-9-_]+/i)) {
            return false
        }

        const currentSections = this.data[siteName].sections

        for (const section of currentSections) {
            if (section.name.toLowerCase() == sectionName.toLowerCase()) {
                return false
            }
        }

        this.data[siteName].sections.push({
            name: sectionName,
            pretime: 5,
            bnc: null,
            tags: [],
            skiplists: [],
            rules: [],
            dupeRules: { 'source.firstWins': false, 'source.priority': '' },
            downloadOnly: false
        })
        this.save()
        return true
    }

    public findAllBNCs(siteName: string): SiteBNC[] {
        const site = this.getSite(siteName)

        const bncs: SiteBNC[] = []
        for (const section of site.sections) {
            if (
                section.bnc &&
                section.bnc.length &&
                !bncs.includes(section.bnc)
            ) {
                bncs.push(section.bnc)
            }
        }

        if (bncs.length === 0) {
            bncs.push(siteName)
        }

        return bncs
    }

    public applySkiplistToEverySection(name: string): number {
        let c = 0
        for (const [, siteInfo] of Object.entries(this.data)) {
            for (const section of siteInfo['sections']) {
                if (!section.skiplists.includes(name)) {
                    section.skiplists.push(name)
                    c++
                }
            }
        }
        this.save()
        return c
    }

    public repairSite(siteName: string): boolean {
        let repaired = false
        if (!this.data[siteName].banned_groups) {
            this.data[siteName].banned_groups = []
            repaired = true
        }

        if (
            !this.data[siteName].credits ||
            !is_object(this.data[siteName].credits)
        ) {
            this.data[siteName].credits = {}
            repaired = true
        }

        this.data[siteName].affils = this.data[siteName].affils.map(affil =>
            affil.toUpperCase()
        )
        this.data[siteName].banned_groups = this.data[
            siteName
        ].banned_groups.map(grp => grp.toUpperCase())
        this.save()

        return repaired
    }

    public setCredits(siteName: string, bnc: string, credits: Credits) {
        if (!this.data[siteName].credits) {
            this.data[siteName].credits = {
                [bnc]: credits
            }
        } else {
            this.data[siteName]['credits'][bnc] = credits
        }

        this.save()
    }

    public renameSection(
        siteName: string,
        existingSectionName: string,
        newSectionName: string
    ): boolean {
        const sectionIndex = this.data[siteName].sections.findIndex(
            s => s.name === existingSectionName
        )

        if (sectionIndex < 0) {
            return false
        }

        this.data[siteName].sections[sectionIndex].name = newSectionName
        this.save()

        return true
    }

    public repairSection(siteName: string, sectionName: string) {
        const currentSections = this.data[siteName].sections

        let repaired = false
        for (const section of currentSections) {
            if (section.name == sectionName) {
                if (!section.pretime) {
                    repaired = true
                    section.pretime = 5
                }

                if (!section.bnc) {
                    repaired = true
                    section.bnc = null
                }

                if (!section.tags) {
                    repaired = true
                    section.tags = []
                }

                if (!section.dupeRules) {
                    repaired = true
                    if (section.dupeRules === null) {
                        section.dupeRules = {
                            'source.firstWins': false,
                            'source.priority': ''
                        }
                    }
                }

                if (!section.skiplists) {
                    repaired = true
                    section.skiplists = []
                }

                if (!section.rules) {
                    repaired = true
                    section.rules = []
                }

                this.save()

                break
            }
        }
        return repaired
    }

    public validate() {
        //
    }

    private validateSite(name: string) {
        const site = this.getSite(name)
        if (!site) {
            return false
        }

        // validate irc bot + channel
        if (site.irc.bot.length) {
            validateRegex(site.irc.bot)
        }
        if (site.irc.channel.length) {
            validateRegex(site.irc.channel)
        }

        // validate strings
        if (site.irc.strings['newstring-isregex']) {
            validateRegex(site.irc.strings.newstring)
        }
        if (site.irc.strings['endstring-isregex']) {
            validateRegex(site.irc.strings.endstring)
        }
        if (site.irc.strings['prestring-isregex']) {
            validateRegex(site.irc.strings.prestring)
        }

        // validate triggers + rules
        site.sections.forEach(({ tags, rules }) => {
            rules
                .filter(rule => rule.length > 0)
                .forEach(rule => validateRule(rule))
            tags.forEach(tag => {
                validateRegex(tag.trigger)
                if (tag.rules) {
                    tag.rules
                        .filter(rule => rule.length > 0)
                        .forEach(rule => validateRule(rule))
                }
            })
        })
    }
}

export default new Sites()
