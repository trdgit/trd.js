import { SettingsModelType } from '@trd/shared/src/types'

import { validateRegex } from '../helpers/regex'
import Model from './model'

class Settings extends Model<SettingsModelType> {
    protected refreshInterval = 60
    //protected data: SettingsModelType

    constructor() {
        super('settings')
    }

    public exists(key: keyof SettingsModelType): boolean {
        return key in this.data
    }

    public get<K extends keyof SettingsModelType>(
        key: K
    ): SettingsModelType[K] {
        if (key in this.data) {
            return this.data[key]
        }

        return
    }

    public validate(data) {
        // validate bad_dir
        validateRegex(data.baddir)

        // tag options
        Object.keys(data.tag_options).forEach(tag => {
            const allow = data.tag_options[tag].tag_requires ?? []
            const deny = data.tag_options[tag].tag_skiplist ?? []

            allow.forEach(regex => validateRegex(regex, true))
            deny.forEach(regex => validateRegex(regex, true))
        })
    }
}

export default new Settings()
