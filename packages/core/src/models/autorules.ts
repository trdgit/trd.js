import { AutoRule, AutoRules } from '@trd/shared/src/types'

import { clone } from '../helpers/data_types'
import RuleResponse from '../parser/responses'
import Rules from '../parser/rules'
import RaceResultData from '../race/data'
import Model from './model'

class AutoRulesModel extends Model<AutoRules> {
    protected refreshInterval = 60
    //protected data: AutoRules

    constructor() {
        super('autorules')
    }

    public validate() {
        //
    }

    public addRule(newRule): boolean {
        newRule = newRule.trim()

        const found = this.data.rules.some(rule => rule === newRule)
        if (!found) {
            this.data.rules.push(newRule)
            return true
        }

        return false
    }

    public removeRule(rule): boolean {
        for (const [k, r] of Object.entries(this.data.rules)) {
            if (r === rule) {
                delete this.data.rules[k]
                // TODO: should we save here?
                return true
            }
        }

        return false
    }

    public evaluate(chain, data: RaceResultData): AutoRule | false {
        const parser = new Rules()

        const rulesData = this.getData()
        let rules = rulesData.rules

        if (rules !== null) {
            const newData = clone(data)
            newData.attachData('chain', chain)
            parser.addData(newData.toRuleData())
            rules = parser.sortRules(rules)
            for (let rule of rules) {
                rule = rule.trim()
                if (rule && rule.length) {
                    try {
                        if (parser.parseRule(rule) == RuleResponse.IsTrue) {
                            return rule
                        }
                    } catch (_e) {
                        //
                    }
                }
            }
        }
        return false
    }
}

export default new AutoRulesModel()
