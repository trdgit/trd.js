import * as semver from 'semver'

// Import package.json as an ES Module
import packageJson from './package.json'

// Import Node.js modules for file system, child process, and readline
import { writeFileSync } from 'fs'
import { execSync } from 'child_process'
import { createInterface } from 'readline'

// Extract the current version
const { version } = packageJson

// Create readline interface for prompting
const rl = createInterface({
    input: process.stdin,
    output: process.stdout
})

// Validate and increment the version
if (semver.valid(version)) {
    const newVersion = semver.inc(version, 'patch')
    console.log(`Current version: ${version}`)
    console.log(`Proposed next version: ${newVersion}`)

    // Prompt the user for confirmation
    rl.question(
        'Do you want to patch package.json and commit the change? (y/n): ',
        answer => {
            if (
                answer.toLowerCase() === 'y' ||
                answer.toLowerCase() === 'yes'
            ) {
                // Update package.json with the new version
                const updatedPackageJson = {
                    ...packageJson,
                    version: newVersion
                }

                // Write the updated content back to package.json
                writeFileSync(
                    './package.json',
                    JSON.stringify(updatedPackageJson, null, 2),
                    'utf8'
                )
                console.log('Updated package.json with new version')

                // Stage, commit, and optionally push the change
                try {
                    execSync('git add package.json')
                    execSync(`git commit -m "Bump version to ${newVersion}"`)
                    console.log(`Committed version bump to ${newVersion}`)
                    const tagName = `v${newVersion}`
                    execSync(`git tag ${tagName}`)
                    console.log(`Created tag ${tagName}`)
                    execSync(`git push origin tag ${tagName}`)
                    console.log('Pushed tag to remote')
                } catch (error) {
                    if (error instanceof Error) {
                        console.error(
                            'Error during git operations:',
                            error.message
                        )
                    }
                }
            } else {
                console.log('Operation cancelled.')
            }

            // Close the readline interface
            rl.close()
        }
    )
} else {
    console.log('Version is not a valid semver string')
}
